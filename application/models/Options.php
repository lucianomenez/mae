<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Options extends CI_Model {

    public function __construct(){
        parent::__construct();
        $this->load->library('cimongo/Cimongo', '', 'db');
        $this->config->load('cimongo');
    }


    /*
     * Return all objs or query
     */

    function get_options($container,$id=null){

        $query=array();
        if ($id!=null) {
            $query['id']=$id;
        }
        $container=$container;
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function get_items_catalogo($id=null){
        $this->db->switch_db($this->config->item('cmsdb'));
        $fields=array(
              'title',
              'slug'
            );
        $query=array();
         $query['post_type']='items_catalogo';
        
        if ($id==!null) {
            $query['id']=$id;
        }
        $container='container.pages';
        $this->db->where($query);
        $result = $this->db ->select($fields)->get($container)->result_array();
        $this->db->switch_db($this->config->item('db'));
        return $result;
    }

    function insert_option($container,$data){
        
                $this->db->insert($container,$data);
    }

    function search($args,$search, $limit=20){

        $search_regex=$this->to_regex($search);
        $args['title_question']=new MongoRegex('/'.$search_regex.'/i');
        $this->db->switch_db($this->config->item('cmsdb'));
        

            $result =$this->db
                    ->where($args)
                    ->get('container.pages',$limit)
                    ->result_array();
        $this->db->switch_db($this->config->item('db'));

        return $result;
    }


    function  search_by_organismo($args,$search, $limit=20){


         $this->db->switch_db($this->config->item('cmsdb'));
            $result =$this->db
                    ->where($args)
                    ->or_like('organismos.0.value', $search)
                    ->or_like('organismos.1.value', $search)
                    ->or_like('organismos.2.value', $search)
                    ->or_like('organismos.3.value', $search)
                    ->or_like('organismos.4.value', $search)
                    ->or_like('organismos.5.value', $search)
                    ->or_like('organismos.6.value', $search)
                    ->or_like('organismos.7.value', $search)
                    ->get('container.pages',$limit)
                    ->result_array();
        $this->db->switch_db($this->config->item('db'));
        return $result;
    }


    function update_option($container,$array){
                $result= $this->options->get_options($container);

                if ($result) {
                    $newArray=array();
                     foreach ($result as $key_object => $value_object) {
                        $newArray[$key_object]=$value_object['value'];
                     }
  
                    foreach ($array as  $key_value => $value_item) {
                        if (!in_array($value_item['value'],$newArray)) {
                           $this->options->insert_option($container,$value_item);
                        }
                    }
                }else{
                    foreach ($array as $key_value => $value_item) {
                         $this->options->insert_option($container,$value_item);
                    }
                   
                }
    }


        function update_search($array){
            $data=array();
            $container='container.busquedas';
            $args=array();
            $args['value']=$array['value'];
            $args['from']=$array['from'];

            $result =$this->db
                    ->where($args)
                    ->get($container)
                    ->result_array();

            if ($result) {
                $result[0]['date'][]=$array['date']; 
                $result[0]['cant_busquedas']+=1;

                $this->db->where( $args);
                 $this->db->update($container,$result[0]);
               
            }else{
                $data['from']=$array['from'];
                  $data['slug']=$array['slug'];
                $data['value']=$array['value'];
                $data['date']=array();
                $data['date'][0]=$array['date'];
                $data['cant_busquedas']=1;
                $this->insert_option($container,$data);
            }
    }
       
    function get_value($container,$field,$value){


        $result =$this->db
                    ->like($field, $value)
                    ->get($container)
                    ->result_array();

        return $result;
    }

    function to_regex($search){
        
        $vowels_acentuadas = array("ä","Ä","ë","Ë","ï","Ï","ö","Ö","ü","Ü","á","é","í","ó","ú","á","é","í","ó","ú","Á","É","Í","Ó","Ú","Â","Ê","Î","Ô","Û","â","ê","î","ô","û","à","è","ì","ò","ù","À","È","Ì","Ò","Ù");

        $vowels = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U");

        $regex_especiala='[aäÄááÁÂâàÀ]';
        $regex_especiale='[eëËéÉÊêèÈé]';
        $regex_especiali='[iïÏíÍíìÌÎî]';
        $regex_especialo='[oöÖóóÓÔôòÒ]';
        $regex_especialu='[uüÜúúÚÛûùÙ]';

        $search_regex='';
                
                $length = strlen($search);

                for ($i=0; $i<$length; $i++) {

                    if (in_array($search[$i], $vowels  )) {
                        switch ($search[$i]) {
                            case 'a':
                                $search_regex.=  $regex_especiala;
                                break;
                            
                            case 'e':
                                 $search_regex.=  $regex_especiale;
                                break;
                            case 'i':
                                 $search_regex.=  $regex_especiali;
                                break;
                            case 'o':
                                $search_regex.=  $regex_especialo;
                                break;
                            case 'u':
                                $search_regex.=  $regex_especialu;
                                break;
                        }
                       
                    }else{
                         $search_regex.= $search[$i];
                    }
                    
                }
    return $search_regex;

    }


    public function slugify($text)
      {

         $table = array(
            "à"=>'a',"ä"=>'a',"á"=>'a',"â"=>'a',"æ"=>'a',"å"=>'a',"ë"=>'e',"è"=>'e',"é"=>'e', "ê"=>'e',"î"=>'i',"ï"=>'i',"ì"=>'i',"í"=>'i',"ò"=>'o',"ó"=>'o',"ö"=>'o',"ô"=>'o',"ø"=>'o',"ù"=>'o',"ú"=>'u',"ü"=>'u',"û"=>'u',"ñ"=>'n',"ç"=>'c',"ß"=>'s',"ÿ"=>'y',"œ"=>'o',"ŕ"=>'r',"ś"=>'s',"ń"=>'n',"ṕ"=>'p',"ẃ"=>'w',"ǵ"=>'g',"ǹ"=>'n',"ḿ"=>'m',"ǘ"=>'u',"ẍ"=>'x',"ź"=>'z',"ḧ"=>'h',"·"=>'-',"/"=>'-',"_"=>'-',","=>'-',"=>"=>'-',";"=>'-',"&"=>'-and-',"."=>'',"("=>'',")"=>''
          ); 

        // lowercase
        $text = strtolower($text);
        // Replace special characters using the hash map
        $text =strtr($text, $table);

        if (empty($text)) {
          return 'n-a';
        }

        return $text;
      } 

     public function slugifyfull($text)
      {

         $table = array(
            "à"=>'a',"ä"=>'a',"á"=>'a',"â"=>'a',"æ"=>'a',"å"=>'a',"ë"=>'e',"è"=>'e',"é"=>'e', "ê"=>'e',"î"=>'i',"ï"=>'i',"ì"=>'i',"í"=>'i',"ò"=>'o',"ó"=>'o',"ö"=>'o',"ô"=>'o',"ø"=>'o',"ù"=>'o',"ú"=>'u',"ü"=>'u',"û"=>'u',"ñ"=>'n',"ç"=>'c',"ß"=>'s',"ÿ"=>'y',"œ"=>'o',"ŕ"=>'r',"ś"=>'s',"ń"=>'n',"ṕ"=>'p',"ẃ"=>'w',"ǵ"=>'g',"ǹ"=>'n',"ḿ"=>'m',"ǘ"=>'u',"ẍ"=>'x',"ź"=>'z',"ḧ"=>'h',"·"=>'-',"/"=>'-',"_"=>'-',","=>'-',"=>"=>'-',";"=>'-'," "=>'-',"&"=>'-and-',"."=>'',"("=>'',")"=>''
          ); 

        // lowercase
        $text = strtolower($text);
       // Trim - from start and end of text
        $text = trim($text);
        // Replace special characters using the hash map
        $text =strtr($text, $table);

        if (empty($text)) {
          return 'n-a';
        }

        return $text;
      } 
    
}

?>
