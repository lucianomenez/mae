<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 18/05/2020
 *
 */

class Model_api extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');

        $this->load->config('cimongo');

    }



    /**
     * Buscar Usuarios
     *
     * @name get_usuarios
     *
     * @see Api::users_exportar()
     *
     * @author Diego Otero <xxcynicxx@gmail.com>
     *
     * @date May 12, 2017
     *
     * @param type $query
     */
    function get_usuarios($sort='lastname',$limit=9999,$start=0){
        $result = $this->db->get('users', $limit, $start)->result_array();
        return $result;
    }

    /**
     * Buscar Inscripciones
     *
     * @name get_usuarios
     *
     * @see Api::users_exportar()
     *
     * @author Diego Otero <xxcynicxx@gmail.com>
     *
     * @date May 12, 2017
     *
     * @param type $query $fieldnames
     */

    function get_inscripciones($query, $fields){

        return $this->db->select($fields)->where($query)->get('container.inscripciones')->result_array();
    }

    #OPCIONAL
    function get($container, $match, $fields, $skip = 0, $limit = 9999){

        $result = array(
        'aggregate' => $container,
        'pipeline' =>
           array(
               array(
                   '$match' => array($match)
                ),
                array (
                     '$sort' => array ( $sort => 1)
                 ),
                array (
                     '$skip' => $skip
                ),
                array (
                     '$limit' => $limit
                 ),
               //  array(
               //     '$project' => array(
               //         'idcase' => '$idcase',
               //          'idwf' => '$idwf',
               //          'name' => '$name',
               //          'lastname'=>'$lastname',
               //          'idnumber'=>'$idnumber',
               //      //    'genero' =>'$genero',
               //     //     'id' =>'$id',
               //          'disciplina_principal'=> array(
               //              '$cond'=>[
               //                  array('$ifNull'=> ['$disciplina_principal', false]), // if
               //                  '$disciplina_principal', // then
               //                  '*' // else
               //              ]
               //          )
               //
               //      )
               // ),
           )
        );
    $rs =  $this->db->command($result);
    return $rs;
    }




  // function get(){
  //   $query = array('aggregate'=> $container);
  //     $pipeline = array(
  //       'pipeline'=> array(
  //               array (
  //                 '$match' => array (
  //                   'idwf' => $idwf,
  //                   'ano' => $ano
  //                 )
  //               )
  //             ));
  //
  //   $query = $query + $pipeline;
  //   return ($this->db->command($query)['result']);
  // }

 function join($s){
    $container = 'users';
    $s=str_replace("%40", "@", $s);
    $s=str_replace("%20", " ", $s);
    $regex = new MongoRegex("/".$s."/i");
    $rol = new MongoRegex("/jurado/i");
    $result = array(
            'aggregate' => $container,
            'pipeline' =>
               array(
                   array(
                       '$match' => array (
                            'perfil' => array('$elemMatch' => array('rol' => $rol)),
                            '$or' => array(
                               array('name' => $regex),
                               array('lastname' => $regex),
                               array('email' => $regex),
                               array('idnumber' => $regex),
                               array('nroBeneficiario' => $regex),

                            ),
                        ),

                    ),
            ));
    $rs =  $this->db->command($result);
    if (!$rs['result']) {
    $result = array(
            'aggregate' => 'container.jurados',
            'pipeline' =>
               array(
                   array(
                       '$match' => array (
                            '$or' => array(
                               array('asistente.asistente_name' => $regex),
                               array('asistente.asistente_lastname' => $regex),
                               array('asistente.asistente_idnumber' => $regex),
                               array('asistente.asistente_cuit-cuil' => $regex),
                               array('asistente.asistente_email' => $regex),

                            ),
                        ),

                    ),
                  array (
                        '$lookup' => array (
                            'from' => "users",
                             'localField' => "idu",
                            'foreignField' => "idu",
                            "as" =>  "user")
                      ),
            ));
    $rs =  $this->db->command($result);
    }
     return $rs['result'];
  }

    function search($s, $idwf,$ano){
        $s=str_replace("%40", "@", $s);
        $s=str_replace("%20", " ", $s);
        $s=str_replace("%C3%B1", "ñ", $s);
        $s=str_replace("%C3%91", "Ñ", $s);
        $regex = new MongoRegex("/".$s."/i");
        $result = array(
        'aggregate' => 'container.inscripciones',
        'pipeline' =>
           array(
               array(
                   '$match' => array (
                        'ano'=>$ano,
                        'idwf'=>$idwf,
                        '$or' => array(
                            array('idcase'=> $regex),
                            array('name' => $regex),
                            array('lastname'=> $regex),
                            array('idnumber'=> $regex),
                            array('email'=> $regex),
                            array('email_alternativo'=> $regex),
                        ),
                    )
                ),

        ));
        $rs =  $this->db->command($result);
        return $rs['result'];

    }

    /* GRID DEMO FS */
    public function grabarArchivo($file, $metadata = null){
      $this->load->library('Gridfs');



      if (!file_exists($file["tmp_name"]) || is_dir($file["tmp_name"]))
      {
        return FALSE;
      }

      $metadata['date'] = new MongoDate();
      $metadata['originalFilename'] = $file["name"];
      $metadata['type'] = $file["type"];
      $metadata['size'] = $file["size"];

      $rs = $this->gridfs->storeFile($file["tmp_name"],$metadata);

      return $rs->{'$id'};

    }


    /*GRID DEMO FS*/
    public function getArchivo($id){
      $this->load->library('Gridfs');

      if ($id) {
        $id = new MongoId($id);
        return $this->gridfs->get($id);
      } else {
        return FALSE;
      }

    }

    public function deleteArchivo($id){
      $this->load->library('Gridfs');

      if ($id) {
        $id = new MongoId($id);

        $criteria = array('_id' => $id);

        return $this->gridfs->remove($criteria);
      } else {
        return FALSE;
      }

    }



    //FUNCION CHEQUEADA.
    function count_collection_by_query($collection, $match, $group){
        $rtn = array();
        $query =array(
            'aggregate'=> $collection,
            'pipeline'=>
                array(
                    array (
                    '$match' => $match
                    ),
                    array(
                        '$group' => array(
                            '_id' => $group,
                            'cantidad' => array('$sum' => 1),
                        )
                    )
                ));
        //TODO: consultar esquema de manejo de error, result puede ser 0 e informar error.
         $rtn = ($this->db->command($query)['result'][0]['cantidad']);
         return $rtn;
    }


    function get_collection_by_query($collection, $match, $group){
        $rtn = array();
        $query =array(
            'aggregate'=> $collection,
            'pipeline'=>
                array(
                    array (
                    '$match' => $match
                    )
                ));
        //TODO: consultar esquema de manejo de error, result puede ser 0 e informar error.
         $rtn = ($this->db->command($query)['result']);
         return $rtn;
    }




}
