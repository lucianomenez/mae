
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Api
 *
 * Description of the class
 *
 * @author Luciano Menez <menezl@jefatura.gob.ar>
 */
class Api extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        //$this->user->authorize();
        $this->load->helper('file');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('Model_api');
        $this->load->library('phpmailer/phpmailer');
        $this->load->config('config');
        $this->load->library('parser');
        $this->idu = $this->user->idu;
    }



    function users($provincias =null, $gender = null, $range = array ('0','100')) {
        $data = array();
        $data =  $this->Model_api->get_usuarios();
        echo json_encode($data);
    }

     function get($container = "case" , $idwf = "accion_estatal",$group = '$idwf'){
        $json = true;
        $query['idwf'] =  $idwf;
        $data['count'] = $this->Model_api->count_collection_by_query($container, $query, $group);
        $data['result'] = $this->Model_api->get_collection_by_query($container, $query, $fields = array());
        echo json_encode($data);
     }



    function count_inscripciones($json = true, $idwf = null, $ano){

      if (is_string($idwf)){
        $idwf = array ( 0  => array ('idwf' => $idwf));
      } else {
        $idwf = $this->idwfs;
      }


      foreach ($idwf as $wf){
        if($wf['idwf']=='beca_promocion'){
          $cant = $this->Model_api->count_inscripciones_tercerCierre($wf['idwf'], $ano, 'true');
        }else{
          $cant = $this->Model_api->count_inscripciones_ori($wf['idwf'], $ano);
        }
          $all = $all + $cant;
      }


      if ($json == true) {

        $all = array ( 'total' => $all) ;

        $retVal = output_json($all);
      } else {
        $retVal = $all;
      }

      //$retVal = ($json)?output_json($total):$total;
      return $retVal;
    }

    function count_prestamos_enviados($json = true, $idwf = null){

      if (is_string($idwf)){
        $idwf = array ( 0  => array ('idwf' => $idwf));
      } else {
        $idwf = $this->idwfs;
      }

      foreach ($idwf as $wf){
        $cant = $this->Model_api->count_prestamos($wf['idwf']);
        $all = $all + $cant;
      }

      if ($json == true) {

        $all = array ( 'total' => $all) ;

        $retVal = output_json($all);
      } else {
        $retVal = $all;
      }

      //$retVal = ($json)?output_json($total):$total;
      return $retVal;
    }

    function count_prestamos_iniciados($json = true, $idwf = null){

      if (is_string($idwf)){
        $idwf = array ( 0  => array ('idwf' => $idwf));
      } else {
        $idwf = $this->idwfs;
      }

      foreach ($idwf as $wf){
        $cant = $this->Model_api->count_prestamos_iniciados($wf['idwf']);
        $all = $all + $cant;
      }

      if ($json == true) {

        $all = array ( 'total' => $all) ;

        $retVal = output_json($all);
      } else {
        $retVal = $all;
      }

      //$retVal = ($json)?output_json($total):$total;
      return $retVal;
    }

    function count_iniciados($json = true, $idwf = null){

      if (is_string($idwf)){
        $idwf = array ( 0  => array ('idwf' => $idwf));
      } else {
        $idwf = $this->idwfs;
      }

      foreach ($idwf as $wf){
        $cant = $this->Model_api->count_iniciados($wf['idwf']);
        $all = $all + $cant;
      }

      if ($json == true) {

        $all = array ( 'total' => $all) ;

        $retVal = output_json($all);
      } else {
        $retVal = $all;
      }

      //$retVal = ($json)?output_json($total):$total;
      return $retVal;
    }

   function count_iniciados_by_ano($json = true, $idwf = null, $ano=null){

      if (is_string($idwf)){
        $idwf = array ( 0  => array ('idwf' => $idwf));
      } else {
        $idwf = $this->idwfs;
      }

      foreach ($idwf as $wf){

        if($wf['idwf']=='beca_promocion') {
          $cant = $this->Model_api->count_iniciados_by_anoTercero($wf['idwf'],$ano, 'true');

      }else{
        $cant = $this->Model_api->count_iniciados_by_ano($wf['idwf'],$ano);
      }
$all= $all + $cant;
      }

      if ($json == true) {

        $all = array ( 'total' => $all) ;

        $retVal = output_json($all);
      } else {
        $retVal = $all;
      }

      //$retVal = ($json)?output_json($total):$total;
      return $retVal;
    }

    function count_inscripciones_by_token($json = true, $idwf = null, $token){

      if (is_string($idwf)){
        $idwf = array ( 0  => array ('idwf' => $idwf));
      } else {
        $idwf = $this->idwfs;
      }

      foreach ($idwf as $wf){
        $cant = $this->Model_api->count_inscripciones_by_token($wf['idwf'], $token);
        $all = $all + $cant;
      }

      if ($json == true) {

        $all = array ( 'total' => $all) ;

        $retVal = output_json($all);
      } else {
        $retVal = $all;
      }

      //$retVal = ($json)?output_json($total):$total;
      return $retVal;
    }

    function count_inscripciones_by_status($json = true, $idwf = null, $ano, $idStatus, $status){

        $ano=(int)$ano;
        $cant = $this->Model_api->count_by_status($idwf, $ano, $idStatus, $status);
        $all = $cant;

        if ($json == true) {

          $all = array ( 'total' => $all) ;

          $retVal = output_json($all);
        } else {
          $retVal = $all;
        }

      //$retVal = ($json)?output_json($total):$total;
      return $retVal;
    }

    function count_inscripcionesGanadores_by_status($json = true, $idwf = null, $ano, $idStatus, $status){

        $ano=(int)$ano;
        $cant = $this->Model_api->countGanadores_by_status($idwf, $ano, $idStatus, $status);
        $all = $cant;

        if ($json == true) {

          $all = array ( 'total' => $all) ;

          $retVal = output_json($all);
        } else {
          $retVal = $all;
        }

      //$retVal = ($json)?output_json($total):$total;
      return $retVal;
    }

    function count_prestamos_by_status($json = true, $idwf = null, $ano, $idStatus, $status){

        $ano=(int)$ano;
        if ($idwf=='prestamos_generales') {
          $container='container.prestamos.generales';
        }

        if ($idwf=='prestamos_hipotecarios') {
          $container='container.prestamos.hipotecarios';
        }

        if ($idwf=='prestamos_microcreditos') {
          $container='container.prestamos.microcreditos';
        }

        $cant = $this->Model_api->count_prestamos_by_status($idwf, $ano, $idStatus, $status,$container);
        $all = $cant;

        if ($json == true) {

          $all = array ( 'total' => $all) ;

          $retVal = output_json($all);
        } else {
          $retVal = $all;
        }

      //$retVal = ($json)?output_json($total):$total;
      return $retVal;
    }


    function cantidad_usuarios_por_provincia($provincias = null, $anos = null){


        if (is_string($provincias)){
                $provincias = array ( 0  =>  $provincia);
            }else{
            if (!isset($provincia)){
                $provincias = $this->app->get_option(39);
                $provincias = $provincias['data'];

                }
            }
        $total = 0;
        foreach ($provincias as $prov){
            if (isset($prov['value'])){$prov = $prov['value'];}
            $cant = $this->Model_api->count_users($prov);
            $total = $total + $cant['cantidad'];
            }
        return $total;
    }

    function cantidad_usuarios(){
        $cant = $this->Model_api->count_usuarios();
        return $cant;
    }


    function convocatorias($idwf = null){
      $provincias = $this->app->get_option(39);
      $provincias = $provincias['data'];

      if (is_numeric($anos)){
        $anos = array ( 0  =>  $anos);
      }else{
        if (!isset($anos)){
          $anos = $this->anos;
        }
      }


      if (is_string($idwf)){
        $idwf = array ( 0  =>  $idwf);
      }else{
        if (!isset($idwf)){
          $idwf = $this->idwfs;
        }
      }

      $total_wf = 0;

      foreach ($idwf as $wf){
        $suma_ano = 0;
        foreach ($anos as $ano){
          foreach ($provincias as  $prov){
            $data[$wf['idwf']][$ano][$prov['value']] = $this->Model_api->get_convocatorias($wf['idwf'],$ano, $prov['value']);
            $data[$wf['idwf']][$ano]['total'] = $data[$wf['idwf']][$ano]['total'] + $data[$wf['idwf']][$ano][$prov['value']]['cantidad'];
          }
        $data[$wf['idwf']]['total'] =  $data[$wf['idwf']]['total'] + $data[$wf['idwf']][$ano]['total'];
        }
      }

    }


  function get_convocatorias($json = true, $idwf = null, $ano = null, $open = null){

    $total = $this->Model_api->get_convocatoria($idwf,$ano,$open);

    $retVal = ($json)?output_json($total):$total;
    return $retVal;
  }

    function get_convocatorias_permanentes($json = true, $idwf = null){

    $total = $this->Model_api->get_convocatoria_permanente($idwf);

    $retVal = ($json)?output_json($total):$total;
    return $retVal;
  }

    function get_convocatorias_type($json = true, $ano = null, $open = null, $type=null){

    $ano=(int)$ano;

    $total = $this->Model_api->get_convocatoria_type($ano,$open,$type);

    $retVal = ($json)?output_json($total):$total;
    return $retVal;
  }


    function get_categorias($idwf = null, $ano = null){

      $query = array();
        if ($idwf){
          $query['idwf_rel'] = array('$all' => array($idwf));
        };
        if ($ano){
          $query['ano_rel'] = array('$all' => array($ano));
        };



      $total = $this->Model_api->get_categorias($query);
        return $total;
    }



    function view_inscripciones(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $provincias = $this->app->get_option(39);
        $data['provincias'] = $provincias['data'];

        echo $this->parser->parse('inscripciones', $data, true, true);
    }

    function view_usuarios(){

        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $provincias = $this->app->get_option(39);
        $data['provincias'] = $provincias['data'];

        echo $this->parser->parse('usuarios', $data, true, true);


    }


    /**
     * Exportar consulta de Usuarios a Generar XLS
     *
     * Funcion que recibe los POSTs desde API para exportar resultados a un archivo XLS
     *
     * @name users_exportar_xls
     *
     * @author Diego Otero <xxcynicxx@gmail.com>
     *
     * @date May 12, 2017
     *
     * @param type $filter provincias gender $range
     */
    function users_exportar_xls(){

        $query = array();

        /*CSV FILENAME*/
        $filename = 'Usuarios';

        /*PROVINCIAS*/
        if($this->input->post('provincias'))
              $query['provincia'] = array('$in'=> $this->input->post('provincias'));



        /*RANGE*/

        if($this->input->post('range')){
          $range = $this->input->post('range');
          $birth = array();
          $birth[0] = get_birth_data($range[0]);
          $birth[1] = get_birth_data($range[1]);

          $query['birthdate_year'] = array('$gte' => $birth[1], '$lte' => $birth[0]);
        }


        /*GENDER*/
        if($this->input->post('gender')!="null")
          $query['gender'] =  $this->input->post('gender');


         $fieldname = array(
            'name',
            'lastname',
            'idnumber',
            'gender' ,
            'disciplina_principal',
            'birthdate',
            'email',
            'provincia',
            'partido',
            'city',
            'localidad',

          );

        /*MODEL*/
        $result = $this->Model_api->get_users_($query, $fieldname);

        $this->generar_xls($result, $filename, $fieldname);
    }



    /**
     * Exportar consulta de Inscripciones a Generar XLS
     *
     * Funcion que recibe los POSTs desde API para exportar resultados a un archivo XLS
     *
     * @name inscripciones_exportar_xls
     *
     * @author Diego Otero <xxcynicxx@gmail.com>
     *
     * @date May 12, 2017
     *
     * @param type $filter provincias gender $range
     */

    function inscripciones_exportar_xls($wf = null, $ano = null){

        $query = array('idwf'=>array('$exists'=>true));


        if($this->input->post('idwf'))
              $query['idwf'] = array('$in'=> $this->input->post('idwf'));

        if($this->input->post('provincias'))
              $query['provincia'] = array('$in'=> $this->input->post('provincias'));

        /*AÑO*/

        if(($this->input->post('ano')!="null") and ($this->input->post('ano')!= 0))
              $query['ano'] = (int) $this->input->post('ano');

        /*CSV FILENAME*/
        $filename = 'Inscripciones';


        $fieldnames = array(
              'idcase',
              'idwf',
              'name',
              'lastname',
              'idnumber',
              'disciplina_principal',
              'provincia',
              'partido',
              'city',
              'localidad',
              'genero',
          );


        /*MODEL*/
        $result = $this->Model_api->get_inscripciones_custom($query,  $fieldnames);
        #SI OBTERNGO RESULTADOS
        $this->generar_tabs_xls($result, $filename,$fieldnames);
    }


     /**
     * Generar XLS
     *
     * Funcion para exportar resultados a un archivo XLS con Tabs
     *
     * @name generar_xls
     *
     * @author Diego Otero <xxcynicxx@gmail.com>
     *
     * @date May 12, 2017
     *
     * @param type $filter $result, $filename,$fieldnames
     */
    function generar_tabs_xls($result, $filename,$fieldnames){

          #IDWF by POST
          $idwfs = (array)$this->input->post('idwf');


          $objPHPExcel = new PHPExcel();


          $i = 0;
          $arr = array();

          foreach ($idwfs as $idwf) {
            $rtn = array();
            foreach ($result as  $value) {

              if($idwf== $value['idwf']){
                foreach ($fieldnames as $e)
                      $arr[$e] = $value[$e];
              }



              if($idwf== $value['idwf'])
                 $rtn[] = $arr;

            }

                  $incremental = $i++;

                  if($incremental>0)
                       $objPHPExcel->createSheet($incremental) ;


                  $objPHPExcel->setActiveSheetIndex($incremental);
                  $objPHPExcel->getActiveSheet()->setTitle($idwf);
                  $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'idcase');
                  $objPHPExcel->getActiveSheet()->SetCellValue('B1', "idwf");
                  $objPHPExcel->getActiveSheet()->SetCellValue('C1','name');
                  $objPHPExcel->getActiveSheet()->SetCellValue('D1','lastname');
                  $objPHPExcel->getActiveSheet()->SetCellValue('E1','idnumber');
                  $objPHPExcel->getActiveSheet()->SetCellValue('F1','disciplina_principal');
                  $objPHPExcel->getActiveSheet()->SetCellValue('G1','provincia');
                  $objPHPExcel->getActiveSheet()->SetCellValue('H1','partido');
                  $objPHPExcel->getActiveSheet()->SetCellValue('I1','city');
                  $objPHPExcel->getActiveSheet()->SetCellValue('J1','localidad');
                  $objPHPExcel->getActiveSheet()->SetCellValue('K1','genero');
                  $objPHPExcel->getActiveSheet()->fromArray($rtn, null, 'A2');
          }


              header('Content-Type: application/vnd.ms-excel');
              header('Content-Disposition: attachment;filename="'.$filename.'.xls"');
              header('Cache-Control: max-age=0');
              $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
              $objWriter->save('php://output');

    }

    /**
     * Generar XLS
     *
     * Funcion para exportar resultados a un archivo XLS
     *
     * @name generar_xls
     *
     * @author Diego Otero <xxcynicxx@gmail.com>
     *
     * @date May 12, 2017
     *
     * @param type $filter $result, $filename,$fieldnames
     */
    function generar_xls($result, $filename,$fieldnames){

          $objPHPExcel = new PHPExcel();


          $arr = array();


            $rtn = array();
            foreach ($result as  $value) {

                foreach ($fieldnames as $e)
                      $arr[$e] = $value[$e];

                 $rtn[] = $arr;
            }


                  $objPHPExcel->setActiveSheetIndex();
                  $objPHPExcel->getActiveSheet()->setTitle($filename);
                  $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'name');
                  $objPHPExcel->getActiveSheet()->SetCellValue('B1', "lastname");
                  $objPHPExcel->getActiveSheet()->SetCellValue('C1','idnumber');
                  $objPHPExcel->getActiveSheet()->SetCellValue('D1','gender');
                  $objPHPExcel->getActiveSheet()->SetCellValue('E1','disciplina_principal');
                  $objPHPExcel->getActiveSheet()->SetCellValue('F1','birthdate');
                  $objPHPExcel->getActiveSheet()->SetCellValue('G1','email');
                  $objPHPExcel->getActiveSheet()->SetCellValue('H1','provincia');
                  $objPHPExcel->getActiveSheet()->SetCellValue('I1','partido');
                  $objPHPExcel->getActiveSheet()->SetCellValue('J1','city');
                  $objPHPExcel->getActiveSheet()->SetCellValue('K1','localidad');
                  $objPHPExcel->getActiveSheet()->fromArray($rtn, null, 'A2');



              header('Content-Type: application/vnd.ms-excel');
              header('Content-Disposition: attachment;filename="'.$filename.'.xls"');
              header('Cache-Control: max-age=0');
              $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
              $objWriter->save('php://output');

    }



    function listar_inscripciones($json = true, $wf = null, $ano = 2017, $provincias=null, $disciplina = null, $genero = null, $skip =0, $limit = 99999, $sort = 'lastname'){




   //     $query = array('idwf'=>array('$exists'=>true));
        $skip = intval($skip);
        $limit = intval($limit);
        $query['idwf'] =  $wf;
        $query['ano'] = (int) $ano;
        if ($provincias != null) {$query['provincias'] = array('$in'=> $provincias);}
        if ($genero != null) {$query['genero'] = $genero;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}


        $fieldnames = array(
              'idcase',
              'idwf',
              'name',
              'lastname',
              'idnumber',
              'disciplina_principal',
              'provincia',
              'partido',
              'city',
              'localidad',
              'genero',
              'cant_hojas',

          );

        /*MODEL*/
        if ($json == false){
        $result = $this->Model_api->get_inscripciones_custom($query,  $fieldnames, $skip, $limit, $sort);
        }else{

        $data = (isset($skip)) ? array("skip"=>$skip) + $data : $data;
        $data = (isset($limit)) ? array("limit"=>$limit) + $data : $data;
        $data = array("count"=>$total) + $data;
        $data = array("total_count"=>$total_count) + $data;
        $data[$idwf] = $result;

        $retVal = ($json)?output_json($data):$data;
        return $retVal;


        }



        return $result;
    }

    function get_inscripcion_data($idwf, $idcase){
      $data['data'] = $this->Model_inscripciones->detalle_inscripciones($idcase, $idwf);
      //$this->check_owner($data['data'][0]);

      output_json($data['data'][0]);
    }

    function get_informe($idwf, $idcase, $informe, $ano){
      $data['data'] = $this->Model_artista->get_informe($idwf, $idcase, $informe, $ano);

      output_json($data['data'][0]);
    }

    function get_alta($idwf, $idcase){
      $data['data'] = $this->Model_artista->get_beneficiario($idwf, $idcase);
      output_json($data['data'][0]);
    }

    function get_alta_idu($idwf, $idcase, $idu){
      $data['data'] = $this->Model_artista->get_beneficiario_idu($idwf, $idcase, $idu);
      output_json($data['data'][0]);
    }

    function check_owner($data){
      $groups = array(1, 1002, 1003, 1004, 2011, 2014, 1800, 2016, 1500, 1501, 1900, 1901, 2000,2020,2015, 2025, 2026, 2101, 2102,2103,2104,2105,2016,2107,2108,2109,2110,500,1701,1702,2301,2302,2303,2304,2305,2306,2307,2308,2309,2310,2311,2312,2313,2314,2315,2316,2317,2318,2319,2320,2321,2322);
      $user = $this->user->get_user($this->idu);
      if ($user->idu == $data['idu']){
       return true;
      }
      else{
        foreach ($groups as $group){
          if (in_array($group,$user->group)){
           return true;
          }
        }
        show_error ( "Usted no tiene los permisos necesarios para realizar esta acción",$data['idu'],$heading = 'Acceso denegado');
      }
    }

    function check_ganador($data){
      $groups = array(1, 1002, 1003, 1004, 2011, 2014, 1800, 2016, 1500, 1501, 1900, 1901, 2000);
      $user = $this->user->get_user($this->idu);
      $flag=false;

      if (is_array($user->perfil)){
        foreach ($user->perfil as $key => $perfil) {
          if ($data['idu']==$this->idu&&$perfil['ano']==$data['ano']&&$perfil['idwf']==$data['idwf']&&$perfil['rol']=="ganador") {
            $flag= true;
          }
        }
      }

      if(!$flag){
        foreach ($groups as $group){
          if (in_array($group,$user->group)){
           $flag =true;
          }
        }
      }

      if ($flag) {
        return true;
      }else{
        show_error( "Usted no tiene los permisos necesarios para realizar esta acción",$status_code,$heading = 'Acceso denegado');
      }

    }

    function check_informe_beca($data){
        $groups = array(1, 1002, 1003, 1004, 2011, 2014, 1800, 2016, 1500, 1501, 1900, 1901, 2000);
       //Traigo data del informe
        $informe = (array)$this->Model_artista->get_informe($data['idwf'],$data['idcase'], $data['informe'],(string)$data['ano'])[0];
        $flag=false;
        if ($informe['status_tarea'] == 'close'){
         $flag =true;
        }

        if(!$flag){
           $user = $this->user->get_user($this->idu);
          foreach ($groups as $group){
            if (in_array($group,$user->group)){
             $flag =true;
            }
          }
        }

        if ($flag) {
          return true;
        }else{
          show_error ( "Para completar el informe final, primero debe enviar el primer informe",$status_code, $heading = 'Acceso denegado');
        }
      }

   function check_informe($data){
        if ($data=='true'){
         return true;
        }
        else{
          show_error ( "Nota aceptación no enviada",$data,$heading = 'Acceso denegado' );
        }
      }

    function users_by_date(){
      output_json($this->Model_api->users_by_date());
    }


    function revisadas($idwf, $ano, $genero,$revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $disciplina = null,$tipo_organizacion = null, $material = null, $categoria = null, $subcategoria = null,$subgenero = null,$provincia=null){


        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['prejurados'] = array('$all' => array($assign));}

        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($subgenero == 'null'){$subgenero = null;}
        if ($subgenero != null) {$query['subgenero'] = $subgenero;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($material == 'null'){$material = null;}
        if ($material != null) {$query['material'] = $material;}


        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($tipo_organizacion == 'null'){$tipo_organizacion = null;}
        if ($tipo_organizacion != null) {$query['tipo_organizacion'] = $tipo_organizacion;}

        if ($subcategoria == 'null'){$subcategoria= null;}
        if ($subcategoria != null) {
          $query['subcategoria'] = array('$all'=> array($subcategoria));
        }

        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
        //$query['revision.dictamen'] = $revision;


          switch ($status) {
              case 'pendientes':
              $query['preevaluacion'] = array(

                '$not' => array('$elemMatch'=> array('idu' => $this->idu))

              );
             break;
              case 'si':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array('idu' => $this->idu, 'dictamen' => 'si'),

                );
                break;
              case 'no':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array('idu' => $this->idu, 'dictamen' => 'no'),
                );
              break;
              case 'quiza':
              $query['preevaluacion'] = array(
               '$elemMatch' => array('idu' => $this->idu, 'dictamen' => 'quiza'),

              );
              break;
              case 'evaluada':
               $query['preevaluacion']  = array('$elemMatch'=> array('idu' => $this->idu));
                  break;
              case 'all':
               unset($query['preevaluacion']);
                  break;
              case 'rotacion':
               $query['preevaluacion']  = array('$not' => array('$elemMatch'=> array('dictamen' => 'no','idu' => $this->idu)));
                  break;
          }

        $count = $this->count_inscripciones_by_query($query);
        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;
    }

    function revisadas_formacion($idwf, $ano, $revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $categoria,$disciplina,$provincia){



        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['prejurados'] = array('$all' => array($assign));}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['tipo'] = $categoria;}

          if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}




        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
        //$query['revision.dictamen'] = $revision;


          switch ($status) {
              case 'pendientes':
               $query['preevaluacion']  = array(
                '$not' => array('$elemMatch'=> array('idu' => $this->idu))
               );
             break;
              case 'si':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assign),

                );
                break;
              case 'no':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assign),
                );
              break;
              case 'quiza':
              $query['preevaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assign),

              );
              break;
              case 'evaluadas':
               $query['preevaluacion']  = array(
                array('$elemMatch'=> array('idu' => $this->idu))

              );
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }

        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function revisadas_formacion2018($idwf, $ano,$status,$assign,$skip = 0,$limit =200,$sort = 'lastname', $categoria,$tipo_beca,$disciplina_principal,$provincia){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['prejurados'] = array('$all' => array($assign));}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['tipo'] = $categoria;}

        if ($tipo_beca == 'null'){$tipo_beca = null;}
        if ($tipo_beca != null) {$query['tipo_beca'] = $tipo_beca;}

        if ($disciplina_principal == 'null'){$disciplina_principal = null;}
        if ($disciplina_principal != null) {$query['disciplina_principal'] = $disciplina_principal;}

        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;

          switch ($status) {
              case 'pendientes':
               $query['preevaluacion']  = array(
                '$not' => array('$elemMatch'=> array('idu' => $this->idu))
               );
             break;
              case 'si':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assign),

                );
                break;
              case 'no':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assign),
                );
              break;
              case 'quiza':
              $query['preevaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assign),

              );
              break;
              case 'evaluadas':
               $query['preevaluacion']  = array(
                array('$elemMatch'=> array('idu' => $this->idu))

              );
                  break;
              case 'all':
               unset($query['preevaluacion']);
                  break;
          }

        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

        function evaluacion_formacion2018($idwf, $ano,$status,$assign,$skip = 0,$limit =200,$sort = 'lastname', $categoria,$tipo_beca,$disciplina_principal,$provincia){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria_beca'] = $categoria;}

        if ($tipo_beca == 'null'){$tipo_beca = null;}
        if ($tipo_beca != null) {$query['tipo_beca'] = $tipo_beca;}

        if ($disciplina_principal == 'null'){$disciplina_principal = null;}
        if ($disciplina_principal != null) {$query['disciplina_principal'] = $disciplina_principal;}

        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;

          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array(
                '$not' => array('$elemMatch'=> array('idu' => $this->idu))
               );
             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assign),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assign),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assign),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array(
                array('$elemMatch'=> array('idu' => $this->idu))

              );
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }

        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }


        function revisadas_prov($idwf, $ano, $status, $skip = 0,$limit =100,$sort = 'lastname', $provincia, $disciplina_beca){

        //Seteo y Sanitizo

        if ($disciplina_beca == 'null'){$disciplina_beca = null;}
        if ($disciplina_beca != null) {$query['disciplina_beca'] = $disciplina_beca;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}


        //Seteo y Sanitizo
        if ($enviado != null) {$query['status'] = $enviado;}

        //ACOMODO STATUS PARA LA QUERY
          switch ($status) {
            case 'all':
            unset ($status);
                break;
            case 'null':
            $query['revision'] = array('$exists'=> $false);
                break;
            case 'aprobada':
            $query['revision.dictamen'] = $status;
                break;
            case 'modifica':
            $query['revision.dictamen'] = $status;
                break;
            case 'rechazada':
                    case 'visto':
            $query['revision.dictamen'] = $status;
                break;
                case 'pendiente':
            $query['revision.dictamen'] = $status;
                break;
                case 'rechazado':
            $query['revision.dictamen'] = $status;
                break;
        }

        //ANO E IDWF
        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
        $count = $this->count_inscripciones_by_query($query);
        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;
        return $data;
    }

   function seleccionados($idwf, $ano, $skip = 0,$limit =100,$sort = 'lastname', $provincia, $disciplina_beca, $seleccion){

        //Seteo y Sanitizo

        if ($seleccion == 'null'){$seleccion = null;}
        if ($seleccion != null) {$query['ganador'] = $seleccion;}

        if ($disciplina_beca == 'null'){$disciplina_beca = null;}
        if ($disciplina_beca != null) {$query['disciplina_beca'] = $disciplina_beca;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}


        //Seteo y Sanitizo
        if ($enviado != null) {$query['status'] = $enviado;}

        //ACOMODO STATUS PARA LA QUERY
          switch ($status) {
            case 'all':
            unset ($status);
                break;
            case 'null':
            $query['revision'] = array('$exists'=> $false);
                break;
            case 'aprobada':
            $query['revision.dictamen'] = $status;
                break;
            case 'modifica':
            $query['revision.dictamen'] = $status;
                break;
            case 'rechazada':
                    case 'visto':
            $query['revision.dictamen'] = $status;
                break;
                case 'pendiente':
            $query['revision.dictamen'] = $status;
                break;
                case 'rechazado':
            $query['revision.dictamen'] = $status;
                break;
        }

        //ANO E IDWF
        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
        $count = $this->count_inscripciones_by_query($query);
        $data = $this->Model_api->get_inscripciones_full($query, $skip, $limit, $sort);
        $data['count'] = $count;
        return $data;
    }

        function no_revisadasVisuales($idwf, $ano, $status, $skip,$limit =100,$sort = 'lastname', $provincia,$categoria){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        //Seteo y Sanitizo
        if ($enviado != null) {$query['status'] = $enviado;}

        //ACOMODO STATUS PARA LA QUERY
          switch ($status) {
            case 'all':
            unset ($status);
                break;
            case 'null':
            $query['revision'] = array('$exists'=> $false);
                break;
            case 'aprobada':
            $query['revision.dictamen'] = $status;
                break;
            case 'modifica':
            $query['revision.dictamen'] = $status;
                break;
            case 'rechazada':
            $query['revision.dictamen'] = $status;
                break;
                case 'visto':
            $query['revision.dictamen'] = $status;
                break;
                case 'pendiente':
            $query['revision.dictamen'] = $status;
                break;
                case 'rechazado':
            $query['revision.dictamen'] = $status;
                break;
        }

        //ANO E IDWF
        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
        $count = $this->count_inscripciones_by_query($query);
        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;
        return $data;
    }

    function revision_visuales($idwf, $ano, $skip,$limit =1500,$sort = 'lastname', $provincia,$categoria=null){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        //ANO E IDWF
        $query['idwf'] =  $idwf;
        $query['ano'] = $ano;
        $count = $this->Model_api->count_inscripciones_by_query($query);
        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;
        return $data;
    }

    function revision($idwf, $ano, $status, $minhojas, $maxhojas, $skip,$limit =250,$sort = 'lastname', $provincia,$genero,$disciplina_principal=null, $tipo_beca=null, $categoria=null, $clases=null,$jurado=null,$zona=null,$disciplina2=null,$region=null,$llamado=null){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}


        if ($llamado != null) {
          if ($llamado=='segundo') {
            $query['segundo'] = 'true';
          }else if($llamado=='tercero') {
            $query['tercero'] = 'true';
          }else{
            $query['segundo'] = array('$ne' => 'true');
            $query['tercero'] = array('$ne' => 'true');
        }
      }

        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($clases == 'null'){$clases = null;}
        if ($clases != null) {$query['clases'] = $clases;}

        if ($tipo_beca == 'null'){$tipo_beca = null;}
        if ($tipo_beca != null) {$query['tipo_beca'] = $tipo_beca;}

        if ($disciplina_principal == 'null'){$disciplina_principal = null;}
        if ($disciplina_principal != null) {$query['disciplina_principal'] = $disciplina_principal;}

        if ($disciplina2 == 'null'){$disciplina2 = null;}
        if ($disciplina2 != null) {$query['disciplina2'] = $disciplina2;}

        if ($jurado == 'null'){$jurado = null;}
        if ($jurado != null) {
          $query['$or']=  array (
                              array('jurados' => array(
                                 '$all' => array((int)$jurado)
                                )
                              ),
                              array('prejurados' => array(
                                  '$all' => array((int)$jurado)
                                )
                              ),
                        );
      }

      if ($region == 'null'){$region = null;}
      if ($region != null) {
          switch ($region) {
            case 'buenos_aires':
              $query['provincia'] = "BA";
              break;
            case 'caba':
              $query['provincia'] = "CABA";
              break;
            case 'interior':
              $query['$and']=  array (
                          array(
                             'provincia'=>array('$ne' => 'CABA')
                           ),
                          array(
                             'provincia'=>array('$ne' => 'BA')
                              )
                          )
              ;
              break;
          }
      }

        //ACOMODO STATUS PARA LA QUERY
          switch ($status) {
              case 'pendientes':
               $query['revision']  = array('$exists'=> false);

             break;
              case 'si':
                $query['revision'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si'),

                );
                break;
              case 'no':
                $query['revision'] = array(
                 '$elemMatch' => array('dictamen' => 'no'),
                );
              break;
              case 'quiza':
              $query['revision'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza'),

              );
              break;
              case 'null':
               $query['revision']  = array('$exists'=> true);
                  break;
              case 'all':
               unset($query['revision']);
                  break;
          }

        if ($zona == 'null'){$zona = null;}
        if ($zona != null) {
          switch ($zona) {
              case 'zona1':
               $query['zonaInput']  = 'Zona 1: Argentina – PESOS DIEZ MIL ($ 10.000.-)';

             break;
              case 'zona2':
                $query['zonaInput']  = 'Zona 2: Países Limítrofes – PESOS QUINCE MIL ($ 15.000.-)';

                break;
              case 'zona3':
               $query['zonaInput']  = 'Zona 3: Otros países de Sudamérica, Centroamérica y Caribe – PESOS TREINTA MIL ($ 30.000.-)';
              break;
              case 'zona4':
                $query['zonaInput']  = 'Zona 4: América del Norte – PESOS CUARENTA MIL ($ 40.000.-)';
              break;
              case 'zona5':
               $query['zonaInput']  = 'Zona 5: Resto del Mundo – PESOS CINCUENTA MIL ($ 50.000.-)';
                  break;
              case 'null':
               unset($query['zonaInput']);
                  break;
          }

        }
     //FILTRO PAGINAS POR MINIMO Y/O MAXIMO
        if (($minhojas != 'null') and ($maxhojas != 'null')){
          $query['cant_hojas_int'] = array('$gte'=> $minhojas,'$lte'=> $maxhojas);

        }else{

              if ($maxhojas != 'null') {
                  $query['cant_hojas_int'] = array('$lte'=> $maxhojas);
              }else{
                  if ($minhojas != 'null'){
                  $query['cant_hojas_int'] = array('$gte'=> $minhojas);

                  }
              }
        }

        //ANO E IDWF
        $query['idwf'] =  $idwf;
        $query['ano'] = $ano;
        $count = $this->Model_api->count_inscripciones_by_query($query);
        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;
        return $data;
    }

    function revision_concursos($idwf, $ano, $skip,$limit =250,$sort = 'lastname', $provincia=null,$genero=null,$categoria=null, $obra_compartida=null){


        $query['idwf'] =  $idwf;
        $query['ano'] = $ano;

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($obra_compartida == 'null'){$obra_compartida = null;}
        if ($obra_compartida != null) {$query['obra_compartida'] = $obra_compartida;}

        //ANO E IDWF

        $count = $this->Model_api->count_inscripciones_by_query($query);
        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;
        return $data;
    }


    function no_revisadas($idwf, $ano, $genero, $status, $minhojas, $maxhojas, $skip = 0,$limit =100,$sort = 'lastname', $disciplina = null, $provincia = null, $enviado = null,$tipo_organizacion = null){

        //Seteo y Sanitizo
        if ($idwf == 'subsidio_espacios'){
          if ($sort== 'lastname'){ $sort = 'fecha';}
        }

        if ($idwf == 'subsidio_proyectos'){
          if ($sort== 'lastname'){ $sort = 'fecha';}
        }


        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($tipo_organizacion == 'null'){$tipo_organizacion = null;}
        if ($tipo_organizacion != null) {$query['tipo_organizacion'] = $tipo_organizacion;}

        //Seteo y Sanitizo
        if ($enviado != null) {$query['status'] = $enviado;}

        //ACOMODO STATUS PARA LA QUERY
          switch ($status) {
            case 'all':
            unset ($status);
                break;
            case 'null':
            $query['revision'] = array('$exists'=> $false);
                break;
            case 'aprobada':
            $query['revision.dictamen'] = $status;
                break;
            case 'modifica':
            $query['revision.dictamen'] = $status;
                break;
            case 'rechazada':
            $query['revision.dictamen'] = $status;
                break;
                case 'visto':
            $query['revision.dictamen'] = $status;
                break;
                case 'pendiente':
            $query['revision.dictamen'] = $status;
                break;
                case 'rechazado':
            $query['revision.dictamen'] = $status;
                break;
        }

        //FILTRO PAGINAS POR MINIMO Y/O MAXIMO
        if (($minhojas != null) and ($maxhojas != null)){
          $query['cant_hojas'] = array('$gte'=> $minhojas,'$lte'=> $maxhojas);

        }else{

              if ($maxhojas != null) {
                  $query['cant_hojas'] = array('$lte'=> $maxhojas);
              }else{
                  if ($minhojas != null){
                  $query['cant_hojas'] = array('$gte'=> $minhojas);

                  }
              }
        }

        //ANO E IDWF
        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
        $count = $this->count_inscripciones_by_query($query);
        $data = $this->Model_api->get_inscripciones_full($query, $skip, $limit, $sort);
        $data['count'] = $count;
        return $data;
    }

    function revision_subsidios($idwf, $ano, $status, $provincia, $responsable, $skip = 0,$limit =100,$sort = 'lastname',$comite='all',$revision='all',$archivo=null, $desestimado=null){

        switch ($archivo ) {
            case 'all':
            unset ($archivo );
            break;
            case 'checked':
              $query['archivo'] = 'checked';
             break;
            case 'null':
              $query['archivo'] = array('$ne' => 'checked');
            break;

        }

        switch ($desestimado ) {
            case 'all':
            unset ($desestimado );
            break;
            case 'checked':
              $query['desestimado'] = 'checked';
             break;
            case 'null':
              $query['desestimado'] = array('$ne' => 'checked');
            break;

        }

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($ano == 'null'){$ano = null;}
        if ($ano != null) {$query['ano'] = $ano;}

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsable'] = $responsable;}

        //ACOMODO STATUS PARA LA QUERY
          switch ($status) {
            case 'all':
            unset ($status);
                break;
            case 'asignados':
            $query['flujo.asignacion'] = 'ok';
                break;
            case 'sin_asignar':
            $query['flujo.asignacion'] = array('$ne' => 'ok');
                break;
        }

        switch ($comite) {
            case 'all':
            unset ($comite);
                break;
            case 'con_comite':
              $query['flujo.comite'] = 'ok';
              $query['revision.dictamen'] = 'Aprueba';
                break;
            case 'sin_ver':
              $query['flujo.comite'] = array('$ne' => 'ok');
              $query['revision.dictamen'] = 'Aprueba';
                break;
            default:
                $query['comite.dictamen'] = $comite;
                $query['revision.dictamen'] = 'Aprueba';
              break;
        }

        switch ($revision) {
            case 'all':
            unset ($revision);
                break;
            case 'revisados':
            $query['flujo.revision'] = 'ok';
                break;
            default:
                $query['revision.dictamen'] = $revision;
              break;
        }

                //ANO E IDWF

        if ($idwf == 'null'){
          $query['type']= "subsidio";
        }else{
          $query['idwf'] =  $idwf;
        }

        $count = $this->count_inscripciones_by_query($query);
        $data = $this->Model_api->get_inscripciones_full($query, $skip, $limit, $sort);
        $data['count'] = $count;
        return $data;
    }

    function revision_prestamos($idwf, $revision, $provincia, $responsable, $skip = 0,$limit =100,$sort = 'lastname',$archivo=null, $desestimado=null){


        switch ($idwf) {
          case 'prestamos_generales':
            $container='container.prestamos.generales';
            break;
           case 'prestamos_hipotecarios':
            $container='container.prestamos.hipotecarios';
            break;
            case 'prestamos_microcreditos':
            $container='container.prestamos.microcreditos';
            break;
        }

        switch ($archivo ) {
            case 'all':
            unset ($archivo );
            break;
            case 'checked':
              $query['archivo'] = 'checked';
             break;
            case 'null':
              $query['archivo'] = array('$ne' => 'checked');
            break;

        }

        switch ($desestimado ) {
            case 'all':
            unset ($desestimado );
            break;
            case 'checked':
              $query['desestimado'] = 'checked';
             break;
            case 'null':
              $query['desestimado'] = array('$ne' => 'checked');
            break;

        }

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsable'] = $responsable;}

        $query['status'] = 'enviado';

        //ACOMODO STATUS PARA LA QUERY
          switch ($revision) {
            case 'all':
            unset ($revision);
                break;
            case 'asignados':
            $query['flujo.asignacion'] = 'ok';
                break;
            case 'sin_asignar':
            $query['flujo.asignacion'] = array('$ne' => 'ok');
                break;
            case 'rechazada':
            $query['revision.dictamen'] = $revision;
                break;
                case 'visto':
            $query['revision.dictamen'] = $revision;
                break;
                case 'pendiente':
            $query['revision.dictamen'] = $revision;
                break;
                case 'rechazado':
            $query['revision.dictamen'] = $revision;
                break;
        }

                //ANO E IDWF
        $count = $this->count_prestamos_by_query($container,$query);
        $data = $this->Model_api->get_prestamos_full($container,$query, $skip, $limit, $sort);
        $data['count'] = $count;
        return $data;
    }

    function count_prestamos_by_query($container,$query){
      $count = $this->Model_admin->count_prestamos_by_query($container,$query);
      return $count;
    }
    function count_inscripciones_by_query($query){

      $count = $this->Model_api->count_inscripciones_by_query($query);

      return $count;
    }

    function count_inscripciones_promocion_by_query($query){

      $data = $this->Model_api->count_inscripciones_promocion($query);

      $cantidad=0;
      if (is_array($data)) {
        foreach ($data  as $key => $value) {

          if ($value['individual']=='si') {
            $cantidad++;
          }else if($value['cantidad_becarios']=='total'){
              $cantidad++;
              if (is_array($value['integrantes'])) {
                foreach ($value['integrantes'] as $key_integrante => $value_integrante) {
                  if ($value_integrante['dni']!='') {
                    $cantidad++;
                  }
                }
              }
          }else{
            $cantidad=$cantidad+(int)$value['becas_otorgar_grupal'];
          }
        }
      }
      return $cantidad;
    }


    function get_jurados_prejurados($idwf,$ano,$categoria = 'null',$rol='null',$sort='lastname',$limit=9999,$skip=0,$alta_beneficiario='null'){

      $fields = array ('name','lastname', 'idu', 'idnumber', 'perfil', 'cuit-cuil','alta_beneficiario_jurado' ,'nroBeneficiario','situacion' );

      if ($alta_beneficiario!='null') {
        switch ($alta_beneficiario) {
          case 'enviados':
            $query['alta_beneficiario_jurado']='enviado';
            break;
          case 'no_enviados':
            $query['alta_beneficiario_jurado']=array('$ne' => 'enviado');
            break;
          case 'all':
            unset($query['alta_beneficiario_jurado']);
          break;
        }

      }

      if ($rol == 'null'){
        $rol=null;
        if ($categoria=='null') {
                $query['$or']= array(
                              array('perfil' => array(
                                 '$elemMatch' => array(
                                  'rol' => 'jurado',
                                  'idwf' => $idwf,
                                  'ano' => (int)$ano
                               ),

                                )),
                              array('perfil' => array(
                                 '$elemMatch' => array(
                                  'rol' => 'prejurado',
                                  'idwf' => $idwf,
                                  'ano' => (int)$ano
                                ),

                                ),
                        ));
        }else{
                $query['$or']= array(
                              array('perfil' => array(
                                 '$elemMatch' => array(
                                  'rol' => 'jurado',
                                  'idwf' => $idwf,
                                  'ano' => (int)$ano,
                                  'categoria'=> $categoria
                               ),

                                )),
                              array('perfil' => array(
                                 '$elemMatch' => array(
                                  'rol' => 'prejurado',
                                  'idwf' => $idwf,
                                  'ano' => (int)$ano,
                                  'categoria'=> $categoria
                                ),

                                ),
                        ));
        }


      }

      if ($rol != null) {
        $query['perfil']['$elemMatch']['rol'] = $rol;
        $query['perfil']['$elemMatch']['idwf'] = $idwf;
        $query['perfil']['$elemMatch']['ano'] = (int)$ano;
        if ($categoria != 'null') {
          $query['perfil']['$elemMatch']['categoria'] = $categoria;
        }

      }

      $count = $this->Model_api->count_users_by_query($query);
      $data = $this->Model_api->get_usuarios($query,$fields,$sort,$limit,$skip);
      $data['count'] = $count;

      return $data;
    }

    function get_prejurados($idwf,$ano,$categoria = null){

      $fields = array ('name','lastname', 'idu', 'idnumber', 'perfil', 'cuit-cuil'  );


      if ($categoria != null){
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'categoria' => $categoria,
                                  'rol' => 'prejurado'
                                  ),
          );
      }else{
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'rol' => 'prejurado'
                                  ),
          );



      }

      $data = $this->Model_api->get_usuarios($query,$fields);

      return $data;
    }

    function get_busqueda_jurados($idnumber,$idwf,$rol,$ano){
        $fields = array ('name','lastname', 'idu', 'idnumber', 'perfil', 'cuit-cuil','alta_beneficiario_jurado' ,'nroBeneficiario','situacion');
          $query['idnumber']=$idnumber;
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'rol' => $rol
                                  ),
          );


      $data = $this->Model_api->get_usuarios($query,$fields);

      return $data;
    }

    function count_users_by_query($query){

      $count = $this->Model_api->count_users_by_query($query);

      return $count;
    }

    function get_prejurados_rel_jurado($idwf,$ano, $iduJurado,$categoria = null){

      $fields = array ('name','lastname', 'idu', 'idnumber', 'perfil' );


      if ($categoria != null){
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'categoria' => $categoria,
                                  'rol' => 'prejurado',
                                  'iduJurado' => $iduJurado
                                  ),
          );
      }else{
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'rol' => 'prejurado',
                                  'iduJurado' => $iduJurado
                                  ),
          );



      }


      $data = $this->Model_api->get_usuarios($query,$fields);

      return $data;
    }



    function get_prejurados_visuales($idwf,$ano,$disciplina, $iduJurado){

      $fields = array ('name','lastname', 'idu', 'idnumber', 'perfil' );


      if ($disciplina != null){
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'categoria' => $disciplina,
                                  'rol' => 'prejurado',
                                  'iduJurado' => $iduJurado
                                  ),
          );
      }


      $data = $this->Model_api->get_usuarios($query,$fields);

      return $data;
    }


    function get_prejurados_formacion($idwf,$ano,$disciplina){

      $fields = array ('name','lastname', 'idu', 'idnumber', 'perfil' );


      if ($disciplina != null){
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'disciplina' => $disciplina,
                                  'rol' => 'prejurado'
                                  ),
          );
      }else{
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'rol' => 'prejurado'
                                  ),
          );



      }


      $data = $this->Model_api->get_usuarios($query,$fields);

      return $data;
    }

    function get_jurados($idwf,$ano,$categoria = null){

      $fields = array ('name','lastname', 'idu', 'idnumber', 'perfil','cuit-cuil');


      if ($categoria != null){
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'categoria' => $categoria,
                                  'rol' => 'jurado'
                                  ),
          );
      }else{
          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'rol' => 'jurado'
                                  ),
          );



      }


      $data = $this->Model_api->get_usuarios($query,$fields);

      return $data;
    }

        function get_jurados_formacion($idwf,$ano,$disciplina){

      $fields = array ('name','lastname', 'idu', 'idnumber', 'perfil');


          $query['perfil'] = array(
            '$elemMatch' => array('idwf' => $idwf,
                                  'ano' => $ano,
                                  'disciplina' => $disciplina,
                                  'rol' => 'jurado',
                                  'asignado',
                                  'utilizado'
                                  ),
          );


      $data = $this->Model_api->get_usuarios($query,$fields);

      return $data;
    }

    function puntuadas($idwf, $ano, $genero,$revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $disciplina = null, $provincia = null, $tipo_organizacion = null){

        if ($assign == 'null'){$assign = null;}
        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}
        if ($assign != null) {$query['prejurados'] = array('$all' => array($assign));}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($tipo_organizacion == 'null'){$tipo_organizacion = null;}
        if ($tipo_organizacion != null) {$query['tipo_organizacion'] = $tipo_organizacion;}


        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;


          switch ($status) {
              case 'pendientes':
               $query['preevaluacion']  = array('$exists'=> false);

             break;
              case 'si':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si'),

                );
                break;
              case 'no':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no'),
                );
              break;
              case 'quiza':
              $query['preevaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza'),

              );
              break;
              case 'evaluadas':
               $query['preevaluacion']  = array('$exists'=> true);
                  break;
              case 'all':
               unset($query['preevaluacion']);
                  break;
          }

        $count = $this->count_inscripciones_by_query($query);


        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }



    function revisadas_fulbright($idwf, $ano, $disciplina_beca,$revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname',$provincia = null){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($disciplina_beca == 'null'){$disciplina_beca = null;}
        if ($disciplina_beca != null) {$query['disciplina_beca'] = $disciplina_beca;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;

          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array('$exists'=> false);

             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assign),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assign),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assign),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array('$exists'=> true);
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }


        $count = $this->count_inscripciones_by_query($query);



        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function revisadas_jurado($idwf, $ano, $genero,$revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $disciplina = null, $provincia = null, $tipo_organizacion = null, $material = null , $proceso = null,$subgenero = null,$categoria=null){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($subgenero == 'null'){$subgenero = null;}
        if ($subgenero != null) {$query['subgenero'] = $subgenero;}

        if ($material == 'null'){$material = null;}
        if ($material != null) {$query['material'] = $material;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($tipo_organizacion == 'null'){$tipo_organizacion = null;}
        if ($tipo_organizacion != null) {$query['tipo_organizacion'] = $tipo_organizacion;}


        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;


          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array(
                '$not' => array('$elemMatch'=> array('idu' => $assign))
              );

             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assign),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assign),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assign),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array('$elemMatch'=> array('idu' => $assign));
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
              case 'rotacion':
                  $query['evaluacion']  = array('$not' => array('$elemMatch'=> array('dictamen' => 'no','idu' => $this->idu)));
                     break;
          }


        $count = $this->count_inscripciones_by_query($query);



        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

        function revisadas_jurado_visuales($idwf, $ano, $status,$assign,$skip = 0,$limit =100,$sort = 'seudonimo', $provincia,$subcategoria, $preevaluacion, $prejurado){

          $prejurado=(int)$prejurado;
        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($subcategoria == 'null'){$subcategoria= null;}
        if ($subcategoria != null) {
          $query['subcategoria'] = array('$all'=> array($subcategoria));
        }


        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}


        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;

        switch ($preevaluacion) {
              case 'pendientes':
               $query['preevaluacion']  = array(
                '$not' => array('$elemMatch'=> array('idu' => $prejurado))
              );

             break;
              case 'si':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$prejurado),

                );
                break;
              case 'no':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$prejurado),
                );
              break;
              case 'quiza':
              $query['preevaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$prejurado),

              );
              break;
              case 'evaluadas':
                $query['preevaluacion']  = array(
                  array('$elemMatch'=> array('idu' => $prejurado))

                );
                  break;
              case 'all':
               unset($query['preevaluacion']);
                  break;
          }

          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array(
                '$not' => array('$elemMatch'=> array('idu' => $this->idu))
              );

             break;
              case 'si':
                $query['$or']= array(
                              array('evaluacion' => array(
                                 '$elemMatch' => array( 'dictamen1' => 'si', 'idu' =>$assign),

                                )),
                              array('evaluacion' => array(
                                 '$elemMatch' => array( 'dictamen2' => 'si', 'idu' =>$assign),

                                ),
                        ));
                break;
              case 'no':
                $query['$or']= array(
                              array('evaluacion' => array(
                                 '$elemMatch' => array( 'dictamen1' => 'no', 'idu' =>$assign),

                                )),
                              array('evaluacion' => array(
                                 '$elemMatch' => array( 'dictamen2' => 'no', 'idu' =>$assign),

                                ),
                        ));
              break;
              case 'quiza':
                $query['$or']= array(
                              array('evaluacion' => array(
                                 '$elemMatch' => array( 'dictamen1' => 'quiza', 'idu' =>$assign),

                                )),
                              array('evaluacion' => array(
                                 '$elemMatch' => array( 'dictamen2' => 'quiza', 'idu' =>$assign),

                                ),
                        ));
              break;
              case 'evaluadas':
               $query['evaluacion']  = array(
                array('$elemMatch'=> array('idu' => $this->idu))

              );
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }

        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }



    function jurado_revisadas($idwf, $ano,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $provincia, $tipo_beca=null,$categoria=null){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($tipo_beca == 'null'){$tipo_beca = null;}
        if ($tipo_beca != null) {$query['tipo_beca'] = $tipo_beca;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        $query['revision.dictamen'] = array('$ne' => 'rechazada');
        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;

          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array(
                '$not' => array('$elemMatch'=> array('idu' => $this->idu))
              );

             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assign),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assign),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assign),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array('$exists'=> true);
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }
        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

      }

    function revisadas_jurado_becas($idwf, $ano,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $provincia,$disciplina_principal, $preevaluacion, $assignPre, $tipo_beca=null,$categoria=null,$partido=null,$gender=null,$zona=null,$categoria_beca=null,$disciplina=null,$llamado=null){

      if ($disciplina == 'null'){$disciplina = null;}
      if ($disciplina != null) {
        $query['disciplinas']  =
              array('$all'=> array($disciplina));
      }

        if ($llamado != null) {
          if ($llamado=='segundo') {
            $query['segundo'] = 'true';
          }else if($llamado=='tercero') {
            $query['tercero'] = 'true';
          }else{
            $query['segundo'] = array('$ne' => 'true');
            $query['tercero'] = array('$ne' => 'true');
          }
        }

        if ($disciplina_principal == 'null'){$disciplina_principal = null;}
        if ($disciplina_principal != null) {
          if ($disciplina_principal=='artes_escenicas') {
                     $query=  array (
                            '$or' => array(
                               array('disciplina_principal' => 'teatro'),
                               array('disciplina_principal' => 'danza'),
                               array('disciplina_principal' => 'artes_escenicas')
                            )
                        );
          }else{
            $query['disciplina_principal'] = $disciplina_principal;
          }

        }

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($assignPre == 'null'){$assignPre = null;}
        if ($assignPre != null) {$query['prejurados'] = array('$all' => array($assignPre));}

        if ($tipo_beca == 'null'){$tipo_beca = null;}
        if ($tipo_beca != null) {$query['tipo_beca'] = $tipo_beca;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($categoria_beca == 'null'){$categoria_beca = null;}
        if ($categoria_beca != null) {$query['categoria_beca'] = $categoria_beca;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {
          switch ($provincia) {
            case 'interior':
              $query['$and']=  array (
                          array(
                             'provincia'=>array('$ne' => 'CABA')
                           ),
                          array(
                             'provincia'=>array('$ne' => 'BA')
                              )
                          )
              ;
              break;
              default:
                $query['provincia'] = $provincia;
              break;
          }

        }

        if ($partido == 'null'){$partido = null;}
        if ($partido != null) {$query['partido'] = $partido;}

        if ($gender == 'null'){$gender = null;}
        if ($gender != null) {$query['gender'] = $gender;}

        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;

            switch ($preevaluacion) {
              case 'pendientes':
               $query['preevaluacion']  = array('$exists'=> false);

             break;
              case 'si':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assignPre),

                );
                break;
              case 'no':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assignPre),
                );
              break;
              case 'quiza':
              $query['preevaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assignPre),

              );
              break;
              case 'evaluadas':
               $query['preevaluacion']  = array('$exists'=> true);
                  break;
              case null:
               unset($query['preevaluacion']);
                  break;
          }

          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array(
                '$not' => array('$elemMatch'=> array('idu' => $this->idu))
              );

             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$this->idu),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$this->idu),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$this->idu),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array('$elemMatch'=> array('idu' => $this->idu));
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
              case 'datos_personales':
               $query['evaluacion']  = array('$not' => array('$elemMatch'=> array('dictamen' => 'no','idu' => $this->idu)));
                  break;
          }

      if ($zona == 'null'){$zona = null;}
        if ($zona != null) {
          switch ($zona) {
              case 'zona1':
               $query['zonaInput']  = 'Zona 1: Argentina – PESOS DOCE MIL ($ 12.000.-)';

             break;
              case 'zona2':
                $query['zonaInput']  = 'Zona 2: Países Limítrofes – PESOS DIECIOCHO MIL ($ 18.000.-)';

                break;
              case 'zona3':
               $query['zonaInput']  = 'Zona 3: Otros países de Sudamérica, Centroamérica y Caribe – PESOS TREINTA Y SEIS MIL ($ 36.000.-)';
              break;
              case 'zona4':
                $query['zonaInput']  = 'Zona 4: América del Norte – PESOS CUARENTA Y OCHO MIL ($ 48.000.-)';
              break;
              case 'zona5':
               $query['zonaInput']  = 'Zona 5: Resto del Mundo – PESOS SESENTA MIL ($ 60.000.-)';
                  break;
              case 'null':
               unset($query['zonaInput']);
                  break;
          }

        }
        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

      }

        function jurados_dinero_asignado($idwf,$ano,$assign,$disciplina,$tipo='jurados',$llamado=null){
          $fields = array ('idwf','ano', 'totales', 'individual', 'grupal','idcase','evaluacion','zonaInput','integrantes','becas_otorgar_grupal','disciplina_interna','gender','disciplina_principal','provincia','categoria_beca','preevaluacion');

          if ($assign == 'null'){$assign = null;}
          if ($assign != null) {$query[$tipo] = array('$all' => array($assign));}

          if ($disciplina == 'null'){$disciplina = null;}
          if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

            if ($llamado != null) {
              if ($llamado=='segundo') {
                $query['segundo'] = 'true';
              }else if($llamado=='tercero') {
                $query['tercero'] = 'true';
              }else{
                $query['segundo'] = array('$ne' => 'true');
                $query['tercero'] = array('$ne' => 'true');
              }
            }

          $query['idwf'] =  $idwf;
          $query['ano'] = (int) $ano;

          $data = $this->Model_api->get_inscripciones($query, $fields);

          return $data;
        }

        function checkGanador($idnumber,$ano,$ganador,$concursos=true){
          $fields = array ('idwf','ano', 'totales', 'individual', 'grupal','idcase','evaluacion','zonaInput','integrantes','becas_otorgar_grupal','disciplina_interna','gender','disciplina_principal','provincia','categoria_beca','preevaluacion','name','lastname');

            //$query[$tipo] = array('$all' => array($assign));
            $query['ano'] = (int) $ano;
            $query['ganador'] = $ganador;
            //$query['idnumber'] = $idnumber;
            if (!$concursos) {
              $query['type'] = array('$ne' => 'concurso');
            }
            $query['$or'] =  array(
              array(
                'integrantes' =>array(
                  '$elemMatch' => array( 'dni' => $idnumber)
                )
              ),
              array('idnumber' =>$idnumber)
            );

            $data = $this->Model_api->get_inscripciones($query, $fields);

          return $data;
        }



      function revisadas_prejurado_becas($idwf, $ano, $status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $provincia,$disciplina_principal, $tipo_beca=null,$categoria=null,$partido=null,$gender=null,$categoria_beca=null,$disciplina=null){

        if ($disciplina_principal == 'null'){$disciplina_principal = null;}
        if ($disciplina_principal != null) {
          if ($disciplina_principal=='artes_escenicas') {
                     $query=  array (
                            '$or' => array(
                               array('disciplina_principal' => 'teatro'),
                               array('disciplina_principal' => 'danza'),
                               array('disciplina_principal' => 'artes_escenicas')
                            )
                        );
          }else{
            $query['disciplina_principal'] = $disciplina_principal;
          }

        }
        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {
          $query['disciplinas']  =
                array('$all'=> array($disciplina));
        }


        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['prejurados'] = array('$all' => array($assign));}

        if ($tipo_beca == 'null'){$tipo_beca = null;}
        if ($tipo_beca != null) {$query['tipo_beca'] = $tipo_beca;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($categoria_beca == 'null'){$categoria_beca = null;}
        if ($categoria_beca != null) {$query['categoria_beca'] = $categoria_beca;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {
          switch ($provincia) {
            case 'interior':
              $query['$and']=  array (
                          array(
                             'provincia'=>array('$ne' => 'CABA')
                           ),
                          array(
                             'provincia'=>array('$ne' => 'BA')
                              )
                          )
              ;
              break;
              default:
                $query['provincia'] = $provincia;
              break;
          }

        }

        if ($partido == 'null'){$partido = null;}
        if ($partido != null) {$query['partido'] = $partido;}

        if ($gender == 'null'){$gender = null;}
        if ($gender != null) {$query['gender'] = $gender;}

        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;

          switch ($status) {
              case 'pendientes':
               $query['preevaluacion']  = array(
                 '$not' => array('$elemMatch'=> array('idu' => $this->idu)));

             break;
              case 'si':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assign),

                );
                break;
              case 'no':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assign),
                );
              break;
              case 'quiza':
              $query['preevaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assign),

              );
              break;
              case 'evaluadas':
               $query['preevaluacion']  = array('$exists'=> true);
                  break;
              case 'all':
               unset($query['preevaluacion']);
                  break;
          }

        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

       function revisadas_prejurado_visuales($idwf, $ano, $revision,$status,$assign,$skip = 0,$limit =100,$sort = 'seudonimo', $provincia,$categoria){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['prejurados'] = array('$all' => array($assign));}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}


        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;


          switch ($status) {
              case 'pendientes':
               $query['preevaluacion']  = array(
                 '$not' => array('$elemMatch'=> array('idu' => $this->idu)));

             break;
              case 'si':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assign),

                );
                break;
              case 'no':
                $query['preevaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assign),
                );
              break;
              case 'quiza':
              $query['preevaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assign),

              );
              break;
              case 'evaluadas':
               $query['preevaluacion']  = array('$exists'=> true);
                  break;
              case 'all':
               unset($query['preevaluacion']);
                  break;
          }


        $count = $this->count_inscripciones_by_query($query);



        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function revisadas_jurado_artesanias($idwf, $ano, $status,$assign,$skip = 0,$limit =100,$sort = 'seudonimo', $categoria = null, $clases = null){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($clases == 'null'){$clases = null;}
        if ($clases != null) {$query['clases'] = $clases;}

        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;

          switch ($status) {
              case 'pendientes':
                $query['evaluacion'] = array(
                '$not' => array('$elemMatch'=> array('idu' => $assign)));
             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' => $assign),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' => $assign),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' => $assign),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array('$exists'=> true, '$elemMatch' => array('idu' => $assign));
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }


        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function revisadas_jurado_assign($idwf, $ano, $genero,$revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $disciplina = null, $provincia = null, $tipo_organizacion = null, $material = null , $proceso = null){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($material == 'null'){$material = null;}
        if ($material != null) {$query['material'] = $material;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($tipo_organizacion == 'null'){$tipo_organizacion = null;}
        if ($tipo_organizacion != null) {$query['tipo_organizacion'] = $tipo_organizacion;}


        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;


          switch ($status) {
              case 'pendientes':
               //$query['evaluacion']  = array('idu' => array( '$not' =>  array('$gt' =>$assign)));
                $query['evaluacion'] = array(
                '$not' => array('$elemMatch'=> array('idu' => $assign)));
             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' => $assign),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' => $assign),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' => $assign),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array('$exists'=> true, '$elemMatch' => array('idu' => $assign));
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }


        $count = $this->count_inscripciones_by_query($query);



        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

        function puntuadas_jurado_letras($idwf, $ano, $genero,$revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $cant_hojas ){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($cant_hojas == 'null'){$cant_hojas = null;}
        if ($cant_hojas != null) {$query['cant_hojas_int'] = $genero;}


        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;


          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array('$exists'=> false);

             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si'),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no'),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza'),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array('$exists'=> true);
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }



        $count = $this->count_inscripciones_by_query($query);


        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }



    function puntuadas_jurado($idwf, $ano, $genero,$revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $disciplina = null, $provincia = null, $tipo_organizacion = null){

        if ($assign == 'null'){$assign = null;}
        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($tipo_organizacion == 'null'){$tipo_organizacion = null;}
        if ($tipo_organizacion != null) {$query['tipo_organizacion'] = $tipo_organizacion;}


        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;


          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array('$exists'=> false);

             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si'),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no'),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza'),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array('$exists'=> true);
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }



        $count = $this->count_inscripciones_by_query($query);


        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

function puntuadas_jurado_formacion($idwf, $ano, $revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $categoria, $disciplina, $provincia){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria_beca'] = $categoria;}

          if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}


        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;


          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array(
               '$not' => array('$elemMatch'=> array('idu' => $this->idu))
             );

             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$assign),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$assign),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$assign),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array(
                array('$elemMatch'=> array('idu' => $this->idu))
               );
                  break;
              case 'all':
               unset($query['evaluacion']);
                  break;
          }



        $count = $this->count_inscripciones_by_query($query);


        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function revision_ayts($idwf, $ano, $revision,$status,$assign,$skip = 0,$limit =100,$sort = 'lastname', $categoria, $tipo_organizacion, $provincia, $campo_social, $universidades='null'){

        if ($assign == 'null'){$assign = null;}
        if ($assign != null) {$query['jurados'] = array('$all' => array($assign));}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($tipo_organizacion == 'null'){$tipo_organizacion = null;}
        if ($tipo_organizacion != null) {$query['tipo_organizacion'] = $tipo_organizacion;}

        if ($universidades == 'null'){$universidades = null;}
        if ($universidades != null) {$query['vinculacionUniversidad'] = $universidades;}



        $query['idwf'] =  $idwf;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        if ($status != 'all'){
          switch ($status) {
              case 'pendientes':
               $query['evaluacion']  = array(
               '$not' => array('$elemMatch'=> array('idu' => $this->idu))
             );

             break;
              case 'si':
                $query['evaluacion'] = array(
                 '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>$this->idu),

                );
                break;
              case 'no':
                $query['evaluacion'] = array(
                 '$elemMatch' => array('dictamen' => 'no', 'idu' =>$this->idu),
                );
              break;
              case 'quiza':
              $query['evaluacion'] = array(
               '$elemMatch' => array( 'dictamen' => 'quiza', 'idu' =>$this->idu),

              );
              break;
              case 'evaluadas':
               $query['evaluacion']  = array(
                array('$elemMatch'=> array('idu' => $this->idu))
               );
                break;
              case 'all':
               unset($query['evaluacion']);
                break;
          }
        }

        if ($campo_social == 'null'){$campo_social = null;}
        if ($campo_social != null) {
          $query['campo_social']  =
                array('$all'=> array($campo_social));
        }


        $count = $this->Model_api->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }



    function getfile($id){
      $file = $this->Model_api->getArchivo($id);

      if ($file) {
        header("Content-type:" .$file->file['metadata']['type']);
        header("Content-Length:" .$file->file['metadata']['size']);
        header('Content-Disposition: inline;filename="'.$file->file['metadata']['originalFilename'].'"');
        $stream = $file->getResource();

        while (!feof($stream)) {
            echo fread($stream, 8192);
        }
      } else {
        echo 'No existe';
      }

    }



    function writefile($id){
      $file = $this->Model_api->getArchivo($id);

      if ($file) {

        $metadata = $file->file['metadata'];
        $inscripcion['idcase'] = $metadata['idcase'];
        $inscripcion['idwf'] = $metadata['idwf'];

        if ($data_inscripcion = $this->Model_inscripciones->detalle_inscripciones($inscripcion['idcase'],$inscripcion['idwf'])[0]) {

          if (!file_exists(FCPATH.'images/user_files/'.$metadata['idu'].'/'.$metadata['idwf'].'/'.$metadata['idcase'])){
            $mkdir = @mkdir(FCPATH.'images/user_files/'.$metadata['idu'].'/'.$metadata['idwf'].'/'.$metadata['idcase'].'/',0775,true);
          }

          $path = FCPATH.'images/user_files/'.$metadata['idu'].'/'.$metadata['idwf'].'/'.$metadata['idcase'].'/'.$file->file['_id'].'.mp3';
          $url = $this->base_url.'images/user_files/'.$metadata['idu'].'/'.$metadata['idwf'].'/'.$metadata['idcase'].'/'.$file->file['_id'].'.mp3';
          $file->write($path);

          foreach ($data_inscripcion['files'] as $key => &$file) {

            if (strpos($file['url'], $id)) {
              $file['url'] = $url;
            }

          }

          $data['files'] = $data_inscripcion['files'];

          $this->Model_inscripciones->update_inscripcion($data,$inscripcion);

          echo "<script type='text/javascript'>";
          echo "window.close();";
          echo "</script>";

          //redirect($url);

        }  else {
          // si la inscripcion fue borrada, elimino el archivo
          $this->Model_api->deleteArchivo($id);
        }

      } else {
        echo 'No existe';
      }

    }

        function get_bc($idwf_aux,$idwf, $ano,$skip = 0,$limit,$sort = 'lastname', $disciplina, $provincia, $responsable){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($idwf_aux == 'null'){$idwf_aux = null;}
        if ($idwf_aux != null) {$query['idwf_aux'] = $idwf_aux;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsableSeguimiento'] = $responsable;}


        $query['ganador'] = "si";
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        $count = $this->count_inscripciones_by_query($query);


        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }



    function get_preseleccionados($idwf, $ano,$skip = 0,$limit,$sort = 'lastname', $disciplina, $provincia, $responsable){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsableSeguimiento'] = $responsable;}


        $query['ganador'] = "si";
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        $count = $this->count_inscripciones_by_query($query);


        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function get_panel($idwf, $ano,$skip = 0,$limit,$sort, $categoria, $tipo,$disciplina, $provincia, $genero='null',$partido='null',$tipo_beca="null",$categoria_beca='null',$jurado='null'){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($partido == 'null'){$partido = null;}
        if ($partido != null) {$query['partido'] = $partido;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($categoria_beca == 'null'){$categoria_beca = null;}
        if ($categoria_beca != null) {$query['categoria_beca'] = $categoria_beca;}

        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($tipo == 'null'){$tipo = null;}
        if ($tipo != null) {$query['tipo'] = $tipo;}

        if ($tipo_beca == 'null'){$tipo_beca = null;}
        if ($tipo_beca != null) {$query['tipo_beca'] = $tipo_beca;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($jurado == 'null'){$jurado = null;}
        if ($jurado != null) {
          $query['evaluacion'] = array(
            '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>(int)$jurado),

           );
        }

        $query['jurados'] = array('$exists' => true);
        //$query['evaluacion'] = array('$exists' => true);

        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function get_panel_circulacion($idwf, $ano,$skip = 0,$limit,$sort,$sort2, $categoria, $tipo,$disciplina, $provincia, $genero='null',$partido='null',$tipo_beca="null",$categoria_beca='null',$jurado='null',$llamado='null',$orden_sort=-1){



      if ($provincia == 'null'){$provincia = null;}
      if ($provincia != null) {$query['provincia'] = $provincia;}

      if ($partido == 'null'){$partido = null;}
      if ($partido != null) {$query['partido'] = $partido;}

      if ($categoria == 'null'){$categoria = null;}
      if ($categoria != null) {$query['categoria'] = $categoria;}

      if ($categoria_beca == 'null'){$categoria_beca = null;}
      if ($categoria_beca != null) {$query['categoria_beca'] = $categoria_beca;}

      if ($genero == 'null'){$genero = null;}
      if ($genero != null) {$query['genero'] = $genero;}

      if ($disciplina == 'null'){$disciplina = null;}
      if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

      if ($tipo == 'null'){$tipo = null;}
      if ($tipo != null) {$query['tipo'] = $tipo;}

      if ($tipo_beca == 'null'){$tipo_beca = null;}
      if ($tipo_beca != null) {$query['tipo_beca'] = $tipo_beca;}

      if ($idwf == 'null'){$idwf = null;}
      if ($idwf != null) {$query['idwf'] = $idwf;}

      if ($llamado == 'null'){$llamado = null;}
      if ($llamado != null) {$query[$llamado] = 'true';}

      if ($jurado == 'null'){
        $jurado = null;
        $query['evaluacion'] = array(
          '$elemMatch' => array( 'dictamen' => 'si'),
         );
      }

      if ($jurado != null) {
        $query['evaluacion'] = array(
          '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>(int)$jurado),

         );
      }

      $query['jurados'] = array('$exists' => true);
      //$query['evaluacion'] = array('$exists' => true);

      $query['ano'] = (int) $ano;
 //     $query['revision.dictamen'] = $revision;

      $count = $this->count_inscripciones_by_query($query);

      if ($orden_sort==-1) {
        $data = $this->Model_api->get_revisadas_descendente($query, $skip, $limit, $sort, $sort2,-1);
      }else{
        $data = $this->Model_api->get_revisadas_descendente($query, $skip, $limit, $sort, $sort2,1);
      }

      $data['count'] = $count;

      return $data;

  }

    function get_panel_seleccionados($idwf, $ano,$skip = 0,$limit,$sort, $categoria, $tipo,$disciplina, $provincia, $genero='null',$partido='null',$tipo_beca="null",$categoria_beca='null',$jurado='null'){

      if ($provincia == 'null'){$provincia = null;}
      if ($provincia != null) {$query['provincia'] = $provincia;}

      if ($partido == 'null'){$partido = null;}
      if ($partido != null) {$query['partido'] = $partido;}

      if ($categoria == 'null'){$categoria = null;}
      if ($categoria != null) {$query['categoria'] = $categoria;}

      if ($categoria_beca == 'null'){$categoria_beca = null;}
      if ($categoria_beca != null) {$query['categoria_beca'] = $categoria_beca;}

      if ($genero == 'null'){$genero = null;}
      if ($genero != null) {$query['genero'] = $genero;}

      if ($disciplina == 'null'){$disciplina = null;}
      if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

      if ($tipo == 'null'){$tipo = null;}
      if ($tipo != null) {$query['tipo'] = $tipo;}

      if ($tipo_beca == 'null'){$tipo_beca = null;}
      if ($tipo_beca != null) {$query['tipo_beca'] = $tipo_beca;}

      if ($idwf == 'null'){$idwf = null;}
      if ($idwf != null) {$query['idwf'] = $idwf;}

      if ($jurado == 'null'){$jurado = null;}
      if ($jurado != null) {
        $query['evaluacion'] = array(
          '$elemMatch' => array( 'dictamen' => 'si', 'idu' =>(int)$jurado),

         );
      }
      $query['seleccionado'] = "si";


      $query['jurados'] = array('$exists' => true);
      //$query['evaluacion'] = array('$exists' => true);

      $query['ano'] = (int) $ano;
 //     $query['revision.dictamen'] = $revision;

      $count = $this->count_inscripciones_by_query($query);

      $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
      $data['count'] = $count;

      return $data;

  }

    function get_panel_multipartido($idwf, $ano,$skip = 0,$limit,$sort, $categoria, $tipo,$disciplina, $provincia, $genero='null',$partido='null'){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($partido == 'null'){$partido = null;}
        if ($partido != null) {$query['partido'] = array ('$in' => $partido);}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($genero == 'null'){$genero = null;}
        if ($genero != null) {$query['genero'] = $genero;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($tipo == 'null'){$tipo = null;}
        if ($tipo != null) {$query['tipo'] = $tipo;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}


        $query['jurados'] = array('$exists' => true);
        //$query['evaluacion'] = array('$exists' => true);

        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function get_panel_visuales($idwf, $ano,$skip = 0,$limit,$sort, $categoria, $subcategoria, $provincia,$seleccionado='null'){
        if ($seleccionado == 'null'){$seleccionado = null;}
        if ($seleccionado != null) {$query['seleccionado'] = 'si';}


        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($subcategoria == 'null'){$subcategoria = null;}
        if ($subcategoria != null) {$query['subcategoria'] = $subcategoria;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}


        $query['jurados'] = array('$exists' => true);
        //$query['evaluacion'] = array('$exists' => true);

        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_descendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function get_panel_ayts($idwf, $ano,$skip = 0,$limit,$sort, $categoria, $provincia, $campo_social, $universidades, $tipo_organizacion){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($subcategoria == 'null'){$subcategoria = null;}
        if ($subcategoria != null) {$query['subcategoria'] = $subcategoria;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($tipo_organizacion == 'null'){$tipo_organizacion = null;}
        if ($tipo_organizacion != null) {$query['tipo_organizacion'] = $tipo_organizacion;}

        $query['jurados'] = array('$exists' => true);
        //$query['evaluacion'] = array('$exists' => true);
        if ($universidades == 'null'){$universidades = null;}
        if ($universidades != null) {$query['vinculacionUniversidad'] = $universidades;}

        if ($campo_social == 'null'){
          $campo_social=null;
          $query['categoria']= array('$ne' => 'sedronar');

        }

        if ($campo_social != null&&$campo_social != 'all') {
          $query['campo_social']  =
                array('$all'=> array($campo_social));
        }

        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        $count = $this->count_inscripciones_by_query($query);

        $data = $this->Model_api->get_revisadas_descendente($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }


      function get_convocatoria($idwf, $ano){

        $data = $this->Model_api->get_convocatoria_ano($idwf,(int)$ano);

              //output_json($data[0]);

              return $data;

    }

   function get_ganadores_bf($idwf, $ano,$skip = 0,$limit,$sort, $disciplina, $provincia, $responsable, $tipo, $tipo_convocatoria, $ganador='si', $prioridad='null', $mes_inicio='null', $liquidado='null', $informeFinal='null'){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsableSeguimiento'] = $responsable;}

        if ($tipo == 'null'){$tipo = null;}
        if ($tipo != null) {$query['tipo'] = $tipo;}

        if ($informeFinal == 'null'){$informeFinal = null;}
        if ($informeFinal != null) {
          switch ($informeFinal) {
            case 'enviado':
               $query['informeFinal'] = $informeFinal;
              break;
            case 'no_enviado':
               $query['informeFinal'] = $informeFinal;
              break;
            case 'Aprobado':
               $query['informeFinal'] = 'enviado';
               $query['informeFinalStatus'] = 'Aprobado';
              break;
            case 'Modifica':
               $query['informeFinal'] = 'enviado';
               $query['informeFinalStatus'] = 'Modifica';
              break;
            case 'Pendiente':
               $query['informeFinal'] = 'enviado';
               $query['informeFinalStatus'] = 'Pendiente';
              break;
          }


        }

        if ($tipo_convocatoria == 'null'){$tipo_convocatoria = null;}
        if ($tipo_convocatoria != null) {$query['tipo_convocatoria'] = $tipo_convocatoria;}

        if ($prioridad == 'null'){$prioridad = null;}
        if ($prioridad != null) {
          if ($prioridad=='false') {
            $query['prioridad.ok'] = array('$ne' => 'true');
          }else{
            $query['prioridad.ok'] = $prioridad;
          }
        }

        if ($liquidado == 'null'){$liquidado = null;}
        if ($liquidado != null) {
          if ($liquidado=='false') {
            $query['liquidado.ok'] = array('$ne' => 'true');
          }else{
            $query['liquidado.ok'] = $liquidado;
          }

        }

        if ($mes_inicio == 'null'){$mes_inicio = null;}
        if ($mes_inicio != null) {$query['mes_inicio'] = $mes_inicio;}

        $query['ganador'] = $ganador;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;
          $count = $this->count_inscripciones_by_query($query);

           $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
          $data['count'] = $count;

        return $data;

    }

    function get_ganadores_bc($idwf_aux, $idwf, $ano,$skip = 0,$limit,$sort, $disciplina_principal, $provincia, $responsable, $informe_status='null', $ganador='si', $prioridad='null', $liquidado='null', $informe=false,$partido=null){

        if ($informe) {
          if ($informe=='segundo_informe') {
            $informe='informe2';
          }else{
            $informe='informe';
          }
        }
        $query['idwf_aux'] =$idwf_aux;

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        $query['ano'] = (int)$ano;

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($partido == 'null'){$partido = null;}
        if ($partido != null) {$query['partido'] = $partido;}

        if ($disciplina_principal == 'null'){$disciplina_principal = null;}
        if ($disciplina_principal != null) {$query['disciplina_principal'] = $disciplina_principal;}

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsableSeguimiento'] = (int)$responsable;}

        if ($informe_status == 'null'){$informe_status = null;}
        if ($informe_status != null) {

          switch ($informe_status) {
            case 'pendiente_comunicacion':
              $query[$informe.'.revision_comunicacion'] = 'pendiente';
              break;
            case 'aprobado_comunicacion':
              $query[$informe.'.revision_comunicacion'] = 'aprobado';
              break;
            case 'modifica_comunicacion':
              $query[$informe.'.revision_comunicacion'] = 'modifica';
              break;

            default:
              $query[$informe.'.revision_planeamiento'] = $informe_status;
              break;
          }

        }

        if ($prioridad == 'null'){$prioridad = null;}
        if ($prioridad != null) {
          if ($prioridad=='false') {
            $query['prioridad.ok'] = array('$ne' => 'true');
          }else{
            $query['prioridad.ok'] = $prioridad;
          }
        }

        if ($liquidado == 'null'){$liquidado = null;}
        if ($liquidado != null) {
          if ($liquidado=='false') {
            $query['liquidado.ok'] = array('$ne' => 'true');
          }else{
            $query['liquidado.ok'] = $liquidado;
          }

        }

        $query['ganador'] = $ganador;

        if ($idwf_aux != null) {
          $count = $this->Model_api->count_inscripciones_by_query_beca($query);
        }else{
          $count = $this->count_inscripciones_by_query($query);
        }

           $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
          $data['count'] = $count;

        return $data;

    }


    function get_ganadores($idwf_aux,$idwf, $ano,$skip = 0,$limit,$sort = 'lastname', $disciplina, $provincia, $responsable, $informe_status=null){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}


        if ($idwf_aux == 'null'){$idwf_aux = null;}
        if ($idwf_aux != null) {$query['idwf_aux'] = $idwf_aux;}

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsableSeguimiento'] = $responsable;}

        if ($informe_status == 'null'){$informe_status = null;}
        if ($informe_status != null) {

          switch ($informe_status) {
            case 'pendiente_comunicacion':
              $query['informe.revision_comunicacion'] = 'pendiente';
              break;
            case 'aprobado_comunicacion':
              $query['informe.revision_comunicacion'] = 'aprobado';
              break;
            case 'modifica_comunicacion':
              $query['informe.revision_comunicacion'] = 'modifica';
              break;

            default:
              $query['informe.revision_planeamiento'] = $informe_status;
              break;
          }

        }

        $query['ganador'] = "si";
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        if ($idwf_aux != null) {
          $count = $this->Model_api->count_inscripciones_by_query_beca($query);
        }else{
          $count = $this->count_inscripciones_by_query($query);
        }

           $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);


        $data['count'] = $count;

        return $data;

    }

    function get_ganadores_concursos_panel($idwf, $ano,$ganador,$skip = 0,$limit,$responsable='null',$situacion='null',$sort,$alta='null',$legales='null',$provincia='null',$partido='null',$pago1='null',$nroBenef='null',$categoria='null',$datosMuestra='null'){

        if ($idwf == 'null'){
          $query['type'] = 'concurso';
          $idwf = null;
        }
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($ganador == 'null'){
          $query['ganador'] =  array('$exists' => 'true');
          $ganador = null;
        }
        if ($ganador != null) {
          if ($ganador=='seleccionado') {
            $query['seleccionado'] = "si";
          }else{
            $query['ganador'] = $ganador;
          }

        }

        if ($ano == 'null'){$ano = null;}
        if ($ano != null) {$query['ano'] = $ano;}

        if ($datosMuestra == 'null'){$datosMuestra = null;}
        if ($datosMuestra != null) {$query['muestra'] = $datosMuestra;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria ;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($partido == 'null'){$partido = null;}
        if ($partido != null) {$query['partido'] = $partido;}

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsable'] = $responsable;}

        //ACOMODO STATUS PARA LA QUERY
        switch ($situacion) {
            case 'null':
            unset ($situacion);
                break;
            case 'para_revisar':
            $query['flujo.situacion'] = array('$ne' => 'ok');
             break;
            case 'revisados':
            $query['flujo.situacion'] = 'ok';
                break;
            case 'activos':
            $query['goSituacion'] = 'activo';
                break;
            case 'inactivos':
            $query['goSituacion'] = 'inactivo';
                break;
            case 'pendientes':
            $query['goSituacion'] = 'pendiente';
                break;
            case 'cambioCbu':
            $query['goSituacion'] = 'cambioCbu';
                break;
            case 'con_alta':
              $query['alta'] = 'enviado';
              $query['goSituacion'] = 'pendiente';
                break;
           }

          switch ($nroBenef) {
            case 'null':
            unset ($nroBenef);
                break;
            case 'con_benef':
                    $query['$and']= array (
                              array('nroBeneficiario' => array('$ne' => '')),
                              array('nroBeneficiario' => array('$exists'=>true)),
                            );
                break;
            case 'sin_benef':
                  $query['$or']= array(
                              array('nroBeneficiario' => array('$exists'=>false)),
                              array('nroBeneficiario' => ''),
                        );
                break;
          }

          switch ($alta) {
            case 'null':
            unset ($alta);
                break;
            case 'no_enviados':
              $query['alta'] = array('$ne' => 'enviado');
              $query['altaDictamen'] = array('$ne' => 'Modificar');
              break;
            case 'enviados':
              $query['alta'] = 'enviado';
             break;
            case 'pendientes':
              $query['altaDictamen'] = 'Pendiente';
                break;
            case 'modifican':
              $query['altaDictamen'] = 'Modificar';
                break;
            case 'completos':
              $query['altaDictamen'] = 'Completa';
                break;
           }

          switch ($legales) {
            case 'null':
            unset ($legales);
                break;
            case 'con_pase':
            $query['slPase'] = 'checked';
                break;
            case 'pendientes':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = array('$ne' => 'checked');
             break;
            case 'con_dictamen':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = 'checked';
             break;
           }

          switch ($pago1) {
            case 'null':
            unset ($pago1);
                break;
            case 'liquidados':
              $query['goLiquidado'] = 'checked';
                break;
            case 'esperan_partida':
              $query['goEsperaPartida'] = 'checked';
                break;
            case 'para_pagar':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = 'checked';
              $query['goPagado'] = array('$ne' => 'checked');
                break;
            case 'pagados':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = 'checked';
              $query['goPagado'] = 'checked';
                break;
           }

   //     $query['revision.dictamen'] = $revision;

          $count = $this->count_inscripciones_by_query($query);
          $data = $this->Model_api->get_revisadas_ascendente_ganadores_concursos($query,$skip, $limit);

          $data['count'] = $count;

        return $data;

    }

      function get_ganadores_becas_promocion_search($idwf, $ano,$ganador,$skip = 0,$limit,$idcase,$sort='lastname'){

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        $query['ganador'] = $ganador;
        $query['ano'] =$ano;

        $query['idcase'] =$idcase;

        //ACOMODO STATUS PARA LA QUERY

          $count = $this->count_inscripciones_promocion_by_query($query);
          $data = $this->Model_api->get_revisadas_ascendente($query,$skip, $limit, $sort);

          $data['count'] = $count;

        return $data;

    }

     function get_ganadores_becas_promocion($idwf, $ano,$ganador,$skip = 0,$limit,$provincia='null',$partido='null',$disciplina_principal='null',$responsable='null',$tipo='null',$situacion='null',$sort,$alta='null',$legales='null',$pago1='null',$nroBenef='null',$mes_inicio_aceptacion='null',$llamado="null",$informe="null"){



        if ($disciplina_principal == 'null'){$disciplina_principal = null;}
        if ($disciplina_principal != null) {
            $query['disciplina_principal'] = $disciplina_principal;
        }


        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}


        if ($tipo == 'null'){$tipo = null;}
        if ($tipo != null) {$query['tipo_beca'] = $tipo;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($partido == 'null'){$partido = null;}
        if ($partido != null) {$query['partido'] = $partido;}

        if ($mes_inicio_aceptacion == 'null'){$mes_inicio_aceptacion = null;}
        if ($mes_inicio_aceptacion != null) {$query['mes_inicio_aceptacion'] = $mes_inicio_aceptacion;}

        $query['ganador'] = $ganador;
        $query['ano'] =$ano;

      if ($llamado == 'null'){$llamado = null;}
        //ACOMODO STATUS PARA LA QUERY
        if ($llamado != null) {
          if ($llamado=='segundo') {
            $query['segundo'] = 'true';
          }else if($llamado=='tercero') {
            $query['tercero'] = 'true';
          }else{
            $query['segundo'] = array('$ne' => 'true');
            $query['tercero'] = array('$ne' => 'true');
        }
      }

          switch ($situacion) {
            case 'null':
            unset ($situacion);
                break;
            case 'para_revisar':
              $query['flujo.situacion'] = array('$exists'=>false);
             break;
            case 'revisados':
            $query['flujo.situacion'] = array('$exists'=>true);
                break;
            case 'alta_enviado':
              $query['altas'] = array('$exists'=>true);
                break;
            default:
              $query['flujo.situacion'] = array('$exists'=>true);
                break;
           }


          switch ($alta) {
            case 'null':
            unset ($alta);
                break;
            case 'no_enviados':
              $query['alta'] = array('$ne' => 'enviado');
              $query['altaDictamen'] = array('$ne' => 'Modificar');
              break;
            case 'enviados':
              $query['alta'] = 'enviado';
             break;
            case 'pendientes':
              $query['altaDictamen'] = 'Pendiente';
                break;
            case 'modifican':
              $query['altaDictamen'] = 'Modificar';
                break;
            case 'completos':
              $query['altaDictamen'] = 'Completa';
                break;
           }

          switch ($legales) {
            case 'null':
            unset ($legales);
                break;
            case 'con_pase':
            $query['slPase'] = 'checked';
                break;
            case 'sin_pase':
            $query['slPase'] = array('$ne' => 'checked');
                break;
            case 'pendientes':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = array('$ne' => 'checked');
             break;
            case 'con_dictamen':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = 'checked';
             break;
           }

          switch ($pago1) {
            case 'null':
            unset ($pago1);
                break;
            case 'liquidados':
              $query['goLiquidado'] = 'checked';
                break;
            case 'esperan_partida':
              $query['goEsperaPartida'] = 'checked';
                break;
            case 'para_pagar':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = 'checked';
              $query['goPagado'] = array('$ne' => 'checked');
                break;
            case 'pagados':
              // $query['slPase'] = 'checked';
              // $query['slDictamen'] = 'checked';
              $query['goPagado'] = 'checked';
                break;
           }

          switch ($informe) {
            case 'null':
              unset ($informe);
                break;
            case 'no_enviados':
              $query['informe'] = array('$ne'=>'enviado');
               $query['informe_dictamen'] = array('$ne'=>'modifica');
              break;
            case 'enviados':
              $query['informe'] = 'enviado';
             break;
            case 'pendientes':
              $query['informe'] = 'enviado';
              $query['informe_dictamen'] = "pendiente";
                break;
            case 'modifican':
              $query['informe'] = 'no_enviado';
              $query['informe_dictamen'] = "modifica";
                break;
            case 'aceptados':
              $query['informe'] = 'enviado';
              $query['informe_dictamen'] = "aprobado";
                break;
           }
   //     $query['revision.dictamen'] = $revision;

          $count = $this->count_inscripciones_promocion_by_query($query);
          $data = $this->Model_api->get_revisadas_ascendente($query,$skip, $limit, $sort);

          $data['count'] = $count;

        return $data;

    }

        function get_ganadores_becas($idwf, $ano,$ganador,$skip = 0,$limit,$provincia='null',$partido='null',$disciplina_principal='null',$resumen='null',$asignacion='null',$responsable='null',$tipo='null',$situacion='null',$sort,$alta='null',$legales='null',$pago1='null',$nroBenef='null',$reformulacion='null',$mes_inicio_aceptacion='null',$categoria='null',$primer_informe='null',$segundo_informe='null',$pago2='null',$pago3='null',$archivo='null',$prorroga='null'){

        if ($disciplina_principal == 'null'){$disciplina_principal = null;}
        if ($disciplina_principal != null) {
          if ($disciplina_principal=='artes_escenicas' && $idwf=='beca_creacion') {
                     $query=  array (
                            '$or' => array(
                               array('disciplina_principal' => 'teatro'),
                               array('disciplina_principal' => 'danza')
                            )
                        );
          }else{
            $query['disciplina_principal'] = $disciplina_principal;
          }
        }

        if ($resumen == 'null'){$resumen = null;}
        if ($resumen != null) {
          switch ($resumen) {
            case 'conResumen':
                  $query['$and'] = array (
                              array('resumen' => array('$ne' => '')),
                              array('resumen' => array('$exists' => 'true')),
                        );

              break;
            case 'sinResumen':
                $query['resumen'] = array('$exists' => false);
              break;
          }
        }

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria_beca'] = $categoria;}

        if ($tipo == 'null'){$tipo = null;}
        if ($tipo != null) {$query['tipo_beca'] = $tipo;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($partido == 'null'){$partido = null;}
        if ($partido != null) {$query['partido'] = $partido;}

        if ($mes_inicio_aceptacion == 'null'){$mes_inicio_aceptacion = null;}
        if ($mes_inicio_aceptacion != null) {
          $query['mes_inicio_aceptacion'] = $mes_inicio_aceptacion;
        }

        $query['ganador'] = $ganador;
        $query['ano'] =$ano;

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsable'] = $responsable;}

        //ACOMODO STATUS PARA LA QUERY
          switch ($asignacion) {
            case 'null':
            unset ($asignacion);
                break;
            case 'asignados':
            $query['flujo.asignacion'] = 'ok';
                break;
            case 'sin_asignar':
            $query['flujo.asignacion'] = array('$ne' => 'ok');
             break;
           }

          switch ($situacion) {
            case 'null':
            unset ($situacion);
                break;
            case 'asignados':
            $query['flujo.situacion'] = 'ok';
                break;
            case 'para_revisar':
            $query['flujo.situacion'] = array('$ne' => 'ok');
             break;
            case 'revisados':
            $query['flujo.situacion'] = 'ok';
                break;
            case 'activos':
            $query['goSituacion'] = 'activo';
                break;
            case 'inactivos':
            $query['goSituacion'] = 'inactivo';
                break;
            case 'pendientes':
            $query['goSituacion'] = 'pendiente';
                break;
            case 'cambioCbu':
            $query['goSituacion'] = 'cambioCbu';
                break;
            case 'con_alta':
              $query['alta'] = 'enviado';
              $query['goSituacion'] = 'pendiente';
                break;
           }


          switch ($primer_informe) {
            case 'null':
            unset ($primer_informe);
                break;
            case 'no_enviados':
              $query['primer_informe'] = array('$ne' => 'enviado');
              $query['primerInformeDictamen'] = array('$ne' => 'modificar');
                break;
            case 'enviados':
              $query['primer_informe'] = 'enviado';
             break;
            case 'pendiente':
              $query['primer_informe'] = 'enviado';
              $query['primerInformeDictamen'] = array('$ne' => 'aprobado');
                break;
            case 'modificar':
              $query['primer_informe'] = 'no_enviado';
              $query['primerInformeDictamen'] = 'modificar';
                break;
            case 'aprobado':
              $query['primer_informe'] = 'enviado';
              $query['primerInformeDictamen'] = 'aprobado';
                break;
           }

          switch ($segundo_informe) {
            case 'null':
            unset ($segundo_informe);
                break;
            case 'no_enviados':
              $query['segundo_informe'] = array('$ne' => 'enviado');
                break;
            case 'enviados':
              $query['segundo_informe'] = 'enviado';
             break;
            case 'pendiente':
              $query['segundo_informe'] = 'enviado';
              $query['segundoInformeDictamen'] = array('$ne' => 'aprobado');
                break;
            case 'modificar':
              $query['segundo_informe'] = 'no_enviado';
              $query['segundoInformeDictamen'] = 'modificar';
                break;
            case 'aprobado':
              $query['segundo_informe'] = 'enviado';
              $query['segundoInformeDictamen'] = 'aprobado';
                break;
           }

           if ($archivo == 'null'){$archivo = null;}
           if ($archivo != null) {
             if ($archivo=='no_archivado') {
              $query['archivo'] = array('$ne'=>'checked');
             }else{
              $query['archivo'] = 'checked';
             }

          }

          if ($prorroga == 'null'){$prorroga = null;}
          if ($prorroga != null) {
            if ($prorroga=='sin_prorroga') {
             $query['prorroga'] = array('$ne'=>'checked');
            }else{
             $query['prorroga'] = 'checked';
            }

         }

          switch ($nroBenef) {
            case 'null':
            unset ($nroBenef);
                break;
            case 'con_benef':
                    $query['$and']= array (
                              array('nroBeneficiario' => array('$ne' => '')),
                              array('nroBeneficiario' => array('$exists'=>true)),
                            );
                break;
            case 'sin_benef':
                  $query['$or']= array(
                              array('nroBeneficiario' => array('$exists'=>false)),
                              array('nroBeneficiario' => ''),
                        );
                break;
          }

          switch ($alta) {
            case 'null':
            unset ($alta);
                break;
            case 'no_enviados':
              $query['alta'] = array('$ne' => 'enviado');
              $query['altaDictamen'] = array('$ne' => 'Modificar');
              break;
            case 'enviados':
              $query['alta'] = 'enviado';
             break;
            case 'pendientes':
              $query['altaDictamen'] = 'Pendiente';
                break;
            case 'modifican':
              $query['altaDictamen'] = 'Modificar';
                break;
            case 'completos':
              $query['altaDictamen'] = 'Completa';
                break;
           }

          switch ($legales) {
            case 'null':
            unset ($legales);
                break;
            case 'con_pase':
            $query['slPase'] = 'checked';
                break;
            case 'sin_pase':
            $query['slPase'] = array('$ne' => 'checked');
                break;
            case 'pendientes':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = array('$ne' => 'checked');
             break;
            case 'con_dictamen':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = 'checked';
             break;
           }

          switch ($pago1) {
            case 'null':
            unset ($pago1);
                break;
            case 'liquidados':
              $query['goLiquidado'] = 'checked';
                break;
            case 'esperan_partida':
              $query['goEsperaPartida'] = 'checked';
                break;
            case 'para_pagar':
              // $query['slPase'] = 'checked';
              $query['alta'] = 'enviado';
              $query['goPagado'] = array('$ne' => 'checked');
                break;
            case 'pagados':
              // $query['slPase'] = 'checked';
              // $query['slDictamen'] = 'checked';
              $query['goPagado'] = 'checked';
                break;
           }

           switch ($pago2) {
            case 'null':
            unset ($pago2);
                break;
            case 'liquidados':
              $query['goLiquidado2'] = 'checked';
                break;
            case 'esperan_partida':
              $query['goEsperaPartida2'] = 'checked';
                break;
            case 'para_pagar':
              // $query['slPase'] = 'checked';
              // $query['slDictamen'] = 'checked';
              $query['primerInformeDictamen'] = 'aprobado';
              $query['primer_informe'] = 'enviado';
              $query['goPagado'] = 'checked';
              $query['goPagado2'] = array('$ne' => 'checked');
                break;
            case 'pagados':
              $query['goPagado2'] = 'checked';
                break;
           }

           switch ($pago3) {
            case 'null':
            unset ($pago3);
                break;
            case 'liquidados':
              $query['goLiquidado3'] = 'checked';
                break;
            case 'esperan_partida':
              $query['goEsperaPartida3'] = 'checked';
                break;
            case 'para_pagar':
              // $query['slPase'] = 'checked';
              // $query['slDictamen'] = 'checked';
              $query['segundo_informe'] = 'enviado';
              $query['segundoInformeDictamen'] = 'aprobado';
              $query['goPagado'] = 'checked';
              $query['goPagado2'] = 'checked';
              $query['goPagado3'] = array('$ne' => 'checked');
                break;
            case 'pagados':
              $query['goPagado3'] = 'checked';
                break;
           }

          switch ($reformulacion) {
            case 'null':
            unset ($reformulacion);
                break;
            case 'no_enviados':
              $query['reformulacion'] = array('$ne'=>'enviado');
              $query['reformulacion_dictamen'] = array('$ne'=>'no_presenta');
              break;
            case 'enviados':
              $query['reformulacion'] = 'enviado';
             break;
            case 'pendientes':
              $query['reformulacion'] = 'enviado';
              $query['reformulacion_dictamen'] = "pendiente";
                break;
            case 'modifican':
              $query['reformulacion'] = 'enviado';
              $query['reformulacion_dictamen'] = "modifica";
                break;
            case 'aceptados':
              $query['reformulacion'] = 'enviado';
              $query['reformulacion_dictamen'] = "aprobado";
                break;
            case 'rechazados':
              $query['reformulacion'] = 'no_presenta';
              $query['reformulacion_dictamen'] = 'no_presenta';
                break;
           }
   //     $query['revision.dictamen'] = $revision;

          $count = $this->count_inscripciones_by_query($query);

          $data = $this->Model_api->get_revisadas_ascendente($query,$skip, $limit, $sort);

          $data['count'] = $count;

        return $data;

    }

    function get_ganadores_cooperativas($idwf, $ano,$ganador,$skip = 0,$limit,$provincia='null',$partido='null',$asignacion='null',$responsable='null',$situacion='null',$sort,$legales='null',$categoria='null',$alta='null'){

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['cooperativa_especialidad'] = $categoria;}

        if ($partido == 'null'){$partido = null;}
        if ($partido != null) {$query['partido'] = $partido;}

        $query['ganador'] = $ganador;
        $query['ano'] =$ano;

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsable'] = $responsable;}

        //ACOMODO STATUS PARA LA QUERY
          switch ($asignacion) {
            case 'null':
            unset ($asignacion);
                break;
            case 'asignados':
            $query['flujo.asignacion'] = 'ok';
                break;
            case 'sin_asignar':
            $query['flujo.asignacion'] = array('$ne' => 'ok');
             break;
           }

          switch ($situacion) {
            case 'null':
            unset ($situacion);
                break;
            case 'asignados':
            $query['flujo.situacion'] = 'ok';
                break;
            case 'para_revisar':
            $query['flujo.situacion'] = array('$ne' => 'ok');
             break;
            case 'revisados':
            $query['flujo.situacion'] = 'ok';
                break;
            case 'activos':
            $query['goSituacion'] = 'activo';
                break;
            case 'inactivos':
            $query['goSituacion'] = 'inactivo';
                break;
            case 'pendientes':
            $query['goSituacion'] = 'pendiente';
                break;
           }

          switch ($legales) {
            case 'null':
            unset ($legales);
                break;
            case 'con_pase':
            $query['slPase'] = 'checked';
                break;
            case 'sin_pase':
            $query['slPase'] = array('$ne' => 'checked');
                break;
            case 'pendientes':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = array('$ne' => 'checked');
             break;
            case 'con_dictamen':
              $query['slPase'] = 'checked';
              $query['slDictamen'] = 'checked';
             break;
           }


          switch ($alta) {
            case 'null':
            unset ($alta);
                break;
            case 'no_enviados':
              $query['alta'] = array('$ne' => 'enviado');
              $query['altaDictamen'] = array('$ne' => 'Modificar');
              break;
            case 'enviados':
              $query['alta'] = 'enviado';
             break;
            case 'pendientes':
              $query['altaDictamen'] = 'Pendiente';
                break;
            case 'modifican':
              $query['altaDictamen'] = 'Modificar';
                break;
            case 'completos':
              $query['altaDictamen'] = 'Completa';
                break;
           }
   //     $query['revision.dictamen'] = $revision;

          $count = $this->count_inscripciones_by_query($query);
           $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);


        $data['count'] = $count;

        return $data;

    }
       function get_seleccionados($idwf, $ano,$skip = 0,$limit,$sort, $disciplina, $categoria, $provincia, $muestra, $publico){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($muestra == 'null'){$muestra = null;}
        if ($muestra != null) {$query['datosMuestra'] = $muestra;}

        if ($publico == 'null'){$publico = null;}
        if ($publico != null) {$query['publico'] = $publico;}


        $query['seleccionado'] = "si";
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;
          $count = $this->count_inscripciones_by_query($query);

           $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
          $data['count'] = $count;

        return $data;

    }

    function get_ganadores_concursos($idwf, $ano,$skip = 0,$limit,$sort, $disciplina, $categoria, $provincia, $muestra, $ganador){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($categoria == 'null'){$categoria = null;}
        if ($categoria != null) {$query['categoria'] = $categoria;}

        if ($muestra == 'null'){$muestra = null;}
        if ($muestra != null) {$query['datosMuestra'] = $muestra;}

        $query['ganador'] = $ganador;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;
          $count = $this->count_inscripciones_by_query($query);

           $data = $this->Model_api->get_revisadas_ascendente($query, $skip, $limit, $sort);
          $data['count'] = $count;

        return $data;

    }



        function get_seguimiento_preseleccionados($idwf_aux,$idwf, $ano,$skip = 0,$limit,$sort = 'lastname', $disciplina, $provincia, $responsable){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}


        if ($idwf_aux == 'null'){$idwf_aux = null;}
        if ($idwf_aux != null) {$query['idwf_aux'] = $idwf_aux;}

        if ($responsable == 'null'){$responsable = null;}
        if ($responsable != null) {$query['responsableSeguimiento'] = $responsable;}


        $query['ganador'] = "si";
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        if ($idwf_aux != null) {
          $count = $this->Model_api->count_inscripciones_by_query_beca($query);
        }else{
          $count = $this->count_inscripciones_by_query($query);
        }

           $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);


        $data['count'] = $count;

        return $data;

    }


    function get_busqueda_becas($idwf_aux, $ano, $data, $ganador){


        if ($idwf_aux == 'null'){$idwf_aux = null;}
        if ($idwf_aux != null) {$query['idwf_aux'] = $idwf_aux;}

        $query['ganador'] = $ganador;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;


        $data = $this->Model_api->search_inscripciones_ganadores_bc($data, $idwf_aux, $ano);
        $data['count'] = $count;

        return $data;

    }


    function get_busqueda_inscripcion_idwf($idwf, $ano, $data, $ganador){


        $query['ganador'] = $ganador;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;


        $data = $this->Model_api->search_inscripciones_ganadores($data, $idwf, $ano);
        $data['count'] = $count;

        return $data;

    }

   function get_busqueda_inscripcion_concurso($ano, $data){

        $data = $this->Model_api->search_inscripciones_ganadores_concursos($data,$ano);
        $data['count'] = $count;

        return $data;

    }

   function get_busqueda_panel_seudonimo($idwf,$ano, $data){

        $data = $this->Model_api->search_inscripciones_panel_seudonimo($data, $idwf,$ano);
        $data['count'] = $count;

        return $data;

    }

    function get_busqueda_seleccionados($idwf,$ano, $data){

        $data = $this->Model_api->search_inscripciones_seleccionadas($data, $idwf,$ano);
        $data['count'] = $count;
        return $data;

    }

       function get_busqueda_panel($idwf,$ano, $data){

        $data = $this->Model_api->search_inscripciones_panel($data, $idwf,$ano);
        $data['count'] = $count;

        return $data;

    }

    function get_suplentes_becas($idwf_aux, $idwf, $ano,$skip = 0,$limit,$sort = 'lastname', $disciplina, $provincia, $ganador){

        if ($provincia == 'null'){$provincia = null;}
        if ($provincia != null) {$query['provincia'] = $provincia;}

        if ($disciplina == 'null'){$disciplina = null;}
        if ($disciplina != null) {$query['disciplina_principal'] = $disciplina;}

        if ($idwf == 'null'){$idwf = null;}
        if ($idwf != null) {$query['idwf'] = $idwf;}

        if ($idwf_aux == 'null'){$idwf_aux = null;}
        if ($idwf_aux != null) {$query['idwf_aux'] = $idwf_aux;}

        $query['ganador'] = $ganador;
        $query['ano'] = (int) $ano;
   //     $query['revision.dictamen'] = $revision;

        $count = $this->Model_api->count_inscripciones_by_query_beca($query);

        $data = $this->Model_api->get_revisadas($query, $skip, $limit, $sort);
        $data['count'] = $count;

        return $data;

    }

    function add(){
      echo '<form enctype="multipart/form-data" action="'.$this->base_url.'api/add_file" method="POST"><input name="file" type="file" /><input type="submit" value="Enviar fichero" /></form>';
    }

    function add_file(){
      $file = $_FILES['file'];
      var_dump($file);
      $this->load->library('aws3');
      $response = $this->aws3->sendFile('test', $file);
      echo json_encode($response);
    }

    function delete_file_convocatorias($bucket, $path){
      $file = $_FILES['file'];
      $this->load->library('aws3');
        $response = $this->aws3->deleteFile($bucket, $path);
        //echo json_encode($response);
      }


    function upload_file_convocatorias($bucket, $path){
      $file = $_FILES['file'];
      $file['path']=$path;
      $this->load->library('aws3');
        $response = $this->aws3->uploadFile($bucket, $file);
        //echo json_encode($response);
      }

    function metadata($bucket, $file){
      $this->load->library('aws3');
      $response = $this->aws3->getMetadata($bucket, urldecode($file));
      echo json_encode($response);
    }

    function add_bucket($bucket){
      $this->load->library('aws3');
      $this->aws3->addBucket($bucket);
    }

    function delete_bucket($bucket){
      $this->load->library('aws3');
      $this->aws3->deleteBucket($bucket);
    }

    function list_buckets(){
      $this->load->library('aws3');
      output_json($this->aws3->listBuckets());
    }

    function searchBucket($bucket){
      $this->load->library('aws3');
      echo $this->aws3->searchBucket($bucket);
    }

    function list_keys($bucket){
      $this->load->library('aws3');
      output_json($this->aws3->listKeys('crudos-becas'));
    }

    function get_files($bucket = "procesos"){
      $this->load->library('aws3');
      $data = $this->aws3->listKeys($bucket);
      return $data;
    }


    function update_beneficiario($data, $inscripcion){

        $result = array();
        $container = 'container.alta_beneficiario';
        $query= array('idcase'=>$inscripcion['idcase'],
                       'idwf' => $inscripcion['idwf']);
        $this->db->where($query);
        $result = $this->db->update($container, $data);
    }

    function get_url($bucket, $key){
      $this->load->library('aws3');
      output_json($this->aws3->getAuthenticatedURL($bucket, $key));
    }

    function crudos_becas(){
      $this->load->library('aws3');
      $urls = $this->aws3->listKeys('crudos-becas');
      foreach ($urls as $key => &$url) {
        echo 'https://archivos.fnartes.gob.ar/crudos-becas/' . $url . '<br>';
      }
      //output_json($urls);
    }


}
