<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Website
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 */
class Website extends MX_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('user');

			$this->load->config('config');
      $this->load->library('parser');
			$this->load->model('cms/Model_page');
			$this->load->model('cms/Model_node');
      $this->load->module('website/blocks');
      $this->load->module('website/layouts');
      $this->load->model('options');
			//---base variables
			$this->base_url = base_url();
			$this->module_url = base_url() . $this->router->fetch_module() . '/';
			$this->lang->load('user/profile', $this->config->item('language'));
			$this->idu = (double) $this->session->userdata('iduser');
		  $this->user = $this->user->get_user($this->idu);

      error_reporting(E_ERROR | E_PARSE);


		}

    function index(){

      $params = array('slug' => $this->uri->uri_string);

			if ($page = $this->Model_node->get_one($params)) {
        $page['base_url'] = $this->base_url;
        $page['module_url'] = $this->module_url;
        $page['isloggedin'] = $this->isloggedin();
        $page['name'] = $this->user->name;
        $page['lastname'] = $this->user->lastname;
        $page['email'] = $this->user->email;
        $this->header($page);
        $page['logo_interno_computer']="";
        $page['logo_interno_mobile']="";

        if ( $page['slug']!='catalogo-covid19') {
          $page['logo_interno_computer']="<div class='navbar-brand-complement computer' style='background-image: url(".$this->base_url."dashboard/assets/img/icon-catalogo.png);'></div>";

          $page['logo_interno_mobile']="<div class='navbar-brand-complement mobile' style='margin-top:-1rem!important;background-image: url(".$this->base_url."dashboard/assets/img/icon-catalogo.png);'></div>";
        }

        $this->navbar($page);




        // si tiene layout
        if (isset($page['layout']) && $page['layout']!='') {

          if ( method_exists('layouts', $page['layout']['name']) ) {

            // manipulo la data de la página
            $page = Modules::run( 'layouts/'.$page['layout']['name'], $page );

          }

          $this->blocks($page, true);

          // renderizo el layout

          if (!isset($page['layout']['name'])){

            $this->parser->parse('layout/'.$page['layout'], $page);

          }else{
          $this->parser->parse('layout/'.$page['layout']['name'], $page);

          }

        } else {
               if ($page["post_type"] == "items_catalogo"){

                  $this->accion_estatal($page);

               }
          $this->blocks($page);

        }

			} else {
        $page['base_url'] = $this->base_url;
        $page['module_url'] = $this->module_url;
        $page['isloggedin'] = $this->isloggedin();
        $page['name'] = $this->user->name;
        $page['lastname'] = $this->user->lastname;
        $page['email'] = $this->user->email;

        $segments_array = $this->uri->segment_array();
        if ($segments_array[2]=='categoria'||$segments_array[2]=='organismo'||$segments_array[2]=='destinatario'||$segments_array[2]=='sector') {
          $page['logo_interno_computer']="<div class='navbar-brand-complement computer' style='background-image: url(".$this->base_url."dashboard/assets/img/icon-catalogo.png);'></div>";

          $page['logo_interno_mobile']="<div class='navbar-brand-complement mobile' style='margin-top:-1rem!important;background-image: url(".$this->base_url."dashboard/assets/img/icon-catalogo.png);'></div>";
          $page['title'] = 'Búsqueda por '.$segments_array[2];
            $this->header($page);
            $this->navbar($page);
            $page['search'] = $segments_array[3];
            $page['item'] = $segments_array[2];
            $this->accion_estatal_busqueda($page);
        }else if(!isset($segments_array[3])){
          $page['logo_interno_computer']="<div class='navbar-brand-complement computer' style='background-image: url(".$this->base_url."dashboard/assets/img/icon-catalogo.png);'></div>";

          $page['logo_interno_mobile']="<div class='navbar-brand-complement mobile' style='margin-top:-1rem!important;background-image: url(".$this->base_url."dashboard/assets/img/icon-catalogo.png);'></div>";
          $page['title'] = 'Búsqueda';
            $this->header($page);
            $this->navbar($page);
            $page['search'] = $segments_array[2];
              $page['item'] = 'busqueda';
            $this->accion_estatal_busqueda($page);
        }else{
                   // muestro 404
            set_status_header(404);

            $page['title'] = 'Página no encontrada';

            $this->header($page);
            $this->navbar($page);

            $this->parser->parse('layout/404', $page); 
        }
    


			}
        $this->footer($page);
        $this->scripts($page);
    }


    private function header($vars)
    {

      /*Los custom css para la web podés agregar acá, como acá no tenemos el json*/
        $vars['custom_css']='
        <link rel="stylesheet" type="text/css" href="'.$this->base_url.'website/assets/css/custom.css">
        ';
        return $this->parser->parse('dashboard/_head_jquery_bootstrap', $vars);
    }


    private function footer($vars)
    {

      return $this->parser->parse('dashboard/_footer_jquery_bootstrap', $vars, false, false);
    }



    private function scripts($vars)
    {
        /*aca los scripts*/
        $vars['js']='
         <script src="'.$this->base_url.'website/assets/js/custom.js"></script>
         
        ';
      return $this->parser->parse('dashboard/_scripts_jquery_bootstrap', $vars, false, false);
    }

    private function navbar($vars)
    {

         if ($this->isloggedin()) {
            $vars['user_email'] = $this->user->email;
            $current_user=(empty($userID))?((int)$this->idu):((int)$userID);

            if ( is_file(FCPATH."images/profiles/".$current_user."/foto_perfil.jpg")){
              $vars['user_image'] =  base_url()."images/profiles/".$current_user."/foto_perfil.jpg";
            }elseif(is_file(FCPATH."images/profiles/".$current_user."/foto_perfil.png")){
              $vars['user_image'] =  base_url()."images/profiles/".$current_user."/foto_perfil.png";
            }else{
              $vars['user_image'] = base_url()."images/avatar/profile.png";
            }

            return $this->parser->parse('dashboard/_navbar_jquery_bootstrap_web_loggedin', $vars);
        } else {
            return $this->parser->parse('dashboard/_navbar_jquery_bootstrap_web_loggedout', $vars);
        }
    }


    private function blocks(&$page, $echo = false){


      if (!empty($page['blocks'])) {
        $blocks = '';
        foreach ($page['blocks'] as $key => &$block) {


          $block['data']['block_index'] = $key;

          if ($block['enabled'] == true) {

            $block['data']['base_url'] = $this->base_url;
            $block['data']['module_url'] = $this->module_url;

            //hookeo los bloques con el controller blocks
            if ( method_exists('blocks', $block['type']) ) {

              if ($block['type']=='novedades') {
                $block['data']['category'] = $category;

              }
              // manipulo la data del bloque
              $block = Modules::run( 'blocks/'.$block['type'], $block );

            }

            if ($block['type'] == 'cards_items') {
              if (is_array($page['items_catalogo'])) {
                foreach ($page['items_catalogo'] as $key => $value) {
                    $args['slug']=$value['value'];
                   $args['post_status']='published';
                   $item= $this->Model_node->get($args,999);

                   if ($item) {
                     $value['text']=$item[0]['title_question'];
                     $block['data']['items_catalogo'][] =$value;
                   }
                }
              }

               
            }

            if ($block['type'] == 'cards_categorias') {

               $block['data']['items_categorias'] =$page['categorias'];
            }
            if ($block['type'] == 'cards_organismos') {

               $block['data']['items_organismos'] =$page['organismos'];
            }

            if ($block['type'] == 'cards_destinatarios') {

               $block['data']['items_destinatarios'] =$page['destinatarios'];
            }
            $block['data']['isloggedin'] = $this->isloggedin();
            if (file_exists(APPPATH.'modules/website/views/blocks/'.$block['type'].'.php')) {

              $blocks .= $this->parser->parse('blocks/'.$block['type'], $block['data'], $echo);
            }

          }

          }
        }

        $page['blocks'] = $blocks;

      return $page;
    }

    private function accion_estatal_busqueda($page)
    {
          $page['principal_visible']='none';
          $page['relacionados_visible']='none';
          $page['relacionados']=array();
          $args['post_status'] = 'published';
          $args['post_type'] = 'items_catalogo';
          $relacionados=array();
          $search_name='';
          $search=urldecode($page['search']);

            $this->add_busqueda($search,$page['item'],$search);
          if ($page['item'] =='organismo') {
                $container= 'container.mapa_del_estado_jurisdicciones';
                $result['data']= $this->options->get_options($container,$page['search']);

                if ( is_array($result['data'])&&!empty($result['data'])) {
                   $search_name=$result['data'][0]['name'];
                }
                $args['organismos']= array( '$elemMatch' => array('value' => $search));
                $relacionados= $this->Model_node->get($args,999);

          }elseif ($page['item'] =='categoria') {
                $container= 'container.categorias';
                $result['data']= $this->options->get_value($container,'value',$page['search']);

                if ( is_array($result['data'])&&!empty($result['data'])) {
                   $search_name=$result['data'][0]['text'];
                }
                $args['categorias']= array( '$elemMatch' => array('value' => $search));
                $relacionados= $this->Model_node->get($args,999);

          }elseif ($page['item'] =='destinatario') {
                $container= 'container.destinatarios';
                $result['data']= $this->options->get_value($container,'value',$page['search']);

                if ( is_array($result['data'])&&!empty($result['data'])) {
                   $search_name=$result['data'][0]['text'];
                }
                $args['destinatarios']= array( '$elemMatch' => array('value' => $search));
                $relacionados= $this->Model_node->get($args,999);

          }elseif ($page['item'] =='sector') {

            //Agrego sector
                $container= 'container.sectores';
                $result['data']= $this->options->get_value($container,'value',$page['search']);

                if ( is_array($result['data'])&&!empty($result['data'])) {
                   $search_name=$result['data'][0]['text'];
                }
                $args['sectores']= array( '$elemMatch' => array('value' => $search));
                $relacionados= $this->Model_node->get($args,999);

          }else{
              unset($resultado);
              $array_palabras=  array("ante","bajo","contra","desde","durante","entre","hacia","hasta","mediante","para"," pro ","segun","sin","sobre","tras"," via ","a traves","aqui","donde","abajo","arriba"," aca ","atras","encima","afuera","bajo","enfrente","ahi","cerca","entre","borde","delante","junto a","alla","dentro","lejos","alli","debajo","alrededor","detras","sobre","actualmente","enseguida","normalmente","ahora","entretanto","nunca","anoche","eternamente","ocasionalmente","anteriormente","finalmente","posteriormente","Antes","Frecuentemente","Primeramente","Antiguamente","Hoy","Pronto","Asiduamente","Inicialmente","Puntualmente","Inmediatamente","Recien","Ayer","Instantaneamente","Recientemente","Constantemente","Jamas","Siempre","Contemporaneamente","Luego","Simultaneamente","Mañana","Tarde","Desde","Mientras","Temprano","Despues","Momentaneamente","Adrede","Fuertemente","Públicamente","Amable","Fuerte","Puntillosamente","Apasionadamente","Gratuitamente","Rapidamente","Asi","Habilmente","rapido","Asiduamente","Igual","Regular","Bajo","Igualmente","Responsablemente","Bien","Inocentemente","Rutinariamente","Brillantemente","Intelectualmente","Salvajemente","Claro","Lentamente","Suavemente","Conforme","Lento","Subitamente","Debilmente","Ligero","Sutilmente","Desgraciadamente","Mal","Talentosamente","Elegantemente","Mejor","Tiernamente","Elocuentemente","Minuciosamente","Tiernamente","Espontaneamente","Nuevamente","Velozmente","Facilmente","Oportunamente","Voluntariamente","Formalmente","Prolijamente","Vulgarmente","Demasiado","Mitad","Solo","Suficientemente","Apenas","Mucho","Tab","Extremadamente","Bastante","Muy"," Tan ","Excesivamente","Casi","Nada","Tanto","Absolutamente","Justo","Poco","Todo","Aproximadamente","No","Nada","Ni","siquiera","Ningun","Nunca","Tampoco","Ninguno","Jamas","Nadie","Ninguna","Cuanto","como","Que","Quien","Quienes","Cuando","Cuantos"," Cuan "," al ","porque","Ya"," con ","por que"," en "," por "," de "," aun "," a ");
             
                $search=urldecode($page['search']);
                 $search=$this->options->slugify($search);
                $search_name= $search;
                foreach ($array_palabras as $key_palabra => $value_palabra) {
                 $value_palabra= $this->options->slugify($value_palabra);

                  $regex= $this->options->to_regex($value_palabra);

                    $search = preg_replace("/".$regex."/i", "", $search);
 
                }

                // $container= 'container.mapa_del_estado_jurisdicciones';
                // $result['data']= $this->options->get_value($container,'name',$search);
                // $args['organismos']= array( '$elemMatch' => array('text' => $search));
                // $resultado=$this->Model_node->get($args,999);
                // if ($resultado) {
                //    $relacionados= array_merge($relacionados,$resultado);
                // }
                // unset( $args['organismos']);


                // $container= 'container.sectores';
                // $result['data']= $this->options->get_value($container,'text',$search)[0];
                // if ( $result['data']) {
                //   $args['sectores']= array( '$elemMatch' => array('value' => $result['data']['value']));
                //   $resultado=$this->Model_node->get($args,999);
                //   if ($resultado) {
                //      $relacionados= array_merge($relacionados,$resultado);
                //   }
                //     unset( $args['sectores']);
                // }
    
              $search_item=$this->options->slugifyfull($search);
                $container= 'container.tag';
                $result['data']= $this->options->get_value($container,'value',$search_item)[0];
                if ( $result['data']) {

                  $args['tag']= array( '$elemMatch' => array('value' => $result['data']['value']));
                  $resultado=$this->Model_node->get($args,999);
                  if ($resultado) {
                     $relacionados= array_merge($relacionados,$resultado);
                  }
                }
                unset( $args['tag']);
          
                $container= 'container.categorias';

                $result['data']= $this->options->get_value($container,'value',$search_item,1)[0];

                if ( $result['data']) {
                  $args['categorias']= array( '$elemMatch' => array('value' => $result['data']['value']));
                  $resultado=$this->Model_node->get($args,999);
                     if ($resultado) {
                     $relacionados= array_merge($relacionados,$resultado);
                  }
                }
                unset( $args['categorias']);

                // $container= 'container.destinatarios';
                // $result['data']= $this->options->get_value($container,'text',$search);
                // $args['destinatarios']= array( '$elemMatch' => array('text' => $search));
                // $resultado=$this->Model_node->get($args,999);
                // if ($resultado) {
                //    $relacionados= array_merge($relacionados,$resultado);
                // }
               
                // unset( $args['destinatarios']);
      
                if ($search!='') {
                  $resultado=$this->options->search($args,$search,999);
           
                  if ($resultado) {
                     $relacionados= array_merge($relacionados,$resultado);
                  }
                }



          }


          $page['categorias']=array();
          $page['destinatarios']=array();
          $page['sectores']=array();
          $page['organismos']=array();
          $categorias=array();
          $destinatarios=array();
          $sectores=array();
          $organismos=array();
          
          $count=0;

          $slugs=array();

          if (is_array($relacionados)) {
            foreach ($relacionados as $key => $value) {


                if (!in_array($value['slug'],$slugs) ){  
                    $slugs[]=$value['slug'];

                    $value['normativa_list']=array();
                      if ( is_array($value['normativa'])) {
                         foreach ($value['normativa'] as $key_normativa => $value_normativa) {
                           if ($value_normativa['text']!=''&&$value_normativa['link']!='') {
                              $value['normativa_list'][]=$value_normativa;
                           }
                         }
                      } 
                  
                    $count++;

                    if (is_array($value['organismos'])) {
                      foreach ($value['organismos'] as $key_organismo => $value_organismo) {
                          if (!in_array($value_organismo['value'],$organismos )) {
                            $organismos[]=$value_organismo['value'];
                            $page['organismos'][]=array('value'=>$value_organismo['value'],'text'=>$value_organismo['text']);
                          }
                      }

                    }

                    if (is_array($value['sectores'])) {
                      foreach ($value['sectores'] as $key_sector => $value_sector) {
                          if (!in_array($value_sector['value'],$sectores )) {
                            $sectores[]=$value_sector['value'];
                            $page['sectores'][]=array('value'=>$value_sector['value'],'text'=>$value_sector['text']);
                          }
                      }


                    }

                    if (is_array($value['categorias'])) {

                      foreach ($value['categorias'] as $key_categoria => $value_categoria) {
                          if (!in_array($value_categoria['value'],$categorias )) {
                            $categorias[]=$value_categoria['value'];
                            $page['categorias'][]=array('value'=>$value_categoria['value'],'text'=>$value_categoria['text']);
                          }

                      }

                    }

                    if (is_array($value['destinatarios'])) {
                      foreach ($value['destinatarios'] as $key_destinatario=> $value_destinatario) {
                          if (!in_array($value_destinatario['value'],$destinatarios )) {
                            $destinatarios[]=$value_destinatario['value'];
                            $page['destinatarios'][]=array('value'=>$value_destinatario['value'],'text'=>$value_destinatario['text']);
                          }
                      }


                    }
                    $page['relacionados'][]=$value;

                }  
             
            }

            $page['sectores']=$this->sort_array($page['sectores'],'text');
            $page['destinatarios']=$this->sort_array($page['destinatarios'],'text');
            $page['categorias']=$this->sort_array($page['categorias'],'text');
            $page['organismos']=$this->sort_array($page['organismos'],'text');
          }

          $page['title_question'] = $search_name;

          if ($count>0) {
             $page['count'] = 'Se encontraron '.$count.' resultados';
             $page['no_result']= '';
          }else{
              $page['relacionados']=array();
            $page['count'] = '';
            $page['no_result']= '<div class="no-results">
                No encontramos resultados para "'.$search_name.'"
                <br />Por favor verificá que las palabras estén bien escritas.
              </div>';

          }
         
      $this->parser->parse('blocks/accion-estatal',  $page);

    }


    private function sort_array($array,$element){
            if (is_array($array)) {
                foreach ($array as $key=> $row) {
                  $aux[$key] = $row[$element];
                }
                array_multisort($aux, SORT_ASC, $array);
            }   
      return $array;
    }

    private function accion_estatal($page)
    {
          $this->add_busqueda($page['title_question'],'accion_estatal',$page['slug']);
        $page['principal_visible']='';
         $page['relacionados']=array();
          $args['post_status'] = 'published';
          $args['post_type'] = 'items_catalogo';
         $page['normativa_list']=array();
          if ( is_array($page['normativa'])) {
             foreach ($page['normativa'] as $key_normativa => $value_normativa) {
               if ($value_normativa['text']!=''&&$value_normativa['link']!='') {
                  $page['normativa_list'][]=$value_normativa;
               }
             }
          }



          $args['slug']=array('$ne' => $page['slug']);

                // $organismo=$page['organimos'][0]['value'];
                // if ( $organismo) {
                //   $container= 'container.mapa_del_estado_jurisdicciones';
                //   $result['data']= $this->options->get_options($container,$organismo);
                //   if ( is_array($result['data'])) {
                //      $search_name=$result['data'][0]['name'];
                //   }
                //   $page['relacionados']= $this->options->search_by_organismo($args,$organismo,20);
                // }


                $search=$page['categorias'][0]['value'];
                 if ( $search) {
                  $args['categorias']= array( '$elemMatch' => array('value' => $search));
                  $resultado= $this->Model_node->get($args,999);
                  $page['relacionados'] = array_merge( $page['relacionados'], $resultado);
                }

                // $destinatario=$page['destinatarios'][0]['value'];
                // if ($destinatario) {
                //     $container= 'container.destinatarios';
                //     $result['data']= $this->options->get_value($container,'value',$destinatario);
                //     if ( is_array($result['data'])&&!empty($result['data'])) {
                //        $search_name=$result['data'][0]['text'];

                //      $resultado= $this->options->search($args,$search_name,$search_name,20);
                //       $page['relacionados'] = array_merge( $page['relacionados'], $resultado);
                //     }

                // }

                // $sector=$page['sector'][0]['value'];
                // if ( $sector) {
                //     $container= 'container.sectores';
                //     $result['data']= $this->options->get_value($container,'value',$sector);
                //     if ( is_array($result['data'])&&!empty($result['data'])) {
                //        $search_name=$result['data'][0]['text'];

                //      $resultado= $this->options->search($args,$search_name,$search_name,20);
                //       $page['relacionados'] = array_merge( $page['relacionados'], $resultado);
                //     }

                // }
               $search=$page['tag'][0]['value'];
                 if ( $search) {
                  $args['tag']= array( '$elemMatch' => array('value' => $search));
                  $resultado= $this->Model_node->get($args,999);
                  $page['relacionados'] = array_merge( $page['relacionados'], $resultado);
                }


                if (empty($page['relacionados'])) {
                  $page['relacionados_visible']='none';
                  $page['relacionados']=array();
                }else{
                  if (is_array($page['relacionados'])) {
                    
                    foreach ($page['relacionados']as $key_relacionado => &$value_relacionado) {
                       $value_relacionado['normativa_list']=array();
                       if ( is_array($value_relacionado['normativa'])) {
                         foreach ($value_relacionado['normativa'] as $key_normativa => $value_normativa) {
                           if ($value_normativa['text']!=''&&$value_normativa['link']!='') {
                              $value_relacionado['normativa_list'][]=$value_normativa;
                           }
                         }
                      }
                    }

                  }
                }
                $page['count'] ='';
 
                $page['no_result']= '';
            $this->parser->parse('blocks/accion-estatal', $page);

    }

    private function isloggedin()
    {
        if (!$this->session->userdata('loggedin')) {
            $this->session->userdata('loggedin',false);
            return false;
        } else {
            return true;
        }
    }

    public function error_404()
    {
      $vars['base_url'] = $this->base_url;
      $vars['module_url'] = $this->module_url;
      $vars['menu'] = $this->menuPrincipal(($vars['color_logo'])? $vars['color_logo'] : 'blanco');
      $vars['title'] = $page['title'];
      //$vars['newsletter'] = $this->load->view('layout/newsletter', NULL, TRUE);
      $this->load->view('layout/header', $vars);
      $this->load->view('layout/404', $vars);
      $this->load->view('layout/footer', $vars);
    }

    private function add_busqueda($value,$item,$slug){
        $item_busqueda=array();
                      $item_busqueda['value']=$value;
                    $item_busqueda['from']=$item;
                     $item_busqueda['slug']=$slug;
                    $item_busqueda['date']=time();
                    $this->options->update_search($item_busqueda);
    }

    private function palabras_ignorar(){
      $array=array();
      $archivo = fopen($this->base_url."website/assets/palabras_ignorar.txt","rb");


      while(!feof($archivo))
      {
        array_push($array, fgets($archivo));
      }
      fclose($archivo);
      return $array;
    }

    public function add_busqueda_externa(){
       $data=$this->input->post();
       $this->add_busqueda($data['value'],$data['item'],$data['slug']);
    }
}
