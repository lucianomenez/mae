
<section id="organismos">
  <div class="container">
    <div class="title mb-4">
      {title}
    </div>
    <div class="row">
       {items_organismos}

      <div class="col-xl-6 col-sm-12 mb-3">
        <div class="card pointer">
          <div class="card-body flx-cntr align-items-center">
            <div class="row align-items-center" style="width:100%!important">
              <div class="col-8 card-title mb-0 text-left">
                {text}
              </div>
              <div class="col-4 text-right">
                <a href="{base_url}catalogo-covid19/organismo/{value}" class="btn card-btn">Consultar</a>
              </div>
            </div>
          </div>
        </div>
      </div>
       {/items_organismos}
      </div>
  </div>
</section>
