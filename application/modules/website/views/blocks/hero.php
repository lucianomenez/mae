<div style="position:relative;">

    <header class="hero flx-cntr inst-gradient">
      <div class="container text-left">
          <!-- <div class="title">
            {header_title}
          </div>
          <div class="subtitle">
            {header_subtitle}
          </div> -->
      </div>
    </header>

    <section id="search" class="text-center mb-4">
      <div class="container">
        <div class="title mb-4">
          ¿En qué podemos ayudarte?
        </div>
        <div class="mt-4" style="position:relative;">
            <input class="full-shadow search-input form-control" id="input_search" type="text" placeholder="Buscar consulta, categoría o destinatario">
            <span class="search-icon input-group-append">
                <button class="btn" type="button"><i class="fas fa-search fa-lg"></i></button>
            </span>
        </div>
    <!-- <input class="full-shadow search-input form-control" id="input_search" type="text" placeholder="Buscar consulta, categoría o destinatario"> -->
      </div>
    </section>
</div>
<section id="mensaje_busqueda">
  <div class="container">
  </div>
</section>