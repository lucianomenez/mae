<?php

include('hero.php')

?>

<section id="referencias" style="">
  <div class="container">
    <div class="row justify-content-between">
      <div class="col-xl-10 col-sm-12">
        <a class="btn-back mr-2" href="{base_url}catalogo-covid19"><i class="fas fa-arrow-left"></i> Volver</a>
        |
        <span class="subtitle">
        Usted consultó:
        </span>
      </div>
      <div class="col-xl-2 col-sm-12">
          <ol class="breadcrumb justify-content-right">
            <li class="breadcrumb-item"><a href="{base_url}catalogo-covid19">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Consulta</li>
        </ol>
      </div>
    </div>
  </div>
  <div class="container mt-1">
    <div class="title">
      {title_question}
    </div>
  </div>
</section>

<section id="resultados">
<div class="container">
  <!-- NO HAY RESULTADOS DE LA BÚSQUEDA -->

  {no_result}

  <div class="row">
    <div class="col-xl-3 col-sm-12" id="etiquetas">
      <div class="container full-shadow p-4">
        <div class="mb-4">
          <div class="label">
            Organismo
          </div>
            {organismos}
                <a class="result" href="{base_url}catalogo-covid19/organismo/{value}">
                  {text}
                </a>
            {/organismos}
        </div>
        <div class="mb-4">
          <div class="label">
            Categoría
          </div>
          {categorias}
          <a class="result" href="{base_url}catalogo-covid19/categoria/{value}">
            {text}
          </a>
          {/categorias}
        </div>
        <div class="mb-4">
          <div class="label">
            Destinatarios
          </div>
            {destinatarios}
          <a class="result" href="{base_url}catalogo-covid19/destinatario/{value}">
            {text}
          </a>
          {/destinatarios}
        </div>
        <div class="">
          <div class="label">
            Sector
          </div>
            {sectores}
          <a class="result" href="{base_url}catalogo-covid19/sector/{value}">
            {text}
          </a>
          {/sectores}
        </div>
      </div>
    </div>
    <div class="col-xl-9 col-sm-12" id="listado">
      <div class="results text-right">
        {count}
      </div>
      <div class="full-shadow card" style="display:{principal_visible}; ">
        <div class="card-body">
          <div class="row justify-content-between">
            <div class="col-xl-8 col-sm-12 card-title">
                {title_question}
            </div>
            <div class="col-xl-4 col-sm-12 col-reso">
              {normativa_list}
                <a href="{link}" class="card-resolucion" target="_blank" style="display: block;">{text}</a>
              {/normativa_list}
            </div>
          </div>
          <div class="card-txt">
           {content}
          </div>
        </div>
        <div class="card-footer">
          <a href="{enlace}" target="_blank" class="item-enlace" data-slug="{slug}" data-title="{title_question}"><button class="btn card-btn">Visitar página</button></a>
        </div>
      </div>
      <div id="divisor" style="display:{relacionados_visible}; " >
        <div class="title">
          Resultados relacionados
        </div>
        <hr class="mt-0"/>
      </div>
      <div id="relacionados">
        {relacionados}
            <div class="full-shadow card mb-3">
              <div class="card-body">
                <div class="row justify-content-between">
                  <div class="col-xl-8 col-sm-12 card-title">
                    {title_question}
                  </div>
               <div class="col-xl-4 col-sm-12 col-reso">
                {normativa_list}
                  <a href="{link}" class="card-resolucion" target="_blank" style="display: block;">{text}</a>
                {/normativa_list}
              </div>
                </div>
                <div class="card-txt">
                    {content}
                </div>
              </div>
              <div class="card-footer">
                <a href="{enlace}" target="_blank" class="item-enlace" data-slug="{slug}" data-title="{title_question}"><button class="btn card-btn">Visitar página</button></a>
              </div>
            </div>
         {/relacionados}
      </div>
    </div>
  </div>
</div>
</section>
