<div style="position:relative;">

<div class="inst-gradient background-header">
<header class="flx-cntr header-img" style="background-image: url('{base_url}website/assets/images/recurso-arg-20.png')">
  <div class="container text-left">
      <div class="title">
        {title}
      </div>
      <div class="subtitle1">
        {subtitle1}
      </div>
      <div class="subtitle2">
        {subtitle2}
      </div>
  </div>
</header>
</div>

<section id="search" class="text-center mb-5">
  <div class="container">
    <div class="title mb-4">
      {question}
    </div>
    <div class="mt-4" style="position:relative;">
        <input class="full-shadow search-input form-control" id="input_search" type="text" placeholder="Buscar consulta, categoría o destinatario">
        <span class="search-icon input-group-append">
            <button class="btn" type="button"><i class="fas fa-search fa-lg"></i></button>
        </span>
    </div>


    <!-- <input class="full-shadow search-input form-control" id="input_search" type="text" placeholder="Buscar consulta, categoría o destinatario"> -->
  </div>
</section>
</div>

<section id="mensaje_busqueda">
  <div class="container">
  </div>
</section>