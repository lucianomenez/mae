

<section id="consultas">
  <div class="container">
    <div class="title mb-4">
     {title}
    </div>
    <div class="row">
      {items_catalogo}
        <div class="col-xl-4 col-sm-12 mb-3">
          <div class="full-shadow card pointer">
            <div class="card-body text-center">
              <div class="card-title">
                {text}
              </div>
            </div>
            <div class="card-footer text-center">
              <a href="{value}" class="btn card-btn">Consultar</a>
            </div>
          </div>
        </div>
      {/items_catalogo}

    </div>

  </div>
</section>
