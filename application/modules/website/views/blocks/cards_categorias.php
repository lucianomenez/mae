
<section id="categorias">
  <div class="container">
    <div class="title mb-4">
      {title}
    </div>
    <div class="row">
      {items_categorias}
          <div class="col-xl-3 col-sm-6 mb-3 ">
            <div class="full-shadow card pointer" >
              <div class="card-body text-center" style=" display: flex;   justify-content: center;">
                <div class="card-title" style="  align-self: center;">
                  {text}
                </div>
              </div>
              <div class="card-footer text-center">
                  <a href="{base_url}catalogo-covid19/categoria/{value}" class="btn card-btn">Consultar</a>
              </div>
            </div>
          </div>
      {/items_categorias}
      </div>
  </div>
</section>
