<section class="page__title" style="margin-top: 6rem;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Página no encontrada</h1>
   
        <p><a href="{base_url}">Ir al inicio</a></p>
      </div>
    </div>
  </div>
</section>
