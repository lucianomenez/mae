<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['accion-estatal'] = 'website/index';
$route['accion-estatal/(:any)'] = 'website/index';

$route['catalogo'] = 'website/index';
$route['catalogo/(:any)'] = 'website/index';
$route['catalogo/(:any)/(:any)'] = 'website/index';
$route['catalogo-covid19'] = 'website/index';
$route['catalogo-covid19/(:any)'] = 'website/index';
$route['catalogo-covid19/(:any)/(:any)'] = 'website/index';
