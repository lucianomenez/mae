<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Post
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 * @date    May 7, 2020
 */


include 'Node.php';

class Carga extends Node
{
    protected $node_type = 'carga';
    protected $card = false;

    function process_file(){
      $this->load->model('options');
      $this->load->library('cimongo/Cimongo', '', 'db');
      $this->config->load('cimongo');

        $tmpName = $_FILES['file']['tmp_name'];
        $data = array_map("str_getcsv", file($tmpName));
        unset($data[0]);
        $cant=0;
        $array_completo=array();
        $not_organismos=array();
        foreach ($data as $item){            
            $reg['tipo_file'] = 'items_catalogo';
            $reg['post_type'] = 'items_catalogo';
             $reg['post_status'] = 'published';

            $reg['title'] = $item[0];
            $reg['title_question'] = $item[0];


            $reg['content'] = $item[1];
            $reg['enlace'] = $item[2];

            $organismos= explode("/", $item[3]);
            $array_organismos=array();
            if (is_array($organismos)) {
              foreach ($organismos as $key_organismo => $value_organismo) {
                if ($value_organismo!='') {
  
                   $organismo_value= $this->options->get_value('container.mapa_del_estado_jurisdicciones','name',$value_organismo)[0];
                   if (!$organismo_value) {
                        if (!in_array( $value_organismo,$not_organismos) ){
                          $not_organismos[]=$value_organismo;
                        }
                      
                    }else{
 
                      $array_organismos[]=array('value'=>$organismo_value['id'],'text'=>$organismo_value['name']);
                    }
      
                }
              }
              $reg['organismos']=  $array_organismos;
            }

            unset($array_completo);
            $destinatarios= explode("/", $item[4]);
            if (is_array($destinatarios)) {
              foreach ($destinatarios as $key_destinatario => $value_destinatario) {
                if ($value_destinatario!='') {
                  $valueSlug=$this->slugify($value_destinatario);
                  $array_completo[]=array("value"=>$valueSlug,"text"=>$value_destinatario);
                }
                
              }
                $container='container.destinatarios';
                $this->options->update_option($container,$array_completo);
                   unset($array_completo['_id']);
                $reg['destinatarios']=$array_completo;
            }

            unset($array_completo);
            $sectores= explode("/", $item[5]);
            if (is_array($sectores)) {
              foreach ($sectores as $key_sector => $value_sector) {
                if ($value_sector!='') {
                  $valueSlug=$this->slugify($value_sector);
                  $array_completo[]=array("value"=>$valueSlug,"text"=>$value_sector);
                }
                
              }
                $container='container.sectores';
                $this->options->update_option($container,$array_completo);
                   unset($array_completo['_id']);
                $reg['sectores']=$array_completo;
            }
            unset($array_completo);

            $categorias= explode("/", $item[6]);
            if (is_array($categorias)) {
                foreach ($categorias as $key_categoria => $value_categoria) {
                  if ($value_categoria!='') {
                    $valueSlug=$this->slugify($value_categoria);
                    $array_completo[]=array("value"=>$valueSlug,"text"=>$value_categoria);
                  }
                  
                }
                $container='container.categorias';
                $this->options->update_option($container,$array_completo);
                   unset($array_completo['_id']);
                $reg['categorias']=$array_completo;
            }
            unset($array_completo);

            $valueTitle=$this->slugify($reg['title']);
            $valueCategoria=$this->slugify($reg['categorias'][0]['value']);
            $reg['slug'] = 'catalogo-covid19/'.$valueCategoria.'/'.$valueTitle;

            
            $etiquetas= explode("/", $item[7]);
            if (is_array($etiquetas)) {
                foreach ($etiquetas as $key_etiqueta=> $value_etiqueta) {
                  if ($value_etiqueta!='') {
                    $valueSlug=$this->slugify($value_etiqueta);
                    $array_completo[]=array("value"=>$valueSlug,"text"=>$value_etiqueta);
                  }
                  
                }
                $container='container.tag';
                $this->options->update_option($container,$array_completo);
                unset($array_completo['_id']);
                $reg['tag']=$array_completo;
            }
            unset($array_completo);    
            $container = 'container.pages'; 
            $this->db->switch_db($this->config->item('cmsdb'));
            $query = array('_id' => new MongoId($data['_id']));
            $this->db->where($query);
            $this->db->update($container, $reg, array('upsert'=> true));
            unset($reg);
            $this->db->switch_db($this->config->item('db'));
            $cant++;
        }

        $response=$cant ." Registros insertados correctamente <br />  No se encontraron los siguientes organismos: <br>";

        foreach ($not_organismos as $key_not_organismos => $value_not_organismos) {
          $response.='<br>'.$value_not_organismos;
        }
        echo  $response;

    } 

    function delete_documents(){
      $this->load->library('cimongo/Cimongo', '', 'db');
      $this->config->load('cimongo');
      $query=array();

      $this->db->where($query)->delete('container.categorias');
      $this->db->where($query)->delete('container.tag');
      $this->db->where($query)->delete('container.destinatarios');

      $query = array('post_type' => 'items_catalogo');
      $this->db->switch_db($this->config->item('cmsdb'));
      if ($this->db->where($query)->delete('container.pages')) {
        return true;
      } else {
        return false;
      }  
      $this->db->switch_db($this->config->item('db'));
    } 


    public function slugify($text)
      {

         $table = array(
            "à"=>'a',"ä"=>'a',"á"=>'a',"â"=>'a',"æ"=>'a',"å"=>'a',"ë"=>'e',"è"=>'e',"é"=>'e', "ê"=>'e',"î"=>'i',"ï"=>'i',"ì"=>'i',"í"=>'i',"ò"=>'o',"ó"=>'o',"ö"=>'o',"ô"=>'o',"ø"=>'o',"ù"=>'o',"ú"=>'u',"ü"=>'u',"û"=>'u',"ñ"=>'n',"ç"=>'c',"ß"=>'s',"ÿ"=>'y',"œ"=>'o',"ŕ"=>'r',"ś"=>'s',"ń"=>'n',"ṕ"=>'p',"ẃ"=>'w',"ǵ"=>'g',"ǹ"=>'n',"ḿ"=>'m',"ǘ"=>'u',"ẍ"=>'x',"ź"=>'z',"ḧ"=>'h',"·"=>'-',"/"=>'-',"_"=>'-',","=>'-',"=>"=>'-',";"=>'-'," "=>'-',"&"=>'-and-',"."=>'',"("=>'',")"=>''
          ); 

        // lowercase
        $text = strtolower($text);
       // Trim - from start and end of text
        $text = trim($text);
        // Replace special characters using the hash map
        $text =strtr($text, $table);

        if (empty($text)) {
          return 'n-a';
        }

        return $text;
      } 

}