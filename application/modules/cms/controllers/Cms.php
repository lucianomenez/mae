
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Website
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 * @date    May 7, 2020
 */
class Cms extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('cms/config');

        $this->load->library('parser');
        $this->load->library('dashboard/ui');

        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');
        $this->load->model('options');
        $this->load->model('cms/Model_node');
        $this->load->helper('form');
        $this->load->helper('cms/dynamic_builder');
        $this->load->helper('cms/block');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->usuario = $this->user->get_user($this->user->idu);
        $this->cms_admin = (in_array(1,$this->usuario->group)||in_array(1500,$this->usuario->group)) ? true:false;
        $this->cms_supervisor = (in_array(1,$this->usuario->group)||in_array(1501,$this->usuario->group)) ? true:false;
        $this->cms_editor = (in_array(1,$this->usuario->group)||in_array(1502,$this->usuario->group)) ? true:false;
    }

    function index(){

        header("Location: ".   $this->base_url ."cms/items_catalogos");
        exit();

        $cms = ($this->session->userdata('json')) ? $this->session->userdata('json'):null;

        if($cms<>''){
            $this->CMS($cms);
        } else {
            $this->CMS($this->config->item('default_cms'));
        }
    }

    function Show($file, $debug = false) {
        //---only admins can debug
        $debug = ($this->user->isAdmin()) ? $debug : false;
        if (!is_file(APPPATH . "modules/cms/views/json/$file.json")) {
            // Whoops, we don't have a page for that!
            return null;
        } else {
            $myconfig = json_decode($this->load->view("cms/json/$file.json", '', true), true);
            if (isset($myconfig['private']) && $myconfig['private'] == true) {
                return;
            }
            $this->CMS("cms/json/$file.json", $debug);
        }
    }

    function get_listados(){
        $data=$this->input->post();

          if ($data['item']=='items_catalogo') {
              $result_temp= $this->options->get_items_catalogo();
              if (is_array( $result_temp)) {
                    $result['data']=array();
                  foreach ($result_temp as $key => $value) {
                        $mongoId=$value['_id']->{'$id'};
                        $slug=$value['slug'];
                     $result['data'][]=array ('id'=> $slug,'name'=> $value['title']);
                  }
              }
          }else{
                $container= 'container.'.$data['item'];

                  $result['data']= $this->options->get_options($container);


          }

          if (!$result['data']) {
             $result='null';
          } 
            echo json_encode($result);

    }

    function get_options_json($id){

          $container= 'container.'.$id;

          $result= $this->options->get_options($container);

          if (!$result) {
            $result='null';
          }
 
            echo json_encode($result);

    }

    function get_data(){
        $this->config->load('cimongo');
        $data=$this->input->post();
        //$args['post_status'] = 'published';
        $args['post_type'] =$data['post_type'];
        $args['_id']=new MongoId($data['_id']);
        $result['data']= $this->Model_node->get($args, 1)[0][$data['item']];
        if (!$result['data']) {
            $result='null';
        }
        echo json_encode($result);

    }



    function CMS($json = 'cms/json/cms.json',$debug = false,$extraData=null) {
        /* eval Group hooks first */
        $this->session->set_userdata('json', $json);
        $user = $this->user->get_user((int) $this->idu);
        $myconfig = $this->parse_config($json, $debug);

        $layout = ($myconfig['view'] <> '') ? $myconfig['view'] : 'layout';
        $customData = $myconfig;
        $customData['lang'] = $this->lang->language;
        $customData['alerts']=Modules::run('dashboard/alerts/get_my_alerts');
        $customData['brand'] = $this->config->item('brand');
        $customData['menu'] = $this->menu();
        $customData['avatar'] = Modules::run('user/profile/get_avatar'); //Avatar URL
        $customData['base_url'] = $this->base_url;

        $customData['module_url'] = $this->module_url;
        $customData['inbox_count'] = $this->msg->count_msgs($this->idu, 'inbox');
        $customData['config_panel'] =$this->parser->parse('_config_panel',  $customData['lang'], true, true);

        $customData['name'] = $user->name . ' ' . $user->lastname;
        $customData['email'] = $user->email;

        // Global JS
        $customData['global_js'] = array(
            'base_url' => $this->base_url,
            'module_url' => $this->module_url,
            'myidu' => $this->idu,
            'lang'=>$this->config->item('language')
        );


        // Toolbar
        $customData['toolbar_inbox'] = Modules::run('inbox/inbox/toolbar');

        $customData['new_toolbar_inbox'] = Modules::run('inbox/inbox/new_toolbar');

        if($extraData){
            $customData=$extraData+$customData;
        }


        $this->ui->compose($layout, $customData);
    }

    function loadDashboardVars($json = 'cms/json/cms.json',$debug = false,$extraData=null) {

        /* eval Group hooks first */
        $this->session->set_userdata('json', $json);
        $user = $this->user->get_user((int) $this->idu);
        $myconfig = $this->parse_config($json, $debug);

        $layout = ($myconfig['view'] <> '') ? $myconfig['view'] : 'layout';

        $customData = $myconfig;
        //$customData['lang'] = $this->lang->language;
        //$customData['alerts']=Modules::run('dashboard/alerts/get_my_alerts');
        $customData['brand'] = $this->config->item('brand');
        $customData['menu'] = $this->menu();
        $customData['avatar'] = Modules::run('user/profile/get_avatar'); //Avatar URL
        $customData['base_url'] = $this->base_url;
        $customData['module_url'] = $this->module_url;
        //$customData['inbox_count'] = $this->msg->count_msgs($this->idu, 'inbox');
        //$customData['config_panel'] =$this->parser->parse('_config_panel',  $customData['lang'], true, true);

        $customData['name'] = $user->name . ' ' . $user->lastname;
        $customData['email'] = $user->email;

        // Global JS
        $customData['global_js'] = array(
            'base_url' => $this->base_url,
            'module_url' => $this->module_url,
            'myidu' => $this->idu,
            'lang'=>$this->config->item('language')
        );


        // Toolbar
        //$customData['toolbar_inbox'] = Modules::run('inbox/inbox/toolbar');

        $customData['new_toolbar_inbox'] = Modules::run('inbox/inbox/new_toolbar');

        if($extraData){
            $customData=$extraData+$customData;
        }
        if($extraData){
            $customData=$extraData+$customData;
        }

      $customData['cms_admin'] = ($this->cms_admin) ? "true":"false";
      $customData['cms_supervisor']= ( $this->cms_supervisor ) ? "true":"false";
      $customData['cms_editor'] = ($this->cms_editor ) ? "true":"false";

        return $this->ui->compose_lte3($layout, $customData, true);
    } 



    /**
     * Realiza los recortes (presets) definidos en cms/config.php sobre la imagen subida en $_FILES['file']
     * Una vez realizados aplicados los recortes, los sube a s3
     */
    function upload_file(){

        $this->load->library('Aws3');
        $this->load->library('image_lib');
        $presets = $this->config->item('presets');
        $imagenOrignal = $_FILES['file']['tmp_name'];

        $file = $_FILES['file'];
        $file['path'] = date('Y') . '/' . date('m') . '/' . date('d') . '/';
        $s3_upload = $this->aws3->uploadFile('cms', $file);
        $s3_upload = str_replace("https//", "https://", $s3_upload );
        $response = array('success' => true, 'url' => $s3_upload);
        echo json_encode($response);
        exit;

        if(strpos($s3_upload,'Error') === false){
            $s3_upload = preg_replace('/[ \t]+/', '', preg_replace('/[\r\n]+/', "", $s3_upload));
            $response = array('success' => true, 'url' => $s3_upload);
        } else {
            $response = array('error' => true, 'message' => 'No se pudo subir su archivo a S3');
            return $response;
        }

        if (strstr($_FILES['file']['type'], 'image/')  && !strstr($_FILES['file']['type'], 'svg')) {
            // Iteramos sobre la lista de presets, los cuales estan definidos en cms/config.php
            foreach($presets as $preset) {
                $config = [];
                $config['image_library'] = $this->config->item('image_library');
                $config['library_path'] = $this->config->item('library_path');
                $config['source_image']	= $imagenOrignal;

                // Definimos una ruta temporal para cada preset
                $upload_dir = FCPATH . '/images/' . date("Y"). '/' . date("m"). '/' . $preset['name'];
                $config['new_image'] = $upload_dir . '/'. $_FILES['file']['name'];
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir, 0777, true);
                }

                // No realizamos compresion
                $config['quality'] = '100%';

                // Se realiza un corte o un resize segun se defina en el preset
                if ($preset['operation'] == 'crop') {
                    $config['maintain_ratio'] = FALSE;
                    $config['width']	= $preset['dimensions']['width'];
                    $config['height']	= $preset['dimensions']['height'];
                    $this->image_lib->initialize($config);
                    if(!$this->image_lib->crop()){
                        echo $this->image_lib->display_errors();
                    }
                } elseif ($preset['operation'] == 'resize') {
                    list($width, $height, $type, $attr) = getimagesize($config['source_image']);
                    $isOriginalImageWiderThanCurrentPreset = $width >= $preset['dimensions']['max_width'];
                    $isOriginalImageHigerThanCurrentPreset = $height >= $preset['dimensions']['max_height'];
                    $config['maintain_ratio'] = TRUE;
                    $config['width']	= ($isOriginalImageWiderThanCurrentPreset) ? $preset['dimensions']['max_width'] : $width;
                    $config['height']	= ($isOriginalImageHigerThanCurrentPreset) ? $preset['dimensions']['max_height']: $height;
                    $this->image_lib->initialize($config);
                    if(!$this->image_lib->resize()){
                        echo $this->image_lib->display_errors();
                    }

                } elseif ($preset['operation'] == 'resize-and-crop') {

                    // La 3ra operacion (resize-and-crop) primero hace un resize y luego cropea esa misma imagen

                    $config['maintain_ratio'] = TRUE;

                    list($width, $height, $type, $attr) = getimagesize($config['source_image']);
                    if($width > $height){
                        $config['height'] = $preset['dimensions']['height'];
                        $config['width'] = 1;
                        $config['master_dim'] = 'height';
                    }else{
                        $config['width'] = $preset['dimensions']['width'];
                        $config['height'] = 1;
                        $config['master_dim'] = 'width';

                    }

                    // Primero realizamos un resize, (en el caso de thumbnail se dejando el lado mas corto en 150px)
                    $this->image_lib->initialize($config);
                    if(!$this->image_lib->resize()){
                        echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();

                    // Luego realizamos el crop
                    $config['width']	= $preset['dimensions']['width'];
                    $config['height']	= $preset['dimensions']['height'];
                    $config['source_image'] = $config['new_image'];
                    $config['maintain_ratio'] = false;
                    $config['overwrite'] = TRUE;
                    $this->image_lib->initialize($config);
                    if(!$this->image_lib->crop()){
                        echo $this->image_lib->display_errors();
                    }

                }

                // Luego de la operacion reseteamos la configuracion
                $this->image_lib->clear();

                // Actualizamos la ruta de la imagen (para que no suba la original) y subimos a s3
                $_FILES['file']['tmp_name'] = $config['new_image'];
                $subfolder = date('Y') . '/' . date('m') . '/' . $preset['name'];
                $s3_upload = $this->aws3->sendFile('cms', $_FILES['file'], $subfolder);
                $s3_upload = str_replace("https//", "https://", $s3_upload );
                // Una vez subida la imagen al bucket, la borramos del disco
                // (excepto en ambientes locales windows, donde unlink da error)
                $isWindows = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
                if (!$isWindows) {
                    unlink($config['new_image']);
                }

                // Verificamos que se haya subido bien, sino retornamos error
                if(strpos($s3_upload,'Error') === false){
                    $s3_upload = preg_replace('/[ \t]+/', '', preg_replace('/[\r\n]+/', "", $s3_upload));
                    $response = array('success' => true, 'url' => $s3_upload);
                } else {
                    $response = array('error' => true, 'message' => 'No se pudo subir su archivo a S3');
                    break;
                }
            }
        }


        header('Content-Type: application/json');
        echo json_encode($response);

    }

    function load_block($block_name, $block_count = 0, $block_data = ''){
        $block_pos = $block_count;
        $block_response = block_build_form($block_name, $block_data, $block_pos);
        header('Content-Type: application/json');
        echo json_encode($block_response);
    }

    function menu() {
        $customData['base_url'] = $this->base_url;
        $customData['module_url'] = $this->module_url;
        $customData['lang'] = $this->lang->language;
        $customData['is_admin']=$this->user->isAdmin();
        $customData['current_page'] = $this->base_url.$this->uri->segment(1).$this->uri->segment(2);

        $customData['menu_cms'] = array();

        if ($this->cms_admin) {
            $pages= array(
                'url' => $this->base_url.'cms/pages',
                'text' => 'Páginas',
                'icon' => 'file'
              );
             array_push($customData['menu_cms'],$pages);
        }

         $items=  array(
            'url' => $this->base_url.'cms/items_catalogos',
            'text' => 'Ítems catálogo',
            'icon' => 'bullseye'
         );
        array_push($customData['menu_cms'],$items);

        if ($this->cms_admin) {
            if ($customData['is_admin']) {
                $carga=array(
                'url' => $this->base_url.'cms/carga',
                'text' => 'Carga CSV',
                'icon' => 'file-upload'
              );
        }
        
        array_push($customData['menu_cms'],$carga);
        }
        return $this->parser->parse('cms/menu', $customData, true, true);
    }

    // ============ Parse JSON config
    function parse_config($file, $debug = false) {
        $myconfig = json_decode($this->load->view($file, '', true), true);
        $minwidth=2;

        //Root config

        foreach ($myconfig as $key => $value) {
            if ($key != 'zones')
                $return[$key] = $value;
        }


        $return['js']=array();
        $return['css']=array();
        $return['inlineJS']="";
        // CSS
        if(isset($myconfig['css'])){
            foreach($myconfig['css'] as $item){
                foreach($item as $k=>$v){
                    if(is_numeric($k))
                        $return['css'][]=$v;
                    else{
                        $return['css'][$k]=$v;
                    }
                }
            }
        }
        // JS
        if(isset($myconfig['js'])){
            foreach($myconfig['js'] as $item){
                foreach($item as $k=>$v){
                    if(is_numeric($k))
                        $return['js'][]=$v;
                    else{
                        $return['js'][$k]=$v;
                    }
                }
            }
        }
        return $return;
    }

}
