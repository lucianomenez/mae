<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* Controlador para gestionar opciones generales
*
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 * @date    May 7, 2020
*/
class Settings extends MX_Controller
{

    function __construct() {
        parent::__construct();
        $this->user->authorize();
        $this->load->library('parser');

        $this->load->model('user');
        $this->load->model('cms/Model_settings');

        $this->load->helper('form');
        $this->load->helper('directory');
        $this->load->helper('cms/dynamic_builder');
        $this->load->helper('cms/block');

        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->idu = (double)$this->session->userdata('iduser');
    }

    function index() {

        // Mostrar settings
        if(empty($_POST)){
            $data = Modules::run('cms/loadDashboardVars');
            $data['settings'] = $this->Model_settings->get_settings();
            $this->parser->parse('settings', $data);
        }
        // Guardar settings
        else{
            $this->Model_settings->save_settings($_POST);
            return redirect('/cms');
        }


    }

}
