<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Post
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 * @date    May 7, 2020
 */
class Node extends MX_Controller
{

  protected $json_file;
  protected $fields_file;
  protected $nodes;
  protected $node_type;
  protected $card;

  function __construct() {
      parent::__construct();
      $this->user->authorize();
      $this->load->library('parser');
      $this->load->library('session');

      $this->load->model('user');
      $this->load->helper('form');
      $this->load->helper('directory');
      $this->load->helper('inflector');
      $this->load->helper('cms/dynamic_builder');
      $this->load->helper('cms/block');
      $this->load->model('user/user');
      $this->usuario = $this->user->get_user($this->user->idu);
      $this->cms_admin = (in_array(1,$this->usuario->group)||in_array(1500,$this->usuario->group)) ? true:false;
      $this->cms_supervisor = (in_array(1,$this->usuario->group)||in_array(1501,$this->usuario->group)) ? true:false;
      $this->cms_editor = (in_array(1,$this->usuario->group)||in_array(1502,$this->usuario->group)) ? true:false;
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';

      $this->idu = (double)$this->session->userdata('iduser');

      if ($this->node_type!='') {
        $this->load->model('cms/Model_'.$this->node_type, 'model');
      } else {
        $this->load->model('cms/Model_node', 'model');
      }

      $this->json_file = 'cms/json/' . plural($this->node_type).'.json';


      $this->fields_file = APPPATH .'modules/cms/forms/nodes/'.$this->node_type.'.php';

      $this->nodes = $this->model->get( array(), 300 );
            $this->default_fields_file ='null';

    if ($this->node_type!='options'&&$this->node_type!='items_catalogo') {
      $this->default_fields_file = APPPATH .'modules/cms/forms/default_fields.php';

    }
  }

  function Index() {

      $data = Modules::run('cms/loadDashboardVars',$this->json_file);
      $data['nodes'] = $this->nodes;

      if ($data['item']!='carga') {
        $this->parser->parse($data['view'], $data);
      }else{
        $this->parser->parse('carga', $data);
      }

  }

  function Edit($id = '') {

    $data = Modules::run('cms/loadDashboardVars', $this->json_file);

    // si viene $_POST guardo el post
    if (!empty($this->input->post())) {

      $data = $this->input->post();

      $data['post_modified'] = new MongoDate();

      if ($return = $this->model->save($data)) {

          $this->session->set_flashdata('save-ok','Item guardado correctamente');

            redirect("cms/".plural($this->node_type)."/edit/".$id);

      }

    }

    //TODO: Revisar como implementar validacion en este punto para poder luego usarlo tambien en campos de blocks y campos fields (ahora llamados params)

      if (empty($id)) {
          $id = new MongoId();
          $data['title'] = 'Nuevo';
      } else {
          $post = $this->model->get_one(['_id' => new MongoId($id)]);
          $this->model->save_revision($post);
          $data['title'] = $post['title'];
      }
       $node_fields =array();
       if ($this->default_fields_file!='null') {
         $node_fields = include($this->default_fields_file);

      }

      // node fields
      if ($this->fields_file) {
        $fields = include($this->fields_file);
        $node_fields = array_merge($node_fields, $fields);
      }


      $form_action = $this->base_url.'cms/'. plural($this->node_type)."/edit/$id";


      $id_hidden = array(
        '_id' => $id,
        'logo_color' => $post['logo_color']?$post['logo_color']:'',
        'post_type' => $this->node_type,
      );

      $data['form_action'] = $form_action;
      $data['id_hidden'] = $id_hidden;

      $form = '';
      $form_sidebar = '<div style="padding:10px 28px;text-align:center;">
                  <div class="btn-group mb-2 btn-block" >
                      <a href="'.$this->base_url.$post['slug'].'" target="_blank" class="btn btn-primary" value="ver">Ver</a>
                  </div>';
      $form_sidebar .= '<div class="btn-group">';

        if ($this->cms_admin||$this->cms_supervisor|| $post['post_type']=='page') {

            $form_sidebar .= form_button(
              array(
                'type' => 'submit',
                'class' => 'btn btn-verde',
                'name' => 'post_status',
                'value' => 'published'
              ),
              'Guardar y publicar');
    
        }
        if ($post['post_type']!='page') {
                $form_sidebar .= form_button(
                  array(
                    'type' => 'submit',
                    'class' => 'btn btn-secondary',
                    'name' => 'post_status',
                    'value' => 'draft',
                  ),
                  'Guardar borrador');
        }else if($this->cms_admin){
           $form_sidebar .= form_button(
              array(
                'type' => 'submit',
                'class' => 'btn btn-secondary',
                'name' => 'post_status',
                'value' => 'draft',
              ),
              'Guardar borrador');
        }


      $form_sidebar.="</div>";
      $form_sidebar.="</div>";

      foreach ($node_fields as $field) {
          $field_name = $field['name'];
          $field_value = '';
          if (isset($post[$field_name])) {
              $field_value = $post[$field_name];
          }
          if (isset($field['position']) && $field['position'] == 'sidebar') {
              $form_sidebar .= form_field_($field, $field_value);
          } else {
            $form .= form_field_($field, $field_value);
          }
      }
      if ($this->node_type=='page') {
       $form .= form_fieldset('Contenido');
      }


      // layout fields
      if (isset($post['layout']) && is_array($post['layout']) && $post['layout']!='') {
        $layout_file = APPPATH .'modules/cms/forms/layouts/'.$post['layout']['name'].'.php';

        if ($layout_file) {

          $layout_fields = include($layout_file);

          foreach ($layout_fields as $field) {

              $field_value = (isset($post['layout'][$field['name']])) ? $post['layout'][$field['name']] : '';
              $field['name'] = 'layout['.$field['name'].']';
              $form .= form_field_($field, $field_value);
          }
        }
      }

      //renderizado de bloques que existen en este documento cuando se carga
      if ($this->node_type=='page') {
        $form .= form_fieldset('Bloques', array('class'=> 'blocks-container'));
        if (!empty($post['blocks'])) {

            foreach ($post['blocks'] as $block_pos => $block) {
                $block_name =  $block['type'];
                $form .= block_build_form($block_name, $block, $block_pos, true);
            }

        }

        //cierre fieldset bloques existentes
        $form .= form_fieldset_close();

        //cierre de fielset contenedor de seccion bloques
        $form .= form_fieldset_close();

        $available_blocks = block_list($this->node_type);

        //renderizado de botones de bloques disponibles
        foreach($available_blocks as $block) {
          $form .= form_button(array('class' => 'btn btn-bloques add-block', 'data-block' => $block['name']), $block['label']);
        }
      }
      $data['form'] = $form;
      $data['form_sidebar'] = $form_sidebar;

      $this->parser->parse('post_form', $data);
  }

  function tags() {

      $term = $_GET['term'];
      $tags = $this->model->get_tags();

      echo json_encode($tags);
  }

  function Delete($id = ''){

    if ($id != '') {

      $response = array('status' => false);

      if ($this->model->delete($id)) {
        $response['status'] = true;
      }

      echo json_encode($response);
    }
  }

}

function ajax_block_form($block_name) {
    $block_file = APP_PATH. '/modules/cms/blocks' . $block_name . '.php';
    $form_fields = '';
    if (file_exists($block_file)) {
       $block_structure = require_once $block_file;

        foreach ($block_structure['fields'] as $field) {
            $field_name = $field['name'];
            $field_value = '';
            $form_fields .= $this->form_field_($field, $field_value);
        }
    }
    return $form_fields;
}
