<?php
if ( ! defined('BASEPATH')) exit('Access denied');

include_once 'Model_node.php';

class Model_options extends Model_node {

    function get($params = array(), $limit = 25, $offset = 0){
        $params['post_type'] = 'options';
	    $result= parent::get($params, $limit);
	    if (!$result) {
	    	$result=array();
	    }
	     return $result;
    }

    function save($data){
        $data['post_type'] = 'options';
        return parent::save($data);
    }

}
