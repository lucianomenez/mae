<?php
if ( ! defined('BASEPATH')) exit('Access denied');

class Model_node extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->library('cimongo/Cimongo', '', 'db');
        $this->config->load('cimongo');
        $this->load->model('options');

    }

    function get_one($conditions){
        $this->db->switch_db($this->config->item('cmsdb'));
        $node = $this->db->get_where('container.pages', $conditions)->row_array();
        $this->db->switch_db($this->config->item('db'));

        return empty($node) ? false : $node;
    }

    function get($params = array(), $limit = 40, $offset = 0) {
    
        $this->db->switch_db($this->config->item('cmsdb'));
        $nodes = $this->db
                    ->order_by(array('post_date' => 'DESC'))
                    ->get_where('container.pages', $params, $limit, $offset)
                    ->result_array();
        $this->db->switch_db($this->config->item('db'));
        return empty($nodes) ? false : $nodes;
    }

    function get_by_tag($params = array(), $limit = 25, $offset = 0) {
        $this->db->switch_db($this->config->item('cmsdb'));

        $regex = new MongoRegex("/".$params['tag']."/i");
        $query = array(
                'aggregate' => 'container.pages',
                'pipeline' =>
                  array(
                    array(
                      '$match' => array (
                          '$or' => array(
                            array('name' => $regex),
                          ),
                        ),
                      ),
                    )
                );
        $nodes =  $this->db->command($query);

        $this->db->switch_db($this->config->item('db'));

        return empty($nodes) ? false : $nodes;
    }

    function get_relacionadas($tag){
        $this->db->switch_db($this->config->item('cmsdb'));
        $container = 'container.pages';
        $query = array(
            'post_status' => 'published',
            'tags'=> array( '$elemMatch' => array('key' => $tag)),
        );
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function update_option($container,$array){
                $result= $this->options->get_options($container);

                if ($result) {
                    $newArray=array();
                     foreach ($result as $key_object => $value_object) {
                        $newArray[$key_object]=$value_object['value'];
                     }
  
                    foreach ($array as  $key_value => $value_item) {
                        if (!in_array($value_item['value'],$newArray)) {
                           $this->options->insert_option($container,$value_item);
                        }
                    }
                }else{
                    foreach ($array as $key_value => $value_item) {
                         $this->options->insert_option($container,$value_item);
                    }
                   
                }
    }

    function save($data){
        $this->usuario = $this->user->get_user($this->user->idu);
        $cms_admin = (in_array(1,$this->usuario->group)||in_array(1500,$this->usuario->group)) ? true:false;
        $cms_supervisor = (in_array(1,$this->usuario->group)||in_array(1501,$this->usuario->group)) ? true:false;
        $cms_editor = (in_array(1,$this->usuario->group)||in_array(1502,$this->usuario->group)) ? true:false;
        if ($data['post_type']=="items_catalogo") {

            if (is_array($data['categorias'])) {

                $array_completo=$data['categorias'];
                $container='container.categorias';
                $this->update_option($container,$array_completo);
            }

            if (is_array($data['destinatarios'])) {
                $array_completo=$data['destinatarios'];
                $container='container.destinatarios';
                $item='destinatarios';
                $this->update_option($container,$array_completo);
            }

            if (is_array($data['tag'])) {
                $array_completo=$data['tag'];
                $container='container.tag';
                $item='tag';
                $this->update_option($container,$array_completo);
            }

            if (is_array($data['sectores'])) {
                $array_completo=$data['sectores'];
                $container='container.sectores';
                $item='sectores';
                $this->update_option($container,$array_completo);
            }
        }


            if (!$cms_admin ) {
               unset($data['slug']);
               unset($data['title']);
            }

        $this->db->switch_db($this->config->item('cmsdb'));

        $container = 'container.pages';
        
        $query = array('_id' => new MongoId($data['_id']));
        unset($data['_id']);
        unset($data['meta_image_upload']);
        unset($data['img_upload']);

        # post processing
        foreach ($data as $key => &$value){
            $method_name = 'field_'. $key . '_alter';
            if(method_exists($this, $method_name)){
                $this->{$method_name}($value);
            }
        }

        # layout processing
        if (is_array($data['layout'])) {
          foreach ($data['layout'] as $key => &$value) {
            $method_name = 'field_'. $key . '_alter';
            if(method_exists($this, $method_name)){
                $this->{$method_name}($value);
            }
          }
        }

        # blocks processing
        // reorder blocks
        $data['blocks'] = array_values($data['blocks']);

        // alter fields
        foreach ($data['blocks'] as &$block){
          foreach ($block['data'] as $key => &$value) {
            $method_name = 'field_'. $key . '_alter';
            if(method_exists($this, $method_name)){
                $this->{$method_name}($value);
            }
          }
        }

        $this->db->where($query);

        return $this->db->update($container, $data, array('upsert'=> true));

    }

    function delete($_id){
      $this->db->switch_db($this->config->item('cmsdb'));

      $_id = new MongoId($_id);
      $query = array('_id' => $_id);

      if ($this->db->where($query)->delete('container.pages')) {
        $this->db->switch_db($this->config->item('db'));
        return true;
      } else {
        $this->db->switch_db($this->config->item('db'));
        return false;
      }

    }

    function save_revision($node){
      $this->db->switch_db($this->config->item('cmsdb'));
      $container = 'container.revisions';
      unset($node['_id']);
      $node['revision_date'] = new MongoDate();

      return $this->db->save($container, $node);
    }

    function get_tags(){
        $this->db->switch_db($this->config->item('cmsdb'));
        $results = $this->db->select(['tags'])
                            ->where(['tags' => ['$ne' => null]])
                            ->get('container.pages')
                            ->result();

        foreach ($results as $result){
            foreach ($result->tags as $tag){
                if ($tag['value']!='') {
                  $tags[] = $tag['value'];
                }
            }


        }
        $unique = array_keys(array_flip($tags));

        return $unique;

    }

    function field_date_alter(&$value){
        $date = date_create_from_format('d/m/Y H:i', $value);
        $value = new MongoDate(strtotime($date->format("Y-m-d H:i")));
    }

    function field_post_date_alter(&$value){
        $date = date_create_from_format('d/m/Y H:i', $value);
        $value = new MongoDate(strtotime($date->format("Y-m-d H:i")));
    }

    function field_open_alter(&$value){
      if ($value == 1) {
        $value = true;
      } else {
        $value = false;
      }
    }

    function field_tags_alter(&$value){

      $data['tags'] = explode(',', $value);

      foreach($data['tags'] as &$tag) {
          $tag = array('key' => strtolower(url_title($tag)), 'value' => $tag);
      }

      $value = $data['tags'];
    }

}
