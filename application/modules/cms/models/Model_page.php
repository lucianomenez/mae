<?php
if ( ! defined('BASEPATH')) exit('Access denied');

include_once 'Model_node.php';

class Model_page extends Model_node{

	function get($params = array(), $limit = 40, $offset = 0 ){
      $params['post_type'] = 'page';
      $result= parent::get($params, $limit);
      	 if (!$result) {
	    	$result=array();
	    }
		    foreach ($result as $key => &$value) {
		    	$value['date_sort']='';
		    	if ($value['post_modified']) {
		    		$value['date_sort']=$value['post_modified']->toDateTime()->getTimestamp();
		    	}
		    	if ($value['post_status']=='draft') {
		    			$value['status_sort']='Borrador';
		    	}else{
		    		$value['status_sort']='Publicado';
		    	}
		    }
	     return $result;
	}

	function save($data){
	    $data['post_type'] = 'page';
	    return parent::save($data);
    }

}
