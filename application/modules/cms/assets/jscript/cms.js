   let cms_admin=false;

      let  cms_supervisor=false;

    let  cms_editor=false;
 let primerCarga=true;

$(document).ready(function(){
    if ($('#tipo_file').val()=='options') {
      $('button.btn[value=draft]').hide();
       $('a.btn[value=ver]').hide();
    }
    initPermisos();
   
    initDataTables();
    initTags();
    initSlug();
    initAjaxUpload();
    initBlocksFunctionalities();
    initBlockPlugins();
    initRepeaters();


        let multipleInput=$('select.multiple');

        let multipleInputlength=multipleInput.length;
        for (i = 0; i< multipleInputlength; i++) {
           let itemId=multipleInput[i].id;
           initMultiple(itemId)
          }


        let tagsinputs=$('input.tagsinput');

        let tagsinputslength=tagsinputs.length;
        for (i = 0; i< tagsinputslength; i++) {
           let itemId=tagsinputs[i].id;
            initTagsInput(itemId)
          }


    $(document).on({
      click: function(e) {
        e.preventDefault();
        $('.media-uploader__modal').modal('show')
        $.ajax({
              url: globals['base_url'] + 'cms/archivos/get_modal',
              type: "POST",
          }).done(function(response) {
            $('.media-uploader__modal .modal-body').html(response)
          })
      }
    }, '.media-uploader__modal-button');



    $(document).on({
      click: function(e) {
        e.preventDefault();
        let node = $(this).parents('.node')
        let url = $(this).attr('href')

        if (window.confirm('Está seguro de borrar este post?')) {
          $.ajax({
              url: url,
              type: "POST",
            }).done(function(response) {
              response = JSON.parse(response)
              if (response.status) {
                node.remove()
              }
            })
        }

      }
    }, '.node-delete');

    $(document).on('change', ".cant_items", function(e) {
      e.preventDefault()
        item=$(this).attr('data-items');
        $('#seleccion_item_txt').text($(this).val());
          $('#'+item).selectpicker({
              maxOptions:$(this).val()
          });
    });

});

        function initMultiple(id){
          let item= $('#'+id).attr('item');
          loadCategorias(item,id);
          $('#'+id).selectpicker();
          $('#'+id).parent().parent().append('<ul class="mt-2" id="listado_'+id+'"></ul>');
          $('form').append('<div id="hiddensMultiple'+id+'"></div>');

              let url=globals['base_url'] + 'cms/get_data';
              let mongoId= $('input[name="_id"]').val();
              let post_type= $('input[name="post_type"]').val();
              $.ajax({
                  url: url,
                  type: "POST",
                  data:{
                    _id:mongoId,
                    post_type:post_type,
                    item:id
                  }
                }).done(function(response) {
                  response = JSON.parse(response)
                  if ($.isArray(response.data)) {
                      let seleccionados = [];
                      $.each(response.data, function( index, value ) {
                         seleccionados.push(value.value)
                        })
                      $('#'+id).selectpicker('val',seleccionados);
                  }
                })


          $('#'+id).on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            let value=$('#'+id).selectpicker('val');

            $('form').find('#hiddensMultiple'+id).html('');
            $('form').find('#hiddensMultiple'+id).append('<input type="hidden" name="'+id+'" value=""/>');
            $('#listado_'+id).html('');
            if ($.isArray(value)) {
                $.each(value, function( index, value ) {
                    let text= $('#'+id+' option[value="'+value+'"]').text();
                    $('form').find('#hiddensMultiple'+id).append('<input type="hidden" name="'+id+'['+index+'][value]" value="'+value+'"/>');
                    $('form').find('#hiddensMultiple'+id).append('<input type="hidden" name="'+id+'['+index+'][text]" value="'+text+'"/>');
                    $('#listado_'+id).append('<li>'+text+'</li>');
                })
            }

          });
         }

         function initTagsInput(id){
              $('form').append('<div id="hiddensTag'+id+'"></div>');

                  var tags = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch:  {
                      ttl: 1,
                      url:globals['base_url'] + 'cms/get_options_json/'+id
                    }
                  });
                  tags.initialize()

              $('#'+id).tagsinput({
                  itemValue: 'value',
                  itemText: 'text' ,
                  trimValue: true,
                  confirmKeys: [13, 44],
                    
                   typeaheadjs: {
                      name: 'tags',
                      displayKey: 'text',
                      source: tags.ttAdapter()
                  }
              });


              let url=globals['base_url'] + 'cms/get_data';
              let mongoId= $('input[name="_id"]').val();
              let post_type= $('input[name="post_type"]').val();
              $.ajax({
                  url: url,
                  type: "POST",
                  data:{
                    _id:mongoId,
                    post_type:post_type,
                    item:id
                  }
                }).done(function(response) {
                  response = JSON.parse(response)
                  if ($.isArray(response.data)) {
                      $.each(response.data, function( index, value ) {
                          $('#'+id).tagsinput('add',value);
                        })
                  }
 
                })


            //let element=$('#'+id).parent().find( '.bootstrap-tagsinput input')
            let element=$('#'+id).parent().find('.tt-input')

             element.on('keydown', function (e) {
                if (e.keyCode === 13 || e.keyCode === 44|| e.keyCode === 188) {
                  e.preventDefault();
                  value=slugify(element.val());
                  if (value!='') {
                    $('#'+id).tagsinput('add', { 
                      value: value, 
                      text: element.val()
                    }); 

                    element.val("");
                  }
                }
              });
              element.on('blur', function (e) {
                  e.preventDefault();
                  value=slugify(element.val());
                  if (value!='') {
                    $('#'+id).tagsinput('add', { 
                      value: value, 
                      text: element.val()
                    }); 

                    element.val("");
                  }
       
              });
              $('#'+id).on('itemAdded', function(event) {
                let items= $('#'+id).tagsinput('items');
                if ($.isArray(items)) {
                  $('form').find('#hiddensTag'+id).html('');
                   $.each(items, function( index, value ) {
                     $('form').find('#hiddensTag'+id).append('<input type="hidden" name="'+id+'['+index+'][text]" value="'+value.text+'"/>');
                     $('form').find('#hiddensTag'+id).append('<input type="hidden" name="'+id+'['+index+'][value]" value="'+value.value+'"/>');

                          if (id=='categorias'&&index==0&&primerCarga) {
                             $('input[name="title"]').change();
                              primerCarga==false;
                          }
                   })
                }
              });
              $('#'+id).on('itemRemoved', function(event) {
                let items= $('#'+id).tagsinput('items');
                 $('form').find('#hiddensTag'+id).html('');
                 $('form').find('#hiddensTag'+id).append('<input type="hidden" name="'+id+'" value=""/>');
                  $('form').find('#hiddensTag'+id).append('<input type="hidden" name="'+id+'" value=""/>');
                  if ($.isArray(items)) {

                     $.each(items, function( index, value ) {
    
                       $('form').find('#hiddensTag'+id).append('<input type="hidden" name="'+id+'['+index+'][text]" value="'+value.text+'"/>');
                       $('form').find('#hiddensTag'+id).append('<input type="hidden" name="'+id+'['+index+'][value]" value="'+value.value+'"/>');
                       if (id=='categorias'&&index==0) {
                              primerCarga==true;
                        }
                     })
                  }
              });

         }

function loadCategorias(item,id){

         let url=globals['base_url'] + 'cms/get_listados';

            $.ajax({
              async: false,
              url: url,
              type: "POST",
              data:{
                item:item
              },
            }).done(function(response) {
              response = JSON.parse(response);

              if (response!='null') {

                if ($.isArray(response.data)) {
                  options='';
                  options+='<option value="">Sin definir</option>';
                   $.each(response.data, function( index, value ) {
                    if (value.id ) {
                        options+='<option value="'+value.id+'">'+value.name+'</option>';
                    }else{
                      options+='<option value="'+value.value+'">'+value.text+'</option>';
                    }
                   
                  });

                  $('#'+id).html(options);
                }
              }

            })


 }


function redactorLoad(e) {
  $(e).redactor({
    buttons: ['format', 'bold', 'italic', 'underline', 'ul', 'ol', 'link', 'html'],
    replaceTags:  {
      'b': 'strong',
      'i': 'em'
    },
    plugins: ['buttons']
  })
}

function tagsLoad(e) {
  $(e).selectize({
      delimiter: ',',
      persist: false,
      valueField: 'text',
      labelField: 'text',
      searchField: 'text',
      plugins: ['remove_button'],
      load: function(query, callback) {
          if (!query.length) return callback();
          $.get( globals['base_url']+'/cms/posts/tags', { term: query } , function( data ) {
              data = JSON.parse(data);
              callback(data.map(function(x) { return { text: x } }));
          });
      }
  });
}

function datepickerLoad(e) {
  $(e).datepicker( $.datepicker.regional[ "es" ] );
}

function undefinedLoad(e) {
  return false;
}

function datetimepickerLoad(e) {
    $.timepicker.regional['es'] = {
        timeText: 'tiempo',
        hourText: 'hora',
        minuteText: 'minutos',
        secondText: 'segundos',
        millisecText: 'milisegundos',
        timezoneText: 'zona horaria',
        timeFormat: 'HH:mm',
        currentText: 'hoy',
        closeText: 'cerrar',
    };
    $(e).datetimepicker( $.timepicker.regional[ "es" ] );

}

function initAjaxUpload() {
    $(document).on('change', '.ajax_upload', function(event) {
        //event.preventDefault();
        var uploadField =  event.target;
        var urlField = '';
        var imgField = '';
        var url = globals['base_url'];
        var uploadUrl = url + 'cms/upload_file';
        var loadingImg = url + 'cms/assets/img/loading-balls.gif';

        file = event.target.files[0];

        isImage = file.type.match('image');
        isVideo = file.type.match('video');

        if (isImage || isVideo) {
            data = new FormData();
            data.append( 'file', file );
            var postData = {
                url: uploadUrl,
                data: data,
                type: "POST",
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false,
                dataType: 'json',
            }
            if (isImage) {
                urlField = $(uploadField).siblings('input[type=hidden]').first();
                imgField = $(uploadField).siblings('img').first();
                imgField.attr('src',loadingImg);
                $.ajax(postData).done(function(data) {
                    if (data.success) {
                        urlField.attr('value', data.url);
                        imgField.attr('src', data.url);
                    }
                });
            } else {
                urlField = $(uploadField).siblings('input[type=text]').first();
                var parentContainer = $(uploadField).parent();
                parentContainer.append('<img class="loading" src="'+loadingImg+'"/>');
                $.ajax(postData).done(function(data) {
                    if (data.success) {
                        urlField.attr('value', data.url);
                        parentContainer.find('.loading').remove();
                    }
                });
            }
        }

    });
}


function initBlocksFunctionalities() {
    $(document).on('click', ".collapse-block", function(e) {
      e.preventDefault()
        var i = $(this).find('i');
        var block = $(this).parents('.block');
        if (block.hasClass('closed')) {
            i.removeClass('fa-expand').addClass('fa-compress');
            block.removeClass('closed');
        } else {
            i.removeClass('fa-compress').addClass('fa-expand');
            block.addClass('closed');
        }

    });

    // ==== Load blocks
    $(document).on('click', '.add-block', function(event) {
        event.preventDefault();
        var blocksCount = $('.blocks-container .block').length;
        var blockName = $(this).data('block');
        var url = globals['base_url'];
        url = url + '/cms/load_block/' +  blockName + '/' + blocksCount;

        var blockContainer = $('.blocks-container');
        $.ajax({
            url: url,
            context: url
        }).done(function(data) {
            blockContainer.append(data.fields);
            $('.blocks-container').sortable({
              handle: ".move-block",
              axis: "y"
            })
            if (blockName=='cards-items') {
                   initMultiple('items_catalogo')
            }
            if (blockName=='cards-categorias') {
                initMultiple('categorias')
            }
     if (blockName=='cards-destinatarios') {
                initMultiple('destinatarios')
            }
                   
        });
    })

    $('.blocks-container').sortable({
      handle: ".move-block",
      axis: "y"
    })

    $(document).on('click', '.remove-block', function(e) {
        e.preventDefault();
        var blockToRemove = $(this).parents('.block');
        if (window.confirm('Borrar Bloque')) {
          blockToRemove.remove();
        }
    });


    $(document).on('DOMNodeInserted', function(e) {
        // console.log($(e.target).find('.load-js') );
        //if ( $(e.target).find('.load-js').length > 0 ) {
        $.each($(e.target).find('.load-js'), function (i,e) {
            var pluginToLoad = $(e).data('load');
            console.log(!$(this).hasClass('plugin-loaded'));
            if (!$(this).hasClass('plugin-loaded')) {
              if (pluginToLoad != 'undefined') {
                var loadFunctionName = pluginToLoad + 'Load';
                $(this).addClass('plugin-loaded')
                window[loadFunctionName](e)
              }
            }


        });
        $('.repeater items').sortable({
          handle: ".move-item",
          axis: "y"
        })

    });
  }



function initSlug() {
    // Provee un valor para el slug en base al titulo
    $('input[name="title"]').on('change', (e) => {
       
        let postType = $('input[name=post_type]').val();
        let postTitle = $('input[name="title"]').val();
        let postCategoria = $('input[name="categorias[0][value]"]').val();

        if (!postCategoria) {
           postCategoria = '';
        }else{
          postCategoria+='/';
        }
        switch(postType){
            case 'page': $str = slugify(postTitle); break;
            case 'items_catalogo': $str = 'catalogo-covid19/' +postCategoria+slugify(postTitle); break;
            default: $str = postType+'/' +slugify(postTitle);
        }

        $('input[name="slug"]').val($str);
    });

    // Inicializa el boton para visualizar el post
    let slug = $('input[name=slug]').val();
    if(slug){
        let postType = $('input[name=post_type]').val();
        $('#node-view').attr('href', '/' + slug);
        if(postType == 'page') postType = 'página';
        $('#node-view').html('Ver ' + postType);
        $('#node-view').show();
    }
}

const slugify = text => {
  // Use hash map for special characters
  let specialChars = {"à":'a',"ä":'a',"á":'a',"â":'a',"æ":'a',"å":'a',"ë":'e',"è":'e',"é":'e', "ê":'e',"î":'i',"ï":'i',"ì":'i',"í":'i',"ò":'o',"ó":'o',"ö":'o',"ô":'o',"ø":'o',"ù":'o',"ú":'u',"ü":'u',"û":'u',"ñ":'n',"ç":'c',"ß":'s',"ÿ":'y',"œ":'o',"ŕ":'r',"ś":'s',"ń":'n',"ṕ":'p',"ẃ":'w',"ǵ":'g',"ǹ":'n',"ḿ":'m',"ǘ":'u',"ẍ":'x',"ź":'z',"ḧ":'h',"·":'-',"/":'-',"_":'-',",":'-',":":'-',";":'-',".":'',"(":'',")":''};

    return text.toString().toLowerCase()
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '')            // Trim - from end of text
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/./g,(target, index, str) => specialChars[target] || target) // Replace special characters using the hash map
      .replace(/&/g, '-and-')         // Replace & with 'and'
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
};

function initTags() {
    $('input[name="tags"]').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'text',
        labelField: 'text',
        searchField: 'text',
        plugins: ['remove_button'],
        create: function(input, callback) {
            return {
                value: input,
                text: input
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.get( globals['base_url']+'/cms/posts/tags', { term: query } , function( data ) {
                data = JSON.parse(data);
                callback(data.map(function(x) { return { text: x } }));
            });
        }
    });

}

function initBlockPlugins() {
    $.each($('.load-js'), function (i,e) {
        var pluginToLoad = $(e).data('load');
        if (pluginToLoad !== 'undefined') {
          $(this).addClass('plugin-loaded');
          var loadFunctionName = pluginToLoad + 'Load';
          window[loadFunctionName](e)
        }

    });
}

function initDataTables() {
    $('.datatables').DataTable({
        "iDisplayLength": 50,
        "order": [[2, "desc" ]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
    });
}

function initRepeaters() {

  $(document).on({
    click: function(e) {
      e.preventDefault();
      let parent = $(this).parents('.repeater').find('.items')

      let item = parent.find('.item').last()
      let index = parent.find('.item').length

        if ( item[0].outerHTML.indexOf("normativa[") > -1) {
          var replace = /normativa\[[0-9]?[0-9]{1,2}\]/g
          var newReplace = "normativa["+index+"]"
        }

      let newItem = $(item[0].outerHTML.replace(replace, newReplace))

      newItem.find('input').val('')
       newItem.find('textarea').text('')

      $(parent).append(newItem);


    }
  }, '.repeater .add-item');


      $('.items').on('change', 'input.text_option',function(e) {

        let postTitle = $(this).val();

        $str = slugify(postTitle);

        $(this).parent().parent().find('input.text_value').val($str);
      });


  $(document).on({
    click: function(e) {
      e.preventDefault();
      let parent_item = $(this).parents('.repeater').find('.items')

      let item = parent_item.find('.item').last()
      let index = parent_item.find('.item').length

      if (index==1) {
           if ( item[0].outerHTML.indexOf("normativa[") > -1) {
                $('input[name^="normativa[0]"]').val('');
          }
      }else{
        let parent = $(this).parent().parent().remove()
      }

      
    }
  }, '.repeater .remove-item');

  $('.repeater .items').sortable({
    handle: ".move-item",
    axis: "y"
  })

}


(function($R)
{
    $R.add('plugin', 'buttons', {
        translations: {
            en: {
                "buttons": "Botones"
            }
        },
        init: function(app)
        {
            this.app = app;
            this.lang = app.lang;
            this.toolbar = app.toolbar;

            // local
            this.styles = {
              "btn-a": {
                title: "Botón",
                args: { class: 'btn btn-outline-secondary btn-sm' }
              },
            };
        },
        start: function()
        {
            var dropdown = {};
            for (var key in this.styles)
            {
                var style = this.styles[key];
                dropdown[key] = {
                  title: style.title,
                  api: 'module.inline.toggle',
                  args: style.args
                };
            }

            var $button = this.toolbar.addButtonAfter('format', 'inline', { title: this.lang.get('buttons') });

            $button.setIcon('<i class="re-icon-inline"></i>');
            $button.setDropdown(dropdown);
        }
    });
})(Redactor);

var initPermisos=()=>{

  if ($('#cms_admin').val()=='false'){
    cms_admin=true;
    $('input[name="title"]').attr("readonly","readonly");
    $('input[name="slug"]').attr("readonly","readonly");
  }

  if ($('#cms_supervisor').val()=='true'){
        cms_supervisor=true;
  }

  if ($('#cms_editor').val()=='true'){
      cms_editor=true;
  }

}
