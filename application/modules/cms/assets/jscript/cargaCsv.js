$(document).ready(function(){    
    $(document).on({
      click: function(e) {
        e.preventDefault();

        let elemento=$(this)
                elemento.addClass('spinner-grow');
        let item=$(this).attr('data-input');
        let formData = new FormData();
        let file=$('#'+item)[0].files[0];
        formData.append("file", file);

        $.ajax({
              url: globals['base_url'] + 'cms/carga/process_file',
              type: "POST",
              processData: false,
               contentType: false,  
              data:formData
          }).done(function(response) {
            elemento.hide();
           $('#succes_msg').show();
           $('#succes_msg_delete').hide();
            $('.deleteFiles').show();
          $('.deleteFiles').removeClass('spinner-grow');
          $('#info_carga').html(response);
          })
      }
    }, '.uploadFile');

    $(document).on({
      click: function(e) {
        e.preventDefault();

        let elemento=$(this)
        elemento.addClass('spinner-grow');

        $.ajax({
              url: globals['base_url'] + 'cms/carga/delete_documents',
              type: "POST",
              processData: false,
               contentType: false
          }).done(function(response) {
            elemento.hide();
                      $('.deleteFiles').removeClass('spinner-grow');
           $('#succes_msg_delete').show();
          })
      }
    }, '.deleteFiles');
})