<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('form_input_'))
{
    function form_input_($value, $key){
        $form_field =  '<div style="margin-bottom:10px;">';
        $form_field .=  form_label($key);
        $form_field .=  form_input(array(
            'type'  => 'text',
            'name'  => $key,
            'value' => $value,
        ));
        $form_field .=  '</div>';
        return $form_field;
    }
}

if ( ! function_exists('form_field_'))
{
    function form_field_($field_settings, $value = ''){

        $data_attr = '';
        $classes = $field_settings['type'].' form-group';

        // if ( isset($field_settings['dependency']) ) {
        //   $dependency = str_replace('['.$field_settings['dependency'].']', '', $field_settings['name']);
        //   $data_attr = " data-dependency='{$field_settings["dependency"]}' data-dependency_value='{$field_settings["dependency_value"]}'";
        //   unset($field_settings["dependency"]);
        //   unset($field_settings["dependency_value"]);
        //   $classes .= ' js-dependency';
        // }

        $form_field =  '<div class="'.$classes.'" '.$data_attr.'>';

        //seteo el label del campo
        if (isset($field_settings['label'])) {
          $form_field .= form_label($field_settings['label']);
          unset($field_settings['label']);
        }


        $ci_field_function = 'form_'. $field_settings['type'];
        $fna_field_function = 'fna_form_' . $field_settings['type'];

        if ($value != '') {
          unset($field_settings['value']);
        }

        if (function_exists($fna_field_function)) {
            $form_field .= $fna_field_function($field_settings, $value);
        } else if (function_exists($ci_field_function)) {
            $form_field .= $ci_field_function($field_settings, $value);
        } else {
            return false;
        }


        $form_field .=  '</div>';
        return $form_field;
    }
}

function fna_form_media_upload($field_settings, $value = '') {

    $form_field = '<div class="media-uploader">';
    $form_field .= form_hidden($field_settings['name'], $value);
    if (isset($value) && $value!='') {
      $form_field .= "<a href='#' id='{$field_settings["name"]}' class='btn media-uploader__modal-button'><img src={$value}></a>";
    } else {
      $form_field .= "<a href='#' id='{$field_settings["name"]}' class='btn media-uploader__modal-button'>Seleccionar imagen</a>";
    }
    $form_field .= '</div>';

    return $form_field;
}

function fna_form_image_upload($field_settings, $value = '') {
    $form_field = '<div class="image-preview">';
    if (isset($value) && $value!='') {
      $form_field .= '<img src="'.$value.'" class="">';
    } else {
      $form_field .= '<img src="" alt="">';
    }

    //$field_settings['name'] = $field_settings['name'] . '_upload';
    $field_settings['class'] = $field_settings['class'] . ' ajax_upload';
    $form_field .= form_upload($field_settings, '');
    $form_field .= form_hidden($field_settings['name'], $value);
    $form_field .= '</div>';
    return $form_field;
}

function fna_form_upload($field_settings, $value = '') {
    $form_field = '<div class="file">';
    if (isset($value) && $value!='') {

      $form_field .= '<img src="'.$value.'" class="">';
    } else {
      $form_field .= '<img src="" alt="" class="">';
    }

    //$field_settings['class'] = $field_settings['class'] . ' ajax_upload';
    $form_field .= form_hidden($field_settings['name'], $value);
    $form_field .= '</div>';
    $form_field .= '<div class="btn select-file">Agregar archivo</div>';
    return $form_field;
}

function fna_form_hidden($field_settings, $value = '') {

  if ( $value == '') {
    $value = $field_settings['value'];
  }

  return form_hidden( $field_settings['name'], $value);
}

function fna_form_dropdown($field_settings, $value = '') {

  return form_dropdown($field_settings, [], $value);
}

function fna_form_compound($field_settings, $value = array()) {
    $form_field = "";
    foreach($field_settings['subfields'] as $subfield) {
        $subfield['value'] = isset($value[$subfield['name']]) ? $value[$subfield['name']] : '';
        $subfield['name'] = $field_settings['name'] . '[' . $subfield['name'] . ']';

        $form_field .= form_field_($subfield, $subfield['value']);
    }

    return $form_field;
}

function fna_form_repeater($field_settings, $items = array()) {
    $form_field = '<div class="items">';
    if (empty($items)) {
      $items = array();
      $items[0] = '';
    }
    foreach ($items as $key => $value) {
      $form_field .= '<div class="item" data-index="'.$key.'">';
      foreach($field_settings['subfields'] as $subfield) {
          $subfield['value'] = isset($value[$subfield['name']]) ? $value[$subfield['name']] : '';
          $subfield['name'] = $field_settings['name'] .'[' .$key. '][' . $subfield['name'] . ']';

          $form_field .= form_field_($subfield, $subfield['value']);
      }
      $form_field .= '<div class="actions">';
      $form_field .= '<a class="action move-item"><i class="fas fa-arrows-alt-v"></i></a>';
      $form_field .= '<a class="action remove-item"><i class="fa fa-trash"></i></a>';
      $form_field .= '</div>';
      $form_field .= '</div>';
    }
    $form_field .= '</div>';
    $form_field .= '<a href="#" class="btn btn-bloques add-item">Agregar</a>';

    return $form_field;
}

function fna_form_video_upload($field_settings, $value = '') {
    $form_field = '';
    $input = form_input(array('name'=> $field_settings['name'], 'class'=> 'form-control', 'readonly' => 'readonly'), $value);
    $field_settings['name'] = $field_settings['name'] . '_upload';
    $field_settings['class'] = $field_settings['class'] . ' ajax_upload';
    $form_field .= form_upload($field_settings, '');
    $form_field .= $input;

    return $form_field;
}


function fna_form_date($field_settings, $value = '') {
    return form_input(array(
      'name'=> $field_settings['name'],
      'class'=> 'form-control load-js',
      'data-load' => 'datepicker'
    ), $value);
}

function fna_form_datetime($field_settings, $value = '') {
  if (is_object($value)) {
    $date = $value->toDateTime();
    $format_date = $date->format('U.u');
  } else {
    $format_date = time();
  }

  return form_input(array(
    'name'=> $field_settings['name'],
    'class'=> 'form-control load-js',
    'data-load' => 'datetimepicker'
  ), date('d/m/Y H:i', $format_date ));
}

function fna_form_section_title($field_settings, $value = '') {
  if (!isset($field_settings['html_tag'])) {
    $field_settings['html_tag'] = 'div';
  }

  $field = "<{$field_settings['html_tag']} class='{$field_settings['class']}'>";
  $field .= "{$field_settings['text']}";
  $field .= "</{$field_settings['html_tag']}>";
  return $field;
}

function fna_form_tags($field_settings, $value = ''){

  if(!empty($value)){
    foreach ($value as $tag_num => $tag) {
      $tags[] .= $tag['value'];
    }
  }

  if ( is_array($tags) ) {
    $tags = implode(',', $tags);
  }

  $field .= form_input(array(
      'type'  => 'text',
      'name'  => $field_settings['name'],
      'value' => $tags,
      'class' => $field_settings['class'] . ' load-js',
      'data-load' => 'tags'
  ));

  return $field;
}
