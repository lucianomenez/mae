<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('block_list'))
{
    function block_list($node_type = 'any'){
        $blocks = directory_map(APPPATH .'/modules/cms/forms/blocks/');
        sort($blocks);
        foreach($blocks as $key => &$block) {
          if (!is_array($block)) {
            $block = substr($block, 0,-4);
            $block = block_structure($block);

            # excluimos el bloque del listado
            if(!is_array($block['node'])) $block['node'] = array($block['node']);
            $exclude_block = ($node_type != 'any') && !in_array($node_type,$block['node']) && !in_array('any', $block['node']);
            if($exclude_block) unset($blocks[$key]);

          } else {
            unset($blocks[$key]);
          }

        }
        return $blocks;
    }
}

if ( ! function_exists('block_file'))
{
    function block_file($block_name) {
        $block_name = str_replace('_', '-', $block_name);
        $block_name = str_replace(' ', '-', $block_name);
        return realpath(APPPATH. 'modules/cms/forms/blocks/' . $block_name . '.php');
    }
}


if ( ! function_exists('block_structure'))
{
    function block_structure($block_name) {
        $block_structure = false;
        $block_file = block_file($block_name);

        if (file_exists($block_file)) {
            $block_structure = require $block_file;

            if (!isset($block_structure['label'])) {
                $block_structure['label'] = $block_structure['name'];
            }
        }

        return $block_structure;
    }
}

if ( ! function_exists('block_build_form')) {
    function block_build_form($block_name, $block_data = '', $pos = '', $html = false)
    {
        $form_fields = '';
        $block_response = array(
            'name' => $block_name,
        );

        $block_structure = block_structure($block_name);

        if (!empty($block_structure)) {
            $block_response = $block_structure;

            $form_fields .= '<div class="block ' . $block_structure['name'] . '">';
            $form_fields .= '<div class="block-title">' . $block_structure['label'] . '</div>';
            $form_fields .= '<div class="actions">';
            $form_fields .= '<a class="action collapse-block" aria-label="Collapse"><i class="fa fa-compress"></i></a>';
            $form_fields .= '<a class="action move-block" aria-label="Reorder"><i class="fas fa-arrows-alt-v"></i></a>';
            $form_fields .= '<a class="action remove-block" aria-label="Close"><i class="fa fa-trash"></i></a>';
            $form_fields .= '</div>';

            $form_fields .= '<div class="block-fields">';
            $block_prefix = 'blocks['.$pos.']';
            $form_fields .= form_hidden($block_prefix . '[type]', $block_structure['type']);
            $form_fields .= form_hidden($block_prefix . '[enabled]', 1);

            foreach ($block_structure['fields'] as $field) {

                $field_value = (isset($block_data['data'][$field['name']])) ? $block_data['data'][$field['name']] : '';
                $field['name'] = 'blocks['.$pos.'][data]['.$field['name'].']';
                $form_fields .= form_field_($field, $field_value);
            }
            $form_fields .= '</div>';

            $block_response['fields'] = $form_fields . '</div>';
        }

        return ($html) ? $block_response['fields'] : $block_response;
    }
}
