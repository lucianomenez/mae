<?php
return array(

    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'name'  => 'tipo_file',
        'value' => 'items_catalogo',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),

    array(
        'type'  => 'input',
        'name'  => 'title',
        'label' => 'Título pestaña navegador',
        'class'  => 'title form-control',
    ),
        array(
        'type'  => 'input',
        'id'  => 'categorias',
        'name'  => 'categorias_value',
        'label' => 'Categorias',
        'class'  => 'form-control tagsinput'
    ),
    array(
        'type'  => 'input',
        'name'  => 'slug',
        'label' => 'Enlace permanente',
        'class'  => 'form-control',
    ),
    array(
        'type'  => 'hidden',
        'name'  => 'class'
    ),
    array(
      'type'  => 'section_title',
      'html_tag' => 'legend',
      'text' => 'Información general',
      'class'  => '',
      'position' => 'sidebar'
    ),


    array(
        'type'  => 'datetime',
        'name'  => 'post_date',
        'label' => 'Fecha',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'hidden',
        'name'  => 'header_class',
        'position' => 'sidebar'
    ),
    array(
      'type'  => 'section_title',
      'html_tag' => 'legend',
      'text' => 'Metadatos <br><small>(Información para las redes)</small>',
      'class'  => '',
      'position' => 'sidebar'
    ),
    array(
        'type'  => 'image_upload',
        'name'  => 'featured_image',
        'label' => 'Imagen destacada',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),

    array(
        'type'  => 'textarea',
        'name'  => 'meta_description',
        'label' => 'Meta Description',
        'class'  => 'form-control',
        'rows'  => 2,
        'position' => 'sidebar'
    ),
    array(
                'type'  => 'input',
                'name'  => 'title_question',
                'label' => 'Título / Pregunta',
                'class' => 'form-control'
            ),

    array(
                'type'  => 'textarea',
                'name'  => 'content',
                'label' => 'Descripción / Respuesta',
                'class'  => 'form-control load-js redactor',
                'data-load' => 'redactor'
            ),
    array(
                'type'  => 'input',
                'name'  => 'enlace',
                'label' => 'Enlace',
                'class' => 'form-control'
            ),
    array(
        'type'  => 'dropdown',
        'id'  => 'organismos',
        'name'  => 'mapa_del_estado_jurisdicciones_value',
        'item'=>'mapa_del_estado_jurisdicciones',
        'label' => 'Organismos',
        'multiple'=>'',
        'options' => array(

        ),
        'class'  => ' multiple'
    ),

    array(
        'type'  => 'input',
        'id'  => 'sectores',
        'name'  => 'sectores_value',
        'label' => 'Sectores',
        'class'  => 'form-control tagsinput'
    ),
    array(
        'type'  => 'input',
        'id'  => 'destinatarios',
        'name'  => 'destinatarios_value',
        'label' => 'Destinatarios',
        'class'  => 'form-control tagsinput'
    ),
    array(
        'type'  => 'input',
        'id'  => 'tag',
        'name'  => 'tag_value',
        'label' => 'Tags',
        'class'  => 'form-control tagsinput'
    ),
          array(
            'type'  => 'repeater',
            'name'  => 'normativa',
            'label' => 'Normativa',
            'class'  => '',
            'data-load' => '',
            'subfields' => array(
              array(
                'type'  => 'input',
                'name'  => 'text',
                'label' => 'Descripción',
                'class'  => 'form-control',
                'data-load' => ''
              ),
              array(
                'type'  => 'input',
                'name'  => 'link',
                'label' => 'Link',
                'class'  => 'form-control',
                'data-load' => ''
              ),
            )
          )
);
