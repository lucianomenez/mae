<?php
return array(
  array(
      'type'  => 'hidden',
      'name'  => 'name',
      'value' => 'page'
  ),
  array(
      'type'  => 'input',
      'name'  => 'subtitle',
      'label' => 'Subtítulo',
      'class'  => 'form-control',
  ),
);
