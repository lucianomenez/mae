<?php
return array(
  'name' => 'cards-items',
  'type' => 'cards_items',
  'label' => 'Bloque Acciones Estatales',
  'class' => '',
  'icon' => '',
  'form_css' => '',
  'form_js' => '',
  'custom_css' => '',
  'custom_js' => '',
  'node' => 'page',
  'fields' =>
      array(
        array(
          'type'  => 'input',
          'name'  => 'title',
          'label' => 'Título',
          'class' => 'form-control'
          ),
        array(
              'type'  => 'dropdown',
              'name'  => 'cant_items',
              'label' => 'Cantidad de resultados',
              'data-items'=>'items_catalogo',
              'options' => array(
                  6 => '6',
                  9 => '9',
                  12 => '12',
                  15 => '15',
                  18 => '18',
                  21 => '21'
              ),
              'class'  => 'form-control cant_items',
          ),
          array(
              'type'  => 'dropdown',
              'id'  => 'items_catalogo',
              'name'  => 'items_catalogo_value',
              'item'=>'items_catalogo',
              'label' => 'Items a mostrar (Seleccionar <span id="seleccion_item_txt">6</span>)',
              'multiple'=>'',
              'options' => array(

              ),
              'class'  => ' multiple'
          ),
      )
);
