<?php
return array(
  'name' => 'cards-organismos',
  'type' => 'cards_organismos',
  'label' => 'Bloque Organismos',
  'class' => '',
  'icon' => '',
  'form_css' => '',
  'form_js' => '',
  'custom_css' => '',
  'custom_js' => '',
  'node' => 'page',
  'fields' =>
      array(
        array(
          'type'  => 'input',
          'name'  => 'title',
          'label' => 'Título',
          'class' => 'form-control'
          ),
        array(
              'type'  => 'dropdown',
              'name'  => 'cant_items',
              'label' => 'Cantidad de resultados',
                'data-items'=>'destinatarios',
              'options' => array(
                  4 => '4',
                  8 => '8',
                  12=> '12'
              ),
              'class'  => 'form-control cant_items',
          ),
       array(
            'type'  => 'dropdown',
            'id'  => 'organismos',
            'name'  => 'mapa_del_estado_jurisdicciones_value',
            'item'=>'mapa_del_estado_jurisdicciones',
            'label' => 'Organismos',
            'multiple'=>'',
            'options' => array(

            ),
            'class'  => ' multiple'
        ),
      )
);
