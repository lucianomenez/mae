<?php
return array(
  'name' => 'cards-destinatarios',
  'type' => 'cards_destinatarios',
  'label' => 'Bloque destinatarios',
  'class' => '',
  'icon' => '',
  'form_css' => '',
  'form_js' => '',
  'custom_css' => '',
  'custom_js' => '',
  'node' => 'page',
  'fields' =>
      array(
        array(
          'type'  => 'input',
          'name'  => 'title',
          'label' => 'Título',
          'class' => 'form-control'
          ),
        array(
              'type'  => 'dropdown',
              'name'  => 'cant_items',
              'label' => 'Cantidad de resultados',
                'data-items'=>'destinatarios',
              'options' => array(
                  9 => '9',
                  12 => '12',
                  16 => '16'
              ),
              'class'  => 'form-control cant_items',
          ),
          array(
              'type'  => 'dropdown',
              'id'  => 'destinatarios',
              'name'  => 'destinatarios_value',
              'item'=>'destinatarios',
              'label' => 'Items a mostrar (Seleccionar <span id="seleccion_item_txt">9</span>)',
              'multiple'=>'',
              'options' => array(

              ),
              'class'  => ' multiple'
          ),
      )
);
