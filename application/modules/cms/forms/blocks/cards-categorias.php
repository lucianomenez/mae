<?php
return array(
  'name' => 'cards-categorias',
  'type' => 'cards_categorias',
  'label' => 'Bloque categorías',
  'class' => '',
  'icon' => '',
  'form_css' => '',
  'form_js' => '',
  'custom_css' => '',
  'custom_js' => '',
  'node' => 'page',
  'fields' =>
      array(
        array(
          'type'  => 'input',
          'name'  => 'title',
          'label' => 'Título',
          'class' => 'form-control'
          ),
        array(
              'type'  => 'dropdown',
              'name'  => 'cant_items',
              'label' => 'Cantidad de resultados',
                'data-items'=>'categorias',
              'options' => array(
                  4 => '4',
                  8 => '8',
                  12 => '12',
                  16 => '16'
              ),
              'class'  => 'form-control cant_items',
          ),
          array(
              'type'  => 'dropdown',
              'id'  => 'categorias',
              'name'  => 'categorias_value',
              'item'=>'categorias',
              'label' => 'Items a mostrar (Seleccionar <span id="seleccion_item_txt">6</span>)',
              'multiple'=>'',
              'options' => array(

              ),
              'class'  => ' multiple'
          ),
      )
);
