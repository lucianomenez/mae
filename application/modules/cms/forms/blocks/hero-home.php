<?php
return array(
  'name' => 'hero-home',
  'type' => 'hero_home',
  'label' => 'Hero Home',
  'class' => '',
  'icon' => '',
  'form_css' => '',
  'form_js' => '',
  'custom_css' => '',
  'custom_js' => '',
  'node' => 'page',
  'fields' =>
      array(
        array(
          'type'  => 'input',
          'name'  => 'title',
          'label' => 'Título',
          'class' => 'form-control'
          ),
        array(
          'type'  => 'input',
          'name'  => 'subtitle1',
          'label' => '1° Subtítulo (big)',
          'class' => 'form-control'
          ),
        array(
          'type'  => 'input',
          'name'  => 'subtitle2',
          'label' => '2° Subtítulo',
          'class' => 'form-control'
        ),
        array(
          'type'  => 'input',
          'name'  => 'question',
          'label' => 'Pregunta buscador',
          'class' => 'form-control'
          ),
      )
);
