<?php
return array(
  'name' => 'accion-estatal',
  'type' => 'accion-estatal',
  'label' => 'Acción estatal',
  'class' => '',
  'icon' => '',
  'form_css' => '',
  'form_js' => '',
  'custom_css' => '',
  'custom_js' => '',
  'node' => 'page',
  'fields' =>
      array(
        array(
          'type'  => 'input',
          'name'  => 'header_title',
          'label' => 'Título',
          'class' => 'form-control'
          ),
        array(
          'type'  => 'input',
          'name'  => 'header_subtitle',
          'label' => 'Subtítulo',
          'class' => 'form-control'
          ),
      )
);
