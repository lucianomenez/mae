      <?php
        $this->load->view('dashboard/_footer_jquery_bootstrap.php')
      ?>

    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Modal -->
    <div class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary">Save changes</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal -->

    <!-- Media uploader modal -->
    <div class="modal media-uploader__modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary">Asignar imagen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.media-uploader__modal -->

  </div>
  <!-- ./wrapper -->

  <!-- JS Global -->
  <script>
    var globals = {global_js};
  </script>


  <!-- jQuery -->
  <script src="{base_url}jscript/librerias/jquery_3-5/plugins/jquery-ui.min.js"></script>

    <script src="{base_url}jscript/frameworks/bootstrap-4.4.1/js/bootstrap.bundle.min.js"></script>

  <script src="{base_url}jscript/librerias/jquery_3-5/plugins/datepicker-es.js"></script>
  <script src="{base_url}jscript/librerias/jquery_3-5/plugins/jquery-ui-timepicker-addon.js"></script>

  <!-- Plugins -->
  <script src="{base_url}cms/assets/tinymce/tinymce.min.js"></script>
  <script src="{base_url}cms/assets/jscript/redactor.min.js"></script>
  <script src="{base_url}jscript/adminlte3/plugins/datatables/DataTables/datatables.min.js"></script>

  <script src="{base_url}jscript/adminlte3/plugins/selectize/selectize.min.js"></script>

  <!-- AdminLTE App -->
  <script src="{base_url}jscript/templates/AdminLTE-3.0.4/dist/js/adminlte.min.js"></script>
  <script src="{base_url}cms/assets/jscript/app.js"></script>

<script src="{base_url}dashboard/assets/jscript/bootstrap.js"></script>

  <!-- CMS js -->
  <script src="{base_url}cms/assets/jscript/cms.js"></script>
  <script src="{base_url}cms/assets/jscript/archivos.js"></script>





<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="{base_url}jscript/frameworks/bootstrap-4.4.1/js/defaults-ar_AR.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="{base_url}jscript/frameworks/bootstrap-4.4.1/js/bootstrap-select.js"></script>

<script src="{base_url}jscript/librerias/jquery_3-5/plugins/tagsinput/typeahead.bundle.min.js"></script>

   <script src="{base_url}jscript/librerias/jquery_3-5/plugins/tagsinput/bootstrap-tagsinput.js?54"></script>
  <!-- JS custom -->
  {js}

  <!-- JS inline -->
  <script>
  {ignore}
  $(document).ready(function(){
  	{inlineJS}
  });
  {/ignore}
  </script>

</body>
</html>
