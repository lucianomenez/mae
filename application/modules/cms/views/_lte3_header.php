<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>{title}</title>
  <link rel="icon" href="{base_url}dashboard/assets/img/favicon.png" type="image/png" sizes="32x32">

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="{base_url}jscript/librerias/jquery_3-5/plugins/jquery-ui.min.css">
  <link rel="stylesheet" href="{base_url}jscript/librerias/jquery_3-5/plugins/jquery-ui-timepicker-addon.css">
  <link rel="stylesheet" href="{base_url}jscript/librerias/jquery_3-5/plugins/tagsinput/bootstrap-tagsinput-typeahead.css">
    <link rel="stylesheet" href="{base_url}jscript/librerias/jquery_3-5/plugins/tagsinput/bootstrap-tagsinput.css">
  <!-- Theme style -->
    <link rel="stylesheet" href="{base_url}jscript/templates/AdminLTE-3.0.4/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="{base_url}jscript/templates/AdminLTE-3.0.4/dist/css/adminlte.min.css">
  <link rel="stylesheet" href="{base_url}jscript/templates/AdminLTE-3.0.4/plugins/selectize/selectize.css">
  <!-- Custom style -->
  <link rel="stylesheet" href="{base_url}dashboard/assets/css/modals.css">
  <link rel="stylesheet" href="{base_url}cms/assets/css/cms.css">
  <link rel="stylesheet" href="{base_url}cms/assets/css/redactor.min.css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="{base_url}jscript/frameworks/bootstrap-4.4.1/css/bootstrap-select.min.css">
    <script src="{base_url}jscript/librerias/jquery_3-5/jquery-3.5.0.min.js"></script>

  <link rel="stylesheet" href="{base_url}jscript/adminlte3/plugins/datatables/DataTables/datatables.min.css">
  

<style type="text/css">
  *{
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
  }
  .nav-link {
    font-size: 1rem;
    color:#000;
    font-weight: 500; }
</style>
  {custom_css}

  {ignore}
  <!-- <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-93384156-1', 'auto');
    ga('send', 'pageview');

  </script> -->
  {/ignore}

</head>
<body class="hold-transition sidebar-mini">

  <input type="hidden" id="cms_admin" value ="{cms_admin}">
  <input type="hidden" id="cms_supervisor" value="{cms_supervisor}">
  <input type="hidden" id="cms_editor" value="{cms_editor}">

  <div class="wrapper">
      <?php
        $this->load->view('dashboard/_modals_bootstrap.php')
      ?>


  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="height: 8vh;">
    <div class="container" style="margin-left:0px!important;margin-right:-30rem;">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>
      <div class="nav justify-content-end">
        <li class="nav-item dropdown">
          <a class="nav-link a-f computer" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{name}<i class="fas fa-chevron-down ml-3"></i></a>
          <div class="dropdown-menu dropdown-menu-right p-3">
             <a class="dropdown-item btn_contrasena" href="#">Cambiar contraseña</a>
            <a class="dropdown-item" href="{base_url}user/logout"><b>Cerrar Sesión</b></a>
          </div>
        </li>
      </div>
    </div>



  </nav>
  <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-1">
      <!-- Brand Logo -->
      <a href="{base_url}catalogo-covid19" class="brand-link" style="height: 4rem;text-align: center;">
        <img class="brand-image" src="{base_url}cms/assets/img/MAE-logo-blanco.png" alt="catalogo-covid19">
      </a>

      <!-- Sidebar -->
      <div class="sidebar d-flex flex-column justify-content-between">

        <!-- Sidebar Menu -->
        {menu}
        <!-- /.sidebar-menu -->

        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel mt-3 mb-3 d-flex">
          <div class="image">
            <img src="{base_url}jscript/adminlte3/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">Alexander Pierce</a>
          </div>
        </div> -->

      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Displays confirmations when a node has been saved -->
        <?php if($this->session->flashdata('save-ok')): ?>
        <div class="alert alert-success alert-dismissible" style="margin: 10px">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4>
                <i class="icon fa fa-check"></i>
                <?php echo $this->session->flashdata('save-ok'); ?>
            </h4>
        </div>
        <?php endif; ?>

        <div>

        </div>
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <div class="m-0 text-title">{title}</div>
            </div><!-- /.col -->
            <div class="col-sm-6">
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
