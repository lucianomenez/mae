<?php
/*
 *  Header : CSS Load & some body
 *
 */
$this->load->view('_lte3_header.php')
?>

<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        
        <form>
          <div class="form-group">
            <label for="fileCsv">Subir CSV Ítems catálogo</label>
            <input type="file" class="form-control-file" id="fileCsv" accept=".csv">
          </div>
            <button class="btn btn-verde mb-4 uploadFile" data-input="fileCsv"><i class="fas fa-plus-circle"></i> Enviar</button>

            <div class="alert alert-success" role="alert" style="display: none;" id="succes_msg">
              CSV cargado correctamente
          </div>

          <div id="info_carga">
            
          </div>
        </form>

            <button class="btn btn-danger mb-4 deleteFiles" data-input="fileCsv"><i class="fas fa-plus-circle"></i> Eliminar todos los Ítems</button>

            <div class="alert alert-success" role="alert" style="display: none;" id="succes_msg_delete">
              Ítems eliminados correctamente
          </div>
      </div>

    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<script src="{base_url}cms/assets/jscript/cargaCsv.js"></script>
{js}
<?php
/*
 *  FOOTER
 *
 */
$this->load->view('_lte3_footer.php')

?>

