<?php
/*
 *  Header : CSS Load & some body
 *
 */
$this->load->view('_lte3_header.php')
?>

<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <a href="{base_url}{edit_url}" class="btn btn-verde mb-4"><i class="fas fa-plus-circle"></i> Crear nuevo</a>
        <table id="posts" class="datatables table table-bordered table-hover" style="background-color: #fff;">
            <thead>
              <tr>
                <th style="text-align: center;">TÍTULO</th>
                <th style="text-align: center;max-width: 90px;">ESTADO</th>
                <th style="text-align: center;">ACCIONES</th>
              </tr>
            </thead>
            <tbody>
              {nodes}
              <tr class="node">
                <td>
                  <a href="{base_url}{edit_url}{_id}" style="width: 100%;display: inline-block;">
                   
                    {if {post_type}=='page'}
                     <span>{title} </span>
                    
                    {else}
                    <span>{title_question} </span>
                    {/if}
                  </a>
                </td>
                <td style="  max-width: 90px;">
                  <span style="display: none;">{status_sort}</span>
                  <div class="alert alert-success {post_status}" role="alert">
                   
                  </div>
    
                </td>
                <td style="text-align: center;">
                  <span style="display: none;">{date_sort}</span>
                  <a href="{base_url}{edit_url}{_id}" style="font-size: 17px;width: 25px;display: inline-block;"><i class="fas fa-edit" ></i></a> <a href="{base_url}{slug}" style="font-size: 17px;width: 25px;display: inline-block;"><i class="fas fa-eye"></i></a><a href="{base_url}cms/node/delete/{_id}" class="node-delete" style="font-size: 17px;width: 25px;display: inline-block;"><i class="fas fa-trash-alt"></i></a></td>
              </tr>
              {/nodes}
            </tbody>
        </table>

      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

{js}
<?php
/*
 *  FOOTER
 *
 */
$this->load->view('_lte3_footer.php')

?>
