<nav class="mt-2">
	<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
		<li class="nav-header">CMS</li>
		{menu_cms}

			<li class="nav-item" style="{margin}">
				<a href="{url}" class="nav-link {if {current_page}=={url}}active{/if}">
					<i class="nav-icon fa fa-{icon}"></i>
					<p>{text}</p>
				</a>
			</li>
		{/menu_cms}
	</ul>
</nav>
