import React, { Component } from "react";
import { render } from "react-dom";

function miComponente() {
  return <p>Hola Mundo</p>;
}

class mi_componente_clase extends Component {
  render() {
    return <p> Hola Forro</p>;
  }
}

class Blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: [
        "Casablanca",
        "El Padrino",
        "Lo Que el Viento se Llevó",
        "Buenos Muchachos"
      ]
    };
  }

  render() {
    return (
      <div>
        {this.state.articles.map(title => {
          return <h2>{title}</h2>;
        })}
      </div>
    );
  }
}

class Formulario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      name: "",
      lastname: ""
    };
  }

  syncChanges(value, property) {
    let state = {};
    state[property] = value;
    this.setState(state);
  }

  submitForm = () => {
    console.log(this.state);
  };

  render() {
    return (
      <form>
        <p>
          <label>Title</label>
          <br />
          <label>
            <input type="radio" name="title" value="mr" />
            Mr
          </label>
          <label>
            <input type="radio" name="title" value="mrs" />
            Mrs
          </label>
          <label>
            <input type="radio" name="title" value="miss" />
            Miss
          </label>
        </p>
        <p>
          <label>First name</label>
          <br />
          <input type="text" name="first_name" />
        </p>
        <p>
          <label>Last name</label>
          <br />
          <input type="text" name="last_name" />
        </p>
        <p>
          <label>Email</label>
          <br />
          <input
            type="email"
            onChange={ev => {
              this.syncChanges(ev.target.value, "email");
            }}
            value={this.state.email}
            name="email"
            required
          />
        </p>
        <p>
          <label>Phone number</label>
          <br />
          <input type="tel" name="phone" />
        </p>
        <p>
          <label>Password</label>
          <br />
          <input
            type="password"
            onChange={ev => {
              this.syncChanges(ev.target.value, "password");
            }}
            value={this.state.password}
            name="password"
          />
        </p>
        <p>
          <label>Country</label>
          <br />
          <select>
            <option>China</option>
            <option>India</option>
            <option>United States</option>
            <option>Indonesia</option>
            <option>Brazil</option>
          </select>
        </p>
        <p>
          <label>
            <input type="checkbox" value="terms" />
            Estoy de acuerdo con los <a href="/terms">terminos y condiciones</a>
          </label>
        </p>
        <p>
          <input onClick={this.SubmitForm} type="submit" value="dale" />

          <button type="reset">Resetear</button>
        </p>
      </form>
    );
  }
}

class App extends Component {
  constructor() {
    super();
    this.state = {
      name: "React"
    };
  }

  render() {
    return (
      <div>
        <miComponente />
      </div>
    );
  }
}

render(<Blog />, document.getElementById("app"));
