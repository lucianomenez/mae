<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_panel extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */

    function get_casos($idu){
        $this->db->select(array ('id','idwf','status','checkdate','iduser'));
        $query = array('iduser'=> $idu,
                        'status' => 'open');
        $this->db->where($query);
        $result = $this->db->get('case')->result_array();
        return $result;
    }

    function get_inscripciones($idu){
        $query = array('idu'=> strval($idu));
        $this->db->where($query);
        $result = $this->db->get('container.inscripciones')->result_array();
        return $result;
    }


    function get_tareas($idu){
      $idu=strval($idu);
        $query = array('idu'=> $idu,
                        'status' => 'pendiente');
        $this->db->where($query);
        $result = $this->db->get('container.tareas')->result_array();
        return $result;
    }

    function borrar_inscripciones_db($idwf, $idcase){ //Borra inscripcion se la pasa id inscripcion

        $container = 'container.inscripciones';
        $data = array(
            'borrado' => 1
            );
        $query= array('idwf'=>$idwf,
                      'idcase' => $idcase);
        $this->db->where($query);
        $this->db->delete($container);
    }

    function update_user($data){
      $container = 'users';
      $query = array('idu'=> intval($data['idu']));
      $this->db->where($query);
      $this->db->update($container, $data);
      return;
    }

    function update_existente($user_data) {
        $this->db->where(array('idnumber' => $user_data['idnumber']));
        $this->db->update('users', $user_data);
        return;
    }    





}
