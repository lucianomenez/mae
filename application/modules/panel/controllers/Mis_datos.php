<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mis_datos extends MX_Controller {

    public function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->model('app');
      $this->load->model('bpm/bpm');
      $this->load->model('Model_panel');
      $this->load->model('user/user');
      $this->load->library('parser');
      $this->load->config('config');
      $this->idu = $this->user->idu;

    }

    function Index() {
        // if user is logged in then send it to default controller
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;

        Modules::run('dashboard/dashboard', 'panel/json/mis_datos.json',$debug, $extraData);
    }

    function contenido(){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $data['usuario'][0] = $this->user->get_user_array($this->idu);
      echo $this->parser->parse('mis_datos',$data, true, true);
    }

    function update_datos(){
      $data = $this->input->post();
      $data['idu'] = intval($data['idu']);
      $this->Model_panel->update_user($data);
      redirect ($this->module_url .'mis_datos');
    }


}
