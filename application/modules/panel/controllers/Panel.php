<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Paneles de Navegación
 *
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 */
class Panel extends MX_Controller {

    function __construct() {
        parent::__construct();
          $this->base_url = base_url();
          $this->module_url = base_url() . $this->router->fetch_module() . '/';
          $this->user->authorize();
          $this->load->helper('file');
          $this->load->helper('url');
          $this->load->model('app');
          $this->load->model('bpm/bpm');
          $this->load->model('user/user');
          $this->load->library('parser');
          $this->load->config('config');
          $this->idu = $this->user->idu;
        }


      function index(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          Modules::run('dashboard/dashboard', 'panel/json/index.json',$debug, $extraData);
      }

      // Parse json

      function contenido(){
          $data['casos'] = $this->Model_panel->get_casos($this->idu);
          $data['inscripciones'] = $this->Model_panel->get_inscripciones($this->idu);
          $data['tareas'] = $this->Model_panel->get_tareas($this->idu);
          echo $this->parser->parse('contenido',$data, true, true);
      }

      function get_modales(){
          echo $this->parser->parse('modales',[], true, true);
      }

      function borrar_inscripcion(){
          $data = $this->input->post();
          $idwf=$data['idwf'];
          $case=$data['idcase'];
          $this->bpm->delete_case($idwf, $case);
          $this->Model_panel->borrar_inscripciones_db($idwf, $case);
          return;
      }

      function updatePass(){
          $data=$this->input->post();
          if ($data['idnumber']!=''&&$data['data']['passw']!='') {
              $user['idnumber'] = $data['idnumber'];
              $user['passw'] = md5($data['data']['passw']);
              $this->Model_panel->update_existente($user);
          }
        }



}
