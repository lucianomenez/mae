<div class="ui small modal inscripcion">
  <div class="header">Seguro que desea borrar su inscripción?</div>
  <div class="content">
    <p>Una vez que borra una solicitud no se puede recuperar, pero puede volver a inscribirse.</p>
  </div>
  <div class="actions submit">
    <div class="ui red approve button">
      <i class="remove icon"></i>
      Borrar
    </div>
    <div class="ui basic cancel button">
      Cancelar
    </div>
  </div>
</div>
<!-- fin modal -->
<!-- local JS -->
