
<div class="ui left aligned basic segment">
  {usuario}
<div class="ui container computer" style="height:60vh!important;">
  <div class="ui grid centered">
    <div class="ten wide column">
        <h1>Actualizar datos</h1>
        <div class="ui divider"></div>
      <form class="ui form" role="form" method="POST" action="{module_url}mis_datos/update_datos">
      <input type="hidden" id="idu" name="idu" value="{idu}">
       <div class="two fields mt2">
           <div class="field">
               <label for="name" class="">Nombre</label>
               <input id="name" type="text" name="name" value="{name}">
            </div>
            <div class="field">
                <label for="lastname" class="">Apellido</label>
                <input id="lastname" type="text" name="lastname" value="{lastname}">
             </div>
       </div>
       <div class="two fields mt2">
           <div class="field">
               <label for="cargo" class="">Cargo</label>
               <input id="cargo" type="text" name="cargo" value="{cargo}">
            </div>
            <div class="field">
                <label for="dni" class="">DNI</label>
                <input id="idnumber" type="text" name="idnumber" value="{idnumber}">
             </div>
       </div>
       <div class="two fields mt2">
         <div class="field">
           <label for="email" class="">Correo Electrónico</label>
           <input id="email" type="email" name="email" value="{email}">
         </div>
         <div class="field">
           <label for="telefono_laboral" class="">Teléfono Laboral</label>
           <input id="telefono_laboral" type="text" name="telefono_laboral" value="{telefono_laboral}">
         </div>
       </div>
       <div class="field">
        <button type="submit" class="ui button green mt2">Guardar</button>
       </div>
    </form>
    </div>
  </div>
</div>


<div class="mobile">

        <h1>Actualizar datos</h1>
        <div class="ui divider"></div>
      <form class="ui form" role="form" method="POST" action="{module_url}mis_datos/update_datos">
        <input type="hidden" id="idu" name="idu" value="{idu}">

       <div class="two fields">
           <div class="field mt1">
               <label for="name" class="">Nombre</label>
               <input id="name" type="text" name="name" value="{name}">
            </div>
            <div class="field mt1">
                <label for="lastname" class="">Apellido</label>
                <input id="lastname" type="text" name="lastname" value="{lastname}">
             </div>
       </div>
       <div class="two fields">
           <div class="field mt1">
               <label for="cargo" class="">Cargo</label>
               <input id="cargo" type="text" name="cargo" value="{cargo}">
            </div>
            <div class="field mt1">
                <label for="dni" class="">DNI</label>
                <input id="idnumber" type="text" name="nick" value="{nick}">
             </div>
       </div>
       <div class="two fields">
         <div class="field mt1">
           <label for="email" class="">Correo Electrónico</label>
           <input id="email" type="email" name="email" value="{email}">
         </div>
         <div class="field mt1">
           <label for="telefono_laboral" class="">Teléfono Laboral</label>
           <input id="telefono_laboral" type="text" name="telefono_laboral" value="{telefono_laboral}">
         </div>
       </div>
       <div class="field mt1">
        <button type="submit" class="ui button green">Guardar</button>
       </div>
    </form>

</div>
<br/>

      {/usuario}


    </div>

    <div class="mt2">
