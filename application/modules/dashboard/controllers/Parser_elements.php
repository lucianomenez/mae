<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Parser_elements extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('user/user');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        $this->idu = $this->user->idu;

    }

    function navbar_jquery_semantic() {
        $usuario = $this->user->get_user((int) $this->idu);      
        $data['base_url'] = $this->base_url;
        $data['nombre_usuario'] = $usuario->name . ' ' . $usuario->lastname;
        $data['name'] = $usuario->name;
        echo $this->parser->parse('dashboard/_navbar_jquery_semantic', $data, true, true);
    }

    function footer_jquery_semantic() {
        $data['base_url'] = $this->base_url;
        echo $this->parser->parse('dashboard/_footer_jquery_semantic', $data, true, true);
    }

    function navbar_jquery_bootstrap() {
        $usuario = $this->user->get_user((int) $this->idu);      
        $data['base_url'] = $this->base_url;
        $data['nombre_usuario'] = $usuario->name . ' ' . $usuario->lastname;
         $data['name'] = $usuario->name;
        echo $this->parser->parse('dashboard/_navbar_jquery_bootstrap_web_loggedin', $data, true, true);
    }

    function footer_jquery_bootstrap() {
        $data['base_url'] = $this->base_url;
        echo $this->parser->parse('dashboard/_footer_jquery_bootstrap', $data, true, true);
    }
}
