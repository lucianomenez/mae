<nav class="bg-background pb2">
	<div class="segment-base">
		<div class="ui container computer">
			<div class="ui secondary menu">
				<div class="ui container">
				<div style="width: 210px;height: 35px;background-position:center;background-repeat: no-repeat;background-size: contain; background-image: url('{base_url}dashboard/assets/img/logo-MAE-color.png');"></div>
				</div>
				<div class="right menu">
					<div class="ui dropdown item">
						<div class="nav-name pr1">
						{nombre_usuario}
						</div>
						<i class="chevron down icon"></i>
						<div class="menu p2">
							<a class="item btn_contrasena">Cambiar contraseña</a>
							<a class="item" href="{base_url}user/logout"><b>Cerrar sesión</b></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="ui container mobile">
			<div class="ui secondary menu">
				<div style="width: 160px;height: 40px;background-position:center;background-repeat: no-repeat;background-size: contain; background-image: url('{base_url}dashboard/assets/img/logo-MAE-color.png');"></div>

				<div class="right menu mt1">
					<div class="ui dropdown"><i class="large bars icon"></i>
						<!-- ACÁ SE AGREGA EL FLEX -->
					<div class="menu p1">
						<div class="nav-name p1">{nombre_usuario}</div>
						<div class="ui divider ml1 mr1"></div>
						<a class="item mt1 btn_contrasena">Cambiar contraseña</a>
						<a class="item mt1" href="{base_url}user/logout"><b>Cerrar Sesión</b></a>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</nav>

<?php
include('_modals_semantic.php')

?>
