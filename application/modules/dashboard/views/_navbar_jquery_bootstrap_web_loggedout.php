<nav class="navbar navbar-light" style="z-index: 9;position: absolute;top:0;width: 100%;">
	<div class="container pt-4">
	<div class="nav justify-content-start">
		<a class="navbar-brand" href="{base_url}catalogo-covid19">
			<div style="background-image: url('{base_url}dashboard/assets/img/logo-MAE-blanco.png');"></div>
		</a>
		{logo_interno_computer}
		<!-- MOSTRAR LOGO SOLO EN PAGINA INTERNA COMPUTER -->
		<!-- <div class="navbar-brand-complement computer" style="background-image: url('{base_url}dashboard/assets/img/icon-catalogo.png');"></div> -->
	</div>

		<div class="nav justify-content-end">
			<a href="{base_url}user/login"><button class="btn btn-ingresar">Ingresar</button></a>
		</div>
	</div>
	{logo_interno_mobile}
	<!-- MOSTRAR LOGO SOLO EN PAGINA INTERNA MOBILE -->
	<!-- <div class="navbar-brand-complement mobile" style="margin-top:-1rem!important;background-image: url('{base_url}dashboard/assets/img/icon-catalogo.png');"></div> -->
</nav>
