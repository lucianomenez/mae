<div class="modal fade" tabindex="-1" role="dialog" id="contrasenaModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal-title text-center">Cambio de contraseña</div>
        <p class="text-center mt-1">Generar una nueva contraseña para tu cuenta</p>
		<form id="contrasenaValidate">
          <div class="form-group">
            <input placeholder="Nueva contraseña" type="password"  id="inputPassword" value="" autocomplete="off" class="form-control">
            <div id="msg_contrasena" class="error"></div>
		  </div>
          <div class="form-group">
			     <input placeholder="Confirmar nueva contraseña" type="password"  id="inputPasswordConfirmar" value="" autocomplete="off" class="form-control">
           <div id="msg_contrasena_validar" class="error"></div>

		  </div>
		</form>
      <button type="button" class="btn btn-block btn-guardar success" id="cambiar_contrasena">Guardar contraseña</button>
  <button type="button" class="btn btn-block btn-cancelar" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
