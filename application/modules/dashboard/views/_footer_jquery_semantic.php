<style type="text/css">
		.bg-background {
		  background-color: #F3F2EF !important; }
		.pt1 {
			padding-top:1rem!important; }
		.pb2 {
			padding-bottom:2rem!important; }
		/* computer */
		@media only screen and (min-width: 768px) {
			.logos-footer {
			 width: 200px!important;
			 height: auto!important; } }
		/* mobile */
		@media only screen and (max-width: 768px) {
			.logos-footer {
			  width: 200px!important;
			  height: auto!important; } }
</style>

<footer class="bg-background pt1 pb2">
	<div class="ui container">
		<div class="ui divider pb2"></div>
			<div class="ui stackable four column grid middle aligned">
  		<div class="column">
  			<img class="ui centered logos-footer image" src="{base_url}dashboard/assets/img/logo-SUBSE-color.png">
  		</div>
			<div class="column">
				<img class="ui centered logos-footer image" src="{base_url}dashboard/assets/img/logo-SECRE-color.png">
			</div>
  		<div class="column">
  			<img class="ui centered logos-footer image" src="{base_url}dashboard/assets/img/logo-ARGUNIDA-color.png">
  		</div>
			<div class="column">
				<img class="ui centered logos-footer image" src="{base_url}dashboard/assets/img/logo-JEFATURA-color.png">
			</div>
		</div>
	</div>
</footer>
