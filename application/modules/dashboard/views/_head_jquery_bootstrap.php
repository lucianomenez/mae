<!DOCTYPE html>
<html>
<head>
    <html lang="es_AR">
    <meta name="viewport" content="initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>{title}</title>
    <link rel="icon" href="{base_url}dashboard/assets/img/favicon.png" type="image/png" sizes="32x32">
    <link rel="shortcut icon" href="" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <script src="{base_url}jscript/librerias/jquery_3-5/jquery-3.5.0.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{base_url}jscript/frameworks/bootstrap-4.4.1/css/bootstrap.min.css"></link>
    <link rel="stylesheet" type="text/css" href="{base_url}dashboard/assets/css/breakpoints.css">
    <link rel="stylesheet" href="{base_url}jscript/templates/AdminLTE-3.0.4/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{base_url}dashboard/assets/css/modals.css">
    {custom_css}
</style>

    <meta name="title" content="{title}"/>
    <meta name="description" content="{meta_description}"/>
    <meta name="keywords" content="{meta_keywords}"/>

    <meta property="og:site_name" content="MAPA DE LA ACCIÓN ESTATAL"/>
    <meta property="og:title" content="{title}"/>
    <meta property="og:description" content="{meta_description}"/>
    <meta property="og:url" content="{base_url}{slug}"/>
    <meta property="og:image" content="{featured_image}"/>
    <meta property="og:locale" content="es_AR" />

</head>
<body style="position: relative;">
<input type="hidden" value="{base_url}" id="base_url">
<input type="hidden" value="{slug}" id="type_page">

<input type="hidden" value="{idu}" id="idu_input">
<div class="toast fade hide alert alert-success" data-delay="4000" role="alert" aria-live="assertive" aria-atomic="true" style="position: absolute!important; top: 20px!important; right: 20px!important;z-index: 9!important;" id="toast">
  <!-- <div class="toast-header">
    <strong class="mr-auto">Bootstrap</strong>
  </div> -->
  <div class="toast-body">
    <b>Tu contraseña fue cambiada con éxito</b>
  </div>
</div>
