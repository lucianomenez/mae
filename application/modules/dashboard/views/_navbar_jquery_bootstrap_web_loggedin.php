

<nav class="navbar navbar-light" style="z-index: 9;position: absolute;top:0;width: 100%;">
	<div class="container  pt-4">
		<div class="nav justify-content-start">
			<a class="navbar-brand" href="{base_url}catalogo-covid19">
				<div style="background-image: url('{base_url}dashboard/assets/img/logo-MAE-blanco.png');"></div>
			</a>
			{logo_interno_computer}
			<!-- MOSTRAR LOGO SOLO EN PAGINA INTERNA COMPUTER -->
			<!-- <div class="navbar-brand-complement computer" style="background-image: url('{base_url}dashboard/assets/img/icon-catalogo.png');"></div> -->
		</div>

			<div class="nav justify-content-end">
				<li class="nav-item dropdown">
					<a class="nav-link a-f computer" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{name}<i class="fas fa-chevron-down ml-3"></i></a>
					<a class="nav-link a-f mobile" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-bars fa-lg"></i></a>
					<div class="dropdown-menu dropdown-menu-right p-3">
						<span class="dropdown-item mobile">{nombre_usuario}</span>
						<div class="dropdown-divider mobile"></div>
						<a class="dropdown-item btn_contrasena" href="#">Cambiar contraseña</a>
						<a class="dropdown-item" href="{base_url}user/logout"><b>Cerrar Sesión</b></a>
					</div>
				</li>
			</div>
		{logo_interno_mobile}
	</div>

  <!-- MOSTRAR LOGO SOLO EN PAGINA INTERNA MOBILE -->
	<!-- <div class="navbar-brand-complement mobile" style="margin-top:-1rem!important;background-image: url('{base_url}dashboard/assets/img/icon-catalogo.png');"></div> -->

</nav>

<?php
include('_modals_bootstrap.php')

?>
