<?php

include('_head_jquery_semantic.php')

?>
    {tag_navbar}
  <div class="bg-background">
    <div class="ui container">
      <div class="ui stackable two column grid pt3 pb3">
          <div class="sixteen wide column">
                  {content}
          </div>
      </div>
    </div>
  </div>


    {tag_footer}
<?php

include('_scripts_jquery_semantic.php')

?>
