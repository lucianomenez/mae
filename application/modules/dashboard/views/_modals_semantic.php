  <div class="ui tiny modal" id="contrasenaModal">

      <div class="scrolling content">
        <div class="modal-title">Cambio de contraseña</div>
        <p>Generar una nueva contraseña para tu cuenta</p>
            <form class="ui form" id="contrasenaValidate">
                <div class="field">
                  <div class="ui input">
                    <input placeholder="Nueva contraseña" type="password"  id="passw" value="" autocomplete="off">
                  </div>
                </div>
                <div class="field">
                  <div class="ui input">
                    <input placeholder="Confirmar nueva contraseña" type="password"  id="passw2" value="" autocomplete="off">
                  </div>
                </div>

                  <div class="ui field mt2">
                    <div class="ui error message"></div>
                  </div>
            </form>
              </div>
      <div class="actions submit" id="actions-submit">
            <div class="ui fluid approve submit button btn-guardar">
              Guardar contraseña
           </div>
           <div class="ui fluid cancel button mt1 btn-cancelar">
             Cerrar
           </div>
      </div>

</div>
