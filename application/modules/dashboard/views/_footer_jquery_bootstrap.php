      <style type="text/css">
       .centered {
          margin-left: auto;
          margin-right: auto;
          text-align: center; }
        .bg-black {
          background-color: #000 !important; }
      </style>
      <footer class="bg-black pb-4" style="margin-top:10vh;">
        <div class="container pt-5 pb-5">
          <!-- <div class="divider pb-2"></div> -->
            <div class="row align-items-center">
              <div class="col-sm mt-5 centered">
                <img src="{base_url}dashboard/assets/img/logo-SUBSE-blanco.png" style=" width: 200px;">
              </div>
              <div class="col-sm mt-5 centered">
                <img src="{base_url}dashboard/assets/img/logo-SECRE-blanco.png" style=" width: 200px;">
              </div>
              <div class="col-sm mt-5 centered">
                <img  src="{base_url}dashboard/assets/img/logo-ARGUNIDA-blanco.png" style=" width: 200px;">
              </div>
              <div class="col-sm mt-5 centered">
                <img  src="{base_url}dashboard/assets/img/logo-JEFATURA-blanco.png" style=" width: 200px;">
              </div>
            </div>
        </div>
      </footer>
