<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{title}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!--====== CSS BASE ===== -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"  rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="{base_url}poncho/assets/css/poncho.min.css" rel="stylesheet">
        <link href="{base_url}poncho/assets/css/icono-arg.css" rel="stylesheet">



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <nav class="navbar navbar-top navbar-default">
      <div class="container">
        <div class="navbar-header">
            <a href="{base_url}" class="navbar-brand" style="padding: 0px;">
                <img alt="Premio Nacional a la Calidad" src="http://pnc.argentina.gob.ar/img/logos/logo_pnc.png" />
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
              <li  class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">AUTOEVALUACIÓN
                  <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li class="sub-li"><a href="{base_url}autoevaluacion/inscripcion">Inscripción</a></li>
                      <li class="sub-li"><a  href="{base_url}autoevaluacion/acceso">Acceso</a></li>
                    </ul>
                     </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Inscripciones
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li class="sub-li"><a href="{base_url}inscripcion/organismo/">Organismo</a></li>
                          <li class="sub-li"><a href="{base_url}inscripcion/evaluador">Evaluador</a></li>
                          <li class="sub-li"><a href="{base_url}inscripcion/juez">Juez</a></li>
                        </ul>
                    </li>

                <li><a href="{base_url}user/login">Ingresar</a></li>
                <li><a href="{base_url}user/register">Registrarse</a></li>

              </ul>
      </div>
    </nav>

    <main class="site-content">
      <div class="container">

        <!-- body -->
        {content}
        <!-- end body -->
    	   {modals}
        {alert}

      </div>
    </main>

    <!-- JS Global -->
    <script>
        //-----declare global vars
        var globals = {global_js};
    </script>

    <!-- JS custom -->
    {js}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- JS inline -->
    <script>
    {ignore}
    $(document).ready(function(){
      {inlineJS}
    });
    {/ignore}
    </script>

    </body>
    </html>
