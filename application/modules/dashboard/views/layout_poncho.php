<?php
/*
 *  Header: load CSS & navbar
 *
 */
include('_header_poncho.php')
?>
<main class="site-content">
  <div class="ui container">

    <!-- body -->
    {content}
    <!-- end body -->
	   {modals}
    {alert}

  </div>
</main>

<?php
/*
 *  Footer: Load scripts
 *
 */
include('_footer_poncho.php')
?>
