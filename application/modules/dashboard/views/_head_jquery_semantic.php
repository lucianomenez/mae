<html>
  <head>
    <!-- Standard Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>{title}</title>
    <link rel="icon" href="{base_url}dashboard/assets/img/favicon.png" type="image/png" sizes="32x32">
    <link rel="shortcut icon" href="{base_url}jscript/semantic/images/favicon.png" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <script src="{base_url}jscript/librerias/jquery_3-5/jquery-3.5.0.min.js"></script>

    <script src="{base_url}jscript/frameworks/semantic_2-4/semantic.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.js"></script>
    <script src="{base_url}jscript/plugins/semanticAlert/Semantic-UI-Alert.js"></script>
    
    <link rel="stylesheet" type="text/css" href="{base_url}jscript/frameworks/semantic_2-4/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.3/dist/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="{base_url}dashboard/assets/css/breakpoints.css">
      <link rel="stylesheet" type="text/css" href="{base_url}jscript/plugins/semanticAlert/Semantic-UI-Alert.css">
          <link rel="stylesheet" href="{base_url}dashboard/assets/css/modals.css">
    {custom_css}
  </head>
  <body class="site" style="/*background: #fefefe;*/">
<input type="hidden" value="{idu}" id="idu_input">