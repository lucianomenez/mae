  let regExp='^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})';

  $('.btn_contrasena').on('click', function (e) {
      $('#inputPassword').val('');
      $('#inputPasswordConfirmar').val('');
      $('#msg_contrasena').html('');
      $('#msg_contrasena_validar').html('');

      $('#contrasenaModal').modal({}).modal('show');
  });

    $('#cambiar_contrasena').on('click',function(e){
      $('#msg_contrasena').html('');
        e.preventDefault();
        var inputPassword = $("#inputPassword").val();
        var inputPasswordConfirmar = $("#inputPasswordConfirmar").val();
        if (!inputPassword.match(regExp)) {
          $('#msg_contrasena').html('La contraseña debe contener 8 dígitos que debe incluir números, símbolos y una combinación de letras mayúsculas y minúsculas ');
           $('#msg_contrasena_validar').html('');
        }else{
          if (inputPassword==inputPasswordConfirmar) {
              var data=new Array();
                var data={
                   passw:inputPassword,
                     idu:$('#idu_input').val()
                 }
               updatePass(data);
            $('#contrasenaModal').modal('toggle')
          }else if(inputPasswordConfirmar==''){
            $('#msg_contrasena').html('');
            $('#msg_contrasena_validar').html('No coinciden las contraseñas');
          }else{
            $('#msg_contrasena').html('');
             $('#msg_contrasena_validar').html('No coinciden las contraseñas');
          }

        }

      });


       function updatePass(data){
        // console.log(data.idu)
        //  console.log(data.passw)
	       $('#toast').removeClass('success');
	       	$('#toast').removeClass('error');

	       	$('#toast').addClass('success');
	        $('#toast').toast('show');

        // $('#toast').addClass('error');
        // $('#toast').toast('show');

        let url=globals.base_url+'user/profile/updatePass';

        $.ajax({
          url :url,
          type: 'POST',
          data: data,
          success: function(data){
          //   $.uiAlert({
          //         textHead: 'Los cambios fueron guardados con éxito',
          //         text: '',
          //         bgcolor: '#2FB537',
          //         textcolor: '#fff',
          //         position: 'top-right', // top And bottom ||  left / center / right
          //         icon: '',
          //         time: 3
          //         });
          },
          error: function(data){
                // $.uiAlert({
                //   textHead: 'Error en el guardado',
                //   text: '',
                //   bgcolor: '#ff0000',
                //   textcolor: '#fff',
                //   position: 'top-right', // top And bottom ||  left / center / right
                //   icon: '',
                //   time: 3
                //  });
          }
        });


        }
