 $(document).ready(function() {
  $('.ui.dropdown').dropdown();


   $('.btn_contrasena').click(function(e) {
		  $('#contrasenaModal').modal({
		       inverted: false,
		       closable : false,
		       onVisible: function () {
		         $('#passw').val('');
		         $('#passw2').val('');
		        },
		       onApprove: function () {
            $('#passw2').blur();
		          if ($('#contrasenaValidate').form('is valid')) {
		              var data=new Array();
		              var data={
		                 passw:$('#passw').val(),
                      idu:$('#idu_input').val()
		               }
		             updatePass(data);
		            return true;
		          } else {

		              return false;
		          }
		        }


		  }).modal('show');
  });

      $('#contrasenaValidate').form({
            inline : true,
             on     : 'blur' ,
           fields: {
                passw: {
                  identifier  : 'passw',
                  rules: [
                    {
                      type: 'regExp[^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})]',
                      prompt : 'La contraseña debe contener 8 dígitos que debe incluir números, símbolos y una combinación de letras mayúsculas y minúsculas '
                    }

                  ],
                },
                passw2: {
                  identifier  : 'passw2',
                  rules: [
                    {
                      type   : 'match[passw]',
                      prompt : 'Por favor ingresá nuevamente la contraseña elegida'
                    }
                  ],
                }
            }
        });
 });

       function updatePass(data){
        // console.log(data.idu);
        // console.log(data.passw);

        // alert();
        // return;
          		// $.uiAlert({
              //       textHead: 'Tu contraseña fue cambiada con éxito',
              //       text: '',
              //       bgcolor: '#2FB537',
              //       textcolor: '#fff',
              //       position: 'top-right', // top And bottom ||  left / center / right
              //       icon: '',
              //       time: 3
              //       });
              //
          		//  $.uiAlert({
              //       textHead: 'Error en el cambio de contraseña',
              //       text: '',
              //       bgcolor: '#ff0000',
              //       textcolor: '#fff',
              //       position: 'top-right', // top And bottom ||  left / center / right
              //       icon: '',
              //       time: 3
              //      });
          let url=globals.base_url+'user/profile/updatePass';

          $.ajax({
            url :url,
            type: 'POST',
            data: data,
            success: function(data){
          		$.uiAlert({
                    textHead: 'Los cambios fueron guardados con éxito',
                    text: '',
                    bgcolor: '#2FB537',
                    textcolor: '#fff',
                    position: 'top-right', // top And bottom ||  left / center / right
                    icon: '',
                    time: 3
                    });
            },
            error: function(data){
                  $.uiAlert({
                    textHead: 'Error en el guardado',
                    text: '',
                    bgcolor: '#ff0000',
                    textcolor: '#fff',
                    position: 'top-right', // top And bottom ||  left / center / right
                    icon: '',
                    time: 3
                   });
            }
          });


        }
