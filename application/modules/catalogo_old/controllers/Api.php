<?php

class api extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->model('Model_api');
      $this->idu = $this->user->idu;
      $this->usuario = $this->user->get_user($this->idu);

  }

    function get_organismos() {
      $data = $this->Model_api->get_organismos();
      foreach ($data as &$organismo){
        $organismo['servicios'] = $this->get_servicios($organismo['id']);
        // foreach ($organismo['servicios'] as &$servicio){
        // //  $servicio['textos'] = $this->get_textos($servicio['id']);
        // }
      }
      return $data;
      //$customData['usercan_create']=true;
    }

    function get_servicios($idrel) {
      $data = $this->Model_api->get_servicios($idrel);
      return $data;
      //$customData['usercan_create']=true;
    }

    function get_textos($idrel) {
      $data = $this->Model_api->get_textos($idrel);
      return $data;
      //$customData['usercan_create']=true;
    }


    function count_organismos(){
      $collection = 'container.servicios_esenciales_organismos';
      return $this->Model_api->count_collection($collection);
    }

    function count_servicios(){
      $collection = 'container.servicios_esenciales';
      return $this->Model_api->count_collection($collection);
    }

    function count_cargos_by_query($jurisdiccion = null, $unidad_de_nivel_politico = null ){
      $collection = 'container.mapa_del_estado_cargos';
      if ($jurisdiccion){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre !=' => null , 'jurisdiccion' => $jurisdiccion));
      }else{
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre !=' => null, 'jurisdiccion' => $jurisdiccion, 'unidad_de_nivel_politico' => $unidad_de_nivel_politico));
      }


      return $this->Model_api->count_collection_by_query($collection, $query);
    }

    function count_cargos_no_designados($jurisdiccion = null, $unidad_de_nivel_politico = null){
      $collection = 'container.mapa_del_estado_cargos';
      if ($jurisdiccion){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre' =>  null, 'jurisdiccion' => $jurisdiccion));
      }else if ($unidad_de_nivel_politico){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre' => null, 'jurisdiccion' => $jurisdiccion, 'unidad_de_nivel_politico' => $unidad_de_nivel_politico));
      }else{
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre' => null));
      }

    }

    function count_genero($param, $jurisdiccion = null, $unidad_de_nivel_politico = null){
      $collection = 'container.mapa_del_estado_cargos';
      if ($jurisdiccion){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_sexo' => $param, 'jurisdiccion' => $jurisdiccion));
      }else if ($unidad_de_nivel_politico){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_sexo' => $param, 'jurisdiccion' => $jurisdiccion, 'unidad_de_nivel_politico' => $unidad_de_nivel_politico));
      }else{
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_sexo' => $param));
      }
    }

    function count_ministerios_by_idrel($id) {
      $collection = 'container.mapa_del_estado_jurisdicciones';
      return $this->Model_api->count_collection_by_query($collection, array('idrel' => $id));
    }

    function count_field($field, $value){
      $collection = 'container.mapa_del_estado_cargos';
      return $this->Model_api->count_collection_by_query($collection, array($field => $value));
    }

} //

?>
