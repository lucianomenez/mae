<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Paneles de Navegación
 *
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 */
class Buscador extends MX_Controller {

    function __construct() {
        parent::__construct();
          $this->base_url = base_url();
          $this->module_url = base_url() . $this->router->fetch_module() . '/';
          $this->user->authorize();
          $this->load->helper('file');
          $this->load->helper('url');
          $this->load->model('app');
          $this->load->model('bpm/bpm');
          $this->load->model('Model_catalogo');
          $this->load->model('user/user');
          $this->load->library('parser');
          $this->load->config('config');
          $this->idu = $this->user->idu;
        }


      function index(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          Modules::run('dashboard/dashboard', 'catalogo/json/buscador.json',$debug, $extraData);
      }

      // Parse json

      function contenido(){
          $this->load->module("catalogo/api");
          $data['module_url'] = $this->module_url;
          //Seteo graficos de prueba
          echo $this->parser->parse('contenido_buscador',$data, true, true);
      }

      function process(){
        $query = $this->input->post('item');
        $data['resultados'] = $this->Model_catalogo->search($query);
        $data['count_resultados'] = Count($data['resultados']);
        $data = array("total_count"=>$total_count) + $data;
        echo $this->parser->parse('tabla_buscador',$data, true, true);
      }




}
