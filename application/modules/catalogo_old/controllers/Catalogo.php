<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Paneles de Navegación
 *
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 */
class Catalogo extends MX_Controller {

    function __construct() {
        parent::__construct();
          $this->base_url = base_url();
          $this->module_url = base_url() . $this->router->fetch_module() . '/';
          $this->user->authorize();
          $this->load->helper('file');
          $this->load->helper('url');
          $this->load->model('app');
          $this->load->model('bpm/bpm');
          $this->load->model('user/user');
          $this->load->library('parser');
          $this->load->config('config');
          $this->idu = $this->user->idu;
        }


      function index(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          Modules::run('dashboard/dashboard', 'catalogo/json/index.json',$debug, $extraData);
      }

      function ver_servicio($id){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          Modules::run('dashboard/dashboard', 'catalogo/json/ver_servicio.json',$debug, $extraData);
      }


      function content_header() {
          $data['base_url'] = $this->base_url;
          $data['title'] = "Catálogo de servicios esenciales";
          $data['nombre_breadcrumb'] = "General";
          echo $this->parser->parse('perfil/content_header', $data, true, true);
      }

      // Parse json

      function contenido(){
          $this->load->module("catalogo/api");
          $data['module_url'] = $this->module_url;
          $data['organismos'] = $this->api->get_organismos();
          //SETEO INDICADORES
          $data['count_organismos'] = $this->api->count_organismos();
          $data['count_servicios'] = $this->api->count_servicios();
          //Seteo graficos de prueba
          echo $this->parser->parse('contenido',$data, true, true);
      }

      function contenido_servicio(){
          $this->load->module("catalogo/api");
          $segments = $this->uri->segment_array();
          $data['module_url'] = $this->module_url;
          $data['count_organismos'] = $this->api->count_organismos();
          $data['count_servicios'] = $this->api->count_servicios();


          $data['textos'] = $this->api->get_textos($segments[3]);

          //SETEO INDICADORES
          //Seteo graficos de prueba
          echo $this->parser->parse('contenido_servicio',$data, true, true);
      }





}
