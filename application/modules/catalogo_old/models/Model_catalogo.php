<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_catalogo extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */


    function get_nombre($id){
      $query = array('id'=> $id);
      $this->db->where($query);
      $result = $this->db->get('container.mapa_del_estado_jurisdicciones')->result_array();
      return $result[0]['name'];
    }

    function search($s){
        $limit=999;
        $offset=0;
        $order_by='_id';
        $fields=array('text','servicio_nombre','organismo_nombre');
        $s=str_replace("%40", "@", $s);
        $container = 'container.servicios_esenciales_buscador';
        $result =$this->db
                ->select($fields)
                ->or_like('text', $s)
                ->or_like('servicio_nombre', $s)
                ->or_like('organismo_nombre', $s)
                ->order_by(array($order_by => 'DESC'))
                ->get($container,$limit, $offset)
                ->result_array();

        return $result;

  }

}
