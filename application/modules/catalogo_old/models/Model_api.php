<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_api extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */


    function get_organismos(){
        $this->db->where($query);
        $result = $this->db->get('container.servicios_esenciales_organismos')->result_array();
        return $result;
    }

    function get_servicios($idrel){
        $this->db->select(array("servicio_name", "servicio_id"));
        $query = array ('idrel' => $idrel);
        $this->db->where($query);
        $result = $this->db->get('container.servicios_esenciales')->result_array();
        return $result;
    }

    function get_textos($idrel){
        $query = array ('idrel_servicio' => $idrel);
        $this->db->where($query);
        $result = $this->db->get('container.servicios_esenciales_textos')->result_array();
        return $result;
    }



    function count_collection($collection){
      return $this->db->count_all($collection);
    }

    function count_collection_by_query($collection, $query, $like_field = "", $like_param = "", $like_method = "both"){

      if (($like_field != "") and ($like_param != "")){
        $this->db->like($like_field, $like_param, $like_method);
      }
      $this->db->where($query);
      return $this->db->count_all_results($collection);

    }





}
