l<div class="row">
  <div class="col-12">
    <h3 class="m-0 text-dark">Se encontraron {count_resultados} resultados</h3>  <!-- /.row -->
    {resultados}
    <div class="col-md-12">
      <div class="card card-outline card-success">
        <div class="card-header">
          <h3 class="card-title">{servicio_nombre}</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
            </button>
          </div>
          <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          {text}
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    {/resultados}
    <!-- /.card -->
  </div>
</div>
