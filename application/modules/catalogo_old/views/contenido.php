<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-success"><i class="fas fa-landmark"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Organismos Listados</span>
          <span class="info-box-number">{count_organismos}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-warning"><i class="fas fa-hand-holding"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Servicios Listados</span>
          <span class="info-box-number">{count_servicios}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>

  </div>

  <h3 class="m-0 text-dark">Ministerios</h3>  <!-- /.row -->
  <!-- Main row -->
        <div class="mt-4">
        {organismos}
        <div class="row">
           <div class="col-lg-12">
              <div class="card collapsed-card">
                <div class="card-header bg-gradient-primary">
                  <h3 class="card-title">{name}</h3>

                  <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                      <a href={module_url}nivel_2/index/{id}><button type="button" class="btn btn-tool"><i class="fas fa-eye white" style="background-color : #ffffff"></i></a>
                    </button>
                  </div>
                  <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body col-md-12" style="display: none;">

                              <div class="card card-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="card-footer">
                                  <ul class="nav flex-column mt-6">
                                    {servicios}
                                    <li class="nav-item">
                                      <a href="{module_url}ver_servicio/{servicio_id}" class="nav-link">
                                        {servicio_name}: <span class="float-right badge bg-primary"></span>
                                      </a>
                                    </li>
                                    {/servicios}
                                  </ul>
                                                                    <!-- /.row -->
                                </div>
                              </div>
                            </div>


                  <!-- /.col -->
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          {/organismos}
        </div>
