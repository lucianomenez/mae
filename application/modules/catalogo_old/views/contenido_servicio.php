<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-success"><i class="fas fa-landmark"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Organismos Listados</span>
          <span class="info-box-number">{count_organismos}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-6 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-warning"><i class="fas fa-hand-holding"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Servicios Listados</span>
          <span class="info-box-number">{count_servicios}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
  </div>

  </div>

  <h3 class="m-0 text-dark">Servicio {idrel_servicio}</h3>  <!-- /.row -->
  <!-- Main row -->
  <div class="card card-outline card-info">
    {textos}
    <p>{text}</p>
    {/textos}
  </div>
