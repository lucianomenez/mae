<div class="container-fluid">
<input class="form-control" id="module_url" type="hidden" value="{module_url}">

  <!-- Small boxes (Stat box) -->
  <div class="row">

    <div class="input-group md-form form-sm form-1 pl-0 mb-5">
      <div class="input-group-prepend">
        <span class="input-group-text pink lighten-3" id="basic-text1"><i class="fas fa-search text-white"
            aria-hidden="true"></i></span>
      </div>
      <input class="form-control my-0 py-1" id="item" value="" type="text" placeholder="Buscar servicios, organismos, trámites" aria-label="Search">
    </div>
    <button type="button" class="btn btn-block btn-outline-success mt-8" id="search_button" >Voy a tener suerte!</button>


  </div>


  <div id=result></div>
</div>
