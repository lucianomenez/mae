<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<?php echo $meta_tags; ?>
		<title>Integración DNA2BPM - REACTjs</title>
		<link href="prueba/assets/css/lib/bootstrap.min.css" rel="stylesheet">
		<link href="prueba/assets/css/style.css" rel="stylesheet">
		<script src="prueba/assets/js/lib/jspm-system.js"></script>
		<script src="prueba/assets/js/lib/browser.js"></script>
		<script>
			System.config({
			  transpiler: 'babel',
			  babelOptions: {}
			});
		</script>
	</head>
	<body>
		<div id="content">
			<?php echo $content; ?>
		</div>
		<script src="prueba/assets/js/lib/react.js"></script>
		<script src="prueba/assets/js/lib/react-dom.js"></script>
	</body>
</html>
