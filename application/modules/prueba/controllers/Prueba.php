<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prueba extends MX_React {

  function __construct() {
      parent::__construct();
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('user/user');
        $this->load->library('parser');
        $this->load->config('config');
        $this->idu = $this->user->idu;
      }

	public function index()
	{
		$this->render_view( 'lista' );
	}
}
