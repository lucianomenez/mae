<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Api extends MX_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->config('dashboard/config');
        //---base variables
        $this->base_url = base_url();
        $this->load->model('Model_prueba');
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
    }

    public function get_partidos(){
        $data = $this->Model_prueba->get_partidos();
        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }



}
