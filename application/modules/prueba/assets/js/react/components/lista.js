import AppContainer from "../AppContainer"

class Lista extends React.Component {
	constructor(props) {
		super(props);
		this.state= {
			api_module : "prueba/api/",
			partidos : [
			]
		}
	}

	componentDidMount(){
		console.log("entra");
		let promise = fetch (this.state.api_module + "get_partidos");
		promise.then(response  => response.json()).then(data => {
			this.setState({
				partidos : data
			})
		})
	}

	render() {
		return (
			< AppContainer>
			<div>
			{
				this.state.partidos.map ( (partidos) => {
					return <div className="card" style={{backgroundColor : 'blue'}}><p>{partidos.text}</p></div>
				})
			}
			</div>
			</AppContainer>
		);
	}
}

export default Lista
