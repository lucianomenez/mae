class Footer extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<footer>
				<div className="footer">

				<strong>Copyright &copy; 2020 Subsecretaría de Fortalecimiento Institucional.</strong>
				<div className="float-right d-none d-sm-inline-block">
				  <b>Version</b> 1.0.0
				</div>


				</div>
			</footer>
		);
	}
}

export default Footer
