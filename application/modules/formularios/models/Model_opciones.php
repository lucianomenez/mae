<?php



class Model_opciones extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
         $this->load->library('cimongo/Cimongo.php', '', 'db');
         //$this->dna3->switch_db('dna3');
    }




    function getopcion($idop){

      $query = $arrayName = array('idop' => $idop);
      $this->db->where($query);
      $result = $this->db->get("container.opciones")->result_array();
      unset($result[0]['_id']);
      return $result[0];


    }


    function saveopcion($opcion){

      $query = $arrayName = array('idop' => $opcion['idop']);
      $this->db->where($query);
      $result = $this->db->update("container.opciones",$opcion);


    }


}
