<?php



class Model_formularios extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
         $this->load->library('cimongo/Cimongo.php', '', 'mae');
         $this->load->model('Model_genid');
         //$this->dna3->switch_db('dna3');
    }

    function getform($idform){

      $query = array('idform' => $idform);
      $this->db->where($query);
      $result = $this->db->get("container.formularios")->result_array();
      unset($result[0]['_id']);
      return $result[0];


    }


    function save_formulario($formulario){
      //$array_$formulario = (array)$formulario;

      if($formulario['idform']){
        $query =  array('idform' => $formulario['idform']);
        $this->db->where($query);
        $result = $this->db->update("container.formularios",$formulario);
      }else{
        $formulario['idform'] = $this->Model_genid->genid('idform','container.formularios');
        //return $formulario;
        $query = array('idform' => $formulario['idform']);
        $this->db->where($query);
        $this->db->update('container.formularios', $formulario);
      }


    }

}
