<?php



class Model_preguntas extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
         $this->load->library('cimongo/Cimongo.php', '', 'mae');
         //$this->dna3->switch_db('dna3');
    }

    function getpregunta($idpreg){

      $query = $arrayName = array('idpreg' => $idpreg);
      $this->db->where($query);
      $result = $this->db->get("container.preguntas")->result_array();
      unset($result[0]['_id']);
      return $result[0];


    }

    function savepregunta($pregunta){

      $query = $arrayName = array('idpreg' => $pregunta['idpreg']);
      $this->db->where($query);
      $result = $this->db->update("container.preguntas",$pregunta);


    }

}
