<?php



class Model_form_load extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
         $this->load->library('cimongo/Cimongo.php', '', 'mae');
         //$this->dna3->switch_db('dna3');
         $this->load->model('Model_genid');
    }

    function getform_load($idformload){

      $query = $arrayName = array('idformload' => $idformload);
      $this->db->where($query);
      $result = $this->db->get("container.form_load")->result_array();
      unset($result[0]['_id']);
      return $result[0];


    }

    function save_form_load($formulario){
      //$array_$formulario = (array)$formulario;

      if($formulario['idformload']){
        $query =  array('idformload' => $formulario['idformload']);
        $this->db->where($query);
        $result = $this->db->update("container.form_load",$formulario);
      }else{
        $formulario['idformload'] = $this->Model_genid->genid('idformload','container.form_load');
        //return $formulario;
        $query = array('idformload' => $formulario['idformload']);
        $this->db->where($query);
        $this->db->update('container.form_load', $formulario);
      }


    }

}
