 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');




class Forms_render extends MX_Controller {

    function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->load->library('parser');
      $this->load->model('Model_genid');
      $this->load->model('Model_formularios');
      $this->load->model('Model_form_load');
      $this->load->model('Model_preguntas');
      $this->load->model('Model_opciones');
      //$this->user->authorize();


      //ini_set('max_execution_time',0);
    }

    function prueba_render(){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      Modules::run('dashboard/dashboard', 'formularios/json/prueba_render.json',$debug, $data);
    }

    function view_prueba_render(){
      $data['base_url'] = $this->base_url;
      $data['formulario'] = $this->form_render($idform = 1);
      echo $this->parser->parse('prueba_render', $data, true, true);
    }

    function form_render($idform = 0){
      //floatval($idform);
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $form = $this->Model_formularios->getform($idform);
      $html_form = '<input id="idform" name="idform" type="hidden" value="'.$idform.'">';

      if($form['preguntas']){
        foreach($form['preguntas'] as &$pregunta){
          $preg = $this->Model_preguntas->getpregunta($pregunta['idpreg']);
          $pregunta = array_merge($pregunta, $preg);
          //echo($pregunta['type']);
          if($pregunta['required'] == true){
            $requerido = 'required';
          }else{
            $requerido = '';
          }
          switch($pregunta['type']){
            case "text":
              //var_dump($pregunta['type']);
              $html_form = $html_form.
              '<div class="'.$requerido.' field mt2">
                <label>'.$pregunta['label'].'</label>
                <input type="text"  class="" validate="validate" id="'.$pregunta['idpreg'].'" placeholder="'.$pregunta['placeholder'].'"  value="'.$pregunta['value'].'" name="'.$pregunta['name'].'" validate-prompt="'.$pregunta['description'].'" validate-type="empty">
              </div>';
              break;
            case "hidden":
              //var_dump($pregunta['type']);
                $html_form = $html_form.
                '<div class="'.$requerido.' field mt2">
                  <label>'.$pregunta['label'].'</label>
                  <input type="hidden"  class="" validate="validate" id="'.$pregunta['idpreg'].'" placeholder="'.$pregunta['placeholder'].'"  value="'.$pregunta['value'].'" name="'.$pregunta['name'].'" validate-prompt="'.$pregunta['description'].'" validate-type="empty">
                </div>';
                break;
            case "number":
                //var_dump($pregunta['type']);
                $html_form = $html_form.
                '<div class="'.$requerido.' field mt2">
                  <label>'.$pregunta['label'].'</label>
                  <input type="number"  class="" validate="validate" id="'.$pregunta['idpreg'].'" placeholder="'.$pregunta['placeholder'].'"  value="'.$pregunta['value'].'" name="'.$pregunta['name'].'" validate-prompt="'.$pregunta['description'].'" validate-type="empty">
                </div>';
                break;
            case "date":
                    //var_dump($pregunta['type']);
                    $html_form = $html_form.
                    '<div class="'.$requerido.' field mt2">
                      <label>'.$pregunta['label'].'</label>
                      <input type="text"  class="" validate="validate" id="'.$pregunta['idpreg'].'" placeholder="'.$pregunta['placeholder'].'"  value="'.$pregunta['value'].'" name="'.$pregunta['name'].'" validate-prompt="'.$pregunta['description'].'" validate-type="empty">
                    </div>';
                    break;
            case "select":
                    //var_dump($pregunta['type']);
                    $html_form = $html_form.
                    '<div class=" '.$requerido.' field  mt2">
                      <label>'.$pregunta['label'].'</label>
                      <select class="ui search dropdown" validate="validate" id="'.$pregunta['idpreg'].'" name="'.$pregunta['name'].'" validate-prompt='.$pregunta['description'].' validate-type="empty">
                    ';
                    foreach ($pregunta['values'] as $valor) {
                      $html_form = $html_form.'<option value="'.$valor['value'].'">'.$valor['value'].'</option>';
                    }
                    $html_form = $html_form.'</select></div>';
                    break;
              case "textarea":
                      //var_dump($pregunta['type']);
                      $html_form = $html_form.
                      '<div class="'.$requerido.' field mt2">
                        <label>'.$pregunta['label'].'</label>
                        <textarea type="text"  class="" validate="validate" id="'.$pregunta['idpreg'].'" maxlength="'.$pregunta['maxlength'].'" placeholder="'.$pregunta['placeholder'].'"  value="" name="'.$pregunta['name'].'" validate-prompt="'.$pregunta['description'].'" validate-type="empty">
                        </textarea>
                      </div>';
                    break;
              case "radio-group":
                      //var_dump($pregunta['type']);
                      $html_form = $html_form.
                      '<div class=" '.$requerido.' field  mt2">
                        <label>'.$pregunta['label'].'</label>
                      ';
                      foreach ($pregunta['values'] as $valor) {
                        $html_form = $html_form.'
                        <input type="radio" id="'.$valor['idop'].'" name="'.$pregunta['label'].'" value="'.$valor['value'].'"><label for="'.$pregunta['label'].'">'.$valor['value'].'</label>
                        ';
                      }
                      $html_form = $html_form.'</div>';
                      break;
          }

          if($pregunta['values']){
            foreach($pregunta['values'] as &$opcion){
              $opt = $this->Model_opciones->getopcion($opcion['idop']);
              $opcion = array_merge($opcion, $opt);
            }

          }





        }

      }

      return (($html_form));
      //return(json_encode($form));

    }



    function saveform_load($formulario){
      $form_load = json_decode($formulario,true);
      $form = $this->Model_form_load->save_form_load($form_load);
      //echo($form);
    }

}
