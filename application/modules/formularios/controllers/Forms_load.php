 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');




class Forms_load extends MX_Controller {

    function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->load->library('parser');
      $this->load->model('Model_genid');
      $this->load->model('Model_formularios');
      $this->load->model('Model_form_load');
      $this->load->model('Model_preguntas');
      $this->load->model('Model_opciones');
      //$this->user->authorize();


      //ini_set('max_execution_time',0);
    }



    function getform_load($idformload = 0){
      $idformload = floatval($idformload);
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $form = $this->Model_form_load->getform_load($idformload);

      echo (json_encode($form));
      //return(json_encode($form));


    }



    function saveform_load($formulario){
      $form_load = json_decode($formulario,true);
      $form = $this->Model_form_load->save_form_load($form_load);
      //echo($form);
    }

}
