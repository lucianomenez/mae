 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');




class Formularios extends MX_Controller {

    function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->load->library('parser');
      $this->load->model('Model_genid');
      $this->load->model('Model_formularios');
      $this->load->model('Model_form_load');
      $this->load->model('Model_preguntas');
      $this->load->model('Model_opciones');
      //$this->user->authorize();


      //ini_set('max_execution_time',0);
    }

    function new_idform(){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $id = $this->Model_genid->genid('idform','container.formularios');
      //var_dump($id);
      //echo $this->parser->parse('calculadora', $data, true, true);
      //Modules::run('dashboard/dashboard', 'prestamos/json/sadaic.json',$debug, $data);
    }

    function get_form($idform = 0){
      $idform = floatval($idform);
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $form = $this->Model_formularios->getform($idform);

      if($form['preguntas']){
        foreach($form['preguntas'] as &$pregunta){
          $preg = $this->Model_preguntas->getpregunta($pregunta['idpreg']);
          $pregunta = array_merge($pregunta, $preg);
          //var_dump($preg);
          if($pregunta['options']){
            foreach($pregunta['options'] as &$opcion){
              $opt = $this->Model_opciones->getopcion($opcion['idop']);
              $opcion = array_merge($opcion, $opt);
            }

          }



        }

      }

      echo (json_encode($form));
      //return(json_encode($form));


    }

    function prueba(){
      $formulario = $_POST['formulario'];
      $form_array = json_decode($formulario,true);
      var_dump($form_array);
      echo json_encode('todo bien!');

    }



    function save_form($form=0){
      $form = $_POST['formulario'];
      $form_array = json_decode($form,true);

      //var_dump($form);
      $formulario['idform'] = $this->Model_genid->genid('idform','container.formularios');
      $i = 0;
      foreach ($form_array as &$pregunta) {
        if(!$pregunta['idpreg']){
          $pregunta['idpreg'] = $this->Model_genid->genid('idpreg','container.preguntas');
        }
        if($pregunta['values']){
          foreach ($pregunta['values'] as &$opcion) {
            if(!$opcion['idop']){
              $opcion['idop'] = $this->Model_genid->genid('idop','container.opciones');
            }
            $this->Model_opciones->saveopcion($opcion);

          }
        }
        $this->Model_preguntas->savepregunta($pregunta);
        $formulario['preguntas'][$i]['idpreg'] = $pregunta['idpreg'];
        $i++;
      }
      $this->Model_formularios->save_formulario($formulario);
      //$form = $this->Model_formularios->save_formulario($form);
      //echo($form);
    }

}
