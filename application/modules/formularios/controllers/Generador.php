 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');




class Generador extends MX_Controller {

    function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->load->library('parser');
      $this->load->model('Model_genid');
      $this->load->model('Model_preguntas');
      //$this->user->authorize();


      //ini_set('max_execution_time',0);
    }

    // function index(){
    //   $data['base_url'] = $this->base_url;
    //   $data['module_url'] = $this->module_url;
    //   $id = $this->Model_genid->genid('idpreg','container.preguntas');
    //   var_dump($id);
    //   //echo $this->parser->parse('calculadora', $data, true, true);
    //   //Modules::run('dashboard/dashboard', 'prestamos/json/sadaic.json',$debug, $data);
    // }

    function generador(){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      Modules::run('dashboard/dashboard', 'formularios/json/generador.json',$debug, $data);
    }

    function view_generador(){

       $data['base_url'] = $this->base_url;
       echo $this->parser->parse('generador', $data, true, true);
    }

}
