 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');




class Monitor extends MX_Controller {

    function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->load->library('parser');
      $this->idwf = "accion_estatal";
      $this->load->model('Model_formularios');
      $this->load->model('Model_matriz');
      $this->load->model('bpm/bpm');
      $this->load->model('options');
      $this->load->model('user/user');
      $this->idu = $this->user->idu;
      $this->usuario = $this->user->get_user($this->idu);
      $this->ano = 2020;
      //$this->user->authorize();
      error_reporting(E_ERROR | E_PARSE);
      //ini_set('max_execution_time',0);
    }

    function view_ficha(){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $segments = $this->uri->segment_array();
      $id = (isset($segments[4]))?$segments[4]:'';
      $data = $this->Model_matriz->get_casos(null,1,$id);
      echo $this->parser->parse('contenido_matriz_relevamiento_ficha',$data[0], true, true);
    }



    function ficha($id){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      Modules::run('dashboard/dashboard', 'formularios/json/ficha.json',$debug, $data);
    }

    function view_monitor() {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['idwf'] = $this->idwf;
        $data['acciones_estatales'] = $this->Model_matriz->get_acciones_estatales();
        foreach ($data['acciones_estatales'] as &$accion){
          $camboyano = $this->user->get_user($accion['iduser']);
          $accion['camboyano'] = $camboyano->name." ".$camboyano->lastname;
          (isset($accion['ano'])) ? $accion['estado'] = '<a class="ui green label">Fase 1<br/>Completa</a>' : $accion['estado'] = '<a class="ui orange label">Fase 1<br/>Pendiente</a>';
        }
        echo $this->parser->parse('tabla_monitor', $data, true, true);
    }



}
