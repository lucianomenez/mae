 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');




class Matriz_de_relevamiento extends MX_Controller {

    function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->load->library('parser');
      $this->idwf = "accion_estatal";
      $this->load->model('Model_formularios');
      $this->load->model('Model_matriz');
      $this->load->model('bpm/bpm');
      $this->load->model('options');
      $this->load->model('user/user');
      $this->idu = $this->user->idu;
      $this->usuario = $this->user->get_user($this->idu);


      $this->ano = 2020;
      //$this->user->authorize();
      error_reporting(E_ERROR | E_PARSE);



      //ini_set('max_execution_time',0);
    }


    function monitor(){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      Modules::run('dashboard/dashboard', 'formularios/json/monitor.json',$debug, $data);
    }



    function fase1(){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      Modules::run('dashboard/dashboard', 'formularios/json/matriz_de_relevamiento.json',$debug, $data);
    }

    function go() {
        $data['base_url'] = $this->base_url;
        $data['idwf'] = $this->idwf;
        echo $this->parser->parse('sidebar_matriz_relevamiento', $data, true, true);
    }

    function sidebar() {
        $data['base_url'] = $this->base_url;
        $acciones_estatales = $this->Model_matriz->get_casos($this->idu);
        $data['iduser'] = $this->idu;
        $data['organismo'] = $this->usuario->organismo_name;
        $data['jurisdiccion'] = $this->usuario->jurisdiccion_name;

        (count($acciones_estatales) > 0 ) ? $data['tiene_acciones'] = 'true' : $data['tiene_acciones'] = false;

        if ($data['tiene_acciones'] == false){
          $caso_default = $this->newcase();
          $acciones_estatales[0]= $caso_default;
        }

        $acciones_estatales[0]['indice'] = 1;
        $acciones_estatales[0]['active']='active';

        $data_parse['form_acciones']='';
        foreach ($acciones_estatales as $key=> $accion){
          if ($accion['id']) {
            $accion['token'] = $this->bpm->get_last_token($this->idwf, $accion['id']);
            $accion['token'] = $accion['token']['_id']->{'$id'};
            $accion['indice'] = $key+1;
            if ($accion['accion_estatal'] == null||$accion['accion_estatal'] == '')
              { $accion['accion_estatal_title'] = "Nueva Acción Estatal";
            }else{
               $accion['accion_estatal_title'] = $accion['accion_estatal'];
            }
            $data['form_acciones'].=$this->parser->parse('acciones_form', $accion, true, true);
          }
        }

        $data['idcase']=$acciones_estatales[0]['id'];
        $data['modals']= $this->parser->parse('modales', $data, true, true);

        echo $this->parser->parse('sidebar_matriz_relevamiento', $data, true, true);
    }

    function contenido() {
        $data['base_url'] = $this->base_url;
        $data_parse= $this->Model_matriz->get_casos($this->idu,1)[0];
        $data_parse['base_url'] = $this->base_url;

        //acomodo token
        $accion['token'] = $this->bpm->get_last_token($this->idwf, $data_parse['id']);
        $data_parse['token'] = $accion['token']['_id']->{'$id'};

        $container= 'container.mapa_del_estado_jurisdicciones';
        $data_parse['organismos_list']= $this->options->get_options($container);

        if ($data_parse['enviado']=='enviado'){
          $data_parse['vinculacion_list']=array();
          if (is_array($data_parse['vinculacion'])) {
            $container= 'container.mapa_del_estado_jurisdicciones';
           foreach ($data_parse['vinculacion']as $key_vinculacion => $value_vinculacion) {
             if ($value_vinculacion!='') {
               $data_parse['vinculacion_list'][]['text']= $this->options->get_options($container,$value_vinculacion)[0]['name'];
             }
           }
          }
            $data_parse['organismos_list']= $this->options->get_options($container);

            if (trim($data_parse['plan'])=='' ) {
              $data_parse['plan']='No especificado';
            }

          $data_parse['archivos']=array();
          if (!empty($data_parse['informacion_complementaria'])){

            foreach ($data_parse['informacion_complementaria'] as &$archivo){
                   if ($archivo['url']!='') {
                      $archivo['url'] = $this->base_url . $archivo['url'];
                       $archivo['type_icon']='';
                        $is_image = strpos($archivo['file_extension'], 'image');
                         if ($is_image !== false) {
                              $archivo['type_icon']='<i class="image outline icon"></i>';
                         }else{
                          $is_pdf= strpos($archivo['file_extension'], 'pdf');
                           if ($is_pdf !== false) {
                             $archivo['type_icon']=' <i class="file pdf outline icon"></i>';
                           }
                         }

                      $data_parse['archivos'][]=$archivo;
                    }
            };
          }
          $data['form_accion']=$this->parser->parse('contenido_matriz_relevamiento_ficha', $data_parse, true, true);



        }else{
          $data['form_accion']=$this->parser->parse('render_form', $data_parse, true, true);
        }

        echo $this->parser->parse('contenido_matriz_relevamiento', $data, true, true);
    }

    function get_form($echo='true') {
        $post=$this->input->post();
        $this->load->helper('file');
        $idcase=$post['id'];
        $data_parse= $this->Model_matriz->get_casos($this->idu,1,$idcase)[0];
        $data_parse['base_url'] = $this->base_url;
        $container= 'container.mapa_del_estado_jurisdicciones';
        $data_parse['organismos_list']= $this->options->get_options($container);


            if ($data_parse['enviado']=='enviado'){
              $data_parse['vinculacion_list']=array();
              if (is_array($data_parse['vinculacion'])) {
                $container= 'container.mapa_del_estado_jurisdicciones';
               foreach ($data_parse['vinculacion']as $key_vinculacion => $value_vinculacion) {
                 if ($value_vinculacion!='') {
                   $data_parse['vinculacion_list'][]['text']= $this->options->get_options($container,$value_vinculacion)[0]['name'];
                 }
               }
              }
              if (trim($data_parse['plan'])=='' ) {
                $data_parse['plan']='No especificado';
              }
               $data_parse['archivos']=array();
              if (!empty($data_parse['informacion_complementaria'])){
                foreach ($data_parse['informacion_complementaria'] as &$archivo){
                    if ($archivo['url']!='') {
                       $archivo['url'] = $this->base_url . $archivo['url'];
                        $archivo['type_icon']='';
                        $is_image = strpos($archivo['file_extension'], 'image');
                         if ($is_image !== false) {
                              $archivo['type_icon']='<i class="image outline icon"></i>';
                         }else{
                          $is_pdf= strpos($archivo['file_extension'], 'pdf');
                           if ($is_pdf !== false) {
                             $archivo['type_icon']=' <i class="file pdf outline icon"></i>';
                           }
                         }


                       $data_parse['archivos'][]=$archivo;
                    }

                };
              }

              $form['data']='ficha';
              if ($echo=='true') {
                $form['form']=$this->parser->parse('contenido_matriz_relevamiento_ficha', $data_parse, true, true);
              }
            }else{
              $form['data']=$data_parse;
              if ($echo=='true') {
                $form['form']=$this->parser->parse('render_form', $data_parse, true, true);
              }

            }

        echo json_encode($form);

    }


    function newcase($ajax = null){
        $this->load->module('bpm/engine');
        $case = $this->engine->Newcase('model', 'accion_estatal', false, null, true);
        if ($ajax == 'true'){
          $case['base_url']= $this->base_url;
          $case['accion_estatal_title'] = "Nueva Acción Estatal";
          $case['token'] = $this->bpm->get_last_token($this->idwf, $case['id']);
          $case['token'] = $accion['token']['_id']->{'$id'};
          $data['sidebar']=$this->parser->parse('acciones_form', $case, true, true);
          $container= 'container.mapa_del_estado_jurisdicciones';
          $case['organismos_list']= $this->options->get_options($container);
          $data['form']=$this->parser->parse('render_form', $case, true, true);
          $data['id']=$case['id'];
          echo json_encode($data);
        }else{
          return $case;
        }
    }

    function solo_guardar(){
      $data = $this->input->post();
      $data['enviado'] ='no_enviado';
      $this->Model_matriz->update_caso($data, $this->idwf);
      return;
    }
    function eliminar(){
      $data = $this->input->post();
      $this->Model_matriz->borrar_caso($data, $this->idwf);
      return;
    }

    function avanza_bpm(){
      $data = $this->input->post();
      $data['organismo'] = $this->usuario->organismo_name;
      $data['jurisdiccion'] = $this->usuario->jurisdiccion_name;
      $data['ano'] = $this->ano;
      $data['enviado'] ='enviado';
      //SETEO EL UPLOAD PATH
      $data['upload_path'] = FCPATH.'application/modules/files/assets/user_files/'.$this->idu.'/'.$this->idwf.'/'.$data_post['id'].'/';

      //GUARDO LOS DATOS DE LA INSCRIPCION
      $this->Model_matriz->update_caso($data,$this->idwf);

      if($data['token']<>'' && $data['id']<>''){
              $token=$this->bpm->get_token_byid($data['token']);
              $resourceId=$token['resourceId'];
              $idwf=$token['idwf'];
              $idcase=$token['case'];
              redirect($this->base_url."bpm/engine/run_post/model/$idwf/$idcase/$resourceId");
          }
       else {
         echo "ERROR EN EL MOTOR DEL BPM";
       }

    }

    function upload_file(){
          $data_post = $this->input->post();
          $data_file = $_FILES['file'];
          $data_image= getimagesize ($data_file['tmp_name']);

          $file_extension = pathinfo($data_file['name'], PATHINFO_EXTENSION);

          // Make dir
          if (!file_exists(FCPATH.'application/modules/files/assets/user_files/'.$this->idu.'/'.$this->idwf.'/'.$data_post['idcase'])){
               @mkdir(FCPATH.'application/modules/files/assets/user_files/'.$this->idu.'/'.$this->idwf.'/'.$data_post['idcase'].'/',0777,true);
          }

          // File configuration
          $config['file_name']            = $data_post['idcase'] ."-" .$data_post['data-filename'];
          $config['upload_path']          = FCPATH.'application/modules/files/assets/user_files/'.$this->idu.'/'.$this->idwf.'/'.$data_post['idcase'].'/';
          $config['allowed_types']        = "gif|jpg|png|doc|DOC|docx|DOCX|jpeg|txt|TXT|JPG|PNG|JPEG|pdf|PDF|mp3|MP3|mp4|MP4|";
          $config['max_size']            = 10000;
          $config['overwrite']           = false;

          // Upload
          $this->load->library('upload', $config);

          // Set response data
          $response['uploadUrl'] = 'files/assets/user_files/'.$this->idu.'/'.$this->idwf.'/'.$data_post['idcase'].'/'.$config['file_name'].'.'.$file_extension;

          $response['extension']=$file_extension;
          $response['file_extension']=$data_file['type'];
          $response['width']=$data_image[0];
          $response['height']=$data_image[1];
          $response['size']=$data_file['size'];
          $response['original_name']=$data_file['name'];
           $response['name']=  $config['file_name'].'.'.$file_extension;

          if ( ! $this->upload->do_upload('file'))
          {
              $error = array('error' => $this->upload->display_errors());
              var_dump($error, $config['upload_path']);
          }
          else
          {
              $data = array('upload_data' => $this->upload->data());
              $data['upload_path'] = $config['upload_path'];
              echo json_encode($response);
          }
        }


            function delete_file(){
              $this->load->helper('file');
               $response='false';
                $data_post = $this->input->post();
                $path = FCPATH.'application/modules/files/assets/user_files/'.$this->idu.'/'.$this->idwf.'/'.$data_post['idcase'].'/';
                $files = $path.$data_post['filename'];

                if (unlink($files)) {
                    $response='true';
                }


                echo json_encode($response);


            }


}
