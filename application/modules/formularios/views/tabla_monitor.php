<br />
<div id="">
<table class="ui fixed selectable basic table" id="myTable">
  <thead>
    <tr>
      <th class="two wide center aligned">Apellido, Nombre</th>
      <th class="two wide center aligned">Acción Estatal</th>
      <th class="one wide center aligned">Tipo</th>
      <th class="five wide center aligned">Objetivo Organizacional</th>
      <th class="one wide center aligned">Ministerio</th>
      <th class="one wide center aligned">Organismo</th>
      <th class="two wide center aligned">Estado</th>
      <th class="one wide center aligned">Ficha</th>
    </tr>
  </thead>
  <tbody>
    {acciones_estatales}
	<tr>
		<td>{camboyano}</td>
		<td>{accion_estatal}</td>
		<td>{tipo_accion_estatal}</td>
    <td>{objetivo_organizacional}</td>
		<td>{jurisdiccion}</td>
    <td>{organismo}</td>
    <td>{estado}</td>
		<td><a href="{module_url}monitor/ficha/{id}">Ver Ficha</a></td>
	</tr>
    {/acciones_estatales}
  </tbody>
</table>

</div>
