
<div class="ui basic segment">
  <div class="ui right aligned container">
    <button class="ui btn-send button pl2 pr2" id="request">Solicitar Modificación</button>
  </div>
  <form class="ui form mt1" id="form_contenido">

    <div class="field mt2">
      <div class="label-accion bold">{accion_estatal}</div>
    </div>

    <div class="stage-big-title mt3">
      Características de la política
    </div>
    <div class="ui divider"></div>

    <div class="field mt2">
      <label>Tipo de acción estatal</label>
      <div class="contenido-campo mt1">{tipo_accion_estatal}</div>
    </div>

    <div class="field mt2">
      <label>Objetivo organizacional</label>
      <div class="contenido-campo mt1">{objetivo_organizacional}</div>
    </div>

    <div class="field mt2">
      <label>Unidad de medida</label>
      <div class="contenido-campo mt1">{unidad_medida}</div>
    </div>

    <div class="field mt2">
      <label>Programa</label>
      <div class="contenido-campo mt1">{programa}</div>
    </div>


    <div class="field mt2">
      <label>Plan</label>
      <div class="contenido-campo mt1">{plan}</div>
    </div>

    <!-- Vinculación solo aparece si el campo anterior (plan) está completo / tiene contenido -->

    {if {plan}!='No especificado'}
        <div class="field mt2">
          <label>Vinculación con otro organismo</label>
          <div class="contenido-campo mt1">
            <ul>
              {vinculacion_list}
                  <li>{text}</li>
               {/vinculacion_list}
            </ul>
          </div>
        </div>
    {/if}
    <div class="field mt2">
      <label>Área responsable</label>
      <div class="contenido-campo mt1">{area_responsable}</div>
    </div>

    <div class="field mt2">
      <label>Destinatarios</label>
      <div class="contenido-campo mt1">
        <ul>
          {destinatarios}
            <li>{text}</li>
          {/destinatarios}
        </ul>
    </div>
    </div>

    <div class="field mt2">
      <label>Presupuesto</label>
      <div class="contenido-campo mt1">$ {presupuesto}</div>
    </div>

    <div class="field mt2">
      <label>Marco normativo</label>
      <div class="contenido-campo mt1">{marco_normativo}</div>
    </div>

    <!-- Información complementaria es dropzone -->
    <div class="field mt2">
      <label>Información complementaria</label>

      <div class="mt1 mb2">
        {archivos}
        <a href="{url}" download="{original_name}"><button type="button" class="ui button inverted primary mt1">{type_icon}{original_name}</button></a>
        {/archivos}
      </div>


    </div>

  </form>
</div>
