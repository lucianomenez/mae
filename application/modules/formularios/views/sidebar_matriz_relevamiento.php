  <!-- SOLO PARA ROL MAE -->
<input type="hidden" id="base_url" value="{base_url}">
<input type="hidden" value="{cant_rows}" id="cant_rows">
<input type="hidden" id="iduser" value="{iduser}">
<input type="hidden" id="idcase" value="{idcase}">

<div class="ui container">
  <form class="ui form" id="form_sidebar">
      <div class=" field">
        <label>Ministerio</label>
          <input type="text" id="jurisdiccion" name="jurisdiccion" value="{jurisdiccion}" disabled>
      </div>
      <div class=" field">
        <label>Organismo</label>
        <input type="text" id="organismo" name="organismo" value="{organismo}" disabled>
      </div>
  </form>
</div>
<div class="ui segment segment-base mt2">
  <!-- SOLO PARA ROL ORGANISMO -->
  <!-- <div class="ui container mb2">
    <div class="title-ministerio">
      Ministerio
    </div>
    <div class="title-organismo">
      Organismo
    </div>
  </div> -->
  <div class="ui container mt1">
    <div class="ui comments" id="append">
        {form_acciones}

    <!-- <div class="comment pointer" id="append">
          <a class="avatar mr2">
            <div class="stage-circle" data-row="1"><p>{indice}</p></div>
          </a>
          <div class="content" >
            <div class="ui radio checkbox">
              <input type="radio" name="nav_forms" id={id} tabindex="0" class="hidden ui_manager">
            </div>
            <a class="stage-name active"> {accion_estatal}</a>
            <div class="stage-status">Sin datos</div>
            <input type="hidden" class="token" name="token[]" value="{token}">
            <input type="hidden" class="idcase" name="idcase[]" value="{id}">
          </div>
        </div> -->
      <!-- <a class="pointer">  <h4 class="ui header"><i class="plus red trash icon"></i> Eliminar</h4></a> -->
<!-- Cuando se completa la acción -->
<!--       <div class="comment pointer mt1">
        <a class="avatar mr2">
          <div class="stage-circle active"><p><i class="check icon m0"></i></p></div>
        </a>
        <div class="content">
          <a class="stage-name">Acción estatal</a>
          <div class="stage-status">Completo</div>
        </div>
      </div> -->
<!-- Cuando se completa la acción -->
    </div>
  </div>
  <div class="ui container mt2">
    <div class="ui divider mb2"></div>
    <a class="pointer" id="add_accion_estatal"><h4 class="ui header"><i class="plus green icon"></i> Nueva acción estatal </h4></a>
    </a>
  </div>

</div>

{modals}
