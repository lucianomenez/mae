<div id="main-content-wrap" class="outer">
        <div class="animated-taglines">
          <h2 class="tagline">
            <span>Drag</span>
            <span>and</span>
            <span>Drop</span>
          </h2>
          <h2 class="tagline full-featured"><strong>Full-featured</strong> Form Editing</h2>
        </div>
        <section id="main_content" class="inner">
          <div id="lang-select-wrap" class="pull-right">
            <select id="setLanguage" class="form-control">

              <option value="ar-SA">
                سعودي
              </option>

              <option value="ar-TN">
                تونسي
              </option>

              <option value="cs-CZ">
                čeština
              </option>

              <option value="da-DK">
                Dansk
              </option>

              <option value="de-DE">
                Deutsch
              </option>

              <option value="en-US">
                English (US)
              </option>

              <option value="es-ES">
                español
              </option>

              <option value="fa-IR">
                فارسى
              </option>

              <option value="fi-FI">
                suomen
              </option>

              <option value="fr-FR">
                français
              </option>

              <option value="he-IL">
                עברית‬
              </option>

              <option value="hu-HU">
                magyar
              </option>

              <option value="it-IT">
                italiano
              </option>

              <option value="ja-JP">
                日本語
              </option>

              <option value="my-MM">
                ဗမာစကာ
              </option>

              <option value="nb-NO">
                norsk
              </option>

              <option value="nl-NL">
                Nederlands
              </option>

              <option value="pl-PL">
                Polska
              </option>

              <option value="pt-BR">
                português
              </option>

              <option value="qz-MM">
                ဗမာစကာ
              </option>

              <option value="ro-RO">
                român
              </option>

              <option value="ru-RU">
                русский язык
              </option>

              <option value="sl-SI">
                Slovenščina (SI)
              </option>

              <option value="tr-TR">
                Türkçe
              </option>

              <option value="uk-UA">
                українська мова
              </option>

              <option value="vi-VN">
                tiếng Việt
              </option>

              <option value="zh-CN">
                简化字
              </option>

              <option value="zh-TW">
                台語
              </option>

            </select>
          </div>
          <div class="build-form">
            <div id="frmb-1589212423887-form-wrap" class="form-wrap form-builder ">
              <ul id="frmb-1589212423887" class="frmb stage-wrap pull-left ui-sortable empty" data-content="Drag a field from the right to this area" style="min-height: 508px;">
              </ul>
            <div id="frmb-1589212423887-cb-wrap" class="cb-wrap pull-right">
                <ul id="frmb-1589212423887-control-box" class="frmb-control ui-sortable">
                  <li class="icon-autocomplete input-control input-control-0 ui-sortable-handle" data-type="autocomplete" style="">
                    <span>Autocomplete</span>
                  </li>
                  <li class="icon-button input-control input-control-1 ui-sortable-handle" data-type="button" style="">
                    <span>Button</span>
                  </li>
                  <li class="icon-checkbox-group input-control input-control-6 ui-sortable-handle" data-type="checkbox-group">
                    <span>Checkbox Group</span>
                  </li>
                  <li class="icon-date input-control input-control-11 ui-sortable-handle" data-type="date">
                    <span>Date Field</span>
                  </li>
                  <li class="icon-file input-control input-control-10 ui-sortable-handle" data-type="file" style="">
                    <span>File Upload</span>
                  </li>
                  <li class="icon-header input-control input-control-4 ui-sortable-handle" data-type="header">
                    <span>Header</span>
                  </li>
                  <li class="icon-hidden input-control input-control-2 ui-sortable-handle" data-type="hidden">
                    <span>Hidden Input</span>
                  </li>
                  <li class="icon-number input-control input-control-12 ui-sortable-handle" data-type="number">
                    <span>Number</span>
                  </li>
                  <li class="icon-paragraph input-control input-control-3 ui-sortable-handle" data-type="paragraph">
                    <span>Paragraph</span>
                  </li>
                  <li class="icon-radio-group input-control input-control-7 ui-sortable-handle" data-type="radio-group">
                    <span>Radio Group</span>
                  </li>
                  <li class="icon-select input-control input-control-5 ui-sortable-handle" data-type="select" style="">
                    <span>Select</span>
                  </li>
                  <li class="icon-text input-control input-control-9 ui-sortable-handle" data-type="text">
                    <span>Text Field</span>
                  </li>
                  <li class="icon-textarea input-control input-control-13 ui-sortable-handle" data-type="textarea">
                    <span>Text Area</span>
                  </li>
                </ul>
                <div class="form-actions btn-group">
                  <button type="button" id="frmb-1589212423887-clear-action" class="clear-all btn btn-danger">Clear</button>
                  <button type="button" id="frmb-1589212423887-data-action" class="btn btn-default get-data">[{…}]</button>
                  <button type="button" id="frmb-1589212423887-save-action" class="btn btn-primary save-template">Save</button>
                </div></div>
              </div>
            </div>
          <div class="render-form clearfix">
            <form id="rendered-form">
              <p class="cta">Add some fields to the formBuilder and render them here.</p>
            </form>
            <div class="btn-group pull-right">
              <button type="button" id="render-form-button" class="btn btn-secondary edit-form">Edit Form</button>
              <button type="button" id="copy-html" class="btn btn-primary copy-html">Copy HTML</button>
            </div>
          </div>
        </section>
      </div>
