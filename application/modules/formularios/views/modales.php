

<div class="ui mini modal" id="eliminarModal">
  <div class="header">Eliminar acción estatal</div>
  <div class="content">
    <p>
      ¿Está seguro que desea eliminar la acción estatal?
    </p>
  </div>
  <div class="actions submit">
    <div class="ui red approve submit button">Eliminar</div>
   <div class="ui cancel button">Cancelar</div>
 </div>
</div>
