
  <div id="tab_{id}">
    <form class="ui form mt1" class="form_contenido" id="form_contenido_{id}">
      <div class="ui right aligned container">
        <button class="ui btn-save button save_form" data-case="{id}">Guardar Acción Estatal</button>
        <button class="ui btn-send button send_form pl2 pr2"  data-case="{id}">Enviar Acción Estatal</button>
      </div>

    <input type="hidden" value="{id}" name="id">
    <input type="hidden" value="{token}" name="token">


    <div class="required field mt2">
      <label class="label-accion">Acción estatal</label>
      <input type="text" maxlength="200" class="" validate='validate' placeholder="Acción estatal" value="{accion_estatal}" name="accion_estatal" validate-prompt='Debes ingresar la acción estatal' validate-type="empty">
    </div>

    <div class="stage-big-title mt3">
      Características de la política
    </div>
    <div class="ui divider"></div>

    <div class="required field mt2">
      <label>Tipo de acción estatal</label>
      <input type="text" maxlength="200" class="" validate='validate' placeholder="Tipo de acción estatal"  value="{tipo_accion_estatal}" name="tipo_accion_estatal" validate-prompt='Debes ingresar el tipo de acción estatal' validate-type="empty">
    </div>

    <div class="required field mt2">
      <label>Objetivo organizacional</label>
      <textarea type="text" maxlength="300" rows="3" validate='validate' placeholder="Objetivo organizacional" name="objetivo_organizacional" validate-prompt='Debes ingresar el objetivo organizacional' validate-type="empty">{objetivo_organizacional}</textarea>
    </div>

    <div class="required field mt2">
      <label>Unidad de medida</label>
      <input type="text" maxlength="200" class="" validate='validate' placeholder="Unidad de medida" value="{unidad_medida}" name="unidad_medida" validate-prompt='Debes ingresar la unidad de medida' validate-type="empty">
    </div>

    <div class="field mt2">
      <label>Programa</label>
      <textarea rows="3" maxlength="200" placeholder="Programa" name="programa">{programa}</textarea>
    </div>

    <div class="field mt2">
      <label>Plan</label>
      <textarea maxlength="200" rows="3" placeholder="Plan" name="plan" >{plan}</textarea>
    </div>

    <!-- Vinculación solo aparece si el campo anterior (plan) está completo / tiene contenido -->

    <div class="required field mt2 hidden" data-show="plan">
      <label>Vinculación con otro organismo</label>
      <select class="ui search dropdown multiple" name="vinculacion[]" validate='validate' data-validate='vinculacion' validate-prompt='Debes indicar la vinculación con los organismos' validate-type="empty" depends='plan'>
        <option value="">Vinculación con otro organismo</option>
        {organismos_list}
          <option value="{id}" {selected}>{name}</option>
        {/organismos_list}

      </select>
    </div>

    <div class="required field mt2">
      <label>Área responsable</label>
      <input type="text" maxlength="200" placeholder="Área responsable" value="{area_responsable}" name="area_responsable" validate='validate' validate-prompt='Debes ingresar el área responsable' validate-type="empty">
    </div>

    <div class="required field mt2">
      <label>Destinatarios</label>
      <input type="text"  class="tagsinput" itemTag="destinatarios" validate='validate' placeholder="Destinatarios" validate-prompt='Debes ingresar los destinatarios' validate-type="empty">
    </div>

    <div class="field mt2">
      <label>Presupuesto</label>
      <input type="number" placeholder="Presupuesto" name="presupuesto" value="{presupuesto}">
    </div>

    <div class="required field mt2">
      <label>Marco normativo</label>
      <input type="text" maxlength="200" class="" validate='validate' placeholder="Marco normativo" name="marco_normativo" validate-prompt='Debes ingresar el marco normativo' validate-type="empty" value="{marco_normativo}">
    </div>

    <!-- Información complementaria es dropzone -->
    <div class="field mt2">
      <label>Información complementaria</label>

      <div class="dropzone" data-idcase="{id}" data-dropzone="informacion_complementaria" data-formatos=".pdf,.jpg" data-maxsize="5"></div>

    </div>
  </form>
</div>
