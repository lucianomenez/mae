  <!-- SOLO PARA ROL MAE -->
<!-- <div class="ui container">
  <form class="ui form" id="form_sidebar">
      <div class="required field">
        <label>FICHAA Ministerio</label>
        <select class="ui search dropdown" validate='validate' name="ministerio" validate-prompt='Debes indicar el ministerio' validate-type="empty">
          <option value="">Ministerio</option>
          <option value="Ministerio 1">Ministerio 1</option>
          <option value="Ministerio 2">Ministerio 2</option>
        </select>
      </div>
      <div class="required field">
        <label>Organismo</label>
        <select class="ui search dropdown" validate='validate' name="organismo" validate-prompt='Debes indicar el organismo' validate-type="empty">
          <option value="">Organismo</option>
          <option value="Organismo 1">Organismo 1</option>
          <option value="Organismo 2">Organismo 2</option>
        </select>
      </div>
  </form>
</div> -->
<div class="ui segment segment-base mt2">
  <!-- SOLO PARA ROL ORGANISMO -->
  <div class="ui container mb2">
    <div class="title-ministerio">
      Ministerio
    </div>
    <div class="title-organismo">
      Organismo
    </div>
  </div>
  <div class="ui container mt1">
    <div class="ui grid middle aligned">
      <div class="five wide column" style="padding:0!important">
        <button class="ui icon button"><i class="edit outline icon"></i></button>
        <button class="ui icon button"><i class="trash icon"></i></button>
      </div>
      <div class="eleven wide column" style="padding:1rem 0 1rem 0!important">
        <div><a class="stage-name active">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></div>
      </div>
    </div>
  </div>
  <div class="ui container mt1">
    <div class="ui grid middle aligned">
      <div class="five wide column" style="padding:0!important">
        <button class="ui icon button"><i class="edit outline icon"></i></button>
        <button class="ui icon button"><i class="trash icon"></i></button>
      </div>
      <div class="eleven wide column" style="padding:1rem 0 1rem 0!important">
        <div><a class="stage-name active">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></div>
      </div>
    </div>
  </div>

<!-- Cuando se completa la acción -->
<!--       <div class="comment pointer mt1">
        <a class="avatar mr2">
          <div class="stage-circle active"><p><i class="check icon m0"></i></p></div>
        </a>
        <div class="content">
          <a class="stage-name">Acción estatal</a>
          <div class="stage-status">Completo</div>
        </div>
      </div> -->
<!-- Cuando se completa la acción -->

  <!-- <div class="ui container mt2">
    <div class="ui divider mb2"></div>
    <a class="pointer" id="add_accion_estatal"><h4 class="ui header"><i class="plus green icon"></i> Nueva acción estatal </h4></a>
  </div> -->

</div>
