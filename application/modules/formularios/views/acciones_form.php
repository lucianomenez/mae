        <div class="ui grid middle aligned" data-accion={indice}>
          <div class="three wide column center aligned">
            <div class="stage-circle {active}" data-id="{id}"><p style="margin-bottom:0!important;">{indice}</p></div>
          </div>
          <div class="eleven wide column" style="padding:1rem 0 1rem 0!important">
            <a class="stage-name {active} accion_btn" data-id="{id}">{accion_estatal_title}</a>
            <div class="stage-status">Sin datos</div>
          </div>
          <div class="one wide column" style="padding:0!important">
            <button class="ui icon button delete-accion" data-id="{id}"><i class="trash icon"></i></button>
          </div>
        </div>
