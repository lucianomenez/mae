Dropzone.autoDiscover = false;

$(document).ready(function() {
  var base_url = $("#base_url").val();

  idcase=$('#idcase').val();
  iduser = $('#iduser').val();
  get_form(idcase,iduser,false);


    $(document).on('click', ".send_form", function(e) {
      e.preventDefault();
         var form = $('#form_contenido_'+$(this).attr('data-case'));
         form.submit();
        if (form.form('is valid')) {

          $.ajax({
                 url: base_url +'formularios/matriz_de_relevamiento/avanza_bpm/',
                 type: "post",
                 data: form.serialize(), // serializes the form's elements.
                 success: function(data)
                 {
                 $.uiAlert({
                    textHead: 'Los cambios fueron guardados con éxito',
                    text: '',
                    bgcolor: '#2FB537',
                    textcolor: '#fff',
                    position: 'top-right', // top And bottom ||  left / center / right
                    icon: '',
                    time: 3
                    });
                  location.reload()                 
               },
                 error : function(){
                  $.uiAlert({
                    textHead: 'Error en el guardado',
                    text: '',
                    bgcolor: '#ff0000',
                    textcolor: '#fff',
                    position: 'top-right', // top And bottom ||  left / center / right
                    icon: '',
                    time: 3
                    });
                 }
               });

          //Enviar
        }
    });

    $(document).on('click', ".save_form", function(e){
        e.preventDefault();
        id=$(this).attr('data-case');
        textToast='Los cambios fueron guardados con éxito';
        save_data(id,textToast);
    });

    $(document).on('click', "#add_accion_estatal", function(e){
        save_data($('#idcase').val(),'null');

        e.preventDefault();
        $.ajax({
         url : $('#base_url').val() + 'formularios/matriz_de_relevamiento/newcase/true',
         type: 'POST',
         success: function(resultado){
           var obj = JSON.parse(resultado);
            indice= $( ".stage-circle" ).last().find('p').text();
            indice=parseInt(indice)+1;
          $('#idcase').val(obj.id)
           $('#append').append(obj.sidebar);
           $( ".stage-circle" ).last().find('p').text(indice);
           $('#append_form').html(obj.form);
            load_elements();
            load_depends();

            validar('form_contenido_'+obj.id,true);
             $('.stage-circle,.stage-name  ').removeClass('active');

              $('.stage-circle[data-id="'+obj.id+'"],.stage-name[data-id="'+obj.id+'"]').addClass('active');
              
         }

       })
  });

    $(document).on('click', ".accion_btn", function(e){
        save_data($('#idcase').val(),'null');
        id = $(this).attr('data-id');
        iduser = $('#iduser').val();
        get_form(id,iduser,true);
    });


    $(document).on('click', ".delete-accion", function(e){
      id = $(this).attr('data-id');
      iduser = $('#iduser').val();
                   $('#eliminarModal').modal({
                      blurring: false,
                      closable : false,
                        onShow: function () {

                       },
                         onApprove: function () {
                           $.ajax({
                               url: base_url +'formularios/matriz_de_relevamiento/eliminar/',
                               type: "post",
                               async: false,
                               data: {
                                  id:id,
                                  iduser:iduser
                               },
                               success: function(data)
                               {
                                  location.reload();
                               },
                               error : function(){
                                 alert ("Error!");
                               }
                             });
                         }
                    }).modal('show');


  });



});

var load_elements=(data=null)=>{
  $('select.dropdown').dropdown();

  $('.ui.checkbox').checkbox();

  $('.ui.dropdown').dropdown();

  load_dropdown_multiple(data);
  load_tags(data);
  load_dropzones(data);
}

var load_dropdown_multiple=(data=null)=>{
   $(".dropdown.multiple select" ).each(function( i ) {
        let name=$(this).attr('name');
        item=name.replace("[", "");
        item=item.replace("]", "");
        if (data &&data[item]) {
          $('[name="'+name+'"]').dropdown("set selected",data[item]);
        }
   })

}

var load_depends=()=>{
   $( "[depends]" ).each(function( i ) {
      let id=$('#idcase').val();
        let depends=$(this).attr('depends');
        let container=$('#form_contenido_'+ id +' [data-show='+depends+']');
        container.append('<input type="hidden" id="validate_'+depends+'">');

         if (depends) {
          
        $(document).on('change', '#form_contenido_'+ id +' [name='+depends+']', function(e){
                
                    if ($(this).val()!='') {
                        $('#validate_'+depends).val('validate');
                        container.show();
                    }else{
                        $('#validate_'+depends).val('');
                        container.hide();
                    }
              });

        $('#form_contenido_'+ id +' [name='+depends+']').change();

         }
   })

}

var get_form=(id,iduser,append_form)=>{

          $.ajax({
               url: $('#base_url').val() +'formularios/matriz_de_relevamiento/get_form/'+append_form,
               type: "post",
               async: false,
               data: {
                  id:id,
                  iduser:iduser
               },
               success: function(data)
               {
                var data = JSON.parse(data);

                if (append_form) {
                  $('#append_form').html(data.form);
                  $('.stage-circle,.stage-name  ').removeClass('active');

                  $('.stage-circle[data-id="'+id+'"],.stage-name[data-id="'+id+'"]').addClass('active');
                  $('#idcase').val(id);
                }
                
                if (data.data!='ficha') {
                    validar('form_contenido_'+id,true);
                    load_depends();
                    load_elements(data.data);
                }
               },
               error : function(){
                 alert ("Error!");
               }
             });
}

var validar =function (form,inline){
        fields_formulario= $( "#"+ form +' [validate="validate"]');
      let fields= {};

       fields_formulario.each(function( i ) {

          let identifier=trimfy($(this).attr('name'));
          let type=$(this).attr('validate-type');
          let prompt=$(this).attr('validate-prompt');
          fields[identifier] = {};
          fields[identifier].identifier = identifier;
          if ($(this).attr('depends')) {
            fields[identifier].depends = 'validate_'+$(this).attr('depends');
          }
          fields[identifier].rules = [];
          let rules={ 'type'   : type, 'prompt' : prompt}
          fields[identifier].rules.push(rules);
       })

      //let jsonfields= JSON.stringify(fields);

        $( "#"+ form).submit(( e )=> {
          e.preventDefault();
        })

        $('#'+form).form({
            inline : inline,
            on     : 'submit' ,
           fields: fields
        });
  }

  var load_tags=(data=null)=>{
        let tagsinputs=$('input.tagsinput');
        let idcase=$('#idcase').val();
          tagsinputs.each(function( i ) {
          let itemId=$(this).attr('itemTag');

          if (data) {
            data=data[itemId];
          }
           initTagsInput(itemId,'form_contenido_'+idcase,idcase,data);
        })
  }

  function load_dropzones(data=null){

       $( ".dropzone" ).each(function( i ) {
            element=$(this).attr('data-dropzone');
            formatos=$(this).attr('data-formatos');
            maxFilesize=$(this).attr('data-maxsize');
            idcase=$(this).attr('data-idcase');
            idwf=$(this).attr('data-idwf');
            appendDropzone(element,formatos,parseInt(maxFilesize),idcase,idwf,data);
       })
  }

  function appendDropzone(element,formatos,maxFilesize,idcase,idwf,data=null){
    base_url = $("#base_url").val();
    $("[data-dropzone="+element+"][data-idcase="+idcase+"]").append('<div id="'+element+'_hiddens"></div>');
    $("div.dropzone[data-dropzone="+element+"][data-idcase="+idcase+"]").append('<input id="'+element+'Validate"/>');
    $('#'+element+'_hiddens').append('<input type="hidden" value="0" id="index'+element+'">');

    this["dropzone_"+element+idcase] = new Dropzone("div.dropzone[data-dropzone="+element+"][data-idcase="+idcase+"]", {
        url: base_url+ 'formularios/matriz_de_relevamiento/upload_file/',
        dictDefaultMessage: '<i class="cloud upload big icon"></i><br>Click para subir archivo<br>(5MB máximo, formato '+formatos+')',
        dictFileTooBig: 'El archivo pesa {{filesize}}MB, el máximo permitido es de {{maxFilesize}}MB.',
        maxFilesize: maxFilesize, //MB
        maxFiles: 10,
        id:element,
        idcase:idcase,
        idwf:idwf,
        acceptedFiles: formatos,
        addRemoveLinks: true,
        dictCancelUpload: "Cancelar",
        dictRemoveFile: "Eliminar archivo",
        parallelUploads: 1,

        init: function() {
          dropzoneObj=this;

          if (data) {
                   if ($.isArray(data[element])) {
                        $.each(data[element], function( index, value ) {
                          if (value['url']!='') {
                              var xhr = new XMLHttpRequest();
                              xhr.open("HEAD", $('#base_url').val()+value['url'], true);
                              xhr.onreadystatechange = function() {
                                  if (this.readyState == this.DONE) {
                                    let file = {
                                       name: value['original_name'],
                                       size: parseInt(value['size'])
                                     };
                                      dropzoneObj.emit('addedfile', file);
                                      dropzoneObj.createThumbnailFromUrl(file, $('#base_url').val()+value['url']);
                                      dropzoneObj.emit('processing', file);
                                      dropzoneObj.emit('complete', file);
                                      dropzoneObj.createThumbnailFromUrl(file, $('#base_url').val()+value['url']);
                                      dropzoneObj.files.push(file);

                                   }

                              };
                                xhr.send();
                          }

                          let index_val=parseInt($('#index'+element).val());
                          loadInputDropzone(index_val,element,value,value['url']);
                          index_val++;
                          $('#index'+element).val(index_val);
                        })
                   }

          }
        },

        sending: function(file, xhr, formData) {
          formData.append("idcase", this.options.idcase);
          index=$('#index'+element).val();
          formData.append("data-filename",element+'-i'+index+'-i');
          formData.append("idwf",this.options.idwf);
        },
        success: function(file, response) {
          response=JSON.parse(response);
           index=parseInt($('#index'+element).val());
            loadInputDropzone(index,element,response,response.uploadUrl);
            index++;
            $('#index'+element).val(index);

            textToast='Archivo cargando con éxito';
            save_data(idcase,textToast);
        },
        removedfile: function(file) {
          file.previewElement.remove();
             dropzoneObj=this;
             let name= '';
            if (file.xhr) {
              if (file.xhr.responseText) {
                name= JSON.parse(file.xhr.responseText).name;
              }
            }

            if (name=='') {
                name=file.name;
            }


            let subname=name.substr(-10);
            let index = subname.match(/\d+/g);

            if (index) {

                data = {'filename' : name,'idcase':dropzoneObj.options.idcase};
                  $.ajax({
                    type: "POST",
                    url: globals.base_url + 'formularios/matriz_de_relevamiento/delete_file',
                    data: data,
                    success: function(response){
                      if (JSON.parse(response)=='true') {

                      }
                    },
                  });
                $('input[name="'+dropzoneObj.options.id+'['+index[0]+'][url]"').val('');
                save_data(dropzoneObj.options.idcase,'Archivo eliminado con éxito');
            }
        }
    });

}


function loadInputDropzone(index,elementName,response,url){
        $('#'+element+'_hiddens').append('<input type="hidden" data-index="'+index+'" name="'+element+'['+index+'][index]" value="'+index+'">');
          $('#'+element+'_hiddens').append('<input type="hidden" data-index="'+index+'" name="'+element+'['+index+'][url]" value="'+url+'">');
          $('#'+element+'_hiddens').append('<input type="hidden" data-index="'+index+'" name="'+element+'['+index+'][name]" value="'+response.name+'">');
          $('#'+element+'_hiddens').append('<input type="hidden" data-index="'+index+'" name="'+element+'['+index+'][original_name]" value="'+response.original_name+'">');
          $('#'+element+'_hiddens').append('<input type="hidden" data-index="'+index+'" name="'+element+'['+index+'][file_extension]" value="'+response.file_extension+'">');
          $('#'+element+'_hiddens').append('<input type="hidden" data-index="'+index+'" name="'+element+'['+index+'][extension]" value="'+response.extension+'">');
          $('#'+element+'_hiddens').append('<input type="hidden" data-index="'+index+'" name="'+element+'['+index+'][size]" value="'+response.size+'">');
          $('#'+element+'_hiddens').append('<input type="hidden" data-index="'+index+'" name="'+element+'['+index+'][width]" value="'+response.width+'">');
            $('#'+element+'_hiddens').append('<input type="hidden" data-index="'+index+'" name="'+element+'['+index+'][height]" value="'+response.height+'">');
}

function save_data(id,textToast){
        let  form = $('#form_contenido_'+id);
        data=form.serialize();
   if (data) {
        $.ajax({
              async: false,
               url: $('#base_url').val() +'formularios/matriz_de_relevamiento/solo_guardar/',
               type: "post",
               data: data, // serializes the form's elements.
               success: function(data)
               {
                texto=$('#form_contenido_'+id +' input[name="accion_estatal"]').val();
                if (texto=='') {
                  texto='Nueva Acción Estatal';
                }
               $('.accion_btn[data-id='+id+']').text(texto);
                if (textToast!='null') {
                  $.uiAlert({
                    textHead: textToast,
                    text: '',
                    bgcolor: '#2FB537',
                    textcolor: '#fff',
                    position: 'top-right', // top And bottom ||  left / center / right
                    icon: '',
                    time: 3
                    });
                  }
               },
               error : function(){
                  $.uiAlert({
                    textHead: 'Error en el guardado',
                    text: '',
                    bgcolor: '#ff0000',
                    textcolor: '#fff',
                    position: 'top-right', // top And bottom ||  left / center / right
                    icon: '',
                    time: 3
                    });
                }
          });
        }
          //Guardar
}



         function initTagsInput(id,form,idcase,data=null){
            
              $('#'+form).append('<div id="hiddensTag'+id+'"></div>');

                  var tags = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('text'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch:  {
                      ttl: 1,
                      url:globals['base_url'] + 'cms/get_options_json/'+id
                    }
                  });
                  tags.initialize()

              $('[itemTag="'+id+'"]').tagsinput({
                  itemValue: 'value',
                  itemText: 'text' ,
                  trimValue: true,
                  confirmKeys: [13, 44],
                    
                   typeaheadjs: {
                      name: 'tags',
                      displayKey: 'text',
                      source: tags.ttAdapter()
                  }
              });

              if (data) {

                  if ($.isArray(data)) {
                        $.each(data, function( index, value ) {
                            $('[itemTag="'+id+'"]').tagsinput('add',value);
                          })
                    }
                
              }


            let element=$('[itemTag="'+id+'"]').parent().find('.tt-input')

             element.on('keydown', function (e) {
                if (e.keyCode === 13 || e.keyCode === 44|| e.keyCode === 188) {
                  e.preventDefault();
                  e.stopPropagation();
                  value=slugify(element.val());
                  if (value!='') {
                    $('[itemTag="'+id+'"]').tagsinput('add', { 
                      value: value, 
                      text: element.val()
                    }); 

                    element.val("");
                  }
                }
              });
              element.on('blur', function (e) {
                  e.preventDefault();
                  value=slugify(element.val());
                  if (value!='') {
                    $('[itemTag="'+id+'"]').tagsinput('add', { 
                      value: value, 
                      text: element.val()
                    }); 

                    element.val("");
                  }
       
              });
              $('[itemTag="'+id+'"]').on('itemAdded', function(event) {
                let items= $('[itemTag="'+id+'"]').tagsinput('items');
                if ($.isArray(items)) {
                  $('#hiddensTag'+id).html('');
                   $.each(items, function( index, value ) {
  
                     $('#hiddensTag'+id).append('<input type="hidden" name="'+id+'['+index+'][text]" value="'+value.text+'"/>');
                     $('#hiddensTag'+id).append('<input type="hidden" name="'+id+'['+index+'][value]" value="'+value.value+'"/>');
                   })
                }
              });
              $('[itemTag="'+id+'"]').on('itemRemoved', function(event) {
                let items= $('[itemTag="'+id+'"]').tagsinput('items');
                 $('#hiddensTag'+id).html('');
                  $('#hiddensTag'+id).append('<input type="hidden" name="'+id+'" value=""/>');
                  $('#hiddensTag'+id).append('<input type="hidden" name="'+id+'" value=""/>');
                  if ($.isArray(items)) {

                     $.each(items, function( index, value ) {
                       $('#hiddensTag'+id).append('<input type="hidden" name="'+id+'['+index+'][text]" value="'+value.text+'"/>');
                       $('#hiddensTag'+id).append('<input type="hidden" name="'+id+'['+index+'][value]" value="'+value.value+'"/>');
                     })
                  }
              });
         }


const slugify = text => {
  // Use hash map for special characters
  let specialChars = {"à":'a',"ä":'a',"á":'a',"â":'a',"æ":'a',"å":'a',"ë":'e',"è":'e',"é":'e', "ê":'e',"î":'i',"ï":'i',"ì":'i',"í":'i',"ò":'o',"ó":'o',"ö":'o',"ô":'o',"ø":'o',"ù":'o',"ú":'u',"ü":'u',"û":'u',"ñ":'n',"ç":'c',"ß":'s',"ÿ":'y',"œ":'o',"ŕ":'r',"ś":'s',"ń":'n',"ṕ":'p',"ẃ":'w',"ǵ":'g',"ǹ":'n',"ḿ":'m',"ǘ":'u',"ẍ":'x',"ź":'z',"ḧ":'h',"·":'-',"/":'-',"_":'-',",":'-',":":'-',";":'-',"[":'',"]":''};

    return text.toString().toLowerCase()
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '')            // Trim - from end of text
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/./g,(target, index, str) => specialChars[target] || target) // Replace special characters using the hash map
      .replace(/&/g, '-and-')         // Replace & with 'and'
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
};

const trimfy = text => {
  // Use hash map for special characters
  let specialChars = {"[":'',"]":''};
  if (text) {
    return text.toString().toLowerCase()
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '')            // Trim - from end of text
      .replace(/./g,(target, index, str) => specialChars[target] || target) // Replace special characters using the hash map
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      }
};