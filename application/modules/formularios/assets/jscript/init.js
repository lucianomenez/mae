


$(document).ready(function() {

    $('#myTable').DataTable({
         
          "pageLength": 100,
 
         
         "language": {
            "lengthMenu": "Mostrando _MENU_ registro por pagina",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando pagina _PAGE_ de _PAGES_",
            "infoEmpty": "No se encontraron resultados",
            "infoFiltered": "(buscado sobre _MAX_ registros)"
         }  
        });


});