<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Paneles de Navegación
 *
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 */
class Panel_inicio extends MX_Controller {

    function __construct() {
        parent::__construct();
          $this->base_url = base_url();
          $this->module_url = base_url() . $this->router->fetch_module() . '/';
          $this->user->authorize();
          $this->load->helper('file');
          $this->load->helper('url');
          $this->load->model('app');
          $this->load->model('user/user');
          $this->load->library('parser');
          $this->load->config('config');
          $this->idu = $this->user->idu;
        }


      function index(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          Modules::run('dashboard/dashboard', 'panel_inicio/json/index.json',$debug, $extraData);
      }

      // Parse json

      function contenido(){
          $data['base_url'] = $this->base_url;

          echo $this->parser->parse('contenido',$data, true, true);
      }

      function get_modales(){
          echo $this->parser->parse('modales',[], true, true);
      }


}
