
<section class="bg-mint">

<div class="container principal">
	<div class="row">
		<div class="col-xl-4 col-12" id="comandos">
				<div class="container segment-base mb-2">
					<div class="row align-items-center pt-2 pb-2">
						<div class="col-xl-4 col-12 text-center">
							<div class="bg-img" style="background-image: url('{base_url}panel_inicio/assets/img/no-picture.png')"></div>
						</div>
						<div class="col-xl-8 col-12 pl-4">
							<div class="name">Micaela</div>
							<div class="lastname">Van Kooten</div>
						</div>
					</div>
				</div>
				<div class="container segment-base col-height computer">
					<ul class="mt-2"id="menu">
						<li>
							<a class="bold" href=""><i class="fas fa-user mr-3"></i>Mi perfil</a>
						</li>
						<li>
							<a class="bold" href=""><i class="fas fa-cog mr-3"></i>Cambiar contraseña</a>
						</li>
						<li>
							<a class="bold" href=""><i class="fas fa-paint-brush mr-3"></i>Personalizar tema</a>
						</li>
						<li>
							<a class="bold" href=""><i class="fas fa-share mr-3"></i>Nuevo acceso directo</a>
						</li>
						<li>
							<a href=""><i class="fas fa-edit mr-3"></i>argentina.gob.ar</a>
						</li>
						<li>
							<a href=""><i class="fas fa-edit mr-3"></i>afip.gob.ar</a>
						</li>
						<li>
							<a href=""><i class="fas fa-edit mr-3"></i>owa.jgm.gob.ar</a>
						</li>
						<li>
							<a class="bold" href=""><i class="fas fa-sign-out-alt mr-3"></i>Cerrar sesión</a>
						</li>
					</ul>
				</div>
				<div class="container segment-base col-height mobile">
					<a class="btn-menu text-center" data-toggle="collapse" data-target="#menu" role="button">
						<i class="fas fa-chevron-down fa-xs mr-2"></i>Ver menú<i class="fas fa-chevron-down fa-xs ml-2"></i>
					</a>
					<ul class="collapse" id="menu">
						<li>
							<a class="bold" href=""><i class="fas fa-user mr-3"></i>Mi perfil</a>
						</li>
						<li>
							<a class="bold" href=""><i class="fas fa-cog mr-3"></i>Cambiar contraseña</a>
						</li>
						<li>
							<a class="bold" href=""><i class="fas fa-paint-brush mr-3"></i>Personalizar tema</a>
						</li>
						<li>
							<a class="bold" href=""><i class="fas fa-share mr-3"></i>Nuevo acceso directo</a>
						</li>
						<li>
							<a href=""><i class="fas fa-edit mr-3"></i>argentina.gob.ar</a>
						</li>
						<li>
							<a href=""><i class="fas fa-edit mr-3"></i>afip.gob.ar</a>
						</li>
						<li>
							<a href=""><i class="fas fa-edit mr-3"></i>owa.jgm.gob.ar</a>
						</li>
						<li>
							<a class="bold" href=""><i class="fas fa-sign-out-alt mr-3"></i>Cerrar sesión</a>
						</li>
					</ul>
				</div>
		</div>
		<div class="col-xl-4 col-12" id="accesos">
			<div class="container segment-base text-center mb-2 computer">
					<div class="col-title"><i class="fas fa-share-alt mr-3"></i>Accesos</div>
			</div>
			<div class="container segment-base col-height">
				<div class="col-title text-center mobile mb-4"><i class="fas fa-share-alt mr-3"></i>Accesos</div>
				<ul class="list-group" id="mae">
  				<li class="list-group-item active d-flex justify-content-between align-items-center">
						mapa de la acción estatal<i class="fas fa-chevron-right fa-sm"></i></li>
  				<li class="list-group-item d-flex justify-content-between align-items-center">
						Por provincia<i class="fas fa-chevron-right fa-sm"></i></li>
  				<li class="list-group-item d-flex justify-content-between align-items-center">
						Por jurisdicción / organismo<i class="fas fa-chevron-right fa-sm"></i></li>
  				<li class="list-group-item d-flex justify-content-between align-items-center">
						Por ODS<i class="fas fa-chevron-right fa-sm"></i></li>
  				<li class="list-group-item d-flex justify-content-between align-items-center">
						Por producto<i class="fas fa-chevron-right fa-sm"></i></li>
				</ul>
				<ul class="list-group mt-4" id="matriz">
					<li class="list-group-item active d-flex justify-content-between align-items-center">
						matriz de relevamiento</li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Administrador de matrices<i class="fas fa-chevron-right fa-sm"></i></li>
					<li class="list-group-item d-flex justify-content-between align-items-center">
						Administrador de acciones<i class="fas fa-chevron-right fa-sm"></i></li>
				</ul>
			</div>
		</div>
		<div class="col-xl-4 col-12" id="notificaciones">
			<div class="container segment-base text-center mb-2 computer">
					<div class="col-title"><i class="fas fa-bell mr-3"></i>Notificaciones</div>
			</div>
			<div class="container segment-base col-height">
				<div class="col-title text-center mobile mb-4"><i class="fas fa-share-alt mr-3"></i>Notificaciones</div>
				<div class="row">
					<div class="container">
						<div class="date">
							26/05/2020
						</div>
						<div class="sender">
							Comunicación interna
						</div>
						<div class="subject">
							Asunto documentación
						</div>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="container">
						<div class="date">
							26/05/2020
						</div>
						<div class="sender">
							Comunicación interna
						</div>
						<div class="subject">
							Asunto documentación
						</div>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="container">
						<div class="date">
							26/05/2020
						</div>
						<div class="sender">
							Comunicación interna
						</div>
						<div class="subject">
							Asunto documentación
						</div>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class="container">
						<div class="date">
							26/05/2020
						</div>
						<div class="sender">
							Comunicación interna
						</div>
						<div class="subject">
							Asunto documentación
						</div>
					</div>
				</div>
				<button class="btn btn-block mt-5">Ver todas las notificaciones</button>
			</div>
		</div>
	</div>
</div>

</section>
