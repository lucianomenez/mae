<div class="ui grid middle aligned center aligned">
  <div class="ten wide column" style="margin-top:1rem; background:#fff;">
    <div class="ui container p1" style="text-align:left;">
    <h2 class="m0">Hola {user name}</h2>
    <h3 class="mt1">
      Est&aacutes recibiendo este mail porque te registraste en la plataforma del Mapa de Acción Estatal
    </h3>
      <p>Para activar tu cuenta haz click, solo una vez, en el siguiente link: <a href="{base_url}user/register/activar/{token}">{base_url}perfil/activar/{token}</a></a>
      </p>
      <p>
        Si el link no te funciona, simplemente copia la direccion en el navegador y completa los pasos que detallan.
      </p>
      <p>
      	Luego que hayas activado tu usuario debes ingresar a <a href="https://mapaaccionestatal.jefatura.gob.ar">pnc.argentina.gob.ar</a>
      </p>
      <hr class="mt2 mb2">
      <div>
        <b>
          Gracias por utilizar la plataforma y bienvenido!
        </b>
      </div>
      <div class="mt1">
        <a href="https://mapaaccionestatal.jefatura.gob.ar"><img alt="Mapa Acción Estatal" style="width:auto; height:50px;" src="http://pnc.argentina.gob.ar/img/logos/logo_pnc.png"/></a>
      </div>
    </div>
  </div>
</div>
