<div class="ui grid middle aligned center aligned" style="height:50%!important;">
  <div class="column">
    <div class="ui container" style="margin-top:2rem;">

      <h2 class="ui image header">
        <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
          <lottie-player src="https://assets2.lottiefiles.com/packages/lf20_DeE8dx.json"  background="transparent"  speed="1"  style="width: 75px; height: 75px;" autoplay >
        </lottie-player>
      </h2>
      <h1 class="ui header h1" style="margin-top:0!important;">Tu perfil se ha activado con éxito!</h1>
      <a href="{base_url}/user/login"><button class="ui inverted secondary button" style="margin-top:1rem;">Ingresar a la plataforma</button></a>

    </div>
  </div>
</div>
