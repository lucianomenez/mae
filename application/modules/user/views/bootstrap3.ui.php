<!DOCTYPE html>
<html>
    <head>
        <title>{title}</title>
        <link rel="icon" href="{base_url}dashboard/assets/img/favicon.png" type="image/png" sizes="32x32">
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- <link href="{base_url}jscript/bootstrap-wysihtml5/css/bootstrap.min.css" rel="stylesheet"> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{base_url}jscript/fontawesome/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="{base_url}user/assets/css/semantic.css">
        <link rel="stylesheet" href="{base_url}css/fonts/droidsans_regular_macroman/stylesheet.css" />
 		    <!-- <link href="{base_url}jscript/bootstrap-wysihtml5/css/AdminLTE.css" rel="stylesheet"> -->
        <link rel="stylesheet" type="text/css" href="{base_url}user/assets/css/breakpoints.css">
        <link rel="stylesheet" type="text/css" href="{base_url}user/assets/css/main.css">
        <!-- CSS -->
        {css}

    </head>
    <body style="font-family:'Roboto'!important;">

<!-- header -->

  {content}

<!-- footer -->


        <!-- Boot -->
        <script type="text/javascript">
            //-----declare global vars
            var globals={inline_js};
        </script>
        <script type="text/javascript" src="{base_url}jscript/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="{base_url}user/assets/jscript/semantic.min.js"></script>
        <!-- <script type="text/javascript" src="{base_url}jscript/bootstrap-wysihtml5/js/bootstrap.min.js"></script> -->
        {js}
    </body>
</html>
