<div class="jumbotron">
  <h1>Error!</h1>
    <p>El nombre de usuario que elegiste ya no se encuentra disponible Haz click aquí para generar otro: <a class="btn btn-primary btn-lg" href="{base_url}/perfil" role="button">Registrar</a></p>
</div>