<div class="ui grid middle aligned center aligned">
  <div class="ten wide column" style="margin-top:1rem; background:#fff;">
    <div class="ui container p1" style="text-align:left;">
    <h2 class="m0">Hola</h2>
    <h3 class="mt1">
    Est&aacutes recibiendo este mail porque te solicitaste cambiar tu clave en la plataforma del Premio Nacional a la Calidad.
    </h3>
      <p>Para activar el cambio de clave de tu cuenta haz click en el link: <a href="{base_url}user/recover/new_pass/{token}">{base_url}user/recover/new_pass/{token}</a>
      </p>
      <p>
        Si el link no te funciona, simplemente copia la dirección en el navegador y completa los pasos que detallan.
      </p>
      <hr class="mt2 mb2">
      <div class="mt1">
        <b>
          Gracias por utilizar la plataforma y bienvenido!
        </b>
      </div>
      <div class="mt1">
        <a href="https://pnc.argentina.gob.ar"><img alt="Premio Nacional a la Calidad" style="width:auto; height:50px;" src="http://pnc.argentina.gob.ar/img/logos/logo_pnc.png"/></a>
      </div>
    </div>
  </div>
</div>
