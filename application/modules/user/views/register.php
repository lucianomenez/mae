<div class="ui fluid container" style="padding:0; margin-left:0!important; margin-right:0!important;">
  <div class="ui stackable two column grid" style="margin:0;">
    <div class="nine wide column bg-instit bg-img col-log-left"
        style="background-image: url('{base_url}user/assets/images/recurso-fondo.png');">
      <div class="ui container" style="height:20%!important;">
        <a class="a-unset" href="{base_url}"><img class="img-log" src="{base_url}user/assets/images/MAE-logo-blanco.png"></a>
      </div>
      <div class="ui container flx-cntr" style="height:60%!important;">
        <div class="txt-log txt-white bold">
          Es un registro sistemático e integral de las acciones del Estado para el desarrollo de las políticas públicas.
        </div>
      </div>
      <div class="ui container" style="height:20%!important">
        <img class="img-log-bottom" src="{base_url}user/assets/images/logo-corazon-arg-unida.png" style="padding-bottom: 0;">
      </div>
    </div>
    <div class="seven wide column bg-white col-log-right">
      <div class="ui container p1" id="login-box">
        <h1 class="ui header txt-basegrey mb2 mt1">
            Registrarse
        </h1>
        <form class="ui form" id="formAuth" action="{module_url}register/process" method="post">
          <!--  MSG -->
          {if {show_warn}}
          <div class="ui negative message alert alert-warning">
            <!-- <button type="button" class="close" data-dismiss="alert">&times;</button> -->
            <strong>{msgcode}</strong>
          </div>
          {/if}
          <div class="field">
            <input type="text" name="name" placeholder="Ingrese su nombre"/>
          </div>

          <div class="field">
            <input type="hidden" name="nick"/>
          </div>

          <div class="field">
            <input type="text" name="lastname" placeholder="Ingrese su apellido"/>
          </div>
          <!--  NAME -->
          <div class="field">
            <input type="text" name="idnumber" placeholder="Número de documento (DNI)"/>
          </div>

          <div class="field">
            <input type="text" name="email" placeholder="Ingrese su E-mail"/>
          </div>
          <!--  PASS -->
          <div class="field">
            <input type="password" name="passw" placeholder="Ingresa una Contraseña"/>
          </div>

          <div class="field">
            <input type="password" name="password2" placeholder="Repita nuevamente la Contraseña"/>
          </div>
            <!--  FOOTER -->
              <div class="field">
                  <button class="ui fluid button btn-basegrey" type="submit">Registarse</button>
                  <!-- <button class="fluid ui button btn-outline-basegrey" type="submit"><a class="a-unset" href="{module_url}register">Crear cuenta</a></button> -->
              </div>
        </form>
      </div>
    </div>
  </div>
</div>
