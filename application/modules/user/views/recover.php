<div class="ui fluid container" style="padding:0; margin-left:0!important; margin-right:0!important;">
  <div class="ui stackable two column grid" style="margin:0;">
    <div class="nine wide column bg-instit bg-img col-log-left"
        style="background-image: url('{base_url}user/assets/images/recurso-fondo.png')">
      <div class="ui container" style="height:20%!important;">
        <a class="a-unset" href="{base_url}"><img class="img-log" src="{base_url}user/assets/images/MAE-logo-blanco.png"></a>
      </div>
      <div class="ui container flx-cntr" style="height:60%!important;">
        <div class="txt-log txt-white bold">
          Constituye un sistema de información que permite evaluar el seguimiento y monitoreo de las políticas públicas.        </div>
      </div>
      <div class="ui container" style="height:20%!important">
        <img class="img-log-bottom" src="{base_url}user/assets/images/logo-corazon-arg-unida.png" style="padding-bottom: 0;">
      </div>
    </div>
    <div class="seven wide column bg-white col-log-right">
      <div class="ui container p1" id="login-box">
        <h3 class="ui header txt-instit mb1 mt1">
            <a class="a-unset" href="{base_url}"><i class="fas fa-arrow-left mr1"></i>Volver</a>
        </h3>
        <h1 class="ui header txt-basegrey mb2 mt1">
            Recuperar contraseña
        </h1>
        <form class="ui form" action="#" method="post" id="recover1">
          <div class="field">
            <input type="text" name="mail" placeholder="Por favor ingrese su email"/>
          </div>
            <!--  FOOTER -->
              <div class="field">
                  <button class="ui fluid button btn-basegrey" type="submit">Enviar</button>
              </div>
        </form>
      </div>
    </div>
  </div>
</div>

         <!-- <div class="form-box" id="login-box">
            <div class="header bg-navy">{lang loginMsgR}</div>
                <form  action="#" method="post" id="recover1">


                    <div class="body bg-gray">
                        <label>{lang type_your_email}</label>
                        <div class="form-group">
                            <input type="text" name="mail" class="form-control" placeholder="{lang email}"/>
                        </div>


                    </div>
                    <div class="footer">
                        <button type="submit" class="btn bg-olive btn-block">{lang loginButtonR}</button>


                    </div>
                </form>


        </div> -->
