<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends MX_Controller {

    public function __construct() {
        parent::__construct();
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        //----load parser
        $this->load->library('parser');
        $this->load->config('config');
        $this->load->model('app');
        $this->load->model('msg');

    }

    function Nouser() {
        $msg = $this->session->userdata('msg');
        $this->lang->load('login', $this->config->item('language'));
        //---add language data
        $cpData = $this->lang->language;
        $cpData['authUrl'] = base_url() . 'user/authenticate';
        $cpData['base_url'] = base_url();
        $cpData['show_warn'] = $this->config->item('show_warn');
        $cpData['theme'] = $this->config->item('theme');
        $cpData['plugins'] = (class_exists('Userlayer')) ? implode(',', $this->config->item('user_plugin')) : array();
        //----load login again with msg
        $this->parser->parse('user/login', $cpData);
    }

    function validate_captcha(){

      $data["json"]["post"] = $this->input->post();
      $this->load->helper('user/recaptchalib');
      $privatekey = "6LeZ8s8UAAAAANRn5fWutCG4wdrCXYtH6QVC8vD2";

      $captcha = $this->input->post('g-recaptcha-response');
      $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$privatekey."&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);

      $obj=json_decode($response);

      return $obj->success;

  }

    function Index() {
        // if user is logged in then send it to default controller
        $msg = $this->session->userdata('msg');
        if($this->session->userdata('loggedin') && !$msg)
            redirect(base_url() . $this->config->item('default_controller'));

        //----LOAD LANGUAGE
        $this->lang->load('login', $this->config->item('language'));
        //---add language data
        $cpData['lang'] = $this->lang->language;

        $cpData['title'] = 'Registrarse';
        $cpData['authUrl'] = base_url() . 'user/authenticate';
        $cpData['base_url'] = $this->base_url;
        $cpData['module_url'] = $this->module_url;
        $cpData['theme'] = $this->config->item('theme');
        $cpData['plugins'] = (class_exists('Userlayer')) ? implode(',', $this->config->item('user_plugin')) : array();
        //----NO USER

        if ($msg == 'nouser') {
            $cpData['msgcode'] = $this->lang->line('nousr');
        }
        //----USER DOESN'T HAS PROPPER LEVELS

        if ($msg == 'nolevel') {
            $cpData['msgcode'] = $this->lang->line('nolevel') . "<br>" . $this->session->userdata('redir');
        }

        //----USER has to be logged first
        if ($msg == 'hastolog') {
            $cpData['msgcode'] = $this->lang->line('hastolog') . "<br>" . $this->session->userdata('redir');

            if ($this->session->userdata('redir')==base_url())
            $msg='';
        }

        $this->session->set_userdata('msg', $msg);
        //---build UI
        //---define files to viewport
        $cpData['css'] = array(
            $this->module_url . "assets/css/login.css" => 'Register Specific',
        );
        $cpData['js'] = array(
                $this->base_url."jscript/jquery-validation/dist/jquery.validate.js"=>"Validate",
                $this->base_url."jscript/jquery-validation/dist/additional-methods.js"=>"Aditional Methods",
                $this->base_url."jscript/jquery-validation/dist/localization/messages_es.js"=>"JQuery Messages",
                $this->base_url."user/assets/jscript/register.js"=>"RULES",
          //      $this->base_url."user/assets/jscript/form1.js"=>"RULES"

                );
        //---
        $cpData['global_js'] = array(
            'base_url' => $this->base_url,
            'module_url' => $this->module_url
        );
        $cpData['show_warn'] = ($this->config->item('show_warn') and $msg <> '');
        //----clear data
        $this->session->unset_userdata('msg');
        //$this->ui->makeui('user/ext.ui.php', $cpData);
        //$this->parser->parse('user/login', $cpData);

        $this->ui->compose('user/register.php', 'user/bootstrap3.ui.php', $cpData);
    }

      function reload_partidos($idprov=''){
        $partidos = $this->app->get_option_by_idrel(58,$idprov);
        $data['partidos_exist'] = (empty($partidos['data'])) ? false : true ;
        foreach ($partidos['data'] as &$partido){
          ($partido['idrel'] == $idprov)?($data['partidos'][] = $partido):'';
        }
        echo $this->parser->parse('select-partidos', $data, true, true);
      }

      function process() {
        $this->load->library('user/ui');

        // var_export($this->input->post());exit;
        $data['base_url'] = base_url();
        $data['css'] = array($this->base_url . "user/assets/css/login.css" => '',);

        $dbobj=array(
            "group"=>array(
                1000,//----Usuario
                ),
            );
            $template="user/exito_registro";
            $allowed=array(
              'nick',
              'passw',
              'name',
              'lastname',
              'idnumber',
              'email',
            );


        //lo que esta en la base
        // $dbobj = (array) $this->user->get_user((int) $iduser);

        foreach($allowed as $field){
            //---set password if posted

            if(in_array($field,$allowed)){
            //---sanitize fields
                if(is_string($this->input->post($field))){
                    $dbobj[$field]=strip_tags($this->input->post($field),'<p><a><br><hr><b><strong>');
                } else {
                    $dbobj[$field]=$this->input->post($field);
                }
            }

            if($field=='passw'){
                $dbobj['passw']=$this->user->hash($this->input->post('passw'));
            }

            # Issue 2
            if($field=='nick'){
                $dbobj['nick']=trim($this->input->post('idnumber'));
            }

            # Issue 203
            if($field=='idnumber'){
                $dbobj['idnumber']=str_replace(" ", "", $this->input->post('idnumber'));
            }

            if($field=='name'){
              $dbobj['name']=ucwords(strtolower($this->input->post('name')));
            }

            if($field=='provincia'){
              $dbobj['provincia']=($this->input->post('provincia'));
            }

            if($field=='partido'){
              $dbobj['partido']=($this->input->post('partido'));
            }

            if($field=='lastname'){
              $dbobj['lastname']=ucwords(strtolower($this->input->post('lastname')));
            }


        }
//      var_dump($dbobj);exit;
        //---update user
        if($this->session->userdata('iduser')){
            var_dump($dbobj);exit;
        } else { //es nuevo


        $user_token=array(
            'token'=>md5($dbobj['nick'].microtime(true)),
            'nick'=>($dbobj['nick']),
            'email'=>$dbobj['email'],
            'date'=>date('Y-m-d'),
            'dbobj'=>$dbobj,
            );

            if ((!empty($dbobj['nick'])) > 8){
               ECHO "ERROR";
               $user_token['HTTP_CLIENT_IP'] =  $_SERVER['HTTP_CLIENT_IP'];
               $user_token['HTTP_X_FORWARDED_FOR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
               $user_token['data'] =  $_SERVER;
               $user_token['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
               unset($user_token['dbobj']);
               $this->user->save_hackeo($user_token);
            } else{

            $result = $this->user->save_token($user_token);
            $data['user']=$dbobj;
            $data['token']=$user_token['token'];
            if($result){
                //---Envía msg
                $user=new stdClass();
                $user->email=$dbobj['email'];
                $user->name=$dbobj['name'];
                $msg=array(
                    'subject'=>'[Mapa Acción Estatal] Confirmación de Registro',
                    'body'=>$this->parser->parse('user/mail_registro',$data,true,true),
                    'from'=>-1,
                    'debug'=>true,
                    );
                $this->msg->send_mail($msg,$user);
                //---Muestra pantalla éxito
                $data['global_js'] = array(
                        'base_url'=>$this->base_url,
                        'module_url'=>$this->module_url,
                );
                $data['title']='Confirmación de Registro';
                $data['email']=$dbobj['email'];
                $data['body'] = "<p>Recibirás un email de confirmación en " . $data['email'] . ".</p><p>Sigue las instrucciones para activar tu cuenta</p>";

                $this->ui->compose('user/exito_registro', 'user/bootstrap3.ui.php', $data);
            } else {
                $data['title']='Error en Registro';
                $data['body'] = "<p>No se pudo completar el registro</p>";
                $this->ui->compose('user/messages', 'user/newui.php', $data);
            }

          }

        }
    }


    function Activar($token=null){
      $this->load->library('user/ui');
      $data['title'] = "Error";
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $data['css'] = array($this->base_url . "user/assets/css/login.css" => '',);
        if($token){
            //---get token
            $result=$this->user->get_token($token);
            if($result){
                //@todo checkear que no haya pasado mucho tiempo para la activación
                $user=$result->dbobj;
                $rs=$this->user->getbynick($user['nick']);
                if($rs){
                    $data['body'] = '<p>El mail que elegiste ya no se encuentra disponible.</p><p class="mt2"><a href="{base_url}user/register">Registrate nuevamente</a></p>';

                    $this->ui->compose('user/messages', 'user/newui.php', $data);
                } else {
                    $user['idu']=$this->user->genid();
                    $this->user->update($user);
                    $data+=$user;
                    //---remove token
                    $this->user->delete_token($token);
                    $data['title'] = "Activación confirmada";
                    $data['body'] = '<p>Tu perfil se ha activado con éxito</p><p class="mt2"><a href="'.$data['base_url'].'user/login" class="ui green button">Ingresar</a></p>';
                    $this->ui->compose('user/messages', 'user/newui.php', $data);
                }

            }else{
              $data['body'] = '<p>Token inválido</p>';

              $this->ui->compose('user/messages', 'user/newui.php', $data);
            }
        } else {
            $data['body'] = '<p>Acceso inválido</p>';

            $this->ui->compose('user/messages', 'user/newui.php', $data);
        }
    }

}
