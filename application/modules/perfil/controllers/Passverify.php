<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Passverify extends MX_Controller {

    public function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->model('app');
      $this->load->model('bpm/bpm');
      $this->load->model('Model_perfil');
      $this->load->model('user/user');
      $this->load->library('parser');
      $this->load->config('config');
      $this->idu = $this->user->idu;

    }

    function prueba($pass){
      $usuarios['password'] = strval($pass);

      $this->idu = (double) $this->session->userdata('iduser');
      $this->password_verify(json_encode($usuarios));

    }

    function password($usuario){
      if(!$usuario){
        $usuario = $_POST['password'];
      }
      //$usuario = $_POST['password'];
      $user = json_decode($usuario,true);
      $new_pass = password_hash($user['password'], PASSWORD_DEFAULT);
      //var_dump($new_pass);
      echo json_encode($new_pass);
      //$hash = '$2y$07$BCryptRequires22Chrcte/VlQH0piJtjXl.0t1XkA8pw9dMXTpOq';

      // if (password_verify($user['password'], $new_pass)) {
      //     echo '¡La contraseña es válida!';
      // } else {
      //     echo 'La contraseña no es válida.';
      // }

    }

    function password_verify($usuario){
      // if(!$pass_text || !$hash){
      //   $pass_text = $_POST['pass_text'];
      //   $hash = $_POST['hash'];
      // }
      $user = json_decode($usuario,true);
      var_dump($this->idu);
      return;
      $hash = '$2y$10$kdK5cwHQVknyYF0rWUoYf.q2YFvWM6lXnCrI6Mgkb8f6LFDzgy7.q';
      var_dump($user);
      if (password_verify($user['password'], $hash)) {
          echo '¡La contraseña es válida!';
      } else {
          echo 'La contraseña no es válida.';
      }

    }




}
