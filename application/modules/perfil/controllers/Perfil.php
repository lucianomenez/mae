<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Perfil extends MX_Controller {

    public function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->helper('file');
      $this->load->helper('url');
      $this->load->model('app');
      $this->load->model('bpm/bpm');
      $this->load->model('Model_perfil');
      $this->load->model('user/user');
      $this->load->library('parser');
      $this->load->config('config');
      $this->idu = $this->user->idu;

    }

    function Index() {
        // if user is logged in then send it to default controller
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        Modules::run('dashboard/dashboard', 'perfil/json/index.json',$debug, $extraData);
    }

    function contenido(){
      $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
      echo $this->parser->parse('contenido',$data, true, true);
    }

    // ============ HEADER DE CONTENT - TITULO Y breadcrumb
    function content_header() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('content_header', $data, true, true);
    }

    // ============ SCRIPTS DEL FOOTER
    function scripts() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('scripts', $data, true, true);
    }

    // ============ SIDEBAR GRIS
    function footer() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('footer', $data, true, true);
    }

    // ============ SIDEBAR GRIS
    function sidebar() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        var_dump($this->user);
        exit;
        echo $this->parser->parse('sidebar', $data, true, true);
    }

    // ============ NAVBAR HORIZONTAL DE ARRIBA
    function navbar() {
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('navbar', $data, true, true);
    }

    // ============ CONFIGURACION DE HEAD. CSS Y LIBRERIAS
    function head() {
        $data['title'] = 'Mapa de la Acción Estatal | Panel';
        $data['base_url'] = $this->base_url;
        $data['admin_lte'] = $this->base_url . 'adminlte/assets/';
        echo $this->parser->parse('head', $data, true, true);
    }







}
