$(document).ready(function(){ 
    
    
    $.ajax({
             type: "POST",
             async: false,
             url: globals.base_url + 'pacc/assets/json/map.json',
             success: function(data) {  
                     
			 var map = AmCharts.makeChart("mapdiv", {
				type: "map",
				pathToImages: globals.base_url + "map/assets/jscript/ammap/images/",
				balloon: {
					color: "#000000"
				},
				dataProvider: data,
				developerMode: true
			});
                       
                      
                      }});
    
partidos_init();

});

function partidos_init() {

$('#P1').tooltip({title:'Adolfo Alsina <br /> Seccion I', container:'#svgload'});
$('#P2').tooltip({title:'Adolfo Gonzales Chaves', container:'#svgload'});
$('#P3').tooltip({title:'Alberti', container:'#svgload'});
$('#P4').tooltip({title:'Almirante Brown', container:'#svgload'});
$('#P5').tooltip({title:'Arrecifes', container:'#svgload'});
$('#P6').tooltip({title:'Avellaneda', container:'#svgload'});
$('#P7').tooltip({title:'Ayacucho', container:'#svgload'});
$('#P8').tooltip({title:'Azul', container:'#svgload'});
$('#P9').tooltip({title:'Bahía Blanca', container:'#svgload'});
$('#P10').tooltip({title:'Balcarce', container:'#svgload'});
$('#P11').tooltip({title:'Baradero', container:'#svgload'});
$('#P13').tooltip({title:'Benito Juarez', container:'#svgload'});
$('#P14').tooltip({title:'Berazategui', container:'#svgload'});
$('#P14').tooltip({title:'La Plata', container:'#svgload'});
$('#P15').tooltip({title:'Berisso', container:'#svgload'});
$('#P16').tooltip({title:'Bolívar', container:'#svgload'});
$('#P17').tooltip({title:'Bragado', container:'#svgload'});
$('#P18').tooltip({title:'Campana', container:'#svgload'});
$('#P19').tooltip({title:'Cañuelas', container:'#svgload'});
$('#P20').tooltip({title:'Capitán Sarmiento', container:'#svgload'});
$('#P21').tooltip({title:'Carlos Casares', container:'#svgload'});
$('#P22').tooltip({title:'Carlos Tejedor', container:'#svgload'});
$('#P23').tooltip({title:'Carmen de Areco', container:'#svgload'});
$('#P24').tooltip({title:'Patagones', container:'#svgload'});
$('#P25').tooltip({title:'Castelli', container:'#svgload'});
$('#P26').tooltip({title:'Colón', container:'#svgload'});
$('#P27').tooltip({title:'Brandsen', container:'#svgload'});
$('#P28').tooltip({title:'Coronel Dorrego', container:'#svgload'});
$('#P29').tooltip({title:'Coronel L. Rosales', container:'#svgload'});
$('#P30').tooltip({title:'Coronel Pringles', container:'#svgload'});
$('#P31').tooltip({title:'Coronel Suarez', container:'#svgload'});
$('#P32').tooltip({title:'Chacabuco', container:'#svgload'});
$('#P33').tooltip({title:'Chascomus', container:'#svgload'});
$('#P34').tooltip({title:'Chivilcoy', container:'#svgload'});
$('#P35').tooltip({title:'Daireaux', container:'#svgload'});
$('#P36').tooltip({title:'De La Costa', container:'#svgload'});
$('#P37').tooltip({title:'Dolores', container:'#svgload'});
$('#P38').tooltip({title:'Ensenada', container:'#svgload'});
$('#P39').tooltip({title:'Escobar', container:'#svgload'});
$('#P40').tooltip({title:'Esteban Echeverría', container:'#svgload'});
$('#P41').tooltip({title:'Exaltación de la Cruz', container:'#svgload'});
$('#P42').tooltip({title:'Ezeiza', container:'#svgload'});
$('#P43').tooltip({title:'Florencio Varela', container:'#svgload'});
$('#P44').tooltip({title:'Florentino Ameghino', container:'#svgload'});
$('#P45').tooltip({title:'General Alvarado', container:'#svgload'});
$('#P46').tooltip({title:'General Alvear', container:'#svgload'});
$('#P47').tooltip({title:'General Arenales ', container:'#svgload'});
$('#P48').tooltip({title:'General Belgrano', container:'#svgload'});
$('#P49').tooltip({title:'General Guido', container:'#svgload'});
$('#P50').tooltip({title:'General Lamadrid', container:'#svgload'});
$('#P51').tooltip({title:'General Las Heras', container:'#svgload'});
$('#P52').tooltip({title:'General Lavalle', container:'#svgload'});
$('#P53').tooltip({title:'General Madariaga', container:'#svgload'});
$('#P54').tooltip({title:'General Paz', container:'#svgload'});
$('#P55').tooltip({title:'General Pinto', container:'#svgload'});
$('#P56').tooltip({title:'General Pueyrredón', container:'#svgload'});
$('#P57').tooltip({title:'General Rodríguez', container:'#svgload'});
$('#P58').tooltip({title:'General San Martín', container:'#svgload'});
$('#P60').tooltip({title:'General Viamonte', container:'#svgload'});
$('#P61').tooltip({title:'General Villegas', container:'#svgload'});
$('#P62').tooltip({title:'Guaminí', container:'#svgload'});
$('#P63').tooltip({title:'Hipólito Yrigoyen', container:'#svgload'});
$('#P64').tooltip({title:'Hurlingham', container:'#svgload'});
$('#P65').tooltip({title:'Ituzaingó', container:'#svgload'});
$('#P66').tooltip({title:'Jose C. Paz', container:'#svgload'});
$('#P67').tooltip({title:'Junín', container:'#svgload'});
$('#P68').tooltip({title:'La Matanza', container:'#svgload'});
$('#P69').tooltip({title:'La Plata', container:'#svgload'});
$('#P70').tooltip({title:'Laprida', container:'#svgload'});
$('#P71').tooltip({title:'Lanús', container:'#svgload'});
$('#P72').tooltip({title:'Las Flores', container:'#svgload'});
$('#P73').tooltip({title:'Leandro N. Alem', container:'#svgload'});
$('#P140').tooltip({title:'Lezama', container:'#svgload'});
$('#P74').tooltip({title:'Lincoln', container:'#svgload'});
$('#P75').tooltip({title:'Lobería', container:'#svgload'});
$('#P76').tooltip({title:'Lobos', container:'#svgload'});
$('#P77').tooltip({title:'Lomas de Zamora', container:'#svgload'});
$('#P78').tooltip({title:'Luján', container:'#svgload'});
$('#P79').tooltip({title:'Magdalena', container:'#svgload'});
$('#P80').tooltip({title:'Maipú', container:'#svgload'});
$('#P81').tooltip({title:'Malvinas Argentinas', container:'#svgload'});
$('#P82').tooltip({title:'Marcos Paz', container:'#svgload'});
$('#P83').tooltip({title:'Mar Chiquita', container:'#svgload'});
$('#P84').tooltip({title:'Mercedes', container:'#svgload'});
$('#P85').tooltip({title:'Merlo', container:'#svgload'});
$('#P86').tooltip({title:'Monte', container:'#svgload'});
$('#P87').tooltip({title:'Monte Hermoso', container:'#svgload'});
$('#P88').tooltip({title:'Moreno', container:'#svgload'});
$('#P89').tooltip({title:'Morón', container:'#svgload'});
$('#P90').tooltip({title:'Navarro', container:'#svgload'});
$('#P91').tooltip({title:'Necochea', container:'#svgload'});
$('#P92').tooltip({title:'Nueve de Julio', container:'#svgload'});
$('#P93').tooltip({title:'Olavarría', container:'#svgload'});
$('#P94').tooltip({title:'Pehuajó', container:'#svgload'});
$('#P95').tooltip({title:'Pellegrini', container:'#svgload'});
$('#P96').tooltip({title:'Pergamino', container:'#svgload'});
$('#P97').tooltip({title:'Pila', container:'#svgload'});
$('#P98').tooltip({title:'Pilar', container:'#svgload'});
$('#P99').tooltip({title:'Pinamar', container:'#svgload'});
$('#P100').tooltip({title:'Presidente Perón', container:'#svgload'});
$('#P101').tooltip({title:'Puán', container:'#svgload'});
$('#P102').tooltip({title:'Punta Indio', container:'#svgload'});
$('#P103').tooltip({title:'Quilmes', container:'#svgload'});
$('#P104').tooltip({title:'Ramallo', container:'#svgload'});
$('#P105').tooltip({title:'Rauch', container:'#svgload'});
$('#P106').tooltip({title:'Rivadavia', container:'#svgload'});
$('#P107').tooltip({title:'Rojas', container:'#svgload'});
$('#P108').tooltip({title:'Roque Pérez', container:'#svgload'});
$('#P109').tooltip({title:'Saavedra', container:'#svgload'});
$('#P110').tooltip({title:'Saladillo', container:'#svgload'});
$('#P111').tooltip({title:'Salto', container:'#svgload'});
$('#P112').tooltip({title:'Salliqueló', container:'#svgload'});
$('#P113').tooltip({title:'San Andres de Giles', container:'#svgload'});
$('#P114').tooltip({title:'San Antonio de Areco', container:'#svgload'});
$('#P115').tooltip({title:'San Cayetano', container:'#svgload'});
$('#P116').tooltip({title:'San Fernando', container:'#svgload'});
$('#P117').tooltip({title:'San Isidro', container:'#svgload'});
$('#P118').tooltip({title:'San Miguel', container:'#svgload'});
$('#P119').tooltip({title:'San Nicolás', container:'#svgload'});
$('#P120').tooltip({title:'San Pedro', container:'#svgload'});
$('#P121').tooltip({title:'San Vicente', container:'#svgload'});
$('#P122').tooltip({title:'Suipacha', container:'#svgload'});
$('#P123').tooltip({title:'Tandil', container:'#svgload'});
$('#P124').tooltip({title:'Tapalqué', container:'#svgload'});
$('#P125').tooltip({title:'Tigre', container:'#svgload'});
$('#P126').tooltip({title:'Tordillo', container:'#svgload'});
$('#P127').tooltip({title:'Tornquist', container:'#svgload'});
$('#P128').tooltip({title:'Trenque Lauquen', container:'#svgload'});
$('#P129').tooltip({title:'Tres Arroyos', container:'#svgload'});
$('#P130').tooltip({title:'Tres de Febrero', container:'#svgload'});
$('#P131').tooltip({title:'Tres Lomas', container:'#svgload'});
$('#P132').tooltip({title:'Veinticinco de Mayo', container:'#svgload'});
$('#P133').tooltip({title:'Vicente Lopez', container:'#svgload'});
$('#P134').tooltip({title:'Villa Gesell', container:'#svgload'});
$('#P135').tooltip({title:'Villarino', container:'#svgload'});
$('#P136').tooltip({title:'Zárate', container:'#svgload'});
$('#P137').tooltip({title:'Capital Federal', container:'#svgload'});

  $('[data-toggle="tooltip"]').tooltip();
  

$('#S1').click( function (){
    
        	$.ajax({
            type: "POST",
            context: $('#table'),
            url: globals.base_url + 'pacc/incubar/reload_seccion/Primera Sección' ,
            success: function(data) {                
                     $(this).replaceWith(data);            
                     }});
    
    
})
$('#S2').click( function (){
    
        	$.ajax({
            type: "POST",
            context: $('#table'),
            url: globals.base_url + 'pacc/incubar/reload_seccion/Segunda Sección' ,
            success: function(data) {                
                     $(this).replaceWith(data);            
                     }});
    
    
})
$('#S3').click( function (){
    
        	$.ajax({
            type: "POST",
            context: $('#table'),
            url: globals.base_url + 'pacc/incubar/reload_seccion/Tercera Sección' ,
            success: function(data) {                
                     $(this).replaceWith(data);            
                     }});
    
    
})
$('#S4').click( function (){
    
        	$.ajax({
            type: "POST",
            context: $('#table'),
            url: globals.base_url + 'pacc/incubar/reload_seccion/Cuarta Sección' ,
            success: function(data) {                
                     $(this).replaceWith(data);            
                     }});
    
    
})
$('#S5').click( function (){
    
        	$.ajax({
            type: "POST",
            context: $('#table'),
            url: globals.base_url + 'pacc/incubar/reload_seccion/Quinta Sección' ,
            success: function(data) {                
                     $(this).replaceWith(data);            
                     }});
    
    
})
$('#S6').click( function (){
    
        	$.ajax({
            type: "POST",
            context: $('#table'),
            url: globals.base_url + 'pacc/incubar/reload_seccion/Sexta Sección' ,
            success: function(data) {                
                     $(this).replaceWith(data);            
                     }});
    
    
})
$('#S7').click( function (){
    
        	$.ajax({
            type: "POST",
            context: $('#table'),
            url: globals.base_url + 'pacc/incubar/reload_seccion/Séptima Sección' ,
            success: function(data) {                
                     $(this).replaceWith(data);            
                     }});
    
    
})
$('#S8').click( function (){
    
        	$.ajax({
            type: "POST",
            context: $('#table'),
            url: globals.base_url + 'pacc/incubar/reload_seccion/Octava Sección' ,
            success: function(data) {                
                     $(this).replaceWith(data);            
                     }});
    
    
})


}























