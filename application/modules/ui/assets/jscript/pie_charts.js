var loader_options = {};
$(document).ready(function() {


	$('.pie_chart').each(
		function(index, item) {
			//Find the url
			var url = $(item).find(".json_url").text();
			// $(this).bload(loader_options);
			$.ajax({
				url: url,
				context: $(item)
			}).done(function(data) {

					$.plot('#' + $(item).attr('id'), data, {
						series: {
							pie: {
								innerRadius: 0.5,
								show: true,
								label: {
									show: true,
									radius: 8 / 9,
									formatter: labelFormatter,
									threshold:0.01,
									background: {
									
										color: 'null'
									}
								}
							}

						}

					});

				}
				//---data contains all the parameters needed
			);

		});

	function labelFormatter(label, series) {
		return "<div style='font-size:8pt; text-align:center; padding:2px; color:black;'><strong>" + label + "</strong><br/>" + Math.round(series.percent) + "%</div>";
	}
	//----end document ready
});