<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_estructuras extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */


    function get_nombre($id){
      $query = array('id'=> $id);
      $this->db->where($query);
      $result = $this->db->get('container.mapa_del_estado_jurisdicciones')->result_array();
      return $result[0]['name'];
    }

    function get_tipos_cargo(){
      $result = $this->db->get('container.mapa_del_estado_nomenclatura_cargos')->result_array();
      return $result;
    }

    function get_tipos_administracion(){
      $result = $this->db->get('container.mapa_del_estado_tipo_administracion')->result_array();
      return $result;
    }

    function get_unidades_de_rango(){
      $result = $this->db->get('container.mapa_del_estado_unidad_rango')->result_array();
      return $result;
    }

}
