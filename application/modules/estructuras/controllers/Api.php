<?php

class api extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
      $this->user->authorize();
      $this->load->model('Model_api');
      $this->idu = $this->user->idu;
      $this->usuario = $this->user->get_user($this->idu);

  }

    function get_jurisdicciones($id) {
        if($id=="0"){
          $data = $this->Model_api->get_jurisdicciones($id);
          foreach ($data as &$ministerio){
            $ministerio['count_masculinos'] = $this->count_genero("M", $ministerio['name']);
            $ministerio['count_femeninos'] = $this->count_genero("F", $ministerio['name']);
            $ministerio['count_cargos_no_designados_2'] = $this->count_cargos_no_designados($ministerio['name']);
            $ministerio['cargos_totales'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']));
            $ministerio['count_cargos_designados_2'] = $ministerio['cargos_totales'] - $ministerio['count_cargos_no_designados_2'];

            $ministerio['count_secretarias'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Secretaría' , 'both' );
            $ministerio['count_subsecretarias'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Subsecretaría' , 'both' );
            $ministerio['count_coordinaciones'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Coordinación' , 'both' );
            $ministerio['count_uai'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']), 'unidad_rango', 'UAI' , 'both' );
            $ministerio['count_presidencias'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Presidencia' , 'both' );
            $ministerio['count_departamentos'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Departamento' , 'both' );
            $ministerio['count_organismos'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']), 'unidad_rango', 'Organismo' , 'both' );
            $ministerio['count_empresas'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Empresa' , 'both' );
            $ministerio['count_otros'] = $ministerio['cargos_totales']-$ministerio['count_secretarias']-$ministerio['count_subsecretarias']-$ministerio['count_coordinaciones']-$ministerio['count_uai']-$ministerio['count_presidencias']-$ministerio['count_departamentos']-$ministerio['count_organismos']-$ministerio['count_empresas'];
          }
          return $data;
        }else{
          $data = $this->Model_api->get_jurisdicciones($id);
          foreach ($data as &$ministerio){
            $ministerio['count_masculinos'] = $this->count_genero("M", null, $ministerio['name']);
            $ministerio['count_femeninos'] = $this->count_genero("F", null, $ministerio['name']);
            $ministerio['cargos_totales'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('unidad_de_nivel_politico' => $ministerio['name']));
            $ministerio['count_secretarias'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('unidad_de_nivel_politico' => $ministerio['name']),'unidad_rango', 'Secretaría' , 'both' );
            $ministerio['count_subsecretarias'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('unidad_de_nivel_politico' => $ministerio['name']),'unidad_rango', 'Subsecretaría' , 'both' );
            $ministerio['count_coordinaciones'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('unidad_de_nivel_politico' => $ministerio['name']),'unidad_rango', 'Coordinación' , 'both' );
            $ministerio['count_uai'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('unidad_de_nivel_politico' => $ministerio['name']), 'unidad_rango', 'UAI' , 'both' );
            $ministerio['count_presidencias'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('unidad_de_nivel_politico' => $ministerio['name']),'unidad_rango', 'Presidencia' , 'both' );
            $ministerio['count_departamentos'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('unidad_de_nivel_politico' => $ministerio['name']),'unidad_rango', 'Departamento' , 'both' );
            $ministerio['count_organismos'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('unidad_de_nivel_politico' => $ministerio['name']), 'unidad_rango', 'Organismo' , 'both' );
            $ministerio['count_empresas'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('unidad_de_nivel_politico' => $ministerio['name']),'unidad_rango', 'Empresa' , 'both' );
            $ministerio['count_otros'] = $ministerio['cargos_totales']-$ministerio['count_secretarias']-$ministerio['count_subsecretarias']-$ministerio['count_coordinaciones']-$ministerio['count_uai']-$ministerio['count_presidencias']-$ministerio['count_departamentos']-$ministerio['count_organismos']-$ministerio['count_empresas'];
          }
          return $data;
      }
      //$customData['usercan_create']=true;
    }

    function get_subjurisdicciones() {
      $data = $this->Model_api->get_subjurisdicciones();
      foreach ($data as &$ministerio){
        $ministerio['cargos_totales'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']));
        $ministerio['count_secretarias'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Secretaría' , 'both' );
        $ministerio['count_subsecretarias'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Subsecretaría' , 'both' );
        $ministerio['count_coordinaciones'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Coordinación' , 'both' );
        $ministerio['count_uai'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']), 'unidad_rango', 'UAI' , 'both' );
        $ministerio['count_presidencias'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Presidencia' , 'both' );
        $ministerio['count_departamentos'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Departamento' , 'both' );
        $ministerio['count_organismos'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']), 'unidad_rango', 'Organismo' , 'both' );
        $ministerio['count_empresas'] = $this->Model_api->count_collection_by_query('container.mapa_del_estado_cargos', array ('jurisdiccion' => $ministerio['name']),'unidad_rango', 'Empresa' , 'both' );
        $ministerio['count_otros'] = $ministerio['cargos_totales']-$ministerio['count_secretarias']-$ministerio['count_subsecretarias']-$ministerio['count_coordinaciones']-$ministerio['count_uai']-$ministerio['count_presidencias']-$ministerio['count_departamentos']-$ministerio['count_organismos']-$ministerio['count_empresas'];
      }
      return $data;
      //$customData['usercan_create']=true;
    }


    function count_ministerios() {
      $collection = 'container.mapa_del_estado_jurisdicciones';
      return $this->Model_api->count_collection_by_query($collection, array ('idrel' => "0"));
    }

    function count_secretarias_presidencia(){
      $collection = 'container.mapa_del_estado_subjurisdicciones';
      return $this->Model_api->count_collection($collection);
    }


    function count_cargos(){
      $collection = 'container.mapa_del_estado_cargos';
      return $this->Model_api->count_collection($collection);
    }

    function count_cargos_by_query($jurisdiccion = null, $unidad_de_nivel_politico = null ){
      $collection = 'container.mapa_del_estado_cargos';
      if ($jurisdiccion){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre !=' => null , 'jurisdiccion' => $jurisdiccion));
      }else{
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre !=' => null, 'jurisdiccion' => $jurisdiccion, 'unidad_de_nivel_politico' => $unidad_de_nivel_politico));
      }


      return $this->Model_api->count_collection_by_query($collection, $query);
    }

    function count_cargos_no_designados($jurisdiccion = null, $unidad_de_nivel_politico = null){
      $collection = 'container.mapa_del_estado_cargos';
      if ($jurisdiccion){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre' =>  null, 'jurisdiccion' => $jurisdiccion));
      }else if ($unidad_de_nivel_politico){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre' => null, 'jurisdiccion' => $jurisdiccion, 'unidad_de_nivel_politico' => $unidad_de_nivel_politico));
      }else{
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_nombre' => null));
      }

    }

    function count_genero($param, $jurisdiccion = null, $unidad_de_nivel_politico = null){
      $collection = 'container.mapa_del_estado_cargos';
      if ($jurisdiccion){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_sexo' => $param, 'jurisdiccion' => $jurisdiccion));
      }else if ($unidad_de_nivel_politico){
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_sexo' => $param, 'jurisdiccion' => $jurisdiccion, 'unidad_de_nivel_politico' => $unidad_de_nivel_politico));
      }else{
        return $this->Model_api->count_collection_by_query($collection, array('autoridad_sexo' => $param));
      }
    }

    function count_ministerios_by_idrel($id) {
      $collection = 'container.mapa_del_estado_jurisdicciones';
      return $this->Model_api->count_collection_by_query($collection, array('idrel' => $id));
    }

    function count_field($field, $value){
      $collection = 'container.mapa_del_estado_cargos';
      return $this->Model_api->count_collection_by_query($collection, array($field => $value));
    }

} //

?>
