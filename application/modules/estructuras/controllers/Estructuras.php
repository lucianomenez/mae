<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Paneles de Navegación
 *
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 */
class Estructuras extends MX_Controller {

    function __construct() {
        parent::__construct();
          $this->base_url = base_url();
          $this->module_url = base_url() . $this->router->fetch_module() . '/';
          $this->user->authorize();
          $this->load->helper('file');
          $this->load->helper('url');
          $this->load->model('app');
          $this->load->model('bpm/bpm');
          $this->load->model('user/user');
          $this->load->library('parser');
          $this->load->config('config');
          $this->idu = $this->user->idu;
        }


      function index(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          Modules::run('dashboard/dashboard', 'estructuras/json/index.json',$debug, $extraData);
      }

      function content_header() {
          $data['base_url'] = $this->base_url;
          $data['title'] = "Mapa del Estado Nacional";
          $data['nombre_breadcrumb'] = "Estructura General";
          echo $this->parser->parse('perfil/content_header', $data, true, true);
      }

      // Parse json

      function contenido(){
          $this->load->module("estructuras/api");
          $data['module_url'] = $this->module_url;

          $data['secretarias_presidencia'] = $this->api->get_subjurisdicciones();
          $data['ministerios'] = $this->api->get_jurisdicciones("0");

          //SETEO INDICADORES
          $data['count_mujeres'] = $this->api->count_genero("F");
          $data['count_hombres'] = $this->api->count_genero("M");
          $data['count_otorgados'] = $data['count_mujeres'] + $data['count_hombres']; 


          $data['count_cargos'] = $this->api->count_cargos();
          $data['count_cargos_no_designados'] = $this->api->count_cargos_no_designados();
          $data['count_ministerios'] = $this->api->count_ministerios();
          $data['count_secretarias_presidencia'] = $this->api->count_secretarias_presidencia();
          //Seteo graficos de prueba
          echo $this->parser->parse('contenido',$data, true, true);
      }



}
