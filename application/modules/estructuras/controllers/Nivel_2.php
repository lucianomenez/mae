<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Paneles de Navegación
 *
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 */
class Nivel_2 extends MX_Controller {

    function __construct() {
        parent::__construct();
          $this->base_url = base_url();
          $this->module_url = base_url() . $this->router->fetch_module() . '/';
          $this->user->authorize();
          $this->load->helper('file');
          $this->load->helper('url');
          $this->load->model('app');
          $this->load->model('bpm/bpm');
          $this->load->model('Model_estructuras');

          $this->load->model('user/user');
          $this->load->library('parser');
          $this->load->config('config');
          $this->idu = $this->user->idu;
          $this->id_actual = "";
          $this->id_nombre = "";

        }


      function index($id){
          $this->id_actual = $id;
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;

          Modules::run('dashboard/dashboard', 'estructuras/json/nivel2.json',$debug, $extraData);
      }

      function content_header() {
          $data['base_url'] = $this->base_url;
          $data['title'] = "Mapa del Estado Nacional";
          $this->id_nombre = $this->Model_estructuras->get_nombre($this->id_actual);
          $data['nombre_breadcrumb'] = "Estructura General / ".$this->id_nombre;
          echo $this->parser->parse('perfil/content_header', $data, true, true);
      }

      // Parse json

      function contenido(){
          $this->load->module("estructuras/api");
          $data['module_url'] = $this->module_url;

    //      $data['secretarias_presidencia'] = $this->api->get_subjurisdicciones();
          $data['ministerios'] = $this->api->get_jurisdicciones($this->id_actual);

          //SETEO INDICADORES
          $data['count_cargos'] = $this->api->count_cargos($this->id_nombre);
          $data['count_cargos_no_designados'] = $this->api->count_cargos_no_designados($this->id_nombre);
          $data['count_ministerios'] = $this->api->count_ministerios_by_idrel($this->id_actual);
          $data['count_secretarias_presidencia'] = $this->api->count_secretarias_presidencia();
          //Seteo graficos de prueba
          echo $this->parser->parse('contenido_nivel2',$data, true, true);
      }



}
