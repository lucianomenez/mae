<?php

class Tipos_cargo extends MX_Controller {

  function __construct() {
      parent::__construct();
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('Model_estructuras');

        $this->load->model('user/user');
        $this->load->library('parser');
        $this->load->config('config');
        $this->idu = $this->user->idu;
      }


      function index(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          Modules::run('dashboard/dashboard', 'estructuras/json/tipos_cargo.json',$debug, $extraData);
      }

      function contenido(){
        $this->load->module("estructuras/api");
        $data['title'] = "Tabla de Tipos de Cargo";
        $data['data'] = $this->Model_estructuras->get_tipos_cargo();
        foreach ($data['data'] as &$cargo){
          $cargo['cantidad'] = $this->api->count_field('cargo',$cargo['name']);
        }
        echo $this->parser->parse('tipos_cargo',$data, true, true);

      }



} //

?>
