<?php

class Tipos_administracion extends MX_Controller {

  function __construct() {
      parent::__construct();
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('Model_estructuras');

        $this->load->model('user/user');
        $this->load->library('parser');
        $this->load->config('config');
        $this->idu = $this->user->idu;
      }


      function index(){
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          Modules::run('dashboard/dashboard', 'estructuras/json/tipos_administracion.json',$debug, $extraData);
      }

      function contenido(){
        $this->load->module("estructuras/api");        
        $data['title'] = "Tabla de Tipos de Administración";
        $data['data'] = $this->Model_estructuras->get_tipos_administracion();
        foreach ($data['data'] as &$unidad){
          $unidad['cantidad'] = $this->api->count_field('tipo_administracion',$unidad['name']);
        }
        echo $this->parser->parse('tipos_cargo',$data, true, true);

      }



} //

?>
