<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-3 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-info"><i class="far fa-user"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Cargos en Organigrama</span>
          <span class="info-box-number">{count_cargos}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-danger"><i class="fas fa-exclamation-triangle"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Cargos sin Designación</span>
          <span class="info-box-number">{count_cargos_no_designados}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-12">
      <div class="info-box">
        <span class="info-box-icon bg-success"><i class="fas fa-landmark"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Areas</span>
          <span class="info-box-number">{count_ministerios}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <!-- /.col -->
  </div>

  <h3 class="m-0 text-dark">Ministerios</h3>  <!-- /.row -->
  <!-- Main row -->
        <div class="mt-4">
        {ministerios}
        <div class="row">
           <div class="col-lg-12">
              <div class="card collapsed-card">
                <div class="card-header bg-gradient-primary">
                  <h3 class="card-title">{name}</h3>

                  <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                      <a href={module_url}nivel_2/index/{id}><button type="button" class="btn btn-tool"><i class="fas fa-eye white" style="background-color : #ffffff"></i></a>
                    </button>
                  </div>
                  <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body col-md-12" style="display: none;">

                              <div class="card card-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-info">
                                  <h3 class="widget-user-username">{nombre} {lastname}</h3>
                                  <h5 class="widget-user-desc">{cargo}</h5>
                                </div>
                                <div class="widget-user-image">
                                  <img class="img-circle elevation-2" src="{module_url}assets/img/{id}.png" alt="User Avatar">
                                </div>
                                <div class="card-footer">
                                  <div class="row">
                                    <div class="col-sm-4 border-right">
                                      <div class="description-block">
                                        <h5 class="description-header">{count_secretarias}</h5>
                                        <span class="description-text">Secretarías</span>
                                      </div>
                                      <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 border-right">
                                      <div class="description-block">
                                        <h5 class="description-header">{count_subsecretarias}</h5>
                                        <span class="description-text">Subsecretarías</span>
                                      </div>
                                      <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                      <div class="description-block">
                                        <h5 class="description-header">{count_coordinaciones}</h5>
                                        <span class="description-text">Coordinaciones</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-4 border-right">
                                      <div class="description-block">
                                        <h5 class="description-header">{count_presidencias}</h5>
                                        <span class="description-text">Presidencias</span>
                                      </div>
                                      <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 border-right">
                                      <div class="description-block">
                                        <h5 class="description-header">{count_uai}</h5>
                                        <span class="description-text">UAI</span>
                                      </div>
                                      <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                      <div class="description-block">
                                        <h5 class="description-header">{count_departamentos}</h5>
                                        <span class="description-text">Departamentos</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-4 border-right">
                                      <div class="description-block">
                                        <h5 class="description-header">{count_organismos}</h5>
                                        <span class="description-text">Org. Descentralizados</span>
                                      </div>
                                      <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 border-right">
                                      <div class="description-block">
                                        <h5 class="description-header">{count_empresas}</h5>
                                        <span class="description-text">Empresas</span>
                                      </div>
                                      <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4">
                                      <div class="description-block">
                                        <h5 class="description-header">{count_otros}</h5>
                                        <span class="description-text">Otros</span>
                                      </div>
                                    </div>
                                  </div>
                                  <br/>
                                  <br/>
                                  <br/>
                                  <ul class="nav flex-column mt-6">
                                    <li class="nav-item">
                                      <a href="#" class="nav-link">
                                        Objetivos Definidos: <span class="float-right badge bg-primary">0{objetivos}</span>
                                      </a>
                                    </li>
                                    <li class="nav-item">
                                      <a href="#" class="nav-link">
                                        Productos declarados: <span class="float-right badge bg-info">0{productos}</span>
                                      </a>
                                    </li>
                                    <li class="nav-item">
                                      <a href="#" class="nav-link">
                                        Tareas completadas: <span class="float-right badge bg-success">0{tareas_completadas}</span>
                                      </a>
                                    </li>
                                    <li class="nav-item">
                                      <a href="#" class="nav-link">
                                        Cargos/Funcionarios: <span class="float-right badge bg-info">{cargos_totales}</span>
                                      </a>
                                    </li>
                                    <li class="nav-item">
                                      <a href="#" class="nav-link">
                                        Empleados totales: <span class="float-right badge bg-danger">{empleados_totales}</span>
                                      </a>
                                    </li>
                                  </ul>
                                                                    <!-- /.row -->
                                </div>
                              </div>

                              <div class="card card-danger col-md-6">
                                <div class="card-header bg-info">
                                  <h3 class="card-title">Distribución de Género (Cargos)</h3>

                                  <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                  </div>
                                </div>
                                <div class="card-body" style="display: block;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                  <canvas class="donutChart" data-femenino={count_femeninos} data-masculino={count_masculinos} style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 172px;" width="172" height="250" class="chartjs-render-monitor"></canvas>
                                </div>
                                <!-- /.card-body -->
                              </div>



                            </div>


                  <!-- /.col -->
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          {/ministerios}
        </div>
